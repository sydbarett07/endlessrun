using System;
using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.UI;

public class PlayfabLogin : MonoBehaviour
{
    // Variables to store user information as displayed in start screen
    private string empID;
    private string userName;
    private string emailId;

    public GameObject buttonText;

    // To store the UI information
    public GameObject registrationFormUI;
    public GameObject empIdUI;
    public GameObject empIdLabelUI;
    public GameObject accountNameUI;
    public GameObject accountNameLabelUI;
    public GameObject emaildIdUI;
    public GameObject emaiIdLabelUI;

    public void Start()
    {
        if (PlayerPrefs.HasKey("EMPID"))
        {
            emailId = PlayerPrefs.GetString("EMAIL");
            empID = PlayerPrefs.GetString("EMPID");

            var request = new LoginWithEmailAddressRequest { Email = emailId, Password = empID };
            PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSuccess, OnLoginFailure);
        }
    }

    /// <summary>
    /// We will use submit button to login (if the user is present) or else register him/her as new user.
    /// </summary>
    public void OnClickSubmitToLogin()
    {
        if (buttonText.gameObject.GetComponent<Text>().text == "SUBMIT")
        {
            var request = new LoginWithEmailAddressRequest { Email = emailId, Password = empID };
            PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSuccess, OnLoginFailure);
        }

    }

    /// <summary>
    /// This event callback is for successful login
    /// </summary>
    /// <param name="obj"></param>
    private void OnLoginSuccess(LoginResult obj)
    {
        Debug.Log("Successful Login");

        // On a successful login we will update the SUBMIT to START to progress in the game
        buttonText.gameObject.GetComponent<Text>().text = "START";

        // To hold the values locally on the system, if any power issue happens after login
        // These values will let the player login automatically to the game.
        PlayerPrefs.SetString("EMAIL", emailId);
        PlayerPrefs.SetString("ACCOUNT-NAME", userName);
        PlayerPrefs.SetString("EMPID", empID);

        // Disable form element and only keep start button on the screen.
        SetUIElementActive(false);

        // Create Instance of PlayerData
        PlayerData.Create();
    }

    /// <summary>
    /// If the user is not registred that means it is a login faliure and hence we will try to regsiter the user.
    /// We are using empID has password as this would be one time event as per the requirement from the client.
    /// </summary>
    /// <param name="error"></param>
    private void OnLoginFailure(PlayFabError error)
    {
        var registerRequest = new RegisterPlayFabUserRequest { Email = emailId, Password = empID, Username = userName };
        PlayFabClientAPI.RegisterPlayFabUser(registerRequest, OnRegisterSuccess, OnRegisterFailure);
    }

    /// <summary>
    /// This is on register faiure event
    /// </summary>
    /// <param name="obj"></param>
    private void OnRegisterFailure(PlayFabError obj)
    {
        Debug.Log(obj.GenerateErrorReport());
    }

    /// <summary>
    /// This is on register success event 
    /// </summary>
    /// <param name="result"></param>
    private void OnRegisterSuccess(RegisterPlayFabUserResult result)
    {
        // To hold the values locally on the system, if any power issue happens after registration
        // These values will let the player login automatically to the game.
        PlayerPrefs.SetString("EMAIL", emailId);
        PlayerPrefs.SetString("ACCOUNT-NAME", userName);
        PlayerPrefs.SetString("EMPID", empID);

        // On a successful login we will update the SUBMIT to START to progress in the game
        buttonText.gameObject.GetComponent<Text>().text = "START";

        // Disable form element and only keep start button on the screen.
        SetUIElementActive(false);

        // Create Instance of PlayerData
        PlayerData.Create();

        Debug.Log("We have registered a user to Playfab services");
    }


    private void SetUIElementActive(bool condition)
    {
        if(!condition)
        {
            registrationFormUI.SetActive(false);

            emaiIdLabelUI.SetActive(false);
            emaildIdUI.SetActive(false);

            accountNameLabelUI.SetActive(false);
            accountNameUI.SetActive(false);

            empIdLabelUI.SetActive(false);
            empIdUI.SetActive(false);
        }
        else
        {
            registrationFormUI.SetActive(true);

            emaiIdLabelUI.SetActive(true);
            emaildIdUI.SetActive(true);

            accountNameLabelUI.SetActive(true);
            accountNameUI.SetActive(true);

            empIdLabelUI.SetActive(true);
            empIdUI.SetActive(true);
        }
    }
    /// <summary>
    /// To get the Employee ID
    /// </summary>
    /// <param name="empIDin"></param>
    public void GetEmployeeID(string empIDin)
    {
        empID = empIDin;
    }

    /// <summary>
    /// To get the user name from the input field
    /// </summary>
    /// <param name="userNameIn"></param>
    public void GetUserName(string userNameIn)
    {
        userName = userNameIn;
    }

    /// <summary>
    /// To get the email ID from input field
    /// </summary>
    /// <param name="emailIdIn"></param>
    public void GetEmailId(string emailIdIn)
    {
        if (IsValidEmail(emailIdIn))
        {
            emailId = emailIdIn;
        }

    }

    /// <summary>
    /// To check if the email address if valid or not
    /// </summary>
    /// <param name="email"></param>
    /// <returns></returns>
    bool IsValidEmail(string email)
    {
        try
        {
            var addr = new System.Net.Mail.MailAddress(email);
            return addr.Address == email;
        }
        catch
        {
            return false;
        }
    }
}
