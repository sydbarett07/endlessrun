﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;
using PlayFab.ClientModels;
using PlayFab;
using System.Linq;
using System.Collections;
using PlayFab.Json;
using System;
using UnityEngine.AddressableAssets;
#if UNITY_ANALYTICS
using UnityEngine.Analytics;
#endif
#if UNITY_EDITOR
using UnityEditor;
#endif

//Location: PlayerData.cs
[System.Serializable]
public struct HighscoreEntry : System.IComparable<HighscoreEntry>
{
    public string name;
    public int score;

    public int CompareTo(HighscoreEntry other)
    {
        // We want to sort from highest to lowest, so inverse the comparison.
        return other.score.CompareTo(score);
    }
}

/// <summary>
/// Save data for the game. This is stored locally in this case, but a "better" way to do it would be to store it on a server
/// somewhere to avoid player tampering with it. Here potentially a player could modify the binary file to add premium currency.
/// </summary>
public class PlayerData
{
    static protected PlayerData m_Instance;
    static public PlayerData instance { get { return m_Instance; } }

    protected string saveFile = "";

    public static Dictionary<string, UserDataRecord> LoginUserData = new Dictionary<string, UserDataRecord>();
    public static bool IsNewUser;

    public int coins;
    public int premium;
    public Dictionary<Consumable.ConsumableType, int> consumables = new Dictionary<Consumable.ConsumableType, int>();   // Inventory of owned consumables and quantity.

    public List<string> characters = new List<string>();    // Inventory of characters owned.
    public int usedCharacter;                               // Currently equipped character.
    public int usedAccessory = -1;
    public List<string> characterAccessories = new List<string>();  // List of owned accessories, in the form "charName:accessoryName".
    public List<string> themes = new List<string>();                // Owned themes.
    public int usedTheme;                                           // Currently used theme.
    public List<HighscoreEntry> highscores = new List<HighscoreEntry>();
    public List<MissionBase> missions = new List<MissionBase>();

    public string previousName = "Michelle";

    public bool licenceAccepted;

    public float masterVolume = float.MinValue, musicVolume = float.MinValue, masterSFXVolume = float.MinValue;

    //ftue = First Time User Expeerience. This var is used to track thing a player do for the first time. It increment everytime the user do one of the step
    //e.g. it will increment to 1 when they click Start, to 2 when doing the first run, 3 when running at least 300m etc.
    public int ftueLevel = 0;
    //Player win a rank ever 300m (e.g. a player having reached 1200m at least once will be rank 4)
    public int rank = 0;

    // This will allow us to add data even after production, and so keep all existing save STILL valid. See loading & saving for how it work.
    // Note in a real production it would probably reset that to 1 before release (as all dev save don't have to be compatible w/ final product)
    // Then would increment again with every subsequent patches. We kept it to its dev value here for teaching purpose. 
    static int s_Version = 12;

    public void Consume(Consumable.ConsumableType type)
    {
        if (!consumables.ContainsKey(type))
            return;

        consumables[type] -= 1;
        if (consumables[type] == 0)
        {
            consumables.Remove(type);
        }

        Save();
    }

    public void Add(Consumable.ConsumableType type)
    {
        if (!consumables.ContainsKey(type))
        {
            consumables[type] = 0;
        }

        consumables[type] += 1;

        Save();
    }

    public void AddCharacter(string name)
    {
        characters.Add(name);
    }

    public void AddTheme(string theme)
    {
        themes.Add(theme);
    }

    public void AddAccessory(string name)
    {
        characterAccessories.Add(name);
    }

    // Mission management

    // Will add missions until we reach 2 missions.
    public void CheckMissionsCount()
    {
        while (missions.Count < 2)
            AddMission();
    }

    public void AddMission()
    {
        int val = UnityEngine.Random.Range(0, (int)MissionBase.MissionType.MAX);

        MissionBase newMission = MissionBase.GetNewMissionFromType((MissionBase.MissionType)val);
        newMission.Created();

        missions.Add(newMission);
    }

    public void StartRunMissions(TrackManager manager)
    {
        for (int i = 0; i < missions.Count; ++i)
        {
            missions[i].RunStart(manager);
        }
    }

    public void UpdateMissions(TrackManager manager)
    {
        for (int i = 0; i < missions.Count; ++i)
        {
            missions[i].Update(manager);
        }
    }

    public bool AnyMissionComplete()
    {
        for (int i = 0; i < missions.Count; ++i)
        {
            if (missions[i].isComplete) return true;
        }

        return false;
    }

    public void ClaimMission(MissionBase mission)
    {
        premium += mission.reward;

#if UNITY_ANALYTICS // Using Analytics Standard Events v0.3.0
        AnalyticsEvent.ItemAcquired(
            AcquisitionType.Premium, // Currency type
            "mission",               // Context
            mission.reward,          // Amount
            "anchovies",             // Item ID
            premium,                 // Item balance
            "consumable",            // Item type
            rank.ToString()          // Level
        );
#endif

        missions.Remove(mission);

        CheckMissionsCount();

        Save();
    }

    // High Score management

    public int GetScorePlace(int score)
    {
        HighscoreEntry entry = new HighscoreEntry();
        entry.score = score;
        entry.name = "";

        int index = highscores.BinarySearch(entry);

        return index < 0 ? (~index) : index;
    }

    public void InsertScore(int score, string name)
    {
        HighscoreEntry entry = new HighscoreEntry();
        entry.score = score;
        entry.name = name;

        highscores.Insert(GetScorePlace(score), entry);

        // Keep only the 10 best scores.
        while (highscores.Count > 10)
            highscores.RemoveAt(highscores.Count - 1);
    }

    // File management

    static public void Create()
    {
        if (m_Instance == null)
        {
            m_Instance = new PlayerData();

            //if we create the PlayerData, mean it's the very first call, so we use that to init the database
            //this allow to always init the database at the earlier we can, i.e. the start screen if started normally on device
            //or the Loadout screen if testing in editor
            CoroutineHandler.StartStaticCoroutine(CharacterDatabase.LoadDatabase());
            CoroutineHandler.StartStaticCoroutine(ThemeDatabase.LoadDatabase());
        }

        if (IsNewUser || !LoginUserData.ContainsKey("version"))
        {
            NewSave();
        }
        else
        {
            m_Instance.Read();
        }

        m_Instance.CheckMissionsCount();
    }

    static public void NewSave()
    {
        m_Instance.characters.Clear();
        m_Instance.themes.Clear();
        m_Instance.missions.Clear();
        m_Instance.characterAccessories.Clear();
        m_Instance.consumables.Clear();

        m_Instance.usedCharacter = 0;
        m_Instance.usedTheme = 0;
        m_Instance.usedAccessory = -1;

        m_Instance.coins = 0;
        m_Instance.premium = 0;

        m_Instance.characters.Add("Michelle");
        m_Instance.themes.Add("Day");

        m_Instance.ftueLevel = 0;
        m_Instance.rank = 0;

        m_Instance.CheckMissionsCount();

        m_Instance.Save();
    }

    public void Read()
    {
        var json = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);

        var data = LoginUserData;
        s_Version = int.Parse(data["version"].Value);

        var consumablesObj = json.DeserializeObject<Dictionary<Consumable.ConsumableType, int>>(data["consumables"].Value);
        consumables = consumablesObj ?? new Dictionary<Consumable.ConsumableType, int>();

        characters = json.DeserializeObject<List<string>>(data["characters"].Value);
        characterAccessories = json.DeserializeObject<List<string>>(data["character_accessories"].Value);

        var missionObjects = new List<Hashtable>();
        var objectJson = data["missions"].Value;
        var parentObjects = json.DeserializeObject<List<object>>(objectJson);

        parentObjects.ForEach(o =>
        {
            var ht = new Hashtable();
            var ja = (JsonArray)o;
            foreach (JsonObject jaObj in ja)
            {
                ht.Add(jaObj["Key"], jaObj["Value"]);
            }
            missionObjects.Add(ht);
        });

        foreach (var m in missionObjects)
        {
            var type = (MissionBase.MissionType)Convert.ToInt32(m["type"]);
            var mission = MissionBase.GetNewMissionFromType(type);
            mission.progress = (float)Convert.ToDouble(m["progress"]);
            mission.max = Convert.ToInt32(m["max"]);
            mission.reward = Convert.ToInt32(m["reward"]);
            missions.Add(mission);
        }

        themes = json.DeserializeObject<List<string>>(data["themes"].Value);

        var settings = json.DeserializeObject<Dictionary<string, string>>(data["settings"].Value);
        usedCharacter = int.Parse(settings["used_character"]);
        usedTheme = int.Parse(settings["used_theme"]);
        premium = int.Parse(settings["premium"]);
        previousName = settings["previous_name"];
        licenceAccepted = bool.Parse(settings["licence_accepted"]);
        ftueLevel = int.Parse(settings["ftue_level"]);
        rank = int.Parse(settings["rank"]);

        var volumeSettings = json.DeserializeObject<Dictionary<string, float>>(data["volume_settings"].Value);
        masterVolume = volumeSettings["master_volume"];
        musicVolume = volumeSettings["music_volume"];
        masterSFXVolume = volumeSettings["master_sfx_volume"];

        highscores.Clear();
        var hsJSON = data["highScores"].Value;
        var hs = json.DeserializeObject<List<Dictionary<string, object>>>(hsJSON);
        foreach (var kvp in hs)
        {
            highscores.Add(new HighscoreEntry()
            {
                score = Convert.ToInt32(kvp["score"]),
                name = kvp["name"].ToString()
            });
        }
    }

    public void Save()
    {
        var json = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);

        var changes = new Dictionary<string, string>();
        changes.Add("version", s_Version.ToString());

        //TODO: handle currency

        changes.Add("consumables", json.SerializeObject(consumables));
        changes.Add("characters", json.SerializeObject(characters));
        changes.Add("character_accessories", json.SerializeObject(characterAccessories));
        changes.Add("themes", json.SerializeObject(themes));

        var missionsObj = new List<Hashtable>();
        missions.ForEach(m =>
        {
            var type = (int)m.GetMissionType();
            missionsObj.Add(new Hashtable()
        {
            {"type", type},
            {"progress", m.progress},
            {"max", m.max},
            {"reward", m.reward},
            {"isComplete", m.isComplete}
        }
            );
        });

        changes.Add("missions", json.SerializeObject(missionsObj));

        var settings = new Dictionary<string, string>()
{
    {"used_character", usedCharacter.ToString()},
    {"used_theme", usedTheme.ToString()},
    {"premium", premium.ToString()},
    {"previous_name", previousName},
    {"licence_accepted", licenceAccepted.ToString()},
    {"ftue_level",ftueLevel.ToString()},
    {"rank",rank.ToString()}

};
        changes.Add("settings", json.SerializeObject(settings));

        var volumeSettings = new Dictionary<string, float>()
{
    {"master_volume", masterVolume},
    {"music_volume", musicVolume},
    {"master_sfx_volume", masterSFXVolume}

};
        changes.Add("volume_settings", json.SerializeObject(volumeSettings));
        changes.Add("highScores", json.SerializeObject(highscores));

        if(highscores.Count !=0)
        {
            //Log the highest score to playfab in the rank statistic
            PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest()
            {
                Statistics = new List<StatisticUpdate>()
            {
                new StatisticUpdate()
                {
                    StatisticName = "rank",
                    Value = highscores.First().score
                }
            }
            }, null, null);
        }

        PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
        {
            Data = changes
        }, null, (error) =>
        {
            Debug.Log(error.GenerateErrorReport());
        });
    }
}

// Helper class to cheat in the editor for test purpose
#if UNITY_EDITOR
public class PlayerDataEditor : Editor
{
    [MenuItem("Trash Dash Debug/Clear Save")]
    static public void ClearSave()
    {
        File.Delete(Application.persistentDataPath + "/save.bin");
    }

    [MenuItem("Trash Dash Debug/Give 1000000 fishbones and 1000 premium")]
    static public void GiveCoins()
    {
        PlayerData.instance.coins += 1000000;
        PlayerData.instance.premium += 1000;
        PlayerData.instance.Save();
    }

    [MenuItem("Trash Dash Debug/Give 10 Consumables of each types")]
    static public void AddConsumables()
    {

        for (int i = 0; i < ShopItemList.s_ConsumablesTypes.Length; ++i)
        {
            Consumable c = ConsumableDatabase.GetConsumbale(ShopItemList.s_ConsumablesTypes[i]);
            if (c != null)
            {
                PlayerData.instance.consumables[c.GetConsumableType()] = 10;
            }
        }

        PlayerData.instance.Save();
    }
}
#endif