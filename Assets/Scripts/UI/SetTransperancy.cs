using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetTransperancy : MonoBehaviour
{
    [Range(0.0f,1.0f)]
    public float alphaLevel;

    public void Start()
    {
        // Set Transparency for image UI
        if(this.gameObject.GetComponent<Image>() != null)
        {
            UnityEngine.Color __alpha = this.gameObject.GetComponent<Image>().color;
            __alpha.a = alphaLevel;
            this.gameObject.GetComponent<Image>().color = __alpha;
        }

        // Set Transparency for text UI
        if (this.gameObject.GetComponent<Text>() != null)
        {
            UnityEngine.Color __alpha = this.gameObject.GetComponent<Text>().color;
            __alpha.a = alphaLevel;
            this.gameObject.GetComponent<Text>().color = __alpha;
        }

    }

    //public void FixedUpdate()
    //{
    //    UnityEngine.Color __alpha = this.gameObject.GetComponent<Image>().color;
    //    __alpha.a = alphaLevel;
    //    __alpha.r = redLevel;
    //    __alpha.b = blueLevel;
    //    __alpha.g = greenLevel;
    //    this.gameObject.GetComponent<Image>().color = __alpha;
    //}
}
