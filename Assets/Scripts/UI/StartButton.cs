﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;

#if UNITY_ANALYTICS
using UnityEngine.Analytics;
#endif
#if UNITY_PURCHASING
using UnityEngine.Purchasing;
#endif

public class StartButton : MonoBehaviour
{
    public GameObject loadingScreen;

    public Slider slider;

    public void StartGame()
    {
        if(this.gameObject.GetComponentInChildren<Text>().text == "START")
        {
            if (PlayerData.instance.ftueLevel == 0)
            {
                PlayerData.instance.ftueLevel = 1;
                PlayerData.instance.Save();
#if UNITY_ANALYTICS
            AnalyticsEvent.FirstInteraction("start_button_pressed");
#endif
            }

#if UNITY_PURCHASING
        var module = StandardPurchasingModule.Instance();
#endif
            LoadLevel("main");
        }
    }

    public void LoadLevel(string sceneName)
    {
        StartCoroutine(LoadAynchronously(sceneName));
    }

    IEnumerator LoadAynchronously(string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);

        loadingScreen.SetActive(true);

        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;

            yield return null;
        }
    }
}
