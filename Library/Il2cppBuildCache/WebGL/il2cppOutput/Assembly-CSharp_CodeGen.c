﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Character::SetupAccesory(System.Int32)
extern void Character_SetupAccesory_mB2C7DA2D49346B7E20692E0CA7BEE5D9EE153E0C (void);
// 0x00000002 System.Void Character::.ctor()
extern void Character__ctor_mC9E775D49A9CE6DD699D3C59405E734464FDCDC2 (void);
// 0x00000003 System.Void CharacterAccessories::.ctor()
extern void CharacterAccessories__ctor_mB1513067B71D750AD7C126D9BF301311E0EC0F68 (void);
// 0x00000004 CharacterCollider/DeathEvent CharacterCollider::get_deathData()
extern void CharacterCollider_get_deathData_m30C15CC27308AF106F16ED253D7BE6879533DD0B (void);
// 0x00000005 UnityEngine.BoxCollider CharacterCollider::get_collider()
extern void CharacterCollider_get_collider_mF0BD765523BD113B2DACD653AC32916C233B727F (void);
// 0x00000006 UnityEngine.AudioSource CharacterCollider::get_audio()
extern void CharacterCollider_get_audio_m7E061A0D0E318DF9E63F020F67CB793A79627A0D (void);
// 0x00000007 System.Void CharacterCollider::Start()
extern void CharacterCollider_Start_m27B2E67B7391F3DF9295F9569C4C7115ADED7BD0 (void);
// 0x00000008 System.Void CharacterCollider::Init()
extern void CharacterCollider_Init_mA2D9141E0186D0B0C583D9A4D9DD4897CF378B40 (void);
// 0x00000009 System.Void CharacterCollider::Slide(System.Boolean)
extern void CharacterCollider_Slide_m0945F1AB95BAC92F8713AB65F5B22D9E24FD2084 (void);
// 0x0000000A System.Void CharacterCollider::Update()
extern void CharacterCollider_Update_m3B82835239BA52689EFD95FE7DBD7D4BDA7BFAE7 (void);
// 0x0000000B System.Void CharacterCollider::OnTriggerEnter(UnityEngine.Collider)
extern void CharacterCollider_OnTriggerEnter_mE8A741472D669D676F14DFA0AA2B3BDB978031A6 (void);
// 0x0000000C System.Void CharacterCollider::SetInvincibleExplicit(System.Boolean)
extern void CharacterCollider_SetInvincibleExplicit_m1B7CED90D87DDE5849DD20DE3D06C1B35030D287 (void);
// 0x0000000D System.Void CharacterCollider::SetInvincible(System.Single)
extern void CharacterCollider_SetInvincible_mB40DC7F8E604BD7ABB32A3356DFAA3141BB14F3D (void);
// 0x0000000E System.Collections.IEnumerator CharacterCollider::InvincibleTimer(System.Single)
extern void CharacterCollider_InvincibleTimer_m288B818C19163001F8B409969DD901CC26652A6F (void);
// 0x0000000F System.Void CharacterCollider::.ctor()
extern void CharacterCollider__ctor_m0FC05AC0F265E5D1643DB51AD048BAAD22311B5C (void);
// 0x00000010 System.Void CharacterCollider::.cctor()
extern void CharacterCollider__cctor_m17F690CF74B43F61BECE966D59F436E5EA34DEA8 (void);
// 0x00000011 System.Collections.Generic.Dictionary`2<System.String,Character> CharacterDatabase::get_dictionary()
extern void CharacterDatabase_get_dictionary_mB1EF1EDC146F5E8C4F660721B4B2430C8FBA3160 (void);
// 0x00000012 System.Boolean CharacterDatabase::get_loaded()
extern void CharacterDatabase_get_loaded_m067F9DC8B88258B179077D66E3157944596E86A3 (void);
// 0x00000013 Character CharacterDatabase::GetCharacter(System.String)
extern void CharacterDatabase_GetCharacter_mCFCFD7FD2884E03104B034609BF7C128917945E1 (void);
// 0x00000014 System.Collections.IEnumerator CharacterDatabase::LoadDatabase()
extern void CharacterDatabase_LoadDatabase_m8E13C9F8709E0C750E508ABC9FBE05C59B2F8F74 (void);
// 0x00000015 System.Void CharacterDatabase::.ctor()
extern void CharacterDatabase__ctor_mC671A1E605A9A25130626BAB30FE70704FCCECCF (void);
// 0x00000016 System.Void CharacterDatabase::.cctor()
extern void CharacterDatabase__cctor_m2C8F10AE2D58E6BE36D205B88CD2DA1DCD665A48 (void);
// 0x00000017 System.Int32 CharacterInputController::get_coins()
extern void CharacterInputController_get_coins_mA7918955D98BAE0CD5EDDADFE78AFDE6DB36494D (void);
// 0x00000018 System.Void CharacterInputController::set_coins(System.Int32)
extern void CharacterInputController_set_coins_m5960AF38ACA1A59E3CC0607715646525231235E8 (void);
// 0x00000019 System.Int32 CharacterInputController::get_premium()
extern void CharacterInputController_get_premium_m4E6BF2906C901783B93C476B3229A981C1E40943 (void);
// 0x0000001A System.Void CharacterInputController::set_premium(System.Int32)
extern void CharacterInputController_set_premium_m1A16E9A4C0F31782E6DA61018B297BF45442735C (void);
// 0x0000001B System.Int32 CharacterInputController::get_currentLife()
extern void CharacterInputController_get_currentLife_m5590661F1E30259092D8953ECC1CEBE167992512 (void);
// 0x0000001C System.Void CharacterInputController::set_currentLife(System.Int32)
extern void CharacterInputController_set_currentLife_m03983DC1898D36E246344E6F107B6B70BB471C61 (void);
// 0x0000001D System.Collections.Generic.List`1<Consumable> CharacterInputController::get_consumables()
extern void CharacterInputController_get_consumables_m0652D2736AF01E8F18A7D5A89DBB39026E18F409 (void);
// 0x0000001E System.Boolean CharacterInputController::get_isJumping()
extern void CharacterInputController_get_isJumping_mC14F24BCA7CADF874B814A4AFEAAE045B907E7F9 (void);
// 0x0000001F System.Boolean CharacterInputController::get_isSliding()
extern void CharacterInputController_get_isSliding_m58C1B77305B725E2426AE278D1F6AD445BF6030C (void);
// 0x00000020 System.Void CharacterInputController::Awake()
extern void CharacterInputController_Awake_m68A226F11F72F5B4050A0C2E35E0E2A77E2D2331 (void);
// 0x00000021 System.Void CharacterInputController::CheatInvincible(System.Boolean)
extern void CharacterInputController_CheatInvincible_mC038DAF855C1A0AEBDCB7C5D68B04E4A694808F1 (void);
// 0x00000022 System.Boolean CharacterInputController::IsCheatInvincible()
extern void CharacterInputController_IsCheatInvincible_m743934E5C84E39A55A8E1853F332D3E5646AB5AE (void);
// 0x00000023 System.Void CharacterInputController::Init()
extern void CharacterInputController_Init_m5115243ADEB34886DD9E604CC75F214B46885CB4 (void);
// 0x00000024 System.Void CharacterInputController::Begin()
extern void CharacterInputController_Begin_m2FE810D29C49E7E977FBC8007472AFBE8D83107B (void);
// 0x00000025 System.Void CharacterInputController::End()
extern void CharacterInputController_End_m5EFAE8CE60439E107213AE4C71F122E1CF8E7713 (void);
// 0x00000026 System.Void CharacterInputController::CleanConsumable()
extern void CharacterInputController_CleanConsumable_m723E6959E5C10F14DAE1C03D35B157D112FC7FD9 (void);
// 0x00000027 System.Void CharacterInputController::StartRunning()
extern void CharacterInputController_StartRunning_m0623F600129315CEF9B31A60145F0190EA741656 (void);
// 0x00000028 System.Void CharacterInputController::StartMoving()
extern void CharacterInputController_StartMoving_mDEDA627C7A1A43252AAB922EC26F270ED79D13AE (void);
// 0x00000029 System.Void CharacterInputController::StopMoving()
extern void CharacterInputController_StopMoving_m45EB7F43E32F8DAC5CF7701C9D755A2E8CD94517 (void);
// 0x0000002A System.Void CharacterInputController::Update()
extern void CharacterInputController_Update_mE3032FA60047013CF79DDF7C6494C9253A200A7B (void);
// 0x0000002B System.Void CharacterInputController::Jump()
extern void CharacterInputController_Jump_m833580A08F5820CB0DEF593C56CF4D4FAF13A8FA (void);
// 0x0000002C System.Void CharacterInputController::StopJumping()
extern void CharacterInputController_StopJumping_mAAAA5DBF4769076B438187A4D645A675251C396A (void);
// 0x0000002D System.Void CharacterInputController::Slide()
extern void CharacterInputController_Slide_mED3F27303504E821BB530469F2DD6520135646A4 (void);
// 0x0000002E System.Void CharacterInputController::StopSliding()
extern void CharacterInputController_StopSliding_m5B25A91BC2D10B6B66AE2C7D48C8F916BBE2B8DC (void);
// 0x0000002F System.Void CharacterInputController::ChangeLane(System.Int32)
extern void CharacterInputController_ChangeLane_m42370B9253F098650A1EBDD8B91C7E9A66C242FE (void);
// 0x00000030 System.Void CharacterInputController::UseInventory()
extern void CharacterInputController_UseInventory_m0A3FA0AAC4929F9198C240B8FDE5ADED632D3061 (void);
// 0x00000031 System.Void CharacterInputController::UseConsumable(Consumable)
extern void CharacterInputController_UseConsumable_m33EF36BFE3E01281AB0FE83261E1C9DBDEA77967 (void);
// 0x00000032 System.Void CharacterInputController::.ctor()
extern void CharacterInputController__ctor_m42D6EBA37CEB23FD380009A69CB41CEB19907B65 (void);
// 0x00000033 System.Void CharacterInputController::.cctor()
extern void CharacterInputController__cctor_mCD0FBB7A39F759057C8D76803B2BC1D6C52EFE46 (void);
// 0x00000034 System.Void RandomAnimation::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void RandomAnimation_OnStateEnter_m274BF0E70C01D2AFD4B0EA42BCCC5E6C26543A0A (void);
// 0x00000035 System.Void RandomAnimation::.ctor()
extern void RandomAnimation__ctor_mC3605F17E55CFA3AE0B47E317E7DCB423C78B284 (void);
// 0x00000036 System.Void RestartRunning::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void RestartRunning_OnStateExit_m764685CF1805A9897CF442C9A0B25E633F474753 (void);
// 0x00000037 System.Void RestartRunning::.ctor()
extern void RestartRunning__ctor_m8DAB72ABB7F6E98073D2FFD1CC7C1D43DB9C10CB (void);
// 0x00000038 System.Void RestartRunning::.cctor()
extern void RestartRunning__cctor_m42DC7ED826E85F2E4EC5B65E6ADB1CE335ED4085 (void);
// 0x00000039 System.Void Coin::.ctor()
extern void Coin__ctor_mF977D2CE7F9F781D0E5B1226BD2757CC523DB637 (void);
// 0x0000003A System.Boolean Consumable::get_active()
extern void Consumable_get_active_m9BD183D0EB92D06D7FB0F799994422319B9CCD15 (void);
// 0x0000003B System.Single Consumable::get_timeActive()
extern void Consumable_get_timeActive_m0CC82DB5FAB168AE412AE3AE79F5296FD2154DD1 (void);
// 0x0000003C Consumable/ConsumableType Consumable::GetConsumableType()
// 0x0000003D System.String Consumable::GetConsumableName()
// 0x0000003E System.Int32 Consumable::GetPrice()
// 0x0000003F System.Int32 Consumable::GetPremiumCost()
// 0x00000040 System.Void Consumable::ResetTime()
extern void Consumable_ResetTime_mD0A56DCE8EAC4296FED58FF4EC9AB7C8ADAC9975 (void);
// 0x00000041 System.Boolean Consumable::CanBeUsed(CharacterInputController)
extern void Consumable_CanBeUsed_mD5E41CD426D84A5550F44DCCC142BACCB31D89DB (void);
// 0x00000042 System.Collections.IEnumerator Consumable::Started(CharacterInputController)
extern void Consumable_Started_mFCCE21EDCF35D6CA314513F2FE36243FB1444049 (void);
// 0x00000043 System.Collections.IEnumerator Consumable::TimedRelease(UnityEngine.GameObject,System.Single)
extern void Consumable_TimedRelease_m4CF53D8423644C0C574BD9846F3E3C9BA221BD89 (void);
// 0x00000044 System.Void Consumable::Tick(CharacterInputController)
extern void Consumable_Tick_m29550B408B4852BAB892FEBC466B237B124074A2 (void);
// 0x00000045 System.Void Consumable::Ended(CharacterInputController)
extern void Consumable_Ended_mED6B794CD1EB05A8A1B0508BA4AB4BC6E271D996 (void);
// 0x00000046 System.Void Consumable::.ctor()
extern void Consumable__ctor_m49924D1710089801735A2523E1C66AD726832A7A (void);
// 0x00000047 System.Void ConsumableDatabase::Load()
extern void ConsumableDatabase_Load_m88167DDB02DDCB33D924CCD0F246E174FE34761E (void);
// 0x00000048 Consumable ConsumableDatabase::GetConsumbale(Consumable/ConsumableType)
extern void ConsumableDatabase_GetConsumbale_m2BF3471F1D6CA78B88ACF97A75119266835E580A (void);
// 0x00000049 System.Void ConsumableDatabase::.ctor()
extern void ConsumableDatabase__ctor_m374AA93053AA4243EF9B7DD87D8A4984BB9C3001 (void);
// 0x0000004A System.Void ConsumableIcon::.ctor()
extern void ConsumableIcon__ctor_mB2568114F3436222C89477C4BF2CFD2E1DFC9E63 (void);
// 0x0000004B System.String CoinMagnet::GetConsumableName()
extern void CoinMagnet_GetConsumableName_m5E1E2CFC12A15C207D770BCB79EA3E963F026096 (void);
// 0x0000004C Consumable/ConsumableType CoinMagnet::GetConsumableType()
extern void CoinMagnet_GetConsumableType_m066EE107836BC29A086C2E6212D4DF2C40039A9B (void);
// 0x0000004D System.Int32 CoinMagnet::GetPrice()
extern void CoinMagnet_GetPrice_mBABE16458C266DE11E641DF90AACEBAE3FCA207F (void);
// 0x0000004E System.Int32 CoinMagnet::GetPremiumCost()
extern void CoinMagnet_GetPremiumCost_m91F1E7BEDC0AD05C2F0A24C0EFF417281C5F819A (void);
// 0x0000004F System.Void CoinMagnet::Tick(CharacterInputController)
extern void CoinMagnet_Tick_mF2DB5370602938FD45F2E6CBEEE8B51EC91ED3FB (void);
// 0x00000050 System.Void CoinMagnet::.ctor()
extern void CoinMagnet__ctor_mE53EF9E42D20C5707050D80C2375BA7B0A0DFD89 (void);
// 0x00000051 System.String ExtraLife::GetConsumableName()
extern void ExtraLife_GetConsumableName_mF131848FFEAEC044D49E96D69190CA3798807B5D (void);
// 0x00000052 Consumable/ConsumableType ExtraLife::GetConsumableType()
extern void ExtraLife_GetConsumableType_mA57A8CB79B55049DCA612C8C5D6ED9B6CFDF5980 (void);
// 0x00000053 System.Int32 ExtraLife::GetPrice()
extern void ExtraLife_GetPrice_m74417195122AC422C13CBAC29700E6891E71EC80 (void);
// 0x00000054 System.Int32 ExtraLife::GetPremiumCost()
extern void ExtraLife_GetPremiumCost_mA98ECEF49095A390D76505F1BBD977D5EF27181A (void);
// 0x00000055 System.Boolean ExtraLife::CanBeUsed(CharacterInputController)
extern void ExtraLife_CanBeUsed_m35DCDF7D299174D2071BFC05D05A32559E47651E (void);
// 0x00000056 System.Collections.IEnumerator ExtraLife::Started(CharacterInputController)
extern void ExtraLife_Started_m7EF4AD4261EDC04DAE7F8EB7986821CA5A72DA50 (void);
// 0x00000057 System.Void ExtraLife::.ctor()
extern void ExtraLife__ctor_mB3DCC1F79E45D78C09749719B8AD67BAC35FCF43 (void);
// 0x00000058 System.Collections.IEnumerator ExtraLife::<>n__0(CharacterInputController)
extern void ExtraLife_U3CU3En__0_mD7F4F46A4A1EA7E8E75497F878041BBF3E5B3234 (void);
// 0x00000059 System.String Invincibility::GetConsumableName()
extern void Invincibility_GetConsumableName_m92BF1C380AB95CFD2C40B458ACFB3067EDDF6680 (void);
// 0x0000005A Consumable/ConsumableType Invincibility::GetConsumableType()
extern void Invincibility_GetConsumableType_m2C7DB2E1C8F20879FF95A0CA87174E141E1702CB (void);
// 0x0000005B System.Int32 Invincibility::GetPrice()
extern void Invincibility_GetPrice_m2F2A0248BE8103C448B561D62C78334844CAFF08 (void);
// 0x0000005C System.Int32 Invincibility::GetPremiumCost()
extern void Invincibility_GetPremiumCost_m51D9B4939272A558EB1EDC572B019EF699E9978D (void);
// 0x0000005D System.Void Invincibility::Tick(CharacterInputController)
extern void Invincibility_Tick_m61DD4437139F5CA49A0F810D409947F7154E55E1 (void);
// 0x0000005E System.Collections.IEnumerator Invincibility::Started(CharacterInputController)
extern void Invincibility_Started_mB345077AEBAE4D21E7FA503F804654AFCDEA0F81 (void);
// 0x0000005F System.Void Invincibility::Ended(CharacterInputController)
extern void Invincibility_Ended_m9429A4F8BFA5A7BF797981E3E7777CA10103CD9F (void);
// 0x00000060 System.Void Invincibility::.ctor()
extern void Invincibility__ctor_m29ABFDCF03E7AA70ACF942C162BE505EEBE89C1B (void);
// 0x00000061 System.Collections.IEnumerator Invincibility::<>n__0(CharacterInputController)
extern void Invincibility_U3CU3En__0_mE46F1B9B439B5512A3CAA99F78C22D5ADE77DF01 (void);
// 0x00000062 System.String Score2Multiplier::GetConsumableName()
extern void Score2Multiplier_GetConsumableName_m6E3BD93D6000A0CA29CB02E63A6D0C67F94EA00D (void);
// 0x00000063 Consumable/ConsumableType Score2Multiplier::GetConsumableType()
extern void Score2Multiplier_GetConsumableType_mF5B2DE2A8B9EA24CF8F09AF5D9EC3B20A3BFC32B (void);
// 0x00000064 System.Int32 Score2Multiplier::GetPrice()
extern void Score2Multiplier_GetPrice_m4C3D295C3E3BAE3D322E4BD810DDA60A0CA26C17 (void);
// 0x00000065 System.Int32 Score2Multiplier::GetPremiumCost()
extern void Score2Multiplier_GetPremiumCost_m6AD93F7710BCBD2331F5A86A4C792A8AAF980B16 (void);
// 0x00000066 System.Collections.IEnumerator Score2Multiplier::Started(CharacterInputController)
extern void Score2Multiplier_Started_mA87019FA02A9CDB61CB55F4E08471BDB3663E6B5 (void);
// 0x00000067 System.Void Score2Multiplier::Ended(CharacterInputController)
extern void Score2Multiplier_Ended_m90600953DCB74A36FB80F835114E3C23C9A3B587 (void);
// 0x00000068 System.Int32 Score2Multiplier::MultiplyModify(System.Int32)
extern void Score2Multiplier_MultiplyModify_mF564041C4522E0465496E14F2B8C25B75309DD61 (void);
// 0x00000069 System.Void Score2Multiplier::.ctor()
extern void Score2Multiplier__ctor_m336BBDDE4D6CA68070A922351850AA47B03EB8D1 (void);
// 0x0000006A System.Collections.IEnumerator Score2Multiplier::<>n__0(CharacterInputController)
extern void Score2Multiplier_U3CU3En__0_mD98F91578AC7A43A24EA7E614E30A51F2FF5E861 (void);
// 0x0000006B CoroutineHandler CoroutineHandler::get_instance()
extern void CoroutineHandler_get_instance_mC30739A5FA9BB17B5885C067029BF17C12537C07 (void);
// 0x0000006C System.Void CoroutineHandler::OnDisable()
extern void CoroutineHandler_OnDisable_m657E1F70FD67D06D5EDD7168B830064BE92B7A08 (void);
// 0x0000006D UnityEngine.Coroutine CoroutineHandler::StartStaticCoroutine(System.Collections.IEnumerator)
extern void CoroutineHandler_StartStaticCoroutine_m2E6879D6BDC14C945D3379FA8838922AE3714099 (void);
// 0x0000006E System.Void CoroutineHandler::.ctor()
extern void CoroutineHandler__ctor_m2F086A05A90813039FA5EE301F265E8F3F63A9AE (void);
// 0x0000006F GameManager GameManager::get_instance()
extern void GameManager_get_instance_m01A2DC805F24ED39DAFB4BE28A43CFD69AE0D00B (void);
// 0x00000070 AState GameManager::get_topState()
extern void GameManager_get_topState_mDB274A1550D33EFD6B938377D13F6AB89B6312C0 (void);
// 0x00000071 System.Void GameManager::Awake()
extern void GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30 (void);
// 0x00000072 System.Void GameManager::OnLoginSuccess(PlayFab.ClientModels.LoginResult)
extern void GameManager_OnLoginSuccess_m1C192B3BDF92D89AEAEF60BBFA65F47059C516B0 (void);
// 0x00000073 System.Void GameManager::OnDisplayAuthentication()
extern void GameManager_OnDisplayAuthentication_mC193AB7DDD2BEDF803919DF135DB820721834FA5 (void);
// 0x00000074 System.Void GameManager::OnPlayFabEnabled()
extern void GameManager_OnPlayFabEnabled_mC54BF508287CB01BC8025D6DD3E1A6B06E826D72 (void);
// 0x00000075 System.Void GameManager::Update()
extern void GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1 (void);
// 0x00000076 System.Void GameManager::OnApplicationQuit()
extern void GameManager_OnApplicationQuit_m84D45A971E10AF1DE8F25809E66AF69557445E8E (void);
// 0x00000077 System.Void GameManager::SwitchState(System.String)
extern void GameManager_SwitchState_m4AE5D7157D2BDD220796F870160C0FE4504D81FA (void);
// 0x00000078 AState GameManager::FindState(System.String)
extern void GameManager_FindState_m1B489EE6B9437B2DF1DCF153CE5DFFDF779AFD82 (void);
// 0x00000079 System.Void GameManager::PopState()
extern void GameManager_PopState_m34765F910BEB1D9F6CD3556200C596E517716577 (void);
// 0x0000007A System.Void GameManager::PushState(System.String)
extern void GameManager_PushState_mB90D8DBD45A3F4565EC885D75B1615BC49387CC8 (void);
// 0x0000007B System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x0000007C System.Void AState::Enter(AState)
// 0x0000007D System.Void AState::Exit(AState)
// 0x0000007E System.Void AState::Tick()
// 0x0000007F System.String AState::GetName()
// 0x00000080 System.Void AState::.ctor()
extern void AState__ctor_m72EF28E952C4E3889571AB87DE08A239914CEE13 (void);
// 0x00000081 System.Void GameOverState::Enter(AState)
extern void GameOverState_Enter_mB0F5CF2A842DDF8A59E9EFBCD47B16EAEE92B3EE (void);
// 0x00000082 System.Void GameOverState::Exit(AState)
extern void GameOverState_Exit_mF3EE8190CEED49553FB9FB0C96FD5E92D6933CAB (void);
// 0x00000083 System.String GameOverState::GetName()
extern void GameOverState_GetName_mF07A53A1AC07A5068AA6DC79FBAC9A0FD9619514 (void);
// 0x00000084 System.Void GameOverState::Tick()
extern void GameOverState_Tick_m0422DA154B140B6E1A4CCE0280CC64DD192DEDF8 (void);
// 0x00000085 System.Void GameOverState::OpenLeaderboard()
extern void GameOverState_OpenLeaderboard_m942B9B675BCEA79ED7C02E5EE9264DF266862762 (void);
// 0x00000086 System.Void GameOverState::GoToStore()
extern void GameOverState_GoToStore_mE82E99460209C6286CFB9E4D68CA121E4EE6DE47 (void);
// 0x00000087 System.Void GameOverState::GoToLoadout()
extern void GameOverState_GoToLoadout_mEEB859F5492905AD23447D894C765509E30A5780 (void);
// 0x00000088 System.Void GameOverState::RunAgain()
extern void GameOverState_RunAgain_m94ADBFE7B989494A4EDE07F3BE4B9E2AFDC06191 (void);
// 0x00000089 System.Void GameOverState::CreditCoins()
extern void GameOverState_CreditCoins_mA4E8EBBCBA2584B43E33415D3542BB88BD50854B (void);
// 0x0000008A System.Void GameOverState::FinishRun()
extern void GameOverState_FinishRun_mE4573AA25C76AE3DFCDC2CF9C67A04212272FE32 (void);
// 0x0000008B System.Void GameOverState::.ctor()
extern void GameOverState__ctor_mBE7DA3F9387C25715444A68655E3010A5117F651 (void);
// 0x0000008C System.Void GameState::Enter(AState)
extern void GameState_Enter_mB5299A6B02EF61E628612A7AD4C4DEB8A021D829 (void);
// 0x0000008D System.Void GameState::Exit(AState)
extern void GameState_Exit_m43F1D15E53913916EE73E6CD8712B1B86627FC92 (void);
// 0x0000008E System.Void GameState::StartGame()
extern void GameState_StartGame_m1721CD5948A5CD365CAFF93108775A6A19706A37 (void);
// 0x0000008F System.String GameState::GetName()
extern void GameState_GetName_m3B44C99B75D40E4DD2D410CB1BA15021C775E408 (void);
// 0x00000090 System.Void GameState::Tick()
extern void GameState_Tick_m5D400090B65A728BB1548843EA00D341024EA0ED (void);
// 0x00000091 System.Void GameState::OnApplicationPause(System.Boolean)
extern void GameState_OnApplicationPause_m72C23784699D91FA9674FC33D8AE8E006B13FAF4 (void);
// 0x00000092 System.Void GameState::OnApplicationFocus(System.Boolean)
extern void GameState_OnApplicationFocus_mBA524788A72F32B49A11BA0D896F96F5DE464F9C (void);
// 0x00000093 System.Void GameState::Pause(System.Boolean)
extern void GameState_Pause_m4BBA1B9D5F6BD0422D824300A436BC25B7EF1D85 (void);
// 0x00000094 System.Void GameState::Resume()
extern void GameState_Resume_mE68B46A86029A232BB32351ED4BC5C8A7438703E (void);
// 0x00000095 System.Void GameState::QuitToLoadout()
extern void GameState_QuitToLoadout_m86D5B2A60767C517D7BBF60BBB9C46EE10F30A40 (void);
// 0x00000096 System.Void GameState::UpdateUI()
extern void GameState_UpdateUI_mF94DAE9FD4E3D150C826044311DD0D9230D9250A (void);
// 0x00000097 System.Collections.IEnumerator GameState::WaitForGameOver()
extern void GameState_WaitForGameOver_mAC7CAC58332275915F6E5F2368EB63CDEA165465 (void);
// 0x00000098 System.Void GameState::ClearPowerup()
extern void GameState_ClearPowerup_m1D7AF56CB2244ADCBA15B28D424DF1A47A1D4A3B (void);
// 0x00000099 System.Void GameState::OpenGameOverPopup()
extern void GameState_OpenGameOverPopup_m3ECBDB05AE426EDE9A7DAB1C5D30664AD56F7BC7 (void);
// 0x0000009A System.Void GameState::GameOver()
extern void GameState_GameOver_mDF01C741C890FF323E32124401B0AD4A5B3376FE (void);
// 0x0000009B System.Void GameState::PremiumForLife()
extern void GameState_PremiumForLife_m2C755DD7C34E1F34D541E7E48157C2194B975A33 (void);
// 0x0000009C System.Void GameState::SecondWind()
extern void GameState_SecondWind_m32755DFDC78C85FF5B0B3075967B9F86103EC0BA (void);
// 0x0000009D System.Void GameState::ShowRewardedAd()
extern void GameState_ShowRewardedAd_m2AC0069C66EF82C3FA2841CA166E3BBAE1347168 (void);
// 0x0000009E System.Void GameState::.ctor()
extern void GameState__ctor_mA14A91584839FF578B3B751214A2921375FD1578 (void);
// 0x0000009F System.Void GameState::.cctor()
extern void GameState__cctor_m555A98DBA8E01F4099A2E2D12F74D2F3A9C85960 (void);
// 0x000000A0 System.Void LoadoutState::Enter(AState)
extern void LoadoutState_Enter_mE09A2008C430DC89B9BFE41DEBD536EA3723C2B4 (void);
// 0x000000A1 System.Void LoadoutState::Exit(AState)
extern void LoadoutState_Exit_m11726804C21B2A67B8D388A220A7E8D9CB93FB2A (void);
// 0x000000A2 System.Void LoadoutState::Refresh()
extern void LoadoutState_Refresh_m1F3B8D718AE1857C83911DA91828DE5A0820C45D (void);
// 0x000000A3 System.String LoadoutState::GetName()
extern void LoadoutState_GetName_m5C5CCC38B793CC1BE8401055C40B606E2069BDAB (void);
// 0x000000A4 System.Void LoadoutState::Tick()
extern void LoadoutState_Tick_m7C62A386EDDF85D67EFA719CB2C33BDEAB9F05A5 (void);
// 0x000000A5 System.Void LoadoutState::GoToStore()
extern void LoadoutState_GoToStore_m8914771659E89C17C441E112E1746858B813B796 (void);
// 0x000000A6 System.Void LoadoutState::ChangeCharacter(System.Int32)
extern void LoadoutState_ChangeCharacter_m29763255FBEFD455603E9B62850A5735037FAAED (void);
// 0x000000A7 System.Void LoadoutState::ChangeAccessory(System.Int32)
extern void LoadoutState_ChangeAccessory_m3335A73AAEED201ECC7D69E18D36057A8D628CA6 (void);
// 0x000000A8 System.Void LoadoutState::ChangeTheme(System.Int32)
extern void LoadoutState_ChangeTheme_m3F1B3EBA142A07917C97EFCA87135E6EC42F550C (void);
// 0x000000A9 System.Collections.IEnumerator LoadoutState::PopulateTheme()
extern void LoadoutState_PopulateTheme_m22B65F81A35B1F3F5091DC053BE48EC9129A88B6 (void);
// 0x000000AA System.Collections.IEnumerator LoadoutState::PopulateCharacters()
extern void LoadoutState_PopulateCharacters_m8397B1FD2F99F48E00EBEBA08CA64064BEAD07F3 (void);
// 0x000000AB System.Void LoadoutState::SetupAccessory()
extern void LoadoutState_SetupAccessory_m4550029359668EF4C849F3CAC503A4C183C24CA3 (void);
// 0x000000AC System.Void LoadoutState::PopulatePowerup()
extern void LoadoutState_PopulatePowerup_mFC89D079BF7C01492B1068CBFA63DE9BFE42C27D (void);
// 0x000000AD System.Void LoadoutState::ChangeConsumable(System.Int32)
extern void LoadoutState_ChangeConsumable_m8FA06821E9F65D89202408C5CB4D378C5CE76529 (void);
// 0x000000AE System.Void LoadoutState::UnequipPowerup()
extern void LoadoutState_UnequipPowerup_mA2F48805147E8845280B14DA689BA236BCBA625C (void);
// 0x000000AF System.Void LoadoutState::SetModifier(Modifier)
extern void LoadoutState_SetModifier_m4E41DD0C4C78D7DFE62E4F4C3FC9FB21367CEF45 (void);
// 0x000000B0 System.Void LoadoutState::StartGame()
extern void LoadoutState_StartGame_m62EBC673F49DC3ED31BBC88D3B9523F98F12FDA5 (void);
// 0x000000B1 System.Void LoadoutState::Openleaderboard()
extern void LoadoutState_Openleaderboard_m6995586BC6F036F79D53BE48136851DBC3811AB1 (void);
// 0x000000B2 System.Void LoadoutState::.ctor()
extern void LoadoutState__ctor_mB4DA08F5C69BDF13769C2F0AA11AD02C104E662D (void);
// 0x000000B3 System.Void Modifier::OnRunStart(GameState)
extern void Modifier_OnRunStart_m9B93F0AF6552ADE96BA79859D380F50796E595FC (void);
// 0x000000B4 System.Void Modifier::OnRunTick(GameState)
extern void Modifier_OnRunTick_mDF590DD20D977706508EB7325BCDC91362D6724A (void);
// 0x000000B5 System.Boolean Modifier::OnRunEnd(GameState)
extern void Modifier_OnRunEnd_mCAA4ECD15F848A4C5F41CA6EB6371DA36D483C01 (void);
// 0x000000B6 System.Void Modifier::.ctor()
extern void Modifier__ctor_mEA4613339B8CD37F6DC43A8D7F18CCD448F405B7 (void);
// 0x000000B7 System.Void LimitedLengthRun::.ctor(System.Single)
extern void LimitedLengthRun__ctor_m9D1034526E210A8A249AB2315E6910A3E763AF21 (void);
// 0x000000B8 System.Void LimitedLengthRun::OnRunTick(GameState)
extern void LimitedLengthRun_OnRunTick_m15BD7C4686312D8542E1C42A1F86AC7D4E9AAB16 (void);
// 0x000000B9 System.Void LimitedLengthRun::OnRunStart(GameState)
extern void LimitedLengthRun_OnRunStart_mEF93A1EBE9F615F22A434C9ED340F6BDF52DC2E9 (void);
// 0x000000BA System.Boolean LimitedLengthRun::OnRunEnd(GameState)
extern void LimitedLengthRun_OnRunEnd_mE58149065198C5312088A8513CAD05AA4309F9A4 (void);
// 0x000000BB System.Void SeededRun::.ctor()
extern void SeededRun__ctor_mABDBF9C949623B12908B9B6A15AB46DF949BFF6C (void);
// 0x000000BC System.Void SeededRun::OnRunStart(GameState)
extern void SeededRun_OnRunStart_m2A3DC411F5DFE0652EDADF5A0AD22CFEB5701DDD (void);
// 0x000000BD System.Boolean SeededRun::OnRunEnd(GameState)
extern void SeededRun_OnRunEnd_m39AE67D079D3F8980550F2A7630CC7691A152B9E (void);
// 0x000000BE System.Void SingleLifeRun::OnRunTick(GameState)
extern void SingleLifeRun_OnRunTick_mFA020C2F5779BA89EFB884ECCD8B9CEE7A1D2DC8 (void);
// 0x000000BF System.Void SingleLifeRun::OnRunStart(GameState)
extern void SingleLifeRun_OnRunStart_m6AF5BCA56A90FB79E6F7C076D2384E6ABCF2CAF3 (void);
// 0x000000C0 System.Boolean SingleLifeRun::OnRunEnd(GameState)
extern void SingleLifeRun_OnRunEnd_m7A8BA96050725B35EC7814938561349457E7CF66 (void);
// 0x000000C1 System.Void SingleLifeRun::.ctor()
extern void SingleLifeRun__ctor_m761CE9228F29DE87E31CAE33FBB32510BF85E4A6 (void);
// 0x000000C2 System.Void PlayFabAuthService::add_OnDisplayAuthentication(PlayFabAuthService/DisplayAuthenticationEvent)
extern void PlayFabAuthService_add_OnDisplayAuthentication_m09307D1077EFDBA69B680DBC3E4565061E3A3C47 (void);
// 0x000000C3 System.Void PlayFabAuthService::remove_OnDisplayAuthentication(PlayFabAuthService/DisplayAuthenticationEvent)
extern void PlayFabAuthService_remove_OnDisplayAuthentication_mF91D71B93627024F5084926D6C3C842A8F4AC2B1 (void);
// 0x000000C4 System.Void PlayFabAuthService::add_OnLoginSuccess(PlayFabAuthService/LoginSuccessEvent)
extern void PlayFabAuthService_add_OnLoginSuccess_m1B44BC59674DA4AEB0F57AD3CA283179AF5EB607 (void);
// 0x000000C5 System.Void PlayFabAuthService::remove_OnLoginSuccess(PlayFabAuthService/LoginSuccessEvent)
extern void PlayFabAuthService_remove_OnLoginSuccess_m6A6AB26D85E1CFEF14668DF1E7338AEF91C54DA0 (void);
// 0x000000C6 System.Void PlayFabAuthService::add_OnPlayFabError(PlayFabAuthService/PlayFabErrorEvent)
extern void PlayFabAuthService_add_OnPlayFabError_m06D8796B2EC01FCEBFCAAEAECC1FAC972EF83DF4 (void);
// 0x000000C7 System.Void PlayFabAuthService::remove_OnPlayFabError(PlayFabAuthService/PlayFabErrorEvent)
extern void PlayFabAuthService_remove_OnPlayFabError_mF5A068C29D48826E5FB846DC0D38ED5A19DE9397 (void);
// 0x000000C8 System.String PlayFabAuthService::get_PlayFabId()
extern void PlayFabAuthService_get_PlayFabId_mFDC42662ECD647411ECE2EA6166E1CEA784E1306 (void);
// 0x000000C9 System.String PlayFabAuthService::get_SessionTicket()
extern void PlayFabAuthService_get_SessionTicket_m57EF01A6618572185DEEB280BACAEE512F963250 (void);
// 0x000000CA PlayFabAuthService PlayFabAuthService::get_Instance()
extern void PlayFabAuthService_get_Instance_mA897E191B308C20E8F816061033E07FD9BE92D35 (void);
// 0x000000CB System.Void PlayFabAuthService::.ctor()
extern void PlayFabAuthService__ctor_mE0B53ED52D15B3C9B3CAD84FC3A8CAA911E9BE42 (void);
// 0x000000CC System.Boolean PlayFabAuthService::get_RememberMe()
extern void PlayFabAuthService_get_RememberMe_m18006A1AEDEB47CA209549E8FBCF17E21B296B69 (void);
// 0x000000CD System.Void PlayFabAuthService::set_RememberMe(System.Boolean)
extern void PlayFabAuthService_set_RememberMe_mA43A893BF87ADCC18C816BDE99980EA570A0BD20 (void);
// 0x000000CE Authtypes PlayFabAuthService::get_AuthType()
extern void PlayFabAuthService_get_AuthType_m991DE11CE6DD6432434747062853D0E0A8C65FEB (void);
// 0x000000CF System.Void PlayFabAuthService::set_AuthType(Authtypes)
extern void PlayFabAuthService_set_AuthType_m0324E91F4871E9701DF88B4E4A9C56FAF7F1CBEA (void);
// 0x000000D0 System.String PlayFabAuthService::get_RememberMeId()
extern void PlayFabAuthService_get_RememberMeId_m23628575AF9F71CBF48270199BF9FEAF5CD4F0C2 (void);
// 0x000000D1 System.Void PlayFabAuthService::set_RememberMeId(System.String)
extern void PlayFabAuthService_set_RememberMeId_m6B743999F4ACB4BE47E2A3FC102B004792C18C5E (void);
// 0x000000D2 System.Void PlayFabAuthService::ClearRememberMe()
extern void PlayFabAuthService_ClearRememberMe_mA008AA2BEF391317EA464A6D21CE349D066818A3 (void);
// 0x000000D3 System.Void PlayFabAuthService::Authenticate(Authtypes)
extern void PlayFabAuthService_Authenticate_m96B8766549B69AFD2C718B10B6783293D1EA65AD (void);
// 0x000000D4 System.Void PlayFabAuthService::Authenticate()
extern void PlayFabAuthService_Authenticate_mFA3335EB389D65B7ABCA8CD104F4A86073FEC8D4 (void);
// 0x000000D5 System.Void PlayFabAuthService::AuthenticateEmailPassword()
extern void PlayFabAuthService_AuthenticateEmailPassword_m302974BB5D0C227928345410E7D8C72DA78FAC28 (void);
// 0x000000D6 System.Void PlayFabAuthService::AddAccountAndPassword()
extern void PlayFabAuthService_AddAccountAndPassword_m2842E09E11CAEF5CC46019A3C2EEA3DCB3A0DEF2 (void);
// 0x000000D7 System.Void PlayFabAuthService::AuthenticateFacebook()
extern void PlayFabAuthService_AuthenticateFacebook_mA8CD3C30836ECF8E89D3E190EC2E08E38398DC97 (void);
// 0x000000D8 System.Void PlayFabAuthService::AuthenticateGooglePlayGames()
extern void PlayFabAuthService_AuthenticateGooglePlayGames_m4517338DFEEC6F9326C4E75639315E301F554061 (void);
// 0x000000D9 System.Void PlayFabAuthService::AuthenticateSteam()
extern void PlayFabAuthService_AuthenticateSteam_mAA36F12AE59F7C2F953F3233F3FA673FE5ADF743 (void);
// 0x000000DA System.Void PlayFabAuthService::SilentlyAuthenticate(System.Action`1<PlayFab.ClientModels.LoginResult>)
extern void PlayFabAuthService_SilentlyAuthenticate_mDD6E523EF570A7D7EA5E367B1C45BEB4700A7618 (void);
// 0x000000DB System.Void PlayFabAuthService::UnlinkSilentAuth()
extern void PlayFabAuthService_UnlinkSilentAuth_m98A2F30829899450ACC5A3993AD511C7A1AD5912 (void);
// 0x000000DC System.Void PlayFabAuthService::<AuthenticateEmailPassword>b__43_2(PlayFab.ClientModels.LoginResult)
extern void PlayFabAuthService_U3CAuthenticateEmailPasswordU3Eb__43_2_m73A9B7F2804D6DAF65391CC91DE8227C2511B59F (void);
// 0x000000DD System.Void PlayFabAuthService::<AddAccountAndPassword>b__44_0(PlayFab.ClientModels.LoginResult)
extern void PlayFabAuthService_U3CAddAccountAndPasswordU3Eb__44_0_mEBB431D2D3658DEA91D3EAFA612584CE7C5A5F98 (void);
// 0x000000DE System.Void Helpers::SetRendererLayerRecursive(UnityEngine.GameObject,System.Int32)
extern void Helpers_SetRendererLayerRecursive_mB6D8813394E7A1C269B980CBEDBA4A46E86EF68F (void);
// 0x000000DF System.Void Helpers::.ctor()
extern void Helpers__ctor_m1B2219AA037C37F001996A5AB3EA87C908D13924 (void);
// 0x000000E0 System.Void LevelLoader::LoadLevel(System.String)
extern void LevelLoader_LoadLevel_mE00D1FE2B6BBF5019DD479F77C311E3946446D52 (void);
// 0x000000E1 System.Void LevelLoader::.ctor()
extern void LevelLoader__ctor_m189433ECD8FC8ADD3F871B6C14ECD7F454E38FB5 (void);
// 0x000000E2 System.Boolean MissionBase::get_isComplete()
extern void MissionBase_get_isComplete_m1856DBFD7530737E7833976E619FF860C82FF1CF (void);
// 0x000000E3 System.Void MissionBase::Serialize(System.IO.BinaryWriter)
extern void MissionBase_Serialize_mF64493FBDF907597DB062B835C29BE9F4F39AE15 (void);
// 0x000000E4 System.Void MissionBase::Deserialize(System.IO.BinaryReader)
extern void MissionBase_Deserialize_m01BFCE90CC1E5EAF6E63A3D6FF7DC4FEE110F694 (void);
// 0x000000E5 System.Boolean MissionBase::HaveProgressBar()
extern void MissionBase_HaveProgressBar_mAF2DDAB2F433B7CCDEDA668E633CADE6000EDA7E (void);
// 0x000000E6 System.Void MissionBase::Created()
// 0x000000E7 MissionBase/MissionType MissionBase::GetMissionType()
// 0x000000E8 System.String MissionBase::GetMissionDesc()
// 0x000000E9 System.Void MissionBase::RunStart(TrackManager)
// 0x000000EA System.Void MissionBase::Update(TrackManager)
// 0x000000EB MissionBase MissionBase::GetNewMissionFromType(MissionBase/MissionType)
extern void MissionBase_GetNewMissionFromType_m458E73FFCA0E51ECE0B670424274CCA2BB5AE1C0 (void);
// 0x000000EC System.Void MissionBase::.ctor()
extern void MissionBase__ctor_mEB6F981EE258FDF62FC79CBBD746B09EEF3EC85D (void);
// 0x000000ED System.Void SingleRunMission::Created()
extern void SingleRunMission_Created_m112E7397F862F55EC1FEBBEE4895D720B2081B3B (void);
// 0x000000EE System.Boolean SingleRunMission::HaveProgressBar()
extern void SingleRunMission_HaveProgressBar_m9BA84B6BD9AA1AF63979EFBE0912068D201E95CC (void);
// 0x000000EF System.String SingleRunMission::GetMissionDesc()
extern void SingleRunMission_GetMissionDesc_m3FA0FA783C9504859B320D1C3FB32D2BB0A24ACA (void);
// 0x000000F0 MissionBase/MissionType SingleRunMission::GetMissionType()
extern void SingleRunMission_GetMissionType_mB565F81AEFA82A97B235511DD9AD9624349BEFC6 (void);
// 0x000000F1 System.Void SingleRunMission::RunStart(TrackManager)
extern void SingleRunMission_RunStart_mDEEF0B66457B37341A766ACDD4320F0B8270C908 (void);
// 0x000000F2 System.Void SingleRunMission::Update(TrackManager)
extern void SingleRunMission_Update_m4FA0B557D42FC4263ECC6BB7A0224F6CF9015462 (void);
// 0x000000F3 System.Void SingleRunMission::.ctor()
extern void SingleRunMission__ctor_mB7F20B0341FAF054716344CE31E6ECB256DB151E (void);
// 0x000000F4 System.Void PickupMission::Created()
extern void PickupMission_Created_m16848F79FE6113C87C0D957A323C01C5468CAEA8 (void);
// 0x000000F5 System.String PickupMission::GetMissionDesc()
extern void PickupMission_GetMissionDesc_m96AA377C275621BED1874802C9C25182194F684E (void);
// 0x000000F6 MissionBase/MissionType PickupMission::GetMissionType()
extern void PickupMission_GetMissionType_m76F4B1C6601853AA345CAF8923A97640F5478B4B (void);
// 0x000000F7 System.Void PickupMission::RunStart(TrackManager)
extern void PickupMission_RunStart_m9A9ADAC867E06E3D0436877BDD7B047E229AF4B7 (void);
// 0x000000F8 System.Void PickupMission::Update(TrackManager)
extern void PickupMission_Update_m2476885511EF40CC3748E0FE5700868FA5FBFAEC (void);
// 0x000000F9 System.Void PickupMission::.ctor()
extern void PickupMission__ctor_mF443916E1BC58541BC90DD6258270A0C2DE605C8 (void);
// 0x000000FA System.Void BarrierJumpMission::Created()
extern void BarrierJumpMission_Created_m7930DFC293EF928BD5459B98FFCB91FBD137B55D (void);
// 0x000000FB System.String BarrierJumpMission::GetMissionDesc()
extern void BarrierJumpMission_GetMissionDesc_m209ECA7AF0E6154090818D18CAAA7C96CE409BDF (void);
// 0x000000FC MissionBase/MissionType BarrierJumpMission::GetMissionType()
extern void BarrierJumpMission_GetMissionType_m54F3F27DFDF9C9919FDEC894590084B631F58390 (void);
// 0x000000FD System.Void BarrierJumpMission::RunStart(TrackManager)
extern void BarrierJumpMission_RunStart_mD0F821BB2C94158457674B55745EF865A3FC99F5 (void);
// 0x000000FE System.Void BarrierJumpMission::Update(TrackManager)
extern void BarrierJumpMission_Update_m6FE7FE1F1C66159D3383561871920DF5B74C6DFB (void);
// 0x000000FF System.Void BarrierJumpMission::.ctor()
extern void BarrierJumpMission__ctor_m9945081C8315076BD4223A4FF379F03A80390219 (void);
// 0x00000100 System.Void SlidingMission::Created()
extern void SlidingMission_Created_m9605AB9B00BA6BC9BE4C1D826BA6002E6B6A446B (void);
// 0x00000101 System.String SlidingMission::GetMissionDesc()
extern void SlidingMission_GetMissionDesc_m5785439A42FD3E65C7E088C2E4DBD68A7AC5A6D4 (void);
// 0x00000102 MissionBase/MissionType SlidingMission::GetMissionType()
extern void SlidingMission_GetMissionType_m3DDE610C348E4968ED5556A70F2082C16B858083 (void);
// 0x00000103 System.Void SlidingMission::RunStart(TrackManager)
extern void SlidingMission_RunStart_m9C16801BDC847044D3EE887764E136D52E3EE0B7 (void);
// 0x00000104 System.Void SlidingMission::Update(TrackManager)
extern void SlidingMission_Update_mF90EF7AB4E6C94760AFD5E76380BEC08D108BEBD (void);
// 0x00000105 System.Void SlidingMission::.ctor()
extern void SlidingMission__ctor_m1E662582E56C6CD05AB9A5FFA4122482F973B9CF (void);
// 0x00000106 System.Boolean MultiplierMission::HaveProgressBar()
extern void MultiplierMission_HaveProgressBar_m9503D2307AE8C4EBF1BFB329346C8EDAD04BE4F1 (void);
// 0x00000107 System.Void MultiplierMission::Created()
extern void MultiplierMission_Created_mBE6F09FE98BA195DCECD42A8B4FDB82AE3DC5A15 (void);
// 0x00000108 System.String MultiplierMission::GetMissionDesc()
extern void MultiplierMission_GetMissionDesc_m3888884232F13A4100BF94F844BED60BD7749D95 (void);
// 0x00000109 MissionBase/MissionType MultiplierMission::GetMissionType()
extern void MultiplierMission_GetMissionType_m99E08F1126B5A324509C99B377AF6F111B1195ED (void);
// 0x0000010A System.Void MultiplierMission::RunStart(TrackManager)
extern void MultiplierMission_RunStart_mC1A8DF7760502476F802E91AEE367D0EEE8BEF6F (void);
// 0x0000010B System.Void MultiplierMission::Update(TrackManager)
extern void MultiplierMission_Update_mC569E0AC1CDBF42397BDA80BCA3423FA084A0049 (void);
// 0x0000010C System.Void MultiplierMission::.ctor()
extern void MultiplierMission__ctor_m91BD2F77C5663CDE825CF5A106440B00DF36A835 (void);
// 0x0000010D System.Collections.IEnumerator AllLaneObstacle::Spawn(TrackSegment,System.Single)
extern void AllLaneObstacle_Spawn_mF8A58ED12FC222A217D34C18056902F48CB12682 (void);
// 0x0000010E System.Void AllLaneObstacle::.ctor()
extern void AllLaneObstacle__ctor_mA4D103F1C74740D57AABCC992DBE47A1E801FDF8 (void);
// 0x0000010F System.Boolean Missile::get_m_Ready()
extern void Missile_get_m_Ready_m69813E8B42B2AE6A82E2930164888C66A28319BB (void);
// 0x00000110 System.Void Missile::set_m_Ready(System.Boolean)
extern void Missile_set_m_Ready_m6E5187A046605EFBB373B5B562ABBB66F08F56A4 (void);
// 0x00000111 System.Void Missile::Awake()
extern void Missile_Awake_m5C1D3D74A04BC3A74B62462D5545379D8DC62848 (void);
// 0x00000112 System.Collections.IEnumerator Missile::Spawn(TrackSegment,System.Single)
extern void Missile_Spawn_m70568F98979CB913200F45539DA71B8E644178BC (void);
// 0x00000113 System.Void Missile::Setup()
extern void Missile_Setup_m67CAA0BADCC751EE930F7E40706B00FA276FFCB7 (void);
// 0x00000114 System.Void Missile::Impacted()
extern void Missile_Impacted_mBBC8F08D53E5EF1CB51A6F9179FEE32505A70D33 (void);
// 0x00000115 System.Void Missile::Update()
extern void Missile_Update_m6CCAF891ABE7958B7A9BF0C719942473B5F6F922 (void);
// 0x00000116 System.Void Missile::.ctor()
extern void Missile__ctor_m31640A83F8A494059B42203A6A8C07B8293C6209 (void);
// 0x00000117 System.Void Missile::.cctor()
extern void Missile__cctor_mF03D8D27B8958DC07639EDC66C5BF3260E0B4B5B (void);
// 0x00000118 System.Void Obstacle::Setup()
extern void Obstacle_Setup_m79556311153491AD55B972F38D498B226E6531F6 (void);
// 0x00000119 System.Collections.IEnumerator Obstacle::Spawn(TrackSegment,System.Single)
// 0x0000011A System.Void Obstacle::Impacted()
extern void Obstacle_Impacted_mCD5912D4F042D86063CA0BFFBC66D5F8195AD616 (void);
// 0x0000011B System.Void Obstacle::.ctor()
extern void Obstacle__ctor_m8DAB1C8ECB2328D09E320C484CC0C76D7C474C96 (void);
// 0x0000011C System.Collections.IEnumerator PatrollingObstacle::Spawn(TrackSegment,System.Single)
extern void PatrollingObstacle_Spawn_mEE60D0668D6019FEA3CCEE9C8A8EBD7F333D0DF1 (void);
// 0x0000011D System.Void PatrollingObstacle::Setup()
extern void PatrollingObstacle_Setup_m7B702390BC97E396A2145ADB2C17131A2BD623FC (void);
// 0x0000011E System.Void PatrollingObstacle::Impacted()
extern void PatrollingObstacle_Impacted_m153BE6D4EFF695CA3D852B829EAC74F8DDD7F86D (void);
// 0x0000011F System.Void PatrollingObstacle::Update()
extern void PatrollingObstacle_Update_m7E1FAEF845B83229C104DAD6B0926988B4AE7045 (void);
// 0x00000120 System.Void PatrollingObstacle::.ctor()
extern void PatrollingObstacle__ctor_m3EFA89EC71E52DC26C423AB24ED317E15C6EAF96 (void);
// 0x00000121 System.Void PatrollingObstacle::.cctor()
extern void PatrollingObstacle__cctor_m014569258BA0243E5A7F23B6B3916EDA5AA3EAFE (void);
// 0x00000122 System.Collections.IEnumerator SimpleBarricade::Spawn(TrackSegment,System.Single)
extern void SimpleBarricade_Spawn_m6BE7D289AB407ADB5BD4B5FEFD3BA5B6B969FED3 (void);
// 0x00000123 System.Void SimpleBarricade::.ctor()
extern void SimpleBarricade__ctor_m21B41405BBF2E02E3B7E7AB03C9E56F417E828AF (void);
// 0x00000124 System.Void OpenURL::OpenURLOnClick()
extern void OpenURL_OpenURLOnClick_mA48C19DB1E9EE99A1D06FD11ED985D613B804589 (void);
// 0x00000125 System.Void OpenURL::.ctor()
extern void OpenURL__ctor_mBD893164A0E0FE8CD0999BFF1AE11E07C5244D5D (void);
// 0x00000126 System.Int32 HighscoreEntry::CompareTo(HighscoreEntry)
extern void HighscoreEntry_CompareTo_mC9269DA9CFADA1A4989CEFC653F8B5B2D7ADF43B (void);
// 0x00000127 PlayerData PlayerData::get_instance()
extern void PlayerData_get_instance_m45C5BA3A1E5AD3B6C0641F9F37CF074EBB3A134E (void);
// 0x00000128 System.Void PlayerData::Consume(Consumable/ConsumableType)
extern void PlayerData_Consume_mF763A92F6789868A9DF6841140AB6703BB02F462 (void);
// 0x00000129 System.Void PlayerData::Add(Consumable/ConsumableType)
extern void PlayerData_Add_mD391CE971F57470E2193B901FC3ED909EB948745 (void);
// 0x0000012A System.Void PlayerData::AddCharacter(System.String)
extern void PlayerData_AddCharacter_mC8CA7E81CCD5A65A49A60A0821D55E2188E6C4A8 (void);
// 0x0000012B System.Void PlayerData::AddTheme(System.String)
extern void PlayerData_AddTheme_mF1B4C815537FA5847A089EB88962A37F91E0B9DC (void);
// 0x0000012C System.Void PlayerData::AddAccessory(System.String)
extern void PlayerData_AddAccessory_mBEEB16CA19888861452A431325871920E28B1A2C (void);
// 0x0000012D System.Void PlayerData::CheckMissionsCount()
extern void PlayerData_CheckMissionsCount_mF645C8980611A628F1DA09DCA2C957CD5AE8A67B (void);
// 0x0000012E System.Void PlayerData::AddMission()
extern void PlayerData_AddMission_m9518AD72ABC04500AF7A5D6EB40AAA88DC0887E8 (void);
// 0x0000012F System.Void PlayerData::StartRunMissions(TrackManager)
extern void PlayerData_StartRunMissions_m89058D12D3C02E9736D69AF23065F8BF2FD620C1 (void);
// 0x00000130 System.Void PlayerData::UpdateMissions(TrackManager)
extern void PlayerData_UpdateMissions_m13932B67E425A9B5047094F44E2FB5F4273896D9 (void);
// 0x00000131 System.Boolean PlayerData::AnyMissionComplete()
extern void PlayerData_AnyMissionComplete_mDB0DDE672CE3881426CCB5D61A625D1577152DCF (void);
// 0x00000132 System.Void PlayerData::ClaimMission(MissionBase)
extern void PlayerData_ClaimMission_m9A2B02F4554D5BCAEB2AF5405F984EA20E2A7D0F (void);
// 0x00000133 System.Int32 PlayerData::GetScorePlace(System.Int32)
extern void PlayerData_GetScorePlace_m46A68740A46CEF6910ABC7417F3A3CC5D5A69CF4 (void);
// 0x00000134 System.Void PlayerData::InsertScore(System.Int32,System.String)
extern void PlayerData_InsertScore_m9FE02042D5279778E64A0F6EA05F18EB1C749C0C (void);
// 0x00000135 System.Void PlayerData::Create()
extern void PlayerData_Create_m245A5D8AEB7E119AF5274EEC21782CE625756AA8 (void);
// 0x00000136 System.Void PlayerData::NewSave()
extern void PlayerData_NewSave_mE50C5BABBD0E75F3CE41BEAD5D226DB38BC300C9 (void);
// 0x00000137 System.Void PlayerData::Read()
extern void PlayerData_Read_mCECA18B90E6F4338609DFE30620D6A44A285DFB3 (void);
// 0x00000138 System.Void PlayerData::Save()
extern void PlayerData_Save_m9B08BB247C2438C0F56CEFB7D7326D573C12CF73 (void);
// 0x00000139 System.Void PlayerData::.ctor()
extern void PlayerData__ctor_mC2DA6B32832AB92B19E799148968590FF1F28C1B (void);
// 0x0000013A System.Void PlayerData::.cctor()
extern void PlayerData__cctor_m329CD40C61128DCA159D2AB563D0676DC3BFF0AF (void);
// 0x0000013B System.Void PlayfabLogin::Start()
extern void PlayfabLogin_Start_mAC1628871670BC543CDEF3DCE5386937DA853FF4 (void);
// 0x0000013C System.Void PlayfabLogin::OnClickSubmitToLogin()
extern void PlayfabLogin_OnClickSubmitToLogin_m86C71391E6894B2D6D72C77E4EC19AA547EDEC38 (void);
// 0x0000013D System.Void PlayfabLogin::OnLoginSuccess(PlayFab.ClientModels.LoginResult)
extern void PlayfabLogin_OnLoginSuccess_m7F1079FB561A7AE5371C3406F532EFA5A8130D90 (void);
// 0x0000013E System.Void PlayfabLogin::OnLoginFailure(PlayFab.PlayFabError)
extern void PlayfabLogin_OnLoginFailure_m96BACE6CE43F13C799B960BE6A80338585F15ECA (void);
// 0x0000013F System.Void PlayfabLogin::OnRegisterFailure(PlayFab.PlayFabError)
extern void PlayfabLogin_OnRegisterFailure_m555629DCDA16C20CDED453E1554979B0B27DCF53 (void);
// 0x00000140 System.Void PlayfabLogin::OnRegisterSuccess(PlayFab.ClientModels.RegisterPlayFabUserResult)
extern void PlayfabLogin_OnRegisterSuccess_mE2A369567529BAA5904029B3E6590B25A40D3522 (void);
// 0x00000141 System.Void PlayfabLogin::SetUIElementActive(System.Boolean)
extern void PlayfabLogin_SetUIElementActive_m6A04CB18FB2BCBDCA8C18FE097B5ACD0777DE9ED (void);
// 0x00000142 System.Void PlayfabLogin::GetEmployeeID(System.String)
extern void PlayfabLogin_GetEmployeeID_mAD560060AE0D849F5B321491EE8ADFC05DBD001D (void);
// 0x00000143 System.Void PlayfabLogin::GetUserName(System.String)
extern void PlayfabLogin_GetUserName_m695858A15D7086590851E14D4841E50E711F847C (void);
// 0x00000144 System.Void PlayfabLogin::GetEmailId(System.String)
extern void PlayfabLogin_GetEmailId_mCB87AA6497730E1A055BC5F7D2425A78C8AF1931 (void);
// 0x00000145 System.Boolean PlayfabLogin::IsValidEmail(System.String)
extern void PlayfabLogin_IsValidEmail_mF5899A11D6734C31407A1DF171F8A5E8FF5817C6 (void);
// 0x00000146 System.Void PlayfabLogin::.ctor()
extern void PlayfabLogin__ctor_mFE24C028A6B68E75DC2FDD38B7A906675E786A2F (void);
// 0x00000147 System.Void Pooler::.ctor(UnityEngine.GameObject,System.Int32)
extern void Pooler__ctor_m34F6F1D167C8B66F249899ACADABDBEAA26DBEAF (void);
// 0x00000148 UnityEngine.GameObject Pooler::Get()
extern void Pooler_Get_m542FB51BE61AC7C7684D9BAA91222F05C2CB566E (void);
// 0x00000149 UnityEngine.GameObject Pooler::Get(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Pooler_Get_m0BCAD76E4049F5F20212F123946CFCCFD9204647 (void);
// 0x0000014A System.Void Pooler::Free(UnityEngine.GameObject)
extern void Pooler_Free_m01C46CF77B7FC2B907AE2D1EF0099ABDCB85E114 (void);
// 0x0000014B System.Void AssignOutputChannel::Awake()
extern void AssignOutputChannel_Awake_mD00B5509094C41B035C39BD7EE2E0F3E4B2EE9D9 (void);
// 0x0000014C System.Void AssignOutputChannel::.ctor()
extern void AssignOutputChannel__ctor_m19DC585EB924A6FBB4CD72D17DA7AA09AA8F2340 (void);
// 0x0000014D System.Void CountdownSound::OnEnable()
extern void CountdownSound_OnEnable_mF91F24E5D8AC40DB891F99AE26ECE0CC940F3515 (void);
// 0x0000014E System.Void CountdownSound::Update()
extern void CountdownSound_Update_m17455E6FC537451B5B8DC7C7799C1D5FBF65C3D6 (void);
// 0x0000014F System.Void CountdownSound::.ctor()
extern void CountdownSound__ctor_m119DCF565320F4539D786BD74B16027C45744EF0 (void);
// 0x00000150 MusicPlayer MusicPlayer::get_instance()
extern void MusicPlayer_get_instance_mDA07A8FACAC1BDD554322313AC3E0247EFE65240 (void);
// 0x00000151 System.Void MusicPlayer::Awake()
extern void MusicPlayer_Awake_mA60904394C1725C664514BD13BA55F8CEC17D6F0 (void);
// 0x00000152 System.Void MusicPlayer::StartMusicSystem()
extern void MusicPlayer_StartMusicSystem_mA5DC573C43977BB44CFFF9CC0CEDB76E8B03E704 (void);
// 0x00000153 System.Void MusicPlayer::SetStem(System.Int32,UnityEngine.AudioClip)
extern void MusicPlayer_SetStem_m7E91B576F617E166A7450FE7F065654F6FE2267B (void);
// 0x00000154 UnityEngine.AudioClip MusicPlayer::GetStem(System.Int32)
extern void MusicPlayer_GetStem_m513DE7A4E2D85486D9FE05740A98B3888154D420 (void);
// 0x00000155 System.Collections.IEnumerator MusicPlayer::RestartAllStems()
extern void MusicPlayer_RestartAllStems_m1A1022BBB009E13E5D9C09DF69C5041BEFEA5BB0 (void);
// 0x00000156 System.Void MusicPlayer::UpdateVolumes(System.Single)
extern void MusicPlayer_UpdateVolumes_mB1895EE8AC52F4FA2B868C9EA466276F32B79330 (void);
// 0x00000157 System.Void MusicPlayer::.ctor()
extern void MusicPlayer__ctor_mA83600A144210E8586F4618ED75C614BFE6450FC (void);
// 0x00000158 System.Void ThemeData::.ctor()
extern void ThemeData__ctor_mE49C6FA2328C48AB61776D3E1B35F1393C4C906E (void);
// 0x00000159 System.Collections.Generic.Dictionary`2<System.String,ThemeData> ThemeDatabase::get_dictionnary()
extern void ThemeDatabase_get_dictionnary_mBEDCF05E4CAD3FAFFF4989EAB1B9917937370264 (void);
// 0x0000015A System.Boolean ThemeDatabase::get_loaded()
extern void ThemeDatabase_get_loaded_m65ACAFFCDE7BC7E25B024EAAB60C6D462E29C72D (void);
// 0x0000015B ThemeData ThemeDatabase::GetThemeData(System.String)
extern void ThemeDatabase_GetThemeData_m8076E358624D3AF2AD4EB88BA40D8AE0861DDC5A (void);
// 0x0000015C System.Collections.IEnumerator ThemeDatabase::LoadDatabase()
extern void ThemeDatabase_LoadDatabase_m4F775ED28F2F50411A0E7DFF276F651D46836758 (void);
// 0x0000015D System.Void ThemeDatabase::.ctor()
extern void ThemeDatabase__ctor_mCACE2AF9675ABCB2D9FF6B9A93F87720EC4D69FB (void);
// 0x0000015E System.Void ThemeDatabase::.cctor()
extern void ThemeDatabase__cctor_mEF60598DBD220D6EB515C6DBF738AB7D00A701D4 (void);
// 0x0000015F TrackManager TrackManager::get_instance()
extern void TrackManager_get_instance_m6BD53491F54FA37AAE9B7411F874507A5EDB0AD5 (void);
// 0x00000160 System.Int32 TrackManager::get_trackSeed()
extern void TrackManager_get_trackSeed_m12145862E2266002296D19E95B615EEDCA45E2D0 (void);
// 0x00000161 System.Void TrackManager::set_trackSeed(System.Int32)
extern void TrackManager_set_trackSeed_m1AABFE4B7C5E646734DB3ED54316AFE41C200603 (void);
// 0x00000162 System.Single TrackManager::get_timeToStart()
extern void TrackManager_get_timeToStart_m8C38CB9E7C3C4A7F7D5A55ECA11AE86039B28854 (void);
// 0x00000163 System.Int32 TrackManager::get_score()
extern void TrackManager_get_score_m72FD5C2C3BD1C6F66223A99B58FCB7092E3FAEBC (void);
// 0x00000164 System.Int32 TrackManager::get_multiplier()
extern void TrackManager_get_multiplier_m245DE2FFD49CC05AB7D7E3ACD5E7FE1782E89B28 (void);
// 0x00000165 System.Single TrackManager::get_currentSegmentDistance()
extern void TrackManager_get_currentSegmentDistance_mAE59532030395D90AD5AEDD384F0CBCF85BDBEF5 (void);
// 0x00000166 System.Single TrackManager::get_worldDistance()
extern void TrackManager_get_worldDistance_mDA0CF15EA5800D0FEB6FDDD083A410322B2BEE27 (void);
// 0x00000167 System.Single TrackManager::get_speed()
extern void TrackManager_get_speed_m5151D7EFA3BC17F6A6A7CBA82035CC544E7FA0FA (void);
// 0x00000168 System.Single TrackManager::get_speedRatio()
extern void TrackManager_get_speedRatio_mB815CE07EA95B3451B0C362E0612118726CC9812 (void);
// 0x00000169 System.Int32 TrackManager::get_currentZone()
extern void TrackManager_get_currentZone_mA6A8E73AF6212A1C0B2086F222B2192F51CB71F8 (void);
// 0x0000016A TrackSegment TrackManager::get_currentSegment()
extern void TrackManager_get_currentSegment_m98BA7068C3E00ED226663B959A8D96C3D042FDE4 (void);
// 0x0000016B System.Collections.Generic.List`1<TrackSegment> TrackManager::get_segments()
extern void TrackManager_get_segments_m01AC8381807DE8C1376DB9421ED015EC8B3FAFE7 (void);
// 0x0000016C ThemeData TrackManager::get_currentTheme()
extern void TrackManager_get_currentTheme_m02BA6A1DE64A6416240A8312B29DC6FB0F30EE2C (void);
// 0x0000016D System.Boolean TrackManager::get_isMoving()
extern void TrackManager_get_isMoving_mE6F3EAEE3A680EE3722D260BC115238B071A17D8 (void);
// 0x0000016E System.Boolean TrackManager::get_isRerun()
extern void TrackManager_get_isRerun_m418889725D618F8A41793C6D7AA8E309D4DA4A8A (void);
// 0x0000016F System.Void TrackManager::set_isRerun(System.Boolean)
extern void TrackManager_set_isRerun_m5B23536B5A807325092F18E480BD137E47BE687D (void);
// 0x00000170 System.Boolean TrackManager::get_isLoaded()
extern void TrackManager_get_isLoaded_mDAAB60A07D0AF02B2D53ED140AD71F69EF07A144 (void);
// 0x00000171 System.Void TrackManager::set_isLoaded(System.Boolean)
extern void TrackManager_set_isLoaded_m716AC7275D329E58751D1245126F5F718A1ECD19 (void);
// 0x00000172 System.Void TrackManager::Awake()
extern void TrackManager_Awake_m1EBF2B746A7AA6304C35D9E43FB4E458B242259E (void);
// 0x00000173 System.Void TrackManager::StartMove(System.Boolean)
extern void TrackManager_StartMove_mD127C009DBC45AE7AD17BC7245E776685C82270B (void);
// 0x00000174 System.Void TrackManager::StopMove()
extern void TrackManager_StopMove_m3C0652AC6AE61E1DCF99D2506A7E886DF57647A1 (void);
// 0x00000175 System.Collections.IEnumerator TrackManager::WaitToStart()
extern void TrackManager_WaitToStart_m1060A329DAF9089A4F26B2D71621128458A42BE2 (void);
// 0x00000176 System.Collections.IEnumerator TrackManager::Begin()
extern void TrackManager_Begin_m65D7D70E8B8B2AF72ACB9A02DFB5412654A397DF (void);
// 0x00000177 System.Void TrackManager::End()
extern void TrackManager_End_m10248F86DAE013A11F592C53E502B42014521F80 (void);
// 0x00000178 System.Void TrackManager::Update()
extern void TrackManager_Update_mDFDCCA9720CEF4EC8F8F62517EF86AAE256C8D0D (void);
// 0x00000179 System.Void TrackManager::PowerupSpawnUpdate()
extern void TrackManager_PowerupSpawnUpdate_m96F701A117BEB2E4B7CC530B4CE7672FCDE6984A (void);
// 0x0000017A System.Void TrackManager::ChangeZone()
extern void TrackManager_ChangeZone_m2403B572099305508A3232F4BF816716FA02F7F9 (void);
// 0x0000017B System.Collections.IEnumerator TrackManager::SpawnNewSegment()
extern void TrackManager_SpawnNewSegment_m1135BFF3841F4A1A83B479EF8775E3D358EBAD71 (void);
// 0x0000017C System.Void TrackManager::SpawnObstacle(TrackSegment)
extern void TrackManager_SpawnObstacle_m7BB0901D95E10848E16BC861C2E7D2B5A18BDD6B (void);
// 0x0000017D System.Collections.IEnumerator TrackManager::SpawnFromAssetReference(UnityEngine.AddressableAssets.AssetReference,TrackSegment,System.Int32)
extern void TrackManager_SpawnFromAssetReference_mD7A7661417A273DEF823C2B338BAB8EACF409C6D (void);
// 0x0000017E System.Collections.IEnumerator TrackManager::SpawnCoinAndPowerup(TrackSegment)
extern void TrackManager_SpawnCoinAndPowerup_m5081C041651AE4A620B03B956796D8685AD4687C (void);
// 0x0000017F System.Void TrackManager::AddScore(System.Int32)
extern void TrackManager_AddScore_m1FD70C0A4EFE3EFF001A27C90148A40160261503 (void);
// 0x00000180 System.Void TrackManager::.ctor()
extern void TrackManager__ctor_m88B01DCC563530070B26E8B4744D683634247BCA (void);
// 0x00000181 System.Void TrackManager::.cctor()
extern void TrackManager__cctor_m9822F392EF450D5785A73007EFDF21243AF2F9CC (void);
// 0x00000182 System.Single TrackSegment::get_worldLength()
extern void TrackSegment_get_worldLength_m589203BAE0A0D7F1E55F824626D902AA4C68D465 (void);
// 0x00000183 System.Void TrackSegment::OnEnable()
extern void TrackSegment_OnEnable_mC37DBD3D3D8FFCE6952141182A179207D7D18192 (void);
// 0x00000184 System.Void TrackSegment::GetPointAtInWorldUnit(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void TrackSegment_GetPointAtInWorldUnit_mAEB7476D616D234DD5D45A715D6C71DC6BC1D824 (void);
// 0x00000185 System.Void TrackSegment::GetPointAt(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void TrackSegment_GetPointAt_mB820F2F30C51193A7A1DCF689A209F01C87F5CB0 (void);
// 0x00000186 System.Void TrackSegment::UpdateWorldLength()
extern void TrackSegment_UpdateWorldLength_m2B16323F6BBC848DCC33186C63A6EEE06880BEFC (void);
// 0x00000187 System.Void TrackSegment::Cleanup()
extern void TrackSegment_Cleanup_mA626F785572815F5E054F65CA758BE1AB1FF3289 (void);
// 0x00000188 System.Void TrackSegment::.ctor()
extern void TrackSegment__ctor_m77AB5AFEC598D9311FB639AC2E27B9638618BB0E (void);
// 0x00000189 System.Void AdsForMission::OnEnable()
extern void AdsForMission_OnEnable_mACD73EB2DB1AE1BF593F3D5D6A787798EEAC2EB0 (void);
// 0x0000018A System.Void AdsForMission::ShowAds()
extern void AdsForMission_ShowAds_mD891DA3A3B99599F54D3CE68D580B2D5E79E08ED (void);
// 0x0000018B System.Void AdsForMission::AddNewMission()
extern void AdsForMission_AddNewMission_m1CB1ADAEC724473380AF24CA125D9FCE27F03173 (void);
// 0x0000018C System.Void AdsForMission::.ctor()
extern void AdsForMission__ctor_mE7BD24C15BD210DC663D80EF6A9F22F4D7ACAE11 (void);
// 0x0000018D System.Void HighscoreUI::.ctor()
extern void HighscoreUI__ctor_m92E286AC7802303DC3B98721B6A49291FC8A9DBF (void);
// 0x0000018E System.Void Leaderboard::Open()
extern void Leaderboard_Open_m620A82E8B80B7C2AF37F1F887F341B2CF4CACEC5 (void);
// 0x0000018F System.Void Leaderboard::Close()
extern void Leaderboard_Close_m21D21F0E8DCDFDD99A10FA901E5E8A14747F90F0 (void);
// 0x00000190 System.Void Leaderboard::Populate()
extern void Leaderboard_Populate_mF5BB2E2FE3458457E8C91D75CD9228EACFA3B087 (void);
// 0x00000191 System.Void Leaderboard::ToggleOpenGlobal()
extern void Leaderboard_ToggleOpenGlobal_m55DCDC6FDD477966C6108B7659613C98AEBB221A (void);
// 0x00000192 System.Void Leaderboard::PopulateGlobal(System.Collections.Generic.List`1<PlayFab.ClientModels.PlayerLeaderboardEntry>)
extern void Leaderboard_PopulateGlobal_mF2DCB155C51B5A5F3D190DF228401E04A5AF4FAC (void);
// 0x00000193 System.Void Leaderboard::.ctor()
extern void Leaderboard__ctor_mC72FE37B9E2BDBA75DDE02AFC34BC5BC06312835 (void);
// 0x00000194 System.Void Leaderboard::<ToggleOpenGlobal>b__10_0(PlayFab.ClientModels.GetLeaderboardResult)
extern void Leaderboard_U3CToggleOpenGlobalU3Eb__10_0_m58D1860E8B526AE4B49F21139C57BB0A5FE6A61D (void);
// 0x00000195 System.Void LicenceDisplayer::Start()
extern void LicenceDisplayer_Start_mCF803BDADB209F8B57DC2756792759617355DA1F (void);
// 0x00000196 System.Void LicenceDisplayer::Accepted()
extern void LicenceDisplayer_Accepted_m08D9D3011611814710D0F2EA143CDA1419A27ED9 (void);
// 0x00000197 System.Void LicenceDisplayer::Refuse()
extern void LicenceDisplayer_Refuse_mDD1F0CF87B86FB2121D27F1780BB283E9502AC5E (void);
// 0x00000198 System.Void LicenceDisplayer::Close()
extern void LicenceDisplayer_Close_m2C81D5CCE03DF167C35165DDA2319D67706E1EEF (void);
// 0x00000199 System.Void LicenceDisplayer::.ctor()
extern void LicenceDisplayer__ctor_m4AAD7015AFCFE6E922976EF3CB3FD72A93D61E92 (void);
// 0x0000019A System.Void MainMenu::LoadScene(System.String)
extern void MainMenu_LoadScene_mAEBC3638BA84257120F4BF0F70082C4EBD9AE43F (void);
// 0x0000019B System.Void MainMenu::.ctor()
extern void MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D (void);
// 0x0000019C System.Void MissionEntry::FillWithMission(MissionBase,MissionUI)
extern void MissionEntry_FillWithMission_mCD69561490BD716986B049675637430A6F255CD8 (void);
// 0x0000019D System.Void MissionEntry::.ctor()
extern void MissionEntry__ctor_m5FAF3C11563D80BE15E0C8C257BDF4E5AEF67860 (void);
// 0x0000019E System.Collections.IEnumerator MissionUI::Open()
extern void MissionUI_Open_mC4CDD6194BB5A512C4494663E89FE6F27DC27B2E (void);
// 0x0000019F System.Void MissionUI::CallOpen()
extern void MissionUI_CallOpen_m1E038EBC531C0D11B1AB026F19E1CB1F4BAE6EE8 (void);
// 0x000001A0 System.Void MissionUI::Claim(MissionBase)
extern void MissionUI_Claim_m2613F64C41AEB8B3111C19D33FD1A317D4ACE2FB (void);
// 0x000001A1 System.Void MissionUI::Close()
extern void MissionUI_Close_mBEB1556AA5E5386B708A6D9F360A511588FFD35F (void);
// 0x000001A2 System.Void MissionUI::.ctor()
extern void MissionUI__ctor_mF4A74576D2E28DB29C3D7641398CDF211A163853 (void);
// 0x000001A3 System.Void PowerupIcon::Start()
extern void PowerupIcon_Start_m23C422E43C12F4BC8EF9D5E16855B7FF33302537 (void);
// 0x000001A4 System.Void PowerupIcon::Update()
extern void PowerupIcon_Update_mE33C602BEDEFE693C71D2CF0353571BFA8F60BCC (void);
// 0x000001A5 System.Void PowerupIcon::.ctor()
extern void PowerupIcon__ctor_mBD31AE76C34839B0001AC1BBF73D70765A127803 (void);
// 0x000001A6 System.Void SetTransperancy::Start()
extern void SetTransperancy_Start_m0FAFE8C7F83D89FEE9F2D3A1F3FCFA116071C511 (void);
// 0x000001A7 System.Void SetTransperancy::.ctor()
extern void SetTransperancy__ctor_mE019C9579F12034B3BF6F2F64E6056F89E950E10 (void);
// 0x000001A8 System.Void DataDeleteConfirmation::Open(LoadoutState)
extern void DataDeleteConfirmation_Open_m4615E1F5AD22EC1131AF78166E3C9C19BD873C7D (void);
// 0x000001A9 System.Void DataDeleteConfirmation::Close()
extern void DataDeleteConfirmation_Close_mAE265625F2B10A2BEE6733E9FAAC4D498D3B9806 (void);
// 0x000001AA System.Void DataDeleteConfirmation::Confirm()
extern void DataDeleteConfirmation_Confirm_m8187E6A3F69B5FCC3F6CD593A239675E64455767 (void);
// 0x000001AB System.Void DataDeleteConfirmation::Deny()
extern void DataDeleteConfirmation_Deny_m1225892E41646760B925EC68F223F6B854853F05 (void);
// 0x000001AC System.Void DataDeleteConfirmation::.ctor()
extern void DataDeleteConfirmation__ctor_mB1BA72EE7D9895DD7F0781A97F8537146CCD6DBB (void);
// 0x000001AD System.Void SettingPopup::Open()
extern void SettingPopup_Open_mF1E67EBA5FBEA2BC03D823235F94652A8D3BC9ED (void);
// 0x000001AE System.Void SettingPopup::Close()
extern void SettingPopup_Close_m73A914BE59B77805775EE3516C8CC2FA84B55E6E (void);
// 0x000001AF System.Void SettingPopup::UpdateUI()
extern void SettingPopup_UpdateUI_m1FF3F9232429701F3378E89AD3A480A3369BACDB (void);
// 0x000001B0 System.Void SettingPopup::DeleteData()
extern void SettingPopup_DeleteData_m10C549B0B69EC6C5975B69C3815FAF998DDD01F1 (void);
// 0x000001B1 System.Void SettingPopup::MasterVolumeChangeValue(System.Single)
extern void SettingPopup_MasterVolumeChangeValue_m267ACE5CE6F6F8EEB701405ABFD9CD6B575ECF8D (void);
// 0x000001B2 System.Void SettingPopup::MusicVolumeChangeValue(System.Single)
extern void SettingPopup_MusicVolumeChangeValue_mAFC6866A77F9775F025A18E5D9D06C53637E63C2 (void);
// 0x000001B3 System.Void SettingPopup::MasterSFXVolumeChangeValue(System.Single)
extern void SettingPopup_MasterSFXVolumeChangeValue_mFFFA1B6C73C9E860F69415865E9195B03EBEEA4D (void);
// 0x000001B4 System.Void SettingPopup::.ctor()
extern void SettingPopup__ctor_m4E2685EEDCED39A2E705C890AA232579F7234D98 (void);
// 0x000001B5 System.Void IAPHandler::.ctor()
extern void IAPHandler__ctor_m68F5FCAA077F82208B3B370A4D1C9405B167F42D (void);
// 0x000001B6 System.Void ShopAccessoriesList::Populate()
extern void ShopAccessoriesList_Populate_m58D2943CDCF2DD974C74000A6DB729A67AA68856 (void);
// 0x000001B7 System.Void ShopAccessoriesList::LoadedCharacter(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>,System.Int32)
extern void ShopAccessoriesList_LoadedCharacter_m21304A3591B14D735985AEEBF2A9F91DFBB932DD (void);
// 0x000001B8 System.Void ShopAccessoriesList::LoadedAccessory(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>,System.Int32,System.Int32)
extern void ShopAccessoriesList_LoadedAccessory_mFCC68D388874EE4EBC7AA388129D3DDB9806499C (void);
// 0x000001B9 System.Void ShopAccessoriesList::RefreshButton(ShopItemListItem,CharacterAccessories,System.String)
extern void ShopAccessoriesList_RefreshButton_m38FF72F06217FB7A7F5360AA11478DBD8024587C (void);
// 0x000001BA System.Void ShopAccessoriesList::Buy(System.String,System.Int32,System.Int32)
extern void ShopAccessoriesList_Buy_m5A3FE08FBEAC22CBCAA3849FC9D0746EB6D81BE9 (void);
// 0x000001BB System.Void ShopAccessoriesList::.ctor()
extern void ShopAccessoriesList__ctor_mC8E347E1A2F05F468BC4731B66652F955EDA2EB6 (void);
// 0x000001BC System.Void ShopAccessoriesList::<Populate>b__2_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>)
extern void ShopAccessoriesList_U3CPopulateU3Eb__2_0_mC229749232D40E11CC2DB0E032E9FB417AE22513 (void);
// 0x000001BD System.Void ShopCharacterList::Populate()
extern void ShopCharacterList_Populate_m55D7E3316D5436A2CA7F02699C132093891C72A7 (void);
// 0x000001BE System.Void ShopCharacterList::RefreshButton(ShopItemListItem,Character)
extern void ShopCharacterList_RefreshButton_mB7704588B7960D246BBE195547FA1839E9401FF1 (void);
// 0x000001BF System.Void ShopCharacterList::Buy(Character)
extern void ShopCharacterList_Buy_m543DD69B66C5AD72E92A1E7580D9F2E4B4D64B43 (void);
// 0x000001C0 System.Void ShopCharacterList::.ctor()
extern void ShopCharacterList__ctor_mA3F452AE15FF912D21D3165D9D4F9DEF4B96B05B (void);
// 0x000001C1 System.Void ShopItemList::Populate()
extern void ShopItemList_Populate_mE7B37EC24953B9A89B7EC0A0C7D51049CB83371E (void);
// 0x000001C2 System.Void ShopItemList::RefreshButton(ShopItemListItem,Consumable)
extern void ShopItemList_RefreshButton_m6EDC5E69391E19D56C83369127A002EB455B4FDC (void);
// 0x000001C3 System.Void ShopItemList::Buy(Consumable)
extern void ShopItemList_Buy_m63A7CD63583D89F487E4DAB4E26A68A0D5641925 (void);
// 0x000001C4 System.Void ShopItemList::.ctor()
extern void ShopItemList__ctor_m38D8145086F26E030AEC5704D4A52AF6A3044E9E (void);
// 0x000001C5 System.Void ShopItemList::.cctor()
extern void ShopItemList__cctor_mEE1B88F89581B80D4F3820475952752FE7FD5CA9 (void);
// 0x000001C6 System.Void ShopItemListItem::.ctor()
extern void ShopItemListItem__ctor_m63A48D5B1DEDADDF59DB56745437DB0096D2372C (void);
// 0x000001C7 System.Void ShopList::Open()
extern void ShopList_Open_m452847222266C0528884BCE719BBED56CD97397A (void);
// 0x000001C8 System.Void ShopList::Close()
extern void ShopList_Close_mCEDB5D14E1E7B8B23132AE827C72E62D01C048C7 (void);
// 0x000001C9 System.Void ShopList::Refresh()
extern void ShopList_Refresh_mDC2607B49700080C3DB0B0F6C871045857BB58E4 (void);
// 0x000001CA System.Void ShopList::Populate()
// 0x000001CB System.Void ShopList::.ctor()
extern void ShopList__ctor_mAFF39E29493A6B2E2AC2E604D26C90FB8C62C421 (void);
// 0x000001CC System.Void ShopThemeList::Populate()
extern void ShopThemeList_Populate_m63C1A43FB483682A66A3FDC01CE26E1640F8AD44 (void);
// 0x000001CD System.Void ShopThemeList::RefreshButton(ShopItemListItem,ThemeData)
extern void ShopThemeList_RefreshButton_m9D6975394E75C69C1D91E990BED4E6DDD72CA5DE (void);
// 0x000001CE System.Void ShopThemeList::Buy(ThemeData)
extern void ShopThemeList_Buy_mD03BDCB23F4149DCC44CE3C85E60387DF6D9A9FE (void);
// 0x000001CF System.Void ShopThemeList::.ctor()
extern void ShopThemeList__ctor_mFAC743F06E76927120DABFAB29530194BFC4CBB8 (void);
// 0x000001D0 System.Void ShopUI::Start()
extern void ShopUI_Start_mDC3CA06BF11EE6E1CE82E5E7828EA991FC15FA84 (void);
// 0x000001D1 System.Void ShopUI::Update()
extern void ShopUI_Update_mCCB762FC4CF3B164B950205AD94C694D0EE8127A (void);
// 0x000001D2 System.Void ShopUI::OpenItemList()
extern void ShopUI_OpenItemList_m1A477C2FBFA235B1E24EA05453F3F58745A0FCB7 (void);
// 0x000001D3 System.Void ShopUI::OpenCharacterList()
extern void ShopUI_OpenCharacterList_mB0AC77D1E084126CE21DB248F5E761B8638A7919 (void);
// 0x000001D4 System.Void ShopUI::OpenThemeList()
extern void ShopUI_OpenThemeList_m29E8848B71A36A53BD3B4B7347340FDD1D8FA8B5 (void);
// 0x000001D5 System.Void ShopUI::OpenAccessoriesList()
extern void ShopUI_OpenAccessoriesList_m41970EA2577A7FF423E018B53F56B15B4455A174 (void);
// 0x000001D6 System.Void ShopUI::LoadScene(System.String)
extern void ShopUI_LoadScene_mDF69EF93C0742043FE40DD6C0433E281770BF5C4 (void);
// 0x000001D7 System.Void ShopUI::CloseScene()
extern void ShopUI_CloseScene_m0F12FDFC964F452CCD191A76F35FA01AE48E648C (void);
// 0x000001D8 System.Void ShopUI::CheatCoin()
extern void ShopUI_CheatCoin_m0BDF7F64CAA040E8B0C24C08F35CA90A86543171 (void);
// 0x000001D9 System.Void ShopUI::.ctor()
extern void ShopUI__ctor_mC1E462240D1846F12A57D4437A01EC35F829384D (void);
// 0x000001DA System.Void StartButton::StartGame()
extern void StartButton_StartGame_mEBB90D61E4FDE822965384E452ABCC9DEE03C9F0 (void);
// 0x000001DB System.Void StartButton::LoadLevel(System.String)
extern void StartButton_LoadLevel_mEF5C5953D118A26CA36612300A120B8BB85F8460 (void);
// 0x000001DC System.Collections.IEnumerator StartButton::LoadAynchronously(System.String)
extern void StartButton_LoadAynchronously_m11F5469D84EDFD0C642BCEBE2FE2F5B5E8C856AF (void);
// 0x000001DD System.Void StartButton::.ctor()
extern void StartButton__ctor_m6DAB2093EB360099E446CA6B746441526C0C7BD9 (void);
// 0x000001DE System.Void WorldCurver::OnEnable()
extern void WorldCurver_OnEnable_m278BF41D4EAC8D8436A193225D4172258F5126CE (void);
// 0x000001DF System.Void WorldCurver::Update()
extern void WorldCurver_Update_m755CA3EB5BA64CCDA9A3437C0A41A38B37F08F57 (void);
// 0x000001E0 System.Void WorldCurver::.ctor()
extern void WorldCurver__ctor_m8D5FACE2C24857A2415B9863DDCEA6E75C5A5CD5 (void);
// 0x000001E1 System.Void CharacterCollider/<InvincibleTimer>d__34::.ctor(System.Int32)
extern void U3CInvincibleTimerU3Ed__34__ctor_m8D5D9230E71664C4F1EB7EEE9B2622E51E2544F1 (void);
// 0x000001E2 System.Void CharacterCollider/<InvincibleTimer>d__34::System.IDisposable.Dispose()
extern void U3CInvincibleTimerU3Ed__34_System_IDisposable_Dispose_mFADBB6E9EA2EF95698FB131E7484A606EDB1BB86 (void);
// 0x000001E3 System.Boolean CharacterCollider/<InvincibleTimer>d__34::MoveNext()
extern void U3CInvincibleTimerU3Ed__34_MoveNext_m43BEFB396EC5822C5A904437DE21482356A255CA (void);
// 0x000001E4 System.Object CharacterCollider/<InvincibleTimer>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInvincibleTimerU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAAB39BBB0D3248E1E0AD17A9A10388FAB3D2FAD9 (void);
// 0x000001E5 System.Void CharacterCollider/<InvincibleTimer>d__34::System.Collections.IEnumerator.Reset()
extern void U3CInvincibleTimerU3Ed__34_System_Collections_IEnumerator_Reset_m7FBA665B91C3D5A88CC60FAC78EAACBBEE61BD70 (void);
// 0x000001E6 System.Object CharacterCollider/<InvincibleTimer>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CInvincibleTimerU3Ed__34_System_Collections_IEnumerator_get_Current_mA433FEE9A1864721F75346E1B31F96B527DAAD16 (void);
// 0x000001E7 System.Void CharacterDatabase/<>c::.cctor()
extern void U3CU3Ec__cctor_m4D74E3CACAC8A07E1EC4B4934B381775E2F921D7 (void);
// 0x000001E8 System.Void CharacterDatabase/<>c::.ctor()
extern void U3CU3Ec__ctor_m56C186B238D4220BB24A536F275BCB1D78939F9F (void);
// 0x000001E9 System.Void CharacterDatabase/<>c::<LoadDatabase>b__7_0(UnityEngine.GameObject)
extern void U3CU3Ec_U3CLoadDatabaseU3Eb__7_0_mF442652EB0DA4EDCC3315FE59F2CECE432D1221E (void);
// 0x000001EA System.Void CharacterDatabase/<LoadDatabase>d__7::.ctor(System.Int32)
extern void U3CLoadDatabaseU3Ed__7__ctor_mC7CDF58302D7B98E7EB22648FAAC2FC4CDE030AD (void);
// 0x000001EB System.Void CharacterDatabase/<LoadDatabase>d__7::System.IDisposable.Dispose()
extern void U3CLoadDatabaseU3Ed__7_System_IDisposable_Dispose_m524840546B4184DAFB9505BD13E5FD60665F9838 (void);
// 0x000001EC System.Boolean CharacterDatabase/<LoadDatabase>d__7::MoveNext()
extern void U3CLoadDatabaseU3Ed__7_MoveNext_m49D1AA9A9936BE1977B771D4718C8D2863B56309 (void);
// 0x000001ED System.Object CharacterDatabase/<LoadDatabase>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadDatabaseU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE917FBCAB2F35B1DC80946FC5FB59DBE5AE0569E (void);
// 0x000001EE System.Void CharacterDatabase/<LoadDatabase>d__7::System.Collections.IEnumerator.Reset()
extern void U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_Reset_m342146DBC2100769356803F0C7ECA06B004D92A0 (void);
// 0x000001EF System.Object CharacterDatabase/<LoadDatabase>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_get_Current_mC1057775743DC5D326EABE7E00BB68E33A430501 (void);
// 0x000001F0 System.Void Consumable/<Started>d__19::.ctor(System.Int32)
extern void U3CStartedU3Ed__19__ctor_mEF42D919739E2A363EF86BC71297EA731B63585B (void);
// 0x000001F1 System.Void Consumable/<Started>d__19::System.IDisposable.Dispose()
extern void U3CStartedU3Ed__19_System_IDisposable_Dispose_mF4BB5903FDC60C398A1FBCF51F917FAEA78FF26C (void);
// 0x000001F2 System.Boolean Consumable/<Started>d__19::MoveNext()
extern void U3CStartedU3Ed__19_MoveNext_m21B971FA84662D30283C6190DBA896F48F47B71C (void);
// 0x000001F3 System.Object Consumable/<Started>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartedU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33766A3905DCDAC9404C0B993C384237C3281A12 (void);
// 0x000001F4 System.Void Consumable/<Started>d__19::System.Collections.IEnumerator.Reset()
extern void U3CStartedU3Ed__19_System_Collections_IEnumerator_Reset_m2D42E18F669C84497F3B35E7633558E42D7ACBCB (void);
// 0x000001F5 System.Object Consumable/<Started>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CStartedU3Ed__19_System_Collections_IEnumerator_get_Current_mCA28224BB04B07A6F7D136CB5986F7534F9F5270 (void);
// 0x000001F6 System.Void Consumable/<TimedRelease>d__20::.ctor(System.Int32)
extern void U3CTimedReleaseU3Ed__20__ctor_m1F59A14FF7931B4EC7E797731AAFDF24606E8E67 (void);
// 0x000001F7 System.Void Consumable/<TimedRelease>d__20::System.IDisposable.Dispose()
extern void U3CTimedReleaseU3Ed__20_System_IDisposable_Dispose_m75E07D756D7211E8CDD6C1F10E9C823DBF382C20 (void);
// 0x000001F8 System.Boolean Consumable/<TimedRelease>d__20::MoveNext()
extern void U3CTimedReleaseU3Ed__20_MoveNext_m70CDF86A57C887FBE22DFA0D946D4ED7B1CAEFBD (void);
// 0x000001F9 System.Object Consumable/<TimedRelease>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTimedReleaseU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99CAAC6AC60A1518342D89A757586121A19C3830 (void);
// 0x000001FA System.Void Consumable/<TimedRelease>d__20::System.Collections.IEnumerator.Reset()
extern void U3CTimedReleaseU3Ed__20_System_Collections_IEnumerator_Reset_m5B0ECA825566D38638A998C40993C898EFAE37C5 (void);
// 0x000001FB System.Object Consumable/<TimedRelease>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CTimedReleaseU3Ed__20_System_Collections_IEnumerator_get_Current_m3D3236E4C725D7CA6CDD038A022E03D87FFCA625 (void);
// 0x000001FC System.Void ExtraLife/<Started>d__7::.ctor(System.Int32)
extern void U3CStartedU3Ed__7__ctor_mFCE003E9A8AF54E840895F8FCAFAC3A0569FE580 (void);
// 0x000001FD System.Void ExtraLife/<Started>d__7::System.IDisposable.Dispose()
extern void U3CStartedU3Ed__7_System_IDisposable_Dispose_mA224EA171282339A50662E2C06D20A13FCB8EEB8 (void);
// 0x000001FE System.Boolean ExtraLife/<Started>d__7::MoveNext()
extern void U3CStartedU3Ed__7_MoveNext_m2378DC2A5BD1BE1368E33AC22E51C671791EE49E (void);
// 0x000001FF System.Object ExtraLife/<Started>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartedU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18DA349166726DEDC3B52E76B536293ED38E3212 (void);
// 0x00000200 System.Void ExtraLife/<Started>d__7::System.Collections.IEnumerator.Reset()
extern void U3CStartedU3Ed__7_System_Collections_IEnumerator_Reset_mE90475D8C756EFFBFB2E739183FBEE5AC4808111 (void);
// 0x00000201 System.Object ExtraLife/<Started>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CStartedU3Ed__7_System_Collections_IEnumerator_get_Current_m8E89D7830FEAE1FE57FAEFF1258ADB4C136A4764 (void);
// 0x00000202 System.Void Invincibility/<Started>d__5::.ctor(System.Int32)
extern void U3CStartedU3Ed__5__ctor_mFC33F89F8403094329D9B7C5C6DAED5681940F85 (void);
// 0x00000203 System.Void Invincibility/<Started>d__5::System.IDisposable.Dispose()
extern void U3CStartedU3Ed__5_System_IDisposable_Dispose_mA9C7C9600CD551FCC31F1521AE88E77526F2BC72 (void);
// 0x00000204 System.Boolean Invincibility/<Started>d__5::MoveNext()
extern void U3CStartedU3Ed__5_MoveNext_m030400298DDD245791EEE3AC6399B9602F11857D (void);
// 0x00000205 System.Object Invincibility/<Started>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartedU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86FEB0F1746E16D84EC94159AA702C98ACF8F616 (void);
// 0x00000206 System.Void Invincibility/<Started>d__5::System.Collections.IEnumerator.Reset()
extern void U3CStartedU3Ed__5_System_Collections_IEnumerator_Reset_mC5A0AA68ACC13F2582248F429790F7C0E41F3368 (void);
// 0x00000207 System.Object Invincibility/<Started>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CStartedU3Ed__5_System_Collections_IEnumerator_get_Current_m277672D5AC175E17D0776DAB55F13F08455AE19E (void);
// 0x00000208 System.Void Score2Multiplier/<Started>d__4::.ctor(System.Int32)
extern void U3CStartedU3Ed__4__ctor_mA80CFD61C96A66A860A2B9D78DEC1BBF6FDBF35E (void);
// 0x00000209 System.Void Score2Multiplier/<Started>d__4::System.IDisposable.Dispose()
extern void U3CStartedU3Ed__4_System_IDisposable_Dispose_m6B1A0F182B3D09478075E36E4A3E202456072418 (void);
// 0x0000020A System.Boolean Score2Multiplier/<Started>d__4::MoveNext()
extern void U3CStartedU3Ed__4_MoveNext_mFC15AEE93FC22539BFAB9F1F260FE92F182DF8E5 (void);
// 0x0000020B System.Object Score2Multiplier/<Started>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartedU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6290AA1767879126BFB0451959FC7BE49CC0BCD5 (void);
// 0x0000020C System.Void Score2Multiplier/<Started>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartedU3Ed__4_System_Collections_IEnumerator_Reset_mE096394F9BA4B1CEA2ACBF2D68F6C9C97FC27BE5 (void);
// 0x0000020D System.Object Score2Multiplier/<Started>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartedU3Ed__4_System_Collections_IEnumerator_get_Current_mF1A628526F6459B7999E32BA331364F8FB1600FB (void);
// 0x0000020E System.Void GameState/<WaitForGameOver>d__47::.ctor(System.Int32)
extern void U3CWaitForGameOverU3Ed__47__ctor_m7E4A5B3AF180B587FB987475A2748DCB7C1D2221 (void);
// 0x0000020F System.Void GameState/<WaitForGameOver>d__47::System.IDisposable.Dispose()
extern void U3CWaitForGameOverU3Ed__47_System_IDisposable_Dispose_m20E1D453FE2C885C6F7E3AAD6FCC88CE46FAE8E0 (void);
// 0x00000210 System.Boolean GameState/<WaitForGameOver>d__47::MoveNext()
extern void U3CWaitForGameOverU3Ed__47_MoveNext_mE86614FE278591285CC15D188EA97CC56566194E (void);
// 0x00000211 System.Object GameState/<WaitForGameOver>d__47::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForGameOverU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD83D4561CDD0BDE00A1B289E7028A850BA4B319E (void);
// 0x00000212 System.Void GameState/<WaitForGameOver>d__47::System.Collections.IEnumerator.Reset()
extern void U3CWaitForGameOverU3Ed__47_System_Collections_IEnumerator_Reset_m92879365CFED874A18B5C23FF6FC651280ECAA03 (void);
// 0x00000213 System.Object GameState/<WaitForGameOver>d__47::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForGameOverU3Ed__47_System_Collections_IEnumerator_get_Current_mD3F369BFA826A6D7DC4DCAD5F60CD6BA4B8B9B20 (void);
// 0x00000214 System.Void LoadoutState/<PopulateTheme>d__41::.ctor(System.Int32)
extern void U3CPopulateThemeU3Ed__41__ctor_m3A2B0FD2BD3D5B9799640E208D97DCB2BCA8B9D7 (void);
// 0x00000215 System.Void LoadoutState/<PopulateTheme>d__41::System.IDisposable.Dispose()
extern void U3CPopulateThemeU3Ed__41_System_IDisposable_Dispose_m9A46096A483CD89012F436A25C5CAC19694F49B9 (void);
// 0x00000216 System.Boolean LoadoutState/<PopulateTheme>d__41::MoveNext()
extern void U3CPopulateThemeU3Ed__41_MoveNext_mD312B1F408B2FB5E174A9C120381DF6A2C877A5D (void);
// 0x00000217 System.Object LoadoutState/<PopulateTheme>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPopulateThemeU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB13834AC2759BF9DA4A1BF432A3BEB6C9B4E82F (void);
// 0x00000218 System.Void LoadoutState/<PopulateTheme>d__41::System.Collections.IEnumerator.Reset()
extern void U3CPopulateThemeU3Ed__41_System_Collections_IEnumerator_Reset_m13C7C872722C3D5F0E8E69CBD6A7E7BB0C25AF5A (void);
// 0x00000219 System.Object LoadoutState/<PopulateTheme>d__41::System.Collections.IEnumerator.get_Current()
extern void U3CPopulateThemeU3Ed__41_System_Collections_IEnumerator_get_Current_mFE6C00DBD349F62EA1C27BC3E2AD348824A469A0 (void);
// 0x0000021A System.Void LoadoutState/<PopulateCharacters>d__42::.ctor(System.Int32)
extern void U3CPopulateCharactersU3Ed__42__ctor_m1B9F99AA08D99489D2AED553F7213B94D3201241 (void);
// 0x0000021B System.Void LoadoutState/<PopulateCharacters>d__42::System.IDisposable.Dispose()
extern void U3CPopulateCharactersU3Ed__42_System_IDisposable_Dispose_m640DE281406BD6BE814BEBE00DF21CA316B4DB3E (void);
// 0x0000021C System.Boolean LoadoutState/<PopulateCharacters>d__42::MoveNext()
extern void U3CPopulateCharactersU3Ed__42_MoveNext_m3A2B50E78F7B1ACADBB406A37BC2A007A1D1EC09 (void);
// 0x0000021D System.Object LoadoutState/<PopulateCharacters>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPopulateCharactersU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A6117B73BA8DA99A0AC10CFAD8498A1D9057462 (void);
// 0x0000021E System.Void LoadoutState/<PopulateCharacters>d__42::System.Collections.IEnumerator.Reset()
extern void U3CPopulateCharactersU3Ed__42_System_Collections_IEnumerator_Reset_mA612E390F3E7900D2046E6208FCA39AC74BB259E (void);
// 0x0000021F System.Object LoadoutState/<PopulateCharacters>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CPopulateCharactersU3Ed__42_System_Collections_IEnumerator_get_Current_m67D7CE7A285EA9E670FC0AC6D6C8DA1E0A73E605 (void);
// 0x00000220 System.Void PlayFabAuthService/DisplayAuthenticationEvent::.ctor(System.Object,System.IntPtr)
extern void DisplayAuthenticationEvent__ctor_mE3C811C7EBEA64D5F889266F5987023918C5A5A4 (void);
// 0x00000221 System.Void PlayFabAuthService/DisplayAuthenticationEvent::Invoke()
extern void DisplayAuthenticationEvent_Invoke_m72C354D6C505453849EA05D96504B7A8AEC68168 (void);
// 0x00000222 System.IAsyncResult PlayFabAuthService/DisplayAuthenticationEvent::BeginInvoke(System.AsyncCallback,System.Object)
extern void DisplayAuthenticationEvent_BeginInvoke_m59AE8B7416765C9A1EAF992EEF65189215E3D42E (void);
// 0x00000223 System.Void PlayFabAuthService/DisplayAuthenticationEvent::EndInvoke(System.IAsyncResult)
extern void DisplayAuthenticationEvent_EndInvoke_m67B82D0905E1485FEEFE25161C1C09B39D028CE7 (void);
// 0x00000224 System.Void PlayFabAuthService/LoginSuccessEvent::.ctor(System.Object,System.IntPtr)
extern void LoginSuccessEvent__ctor_m21A670CD32D8D4ED39A2CAE8A32EEC1ED0B7C44D (void);
// 0x00000225 System.Void PlayFabAuthService/LoginSuccessEvent::Invoke(PlayFab.ClientModels.LoginResult)
extern void LoginSuccessEvent_Invoke_mB83F0BEF94DD4823390DCDCE16735DF3B65058EB (void);
// 0x00000226 System.IAsyncResult PlayFabAuthService/LoginSuccessEvent::BeginInvoke(PlayFab.ClientModels.LoginResult,System.AsyncCallback,System.Object)
extern void LoginSuccessEvent_BeginInvoke_mE2B67040B3876A0B31C48F67AAEDB6549E4722D2 (void);
// 0x00000227 System.Void PlayFabAuthService/LoginSuccessEvent::EndInvoke(System.IAsyncResult)
extern void LoginSuccessEvent_EndInvoke_m313788711791482E732576CC2675CB8FFD63C0A5 (void);
// 0x00000228 System.Void PlayFabAuthService/PlayFabErrorEvent::.ctor(System.Object,System.IntPtr)
extern void PlayFabErrorEvent__ctor_m67E83E7FF37DF0F7974600227E2D8668301C68E8 (void);
// 0x00000229 System.Void PlayFabAuthService/PlayFabErrorEvent::Invoke(PlayFab.PlayFabError)
extern void PlayFabErrorEvent_Invoke_mF4D6C92B9E540096382D7E629570490F1A946BCC (void);
// 0x0000022A System.IAsyncResult PlayFabAuthService/PlayFabErrorEvent::BeginInvoke(PlayFab.PlayFabError,System.AsyncCallback,System.Object)
extern void PlayFabErrorEvent_BeginInvoke_mA1428CC7E4BDDCC334B3AE364D8F2EE0325616C4 (void);
// 0x0000022B System.Void PlayFabAuthService/PlayFabErrorEvent::EndInvoke(System.IAsyncResult)
extern void PlayFabErrorEvent_EndInvoke_mE7779FB47173939D3B762CDB691C422E59953E6F (void);
// 0x0000022C System.Void PlayFabAuthService/<>c::.cctor()
extern void U3CU3Ec__cctor_mC950B412080FFA1441508F56BAA6E23B5D8F02E2 (void);
// 0x0000022D System.Void PlayFabAuthService/<>c::.ctor()
extern void U3CU3Ec__ctor_mA0D5D782BB8934706458F9EFCBA88A731E076CB7 (void);
// 0x0000022E System.Void PlayFabAuthService/<>c::<AuthenticateEmailPassword>b__43_0(PlayFab.ClientModels.LoginResult)
extern void U3CU3Ec_U3CAuthenticateEmailPasswordU3Eb__43_0_m3DA40107E96FFD438F998C784C199B37DBD6D1B5 (void);
// 0x0000022F System.Void PlayFabAuthService/<>c::<AuthenticateEmailPassword>b__43_1(PlayFab.PlayFabError)
extern void U3CU3Ec_U3CAuthenticateEmailPasswordU3Eb__43_1_m01C6285C48F2EDCD6D18C5FB5D19E126FF50239A (void);
// 0x00000230 System.Void PlayFabAuthService/<>c::<AuthenticateEmailPassword>b__43_3(PlayFab.PlayFabError)
extern void U3CU3Ec_U3CAuthenticateEmailPasswordU3Eb__43_3_m6EE735320F6C13B7098EFA51B6FA403C0B421E20 (void);
// 0x00000231 System.Void PlayFabAuthService/<>c::<AddAccountAndPassword>b__44_2(PlayFab.PlayFabError)
extern void U3CU3Ec_U3CAddAccountAndPasswordU3Eb__44_2_m82BCBEC1F04F922A2822158F1B5D4DBD61116BD9 (void);
// 0x00000232 System.Void PlayFabAuthService/<>c::<UnlinkSilentAuth>b__49_0(PlayFab.ClientModels.LoginResult)
extern void U3CU3Ec_U3CUnlinkSilentAuthU3Eb__49_0_mAC5F5587E8220CB9783E3ADB851011D78E4341DA (void);
// 0x00000233 System.Void PlayFabAuthService/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_m8822C9CA81364C07314FF210C805364499ED62C1 (void);
// 0x00000234 System.Void PlayFabAuthService/<>c__DisplayClass44_0::<AddAccountAndPassword>b__1(PlayFab.ClientModels.AddUsernamePasswordResult)
extern void U3CU3Ec__DisplayClass44_0_U3CAddAccountAndPasswordU3Eb__1_mB573E339766BC51F4BCD662932E944B3078925F3 (void);
// 0x00000235 System.Void PlayFabAuthService/<>c__DisplayClass48_0::.ctor()
extern void U3CU3Ec__DisplayClass48_0__ctor_mA56381A054EE29D689BDA977A2DC3B1865B8CDE8 (void);
// 0x00000236 System.Void PlayFabAuthService/<>c__DisplayClass48_0::<SilentlyAuthenticate>b__0(PlayFab.ClientModels.LoginResult)
extern void U3CU3Ec__DisplayClass48_0_U3CSilentlyAuthenticateU3Eb__0_m6281BD7FDE47E2C3F9F8DFB89F36F0639A41BFFA (void);
// 0x00000237 System.Void PlayFabAuthService/<>c__DisplayClass48_0::<SilentlyAuthenticate>b__1(PlayFab.PlayFabError)
extern void U3CU3Ec__DisplayClass48_0_U3CSilentlyAuthenticateU3Eb__1_m7C24F6B2C4D0EDD13ADDFF770B579E27DA8665F3 (void);
// 0x00000238 System.Void AllLaneObstacle/<Spawn>d__0::.ctor(System.Int32)
extern void U3CSpawnU3Ed__0__ctor_m3C12D98CEE9084D1104D3AC19E4DB24F14049F5A (void);
// 0x00000239 System.Void AllLaneObstacle/<Spawn>d__0::System.IDisposable.Dispose()
extern void U3CSpawnU3Ed__0_System_IDisposable_Dispose_m7D173ED73B59596164E3F93AE0B2E81854F5FBD7 (void);
// 0x0000023A System.Boolean AllLaneObstacle/<Spawn>d__0::MoveNext()
extern void U3CSpawnU3Ed__0_MoveNext_m2FB9A92FC985CC7A38574673B1B69FDC493AAE13 (void);
// 0x0000023B System.Object AllLaneObstacle/<Spawn>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACD1991881DDF201112A85D5567ED427D37AFBF2 (void);
// 0x0000023C System.Void AllLaneObstacle/<Spawn>d__0::System.Collections.IEnumerator.Reset()
extern void U3CSpawnU3Ed__0_System_Collections_IEnumerator_Reset_m34B9730116CAEC77500E14E35394DEF368BB6B1A (void);
// 0x0000023D System.Object AllLaneObstacle/<Spawn>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnU3Ed__0_System_Collections_IEnumerator_get_Current_m036F1B038E494E9989082CB4147D70A2657D4FCC (void);
// 0x0000023E System.Void Missile/<Spawn>d__15::.ctor(System.Int32)
extern void U3CSpawnU3Ed__15__ctor_m8B6555F0727267379D91497503A517A14A935396 (void);
// 0x0000023F System.Void Missile/<Spawn>d__15::System.IDisposable.Dispose()
extern void U3CSpawnU3Ed__15_System_IDisposable_Dispose_m90909754072CA037BC9341EBD318B487BDD7F075 (void);
// 0x00000240 System.Boolean Missile/<Spawn>d__15::MoveNext()
extern void U3CSpawnU3Ed__15_MoveNext_m6E58BA2883A14563FA3D03A648694163DD6EDAFD (void);
// 0x00000241 System.Object Missile/<Spawn>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEB6FB3ACA001BB1AEE3F356E8AD6242451DFD8F9 (void);
// 0x00000242 System.Void Missile/<Spawn>d__15::System.Collections.IEnumerator.Reset()
extern void U3CSpawnU3Ed__15_System_Collections_IEnumerator_Reset_m199A0A54883A81C9D0395A881146EEE0A7200F2F (void);
// 0x00000243 System.Object Missile/<Spawn>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnU3Ed__15_System_Collections_IEnumerator_get_Current_m26E129A15722B49953843E3835FEFAAB4D29DEF3 (void);
// 0x00000244 System.Void PatrollingObstacle/<Spawn>d__13::.ctor(System.Int32)
extern void U3CSpawnU3Ed__13__ctor_mC6D26905014FBE3C109464828A997016EA7CA70D (void);
// 0x00000245 System.Void PatrollingObstacle/<Spawn>d__13::System.IDisposable.Dispose()
extern void U3CSpawnU3Ed__13_System_IDisposable_Dispose_m1DE303152F8C88637C1358770031EF4826E9D74F (void);
// 0x00000246 System.Boolean PatrollingObstacle/<Spawn>d__13::MoveNext()
extern void U3CSpawnU3Ed__13_MoveNext_m62453ABA4265E79EA3810E0CA0EEB6D0A5586913 (void);
// 0x00000247 System.Object PatrollingObstacle/<Spawn>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mECAEF1106886D61093B7A6AD5CCA830D8B8655EA (void);
// 0x00000248 System.Void PatrollingObstacle/<Spawn>d__13::System.Collections.IEnumerator.Reset()
extern void U3CSpawnU3Ed__13_System_Collections_IEnumerator_Reset_mF723AA7FEE5B81A88F02DD1007343BF6D75AC0CB (void);
// 0x00000249 System.Object PatrollingObstacle/<Spawn>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnU3Ed__13_System_Collections_IEnumerator_get_Current_m447841FDAE3F33326E46F2162D478C731F852B1E (void);
// 0x0000024A System.Void SimpleBarricade/<Spawn>d__4::.ctor(System.Int32)
extern void U3CSpawnU3Ed__4__ctor_m9C4DBA85AE3E02D720C6EAECA102687880E5AA9B (void);
// 0x0000024B System.Void SimpleBarricade/<Spawn>d__4::System.IDisposable.Dispose()
extern void U3CSpawnU3Ed__4_System_IDisposable_Dispose_mF994CA2235DD73A8DC295DDEF1A35EC4710CCA91 (void);
// 0x0000024C System.Boolean SimpleBarricade/<Spawn>d__4::MoveNext()
extern void U3CSpawnU3Ed__4_MoveNext_mFC40B23E2AF9FE77CA04A60F45C4AA62D4BF0190 (void);
// 0x0000024D System.Object SimpleBarricade/<Spawn>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5EFD4F9B9380DC9E690993D411F37E0164D5B31F (void);
// 0x0000024E System.Void SimpleBarricade/<Spawn>d__4::System.Collections.IEnumerator.Reset()
extern void U3CSpawnU3Ed__4_System_Collections_IEnumerator_Reset_m6C86770A44474E359195E84BD69DC698AC95B7D9 (void);
// 0x0000024F System.Object SimpleBarricade/<Spawn>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnU3Ed__4_System_Collections_IEnumerator_get_Current_m760CF586EB4B879F894A560953BE3F08CE62A641 (void);
// 0x00000250 System.Void PlayerData/<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_m54D040DF2C9C1F4592476A00AE5124279AD67B83 (void);
// 0x00000251 System.Void PlayerData/<>c__DisplayClass40_0::<Read>b__0(System.Object)
extern void U3CU3Ec__DisplayClass40_0_U3CReadU3Eb__0_mD7721E04436E917C68064A05FDFFA8B0A3A2D0EE (void);
// 0x00000252 System.Void PlayerData/<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_mF5D6E4AE83DBCEBCAE8BC7F8DCEA60A7429D2D6F (void);
// 0x00000253 System.Void PlayerData/<>c__DisplayClass41_0::<Save>b__0(MissionBase)
extern void U3CU3Ec__DisplayClass41_0_U3CSaveU3Eb__0_mFA6B6CD79896061D92A00D1B366C07F82056E0FC (void);
// 0x00000254 System.Void PlayerData/<>c::.cctor()
extern void U3CU3Ec__cctor_mE0CD3F1D9EDE692DAD6F8803F57B3CF7A3A98275 (void);
// 0x00000255 System.Void PlayerData/<>c::.ctor()
extern void U3CU3Ec__ctor_m0CBA45D831CFD996668E9C322AFA8139D5557A35 (void);
// 0x00000256 System.Void PlayerData/<>c::<Save>b__41_1(PlayFab.PlayFabError)
extern void U3CU3Ec_U3CSaveU3Eb__41_1_m4594C3DDAEB569B550368E7C40022ABD28F127BF (void);
// 0x00000257 System.Void MusicPlayer/Stem::.ctor()
extern void Stem__ctor_mF215D694E31F6E2732FCCBD26582EE43C861868F (void);
// 0x00000258 System.Void MusicPlayer/<RestartAllStems>d__11::.ctor(System.Int32)
extern void U3CRestartAllStemsU3Ed__11__ctor_m3F0E2479A180FB3E399F7C5CF8B71BB15DB66295 (void);
// 0x00000259 System.Void MusicPlayer/<RestartAllStems>d__11::System.IDisposable.Dispose()
extern void U3CRestartAllStemsU3Ed__11_System_IDisposable_Dispose_m6FEDE0E02A173C3646F429439283D391270E4B65 (void);
// 0x0000025A System.Boolean MusicPlayer/<RestartAllStems>d__11::MoveNext()
extern void U3CRestartAllStemsU3Ed__11_MoveNext_m6DF8F0E74AD18C8366DA6CF4B9119E116BF17014 (void);
// 0x0000025B System.Object MusicPlayer/<RestartAllStems>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRestartAllStemsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50388D880EE7347B31D3B2E552933F6F11D19777 (void);
// 0x0000025C System.Void MusicPlayer/<RestartAllStems>d__11::System.Collections.IEnumerator.Reset()
extern void U3CRestartAllStemsU3Ed__11_System_Collections_IEnumerator_Reset_m79446E16DFB91914B0E767F34FA23F8C74CDC0DD (void);
// 0x0000025D System.Object MusicPlayer/<RestartAllStems>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CRestartAllStemsU3Ed__11_System_Collections_IEnumerator_get_Current_mA2D3590BA4EA306951314F5F05C52A31C3A8EA1F (void);
// 0x0000025E System.Void ThemeDatabase/<>c::.cctor()
extern void U3CU3Ec__cctor_mCCFC65D9286B2E0AF39DBC71206A65923CC5F6D0 (void);
// 0x0000025F System.Void ThemeDatabase/<>c::.ctor()
extern void U3CU3Ec__ctor_m93A659164B518E5301095FCC23FB3268E4818C82 (void);
// 0x00000260 System.Void ThemeDatabase/<>c::<LoadDatabase>b__7_0(ThemeData)
extern void U3CU3Ec_U3CLoadDatabaseU3Eb__7_0_m62EE6DB7BEFDEB0E3DE6FA3019E625977825BEB8 (void);
// 0x00000261 System.Void ThemeDatabase/<LoadDatabase>d__7::.ctor(System.Int32)
extern void U3CLoadDatabaseU3Ed__7__ctor_mD72AAC64AA5475D057F79B59997BF597BA5DF227 (void);
// 0x00000262 System.Void ThemeDatabase/<LoadDatabase>d__7::System.IDisposable.Dispose()
extern void U3CLoadDatabaseU3Ed__7_System_IDisposable_Dispose_mE6CA3308E82B8A002709AC9486224960458478FB (void);
// 0x00000263 System.Boolean ThemeDatabase/<LoadDatabase>d__7::MoveNext()
extern void U3CLoadDatabaseU3Ed__7_MoveNext_mDAFAB7B45306E59E8795D78B1CA14E18EDE1AA30 (void);
// 0x00000264 System.Object ThemeDatabase/<LoadDatabase>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadDatabaseU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48B117B0CD0DDB72351D96529C5A0B78E7866475 (void);
// 0x00000265 System.Void ThemeDatabase/<LoadDatabase>d__7::System.Collections.IEnumerator.Reset()
extern void U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_Reset_mC6A56CA03B68D5D83BDF69977C914EC5F710C119 (void);
// 0x00000266 System.Object ThemeDatabase/<LoadDatabase>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_get_Current_mDA20F872592A1AFEC3753B785647F92C12CDE482 (void);
// 0x00000267 System.Void TrackManager/MultiplierModifier::.ctor(System.Object,System.IntPtr)
extern void MultiplierModifier__ctor_mC2A9965E5B6239CB5518850CF1FE7BD9964E6442 (void);
// 0x00000268 System.Int32 TrackManager/MultiplierModifier::Invoke(System.Int32)
extern void MultiplierModifier_Invoke_m142E206E1CE972E6281BEFF3BB8540C91D24297D (void);
// 0x00000269 System.IAsyncResult TrackManager/MultiplierModifier::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void MultiplierModifier_BeginInvoke_m0FBD37607A355FE47037B32494061C72CF9A710E (void);
// 0x0000026A System.Int32 TrackManager/MultiplierModifier::EndInvoke(System.IAsyncResult)
extern void MultiplierModifier_EndInvoke_m650AD2D50FAA43B4D1336AA5D55B33DCFF1DD8CD (void);
// 0x0000026B System.Void TrackManager/<WaitToStart>d__84::.ctor(System.Int32)
extern void U3CWaitToStartU3Ed__84__ctor_m3D4B56311A798974B7C2A8E3D20E1A8306DAAF2F (void);
// 0x0000026C System.Void TrackManager/<WaitToStart>d__84::System.IDisposable.Dispose()
extern void U3CWaitToStartU3Ed__84_System_IDisposable_Dispose_mA4BA8648817A41A3ED3B3CD9F1E3CEEAAD368241 (void);
// 0x0000026D System.Boolean TrackManager/<WaitToStart>d__84::MoveNext()
extern void U3CWaitToStartU3Ed__84_MoveNext_m5B30D083902B234CEFBAB0224F5386BB0D23B00B (void);
// 0x0000026E System.Object TrackManager/<WaitToStart>d__84::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitToStartU3Ed__84_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85238BFAE3597C9261B286DEAB50F3C929290B3B (void);
// 0x0000026F System.Void TrackManager/<WaitToStart>d__84::System.Collections.IEnumerator.Reset()
extern void U3CWaitToStartU3Ed__84_System_Collections_IEnumerator_Reset_m9384C8A83680182D7B3994AF38C77A948111E2B8 (void);
// 0x00000270 System.Object TrackManager/<WaitToStart>d__84::System.Collections.IEnumerator.get_Current()
extern void U3CWaitToStartU3Ed__84_System_Collections_IEnumerator_get_Current_mE5C8B43BE08F6CFEE838E38084B2ED4832206958 (void);
// 0x00000271 System.Void TrackManager/<Begin>d__85::.ctor(System.Int32)
extern void U3CBeginU3Ed__85__ctor_mAB58B7584FAD5B4633771C03EA4FFB71685A423E (void);
// 0x00000272 System.Void TrackManager/<Begin>d__85::System.IDisposable.Dispose()
extern void U3CBeginU3Ed__85_System_IDisposable_Dispose_mD03DCDA26327337D788708EEBD8EBD64A7B241A0 (void);
// 0x00000273 System.Boolean TrackManager/<Begin>d__85::MoveNext()
extern void U3CBeginU3Ed__85_MoveNext_m79F1010515478A29052BE3337F8D65152AC59E0C (void);
// 0x00000274 System.Object TrackManager/<Begin>d__85::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBeginU3Ed__85_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B38DEBEF616B175340DB85F7FA19B5A9E091E8F (void);
// 0x00000275 System.Void TrackManager/<Begin>d__85::System.Collections.IEnumerator.Reset()
extern void U3CBeginU3Ed__85_System_Collections_IEnumerator_Reset_m4795B9DAC83F02487C6DDCAD0AB9251E36630D12 (void);
// 0x00000276 System.Object TrackManager/<Begin>d__85::System.Collections.IEnumerator.get_Current()
extern void U3CBeginU3Ed__85_System_Collections_IEnumerator_get_Current_m14BA90226D7380E67EB837C5A862853928767C31 (void);
// 0x00000277 System.Void TrackManager/<SpawnNewSegment>d__93::.ctor(System.Int32)
extern void U3CSpawnNewSegmentU3Ed__93__ctor_mC3946A80D2D4BB2E06F0A60CE828B9473567175F (void);
// 0x00000278 System.Void TrackManager/<SpawnNewSegment>d__93::System.IDisposable.Dispose()
extern void U3CSpawnNewSegmentU3Ed__93_System_IDisposable_Dispose_mAE57C25E300472D1292F1AF22404843AF455CDC0 (void);
// 0x00000279 System.Boolean TrackManager/<SpawnNewSegment>d__93::MoveNext()
extern void U3CSpawnNewSegmentU3Ed__93_MoveNext_mC2149ED255A04730C3273F120252DF80752AC40A (void);
// 0x0000027A System.Object TrackManager/<SpawnNewSegment>d__93::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnNewSegmentU3Ed__93_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3F18702C42FFE48A79ABD648500AD6DB065AD63B (void);
// 0x0000027B System.Void TrackManager/<SpawnNewSegment>d__93::System.Collections.IEnumerator.Reset()
extern void U3CSpawnNewSegmentU3Ed__93_System_Collections_IEnumerator_Reset_mD50E95DA7F2E82EDD8149A3E491986A675D1802A (void);
// 0x0000027C System.Object TrackManager/<SpawnNewSegment>d__93::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnNewSegmentU3Ed__93_System_Collections_IEnumerator_get_Current_m188CB9FEF972ECC0872CFF268175B3DFAB15C427 (void);
// 0x0000027D System.Void TrackManager/<SpawnFromAssetReference>d__95::.ctor(System.Int32)
extern void U3CSpawnFromAssetReferenceU3Ed__95__ctor_m1C51A408EB6060A7133D3180C664171034AD8EC0 (void);
// 0x0000027E System.Void TrackManager/<SpawnFromAssetReference>d__95::System.IDisposable.Dispose()
extern void U3CSpawnFromAssetReferenceU3Ed__95_System_IDisposable_Dispose_m27A19B08B4619C2D7B4DFAA3FD804C5622C1A55A (void);
// 0x0000027F System.Boolean TrackManager/<SpawnFromAssetReference>d__95::MoveNext()
extern void U3CSpawnFromAssetReferenceU3Ed__95_MoveNext_mD7A3C955C10F3FB61ED4A52D4E43261709BCCCE2 (void);
// 0x00000280 System.Object TrackManager/<SpawnFromAssetReference>d__95::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnFromAssetReferenceU3Ed__95_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A1C8209672A1660F8736CB5C54B72A727BF2217 (void);
// 0x00000281 System.Void TrackManager/<SpawnFromAssetReference>d__95::System.Collections.IEnumerator.Reset()
extern void U3CSpawnFromAssetReferenceU3Ed__95_System_Collections_IEnumerator_Reset_mF48630E55DA0DB51C12C225529808B68EFA8EB4F (void);
// 0x00000282 System.Object TrackManager/<SpawnFromAssetReference>d__95::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnFromAssetReferenceU3Ed__95_System_Collections_IEnumerator_get_Current_m8F62B25EAA98A90D4BF4C29D773B99F262E649A2 (void);
// 0x00000283 System.Void TrackManager/<SpawnCoinAndPowerup>d__96::.ctor(System.Int32)
extern void U3CSpawnCoinAndPowerupU3Ed__96__ctor_m6919C148252339ED3E1494B186A1BED7A984C7FE (void);
// 0x00000284 System.Void TrackManager/<SpawnCoinAndPowerup>d__96::System.IDisposable.Dispose()
extern void U3CSpawnCoinAndPowerupU3Ed__96_System_IDisposable_Dispose_m87B6CB6FF3D58454AC6347C9A812361448BCBC27 (void);
// 0x00000285 System.Boolean TrackManager/<SpawnCoinAndPowerup>d__96::MoveNext()
extern void U3CSpawnCoinAndPowerupU3Ed__96_MoveNext_m71C0FD407CF0082BA5073EF7AEFA7F9C28F24117 (void);
// 0x00000286 System.Object TrackManager/<SpawnCoinAndPowerup>d__96::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnCoinAndPowerupU3Ed__96_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5A5E99D6802DA7A1700EEC345C4AB9394D9B6A76 (void);
// 0x00000287 System.Void TrackManager/<SpawnCoinAndPowerup>d__96::System.Collections.IEnumerator.Reset()
extern void U3CSpawnCoinAndPowerupU3Ed__96_System_Collections_IEnumerator_Reset_mCD39A066C923D8FE2C3DF6DA90383819ACD15967 (void);
// 0x00000288 System.Object TrackManager/<SpawnCoinAndPowerup>d__96::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnCoinAndPowerupU3Ed__96_System_Collections_IEnumerator_get_Current_m31B94A4C2C7A97578617509C9D5EDB57B9F69C84 (void);
// 0x00000289 System.Void Leaderboard/<>c::.cctor()
extern void U3CU3Ec__cctor_mBC82474B30E70C08AE70F5F08AFB90D32E8432B5 (void);
// 0x0000028A System.Void Leaderboard/<>c::.ctor()
extern void U3CU3Ec__ctor_m5BCF54269757977342F977D81BE710097ABF3835 (void);
// 0x0000028B System.Void Leaderboard/<>c::<ToggleOpenGlobal>b__10_1(PlayFab.PlayFabError)
extern void U3CU3Ec_U3CToggleOpenGlobalU3Eb__10_1_mB254B4DF2B3869BF477D02D2D2031437DA551D86 (void);
// 0x0000028C System.Void MissionEntry/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m05555B132C79BB709680B66BCA0A6F6C3A7BFCFE (void);
// 0x0000028D System.Void MissionEntry/<>c__DisplayClass7_0::<FillWithMission>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CFillWithMissionU3Eb__0_mC4D2BD24F7DD4637B62F01DBF4DE5ADDBDB8901E (void);
// 0x0000028E System.Void MissionUI/<Open>d__3::.ctor(System.Int32)
extern void U3COpenU3Ed__3__ctor_m8AC977975366A5A4ED5DCAB21A1D37D1B95EB264 (void);
// 0x0000028F System.Void MissionUI/<Open>d__3::System.IDisposable.Dispose()
extern void U3COpenU3Ed__3_System_IDisposable_Dispose_m5F0713DDBBA80E241AE5A97F9DEAC6AE128AF282 (void);
// 0x00000290 System.Boolean MissionUI/<Open>d__3::MoveNext()
extern void U3COpenU3Ed__3_MoveNext_mA0B5267ED2F901B552003D82F52EB52C45E2ABC4 (void);
// 0x00000291 System.Object MissionUI/<Open>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COpenU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F4DB819EEEDD4C7D847BE29D85CA72CFB80E644 (void);
// 0x00000292 System.Void MissionUI/<Open>d__3::System.Collections.IEnumerator.Reset()
extern void U3COpenU3Ed__3_System_Collections_IEnumerator_Reset_m6D1892BA8644ABC732A24538492BCDB875868DB7 (void);
// 0x00000293 System.Object MissionUI/<Open>d__3::System.Collections.IEnumerator.get_Current()
extern void U3COpenU3Ed__3_System_Collections_IEnumerator_get_Current_m365CCE1036E1123F495161146CC9A9C1811E761E (void);
// 0x00000294 System.Void ShopAccessoriesList/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m133663033521ACF2B05795B3D1500462E4A1A171 (void);
// 0x00000295 System.Void ShopAccessoriesList/<>c__DisplayClass3_0::<LoadedCharacter>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>)
extern void U3CU3Ec__DisplayClass3_0_U3CLoadedCharacterU3Eb__0_m6C4190FF9A08D5739D136EB186BE5D521DBE21E8 (void);
// 0x00000296 System.Void ShopAccessoriesList/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mBCCDBD57B794712410F8CFA0E221A3F5BCE95B27 (void);
// 0x00000297 System.Void ShopAccessoriesList/<>c__DisplayClass4_0::<LoadedAccessory>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>)
extern void U3CU3Ec__DisplayClass4_0_U3CLoadedAccessoryU3Eb__0_m792BE0AD145BE93015E671F8403DEE3A97533455 (void);
// 0x00000298 System.Void ShopAccessoriesList/<>c__DisplayClass4_0::<LoadedAccessory>b__1(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>)
extern void U3CU3Ec__DisplayClass4_0_U3CLoadedAccessoryU3Eb__1_mAE4993F243D54606CFD81EC649FF9D28973BB43E (void);
// 0x00000299 System.Void ShopAccessoriesList/<>c__DisplayClass4_1::.ctor()
extern void U3CU3Ec__DisplayClass4_1__ctor_m238FDECC7B3A4EBC30DD7CFB572F9BAC2055B541 (void);
// 0x0000029A System.Void ShopAccessoriesList/<>c__DisplayClass4_1::<LoadedAccessory>b__2()
extern void U3CU3Ec__DisplayClass4_1_U3CLoadedAccessoryU3Eb__2_mBBDDFE3E60237F318764681E78477A4DF0B59FC5 (void);
// 0x0000029B System.Void ShopAccessoriesList/<>c__DisplayClass4_1::<LoadedAccessory>b__3()
extern void U3CU3Ec__DisplayClass4_1_U3CLoadedAccessoryU3Eb__3_mE262E46DF06A5A42228C952873A883927641358F (void);
// 0x0000029C System.Void ShopCharacterList/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m1C1E71C2DF6ACD2838C4837D1AE9E95A345739F3 (void);
// 0x0000029D System.Void ShopCharacterList/<>c__DisplayClass0_0::<Populate>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>)
extern void U3CU3Ec__DisplayClass0_0_U3CPopulateU3Eb__0_mABDB22D3482EF4F518205B3DAE814E8543A32F76 (void);
// 0x0000029E System.Void ShopCharacterList/<>c__DisplayClass0_0::<Populate>b__1()
extern void U3CU3Ec__DisplayClass0_0_U3CPopulateU3Eb__1_m5D2617F6DCD098784C233765ECD9C464F5F93387 (void);
// 0x0000029F System.Void ShopCharacterList/<>c__DisplayClass0_1::.ctor()
extern void U3CU3Ec__DisplayClass0_1__ctor_m1CB636D7C24DAFCBA0AFF03619A3977F681ACA6C (void);
// 0x000002A0 System.Void ShopCharacterList/<>c__DisplayClass0_1::<Populate>b__2()
extern void U3CU3Ec__DisplayClass0_1_U3CPopulateU3Eb__2_m0F8D346E63094845548D190C437E19219A394EFC (void);
// 0x000002A1 System.Void ShopItemList/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m1C5F66B53AA34D72838A070FA5B87E02BF1A09AB (void);
// 0x000002A2 System.Void ShopItemList/<>c__DisplayClass1_0::<Populate>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>)
extern void U3CU3Ec__DisplayClass1_0_U3CPopulateU3Eb__0_m0A519017DE6FB73F1ADD1BD360DEDF1DC99DC255 (void);
// 0x000002A3 System.Void ShopItemList/<>c__DisplayClass1_0::<Populate>b__1()
extern void U3CU3Ec__DisplayClass1_0_U3CPopulateU3Eb__1_m4FD0B918A1984D030D47CD2FE1E0053A4008A0DA (void);
// 0x000002A4 System.Void ShopItemList/<>c__DisplayClass1_1::.ctor()
extern void U3CU3Ec__DisplayClass1_1__ctor_m53BAE85CD75D3D00856358AA1FA4B85532E31274 (void);
// 0x000002A5 System.Void ShopItemList/<>c__DisplayClass1_1::<Populate>b__2()
extern void U3CU3Ec__DisplayClass1_1_U3CPopulateU3Eb__2_m7DD837D84E621BE1D55F9E4831229FD8A2A66B37 (void);
// 0x000002A6 System.Void ShopList/RefreshCallback::.ctor(System.Object,System.IntPtr)
extern void RefreshCallback__ctor_mDDC2F95DC59D10F38717870030EEB4A7C6ED3492 (void);
// 0x000002A7 System.Void ShopList/RefreshCallback::Invoke()
extern void RefreshCallback_Invoke_mEDAB6D9D59E5E555B75CF56A26F16CAB2DC9F30E (void);
// 0x000002A8 System.IAsyncResult ShopList/RefreshCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void RefreshCallback_BeginInvoke_m841EB1E14E62E07B70EDE9B36D364D8483BCF22C (void);
// 0x000002A9 System.Void ShopList/RefreshCallback::EndInvoke(System.IAsyncResult)
extern void RefreshCallback_EndInvoke_mEB4AA4EBC12515BA2D1DE1D720CB7B03CC99719E (void);
// 0x000002AA System.Void ShopThemeList/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mD1DC0E5473A0A3A3FC4DDC4F7D0CE75676C0CB72 (void);
// 0x000002AB System.Void ShopThemeList/<>c__DisplayClass0_0::<Populate>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>)
extern void U3CU3Ec__DisplayClass0_0_U3CPopulateU3Eb__0_m02FEB2A833AA2C0330231E305FB66E1876592CE9 (void);
// 0x000002AC System.Void ShopThemeList/<>c__DisplayClass0_0::<Populate>b__1()
extern void U3CU3Ec__DisplayClass0_0_U3CPopulateU3Eb__1_mDF92A259600292B30791994D12560529A9C5FEED (void);
// 0x000002AD System.Void ShopThemeList/<>c__DisplayClass0_1::.ctor()
extern void U3CU3Ec__DisplayClass0_1__ctor_mAF1B9B8D6C26975483ED899B0F2177E6FB941048 (void);
// 0x000002AE System.Void ShopThemeList/<>c__DisplayClass0_1::<Populate>b__2()
extern void U3CU3Ec__DisplayClass0_1_U3CPopulateU3Eb__2_m4D155329EAA45674CF33E32044288800E9917CB5 (void);
// 0x000002AF System.Void StartButton/<LoadAynchronously>d__4::.ctor(System.Int32)
extern void U3CLoadAynchronouslyU3Ed__4__ctor_m2C80714EAC810CC120B102341525405571574FFF (void);
// 0x000002B0 System.Void StartButton/<LoadAynchronously>d__4::System.IDisposable.Dispose()
extern void U3CLoadAynchronouslyU3Ed__4_System_IDisposable_Dispose_m42FC8E64428FC380A50BBA8DCE4D27BA73AB23D6 (void);
// 0x000002B1 System.Boolean StartButton/<LoadAynchronously>d__4::MoveNext()
extern void U3CLoadAynchronouslyU3Ed__4_MoveNext_m234DC952C4B2F315969BB84611A7E5104BC3A27C (void);
// 0x000002B2 System.Object StartButton/<LoadAynchronously>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadAynchronouslyU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5557547C31F7B97F06DB286DA468D7915B50A649 (void);
// 0x000002B3 System.Void StartButton/<LoadAynchronously>d__4::System.Collections.IEnumerator.Reset()
extern void U3CLoadAynchronouslyU3Ed__4_System_Collections_IEnumerator_Reset_m57603CABA0EE1D1465CF65D883D6F5CB7A50BC17 (void);
// 0x000002B4 System.Object StartButton/<LoadAynchronously>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CLoadAynchronouslyU3Ed__4_System_Collections_IEnumerator_get_Current_m6481BEC178F1D855C17F9B03FB18A3D02F57DFFB (void);
static Il2CppMethodPointer s_methodPointers[692] = 
{
	Character_SetupAccesory_mB2C7DA2D49346B7E20692E0CA7BEE5D9EE153E0C,
	Character__ctor_mC9E775D49A9CE6DD699D3C59405E734464FDCDC2,
	CharacterAccessories__ctor_mB1513067B71D750AD7C126D9BF301311E0EC0F68,
	CharacterCollider_get_deathData_m30C15CC27308AF106F16ED253D7BE6879533DD0B,
	CharacterCollider_get_collider_mF0BD765523BD113B2DACD653AC32916C233B727F,
	CharacterCollider_get_audio_m7E061A0D0E318DF9E63F020F67CB793A79627A0D,
	CharacterCollider_Start_m27B2E67B7391F3DF9295F9569C4C7115ADED7BD0,
	CharacterCollider_Init_mA2D9141E0186D0B0C583D9A4D9DD4897CF378B40,
	CharacterCollider_Slide_m0945F1AB95BAC92F8713AB65F5B22D9E24FD2084,
	CharacterCollider_Update_m3B82835239BA52689EFD95FE7DBD7D4BDA7BFAE7,
	CharacterCollider_OnTriggerEnter_mE8A741472D669D676F14DFA0AA2B3BDB978031A6,
	CharacterCollider_SetInvincibleExplicit_m1B7CED90D87DDE5849DD20DE3D06C1B35030D287,
	CharacterCollider_SetInvincible_mB40DC7F8E604BD7ABB32A3356DFAA3141BB14F3D,
	CharacterCollider_InvincibleTimer_m288B818C19163001F8B409969DD901CC26652A6F,
	CharacterCollider__ctor_m0FC05AC0F265E5D1643DB51AD048BAAD22311B5C,
	CharacterCollider__cctor_m17F690CF74B43F61BECE966D59F436E5EA34DEA8,
	CharacterDatabase_get_dictionary_mB1EF1EDC146F5E8C4F660721B4B2430C8FBA3160,
	CharacterDatabase_get_loaded_m067F9DC8B88258B179077D66E3157944596E86A3,
	CharacterDatabase_GetCharacter_mCFCFD7FD2884E03104B034609BF7C128917945E1,
	CharacterDatabase_LoadDatabase_m8E13C9F8709E0C750E508ABC9FBE05C59B2F8F74,
	CharacterDatabase__ctor_mC671A1E605A9A25130626BAB30FE70704FCCECCF,
	CharacterDatabase__cctor_m2C8F10AE2D58E6BE36D205B88CD2DA1DCD665A48,
	CharacterInputController_get_coins_mA7918955D98BAE0CD5EDDADFE78AFDE6DB36494D,
	CharacterInputController_set_coins_m5960AF38ACA1A59E3CC0607715646525231235E8,
	CharacterInputController_get_premium_m4E6BF2906C901783B93C476B3229A981C1E40943,
	CharacterInputController_set_premium_m1A16E9A4C0F31782E6DA61018B297BF45442735C,
	CharacterInputController_get_currentLife_m5590661F1E30259092D8953ECC1CEBE167992512,
	CharacterInputController_set_currentLife_m03983DC1898D36E246344E6F107B6B70BB471C61,
	CharacterInputController_get_consumables_m0652D2736AF01E8F18A7D5A89DBB39026E18F409,
	CharacterInputController_get_isJumping_mC14F24BCA7CADF874B814A4AFEAAE045B907E7F9,
	CharacterInputController_get_isSliding_m58C1B77305B725E2426AE278D1F6AD445BF6030C,
	CharacterInputController_Awake_m68A226F11F72F5B4050A0C2E35E0E2A77E2D2331,
	CharacterInputController_CheatInvincible_mC038DAF855C1A0AEBDCB7C5D68B04E4A694808F1,
	CharacterInputController_IsCheatInvincible_m743934E5C84E39A55A8E1853F332D3E5646AB5AE,
	CharacterInputController_Init_m5115243ADEB34886DD9E604CC75F214B46885CB4,
	CharacterInputController_Begin_m2FE810D29C49E7E977FBC8007472AFBE8D83107B,
	CharacterInputController_End_m5EFAE8CE60439E107213AE4C71F122E1CF8E7713,
	CharacterInputController_CleanConsumable_m723E6959E5C10F14DAE1C03D35B157D112FC7FD9,
	CharacterInputController_StartRunning_m0623F600129315CEF9B31A60145F0190EA741656,
	CharacterInputController_StartMoving_mDEDA627C7A1A43252AAB922EC26F270ED79D13AE,
	CharacterInputController_StopMoving_m45EB7F43E32F8DAC5CF7701C9D755A2E8CD94517,
	CharacterInputController_Update_mE3032FA60047013CF79DDF7C6494C9253A200A7B,
	CharacterInputController_Jump_m833580A08F5820CB0DEF593C56CF4D4FAF13A8FA,
	CharacterInputController_StopJumping_mAAAA5DBF4769076B438187A4D645A675251C396A,
	CharacterInputController_Slide_mED3F27303504E821BB530469F2DD6520135646A4,
	CharacterInputController_StopSliding_m5B25A91BC2D10B6B66AE2C7D48C8F916BBE2B8DC,
	CharacterInputController_ChangeLane_m42370B9253F098650A1EBDD8B91C7E9A66C242FE,
	CharacterInputController_UseInventory_m0A3FA0AAC4929F9198C240B8FDE5ADED632D3061,
	CharacterInputController_UseConsumable_m33EF36BFE3E01281AB0FE83261E1C9DBDEA77967,
	CharacterInputController__ctor_m42D6EBA37CEB23FD380009A69CB41CEB19907B65,
	CharacterInputController__cctor_mCD0FBB7A39F759057C8D76803B2BC1D6C52EFE46,
	RandomAnimation_OnStateEnter_m274BF0E70C01D2AFD4B0EA42BCCC5E6C26543A0A,
	RandomAnimation__ctor_mC3605F17E55CFA3AE0B47E317E7DCB423C78B284,
	RestartRunning_OnStateExit_m764685CF1805A9897CF442C9A0B25E633F474753,
	RestartRunning__ctor_m8DAB72ABB7F6E98073D2FFD1CC7C1D43DB9C10CB,
	RestartRunning__cctor_m42DC7ED826E85F2E4EC5B65E6ADB1CE335ED4085,
	Coin__ctor_mF977D2CE7F9F781D0E5B1226BD2757CC523DB637,
	Consumable_get_active_m9BD183D0EB92D06D7FB0F799994422319B9CCD15,
	Consumable_get_timeActive_m0CC82DB5FAB168AE412AE3AE79F5296FD2154DD1,
	NULL,
	NULL,
	NULL,
	NULL,
	Consumable_ResetTime_mD0A56DCE8EAC4296FED58FF4EC9AB7C8ADAC9975,
	Consumable_CanBeUsed_mD5E41CD426D84A5550F44DCCC142BACCB31D89DB,
	Consumable_Started_mFCCE21EDCF35D6CA314513F2FE36243FB1444049,
	Consumable_TimedRelease_m4CF53D8423644C0C574BD9846F3E3C9BA221BD89,
	Consumable_Tick_m29550B408B4852BAB892FEBC466B237B124074A2,
	Consumable_Ended_mED6B794CD1EB05A8A1B0508BA4AB4BC6E271D996,
	Consumable__ctor_m49924D1710089801735A2523E1C66AD726832A7A,
	ConsumableDatabase_Load_m88167DDB02DDCB33D924CCD0F246E174FE34761E,
	ConsumableDatabase_GetConsumbale_m2BF3471F1D6CA78B88ACF97A75119266835E580A,
	ConsumableDatabase__ctor_m374AA93053AA4243EF9B7DD87D8A4984BB9C3001,
	ConsumableIcon__ctor_mB2568114F3436222C89477C4BF2CFD2E1DFC9E63,
	CoinMagnet_GetConsumableName_m5E1E2CFC12A15C207D770BCB79EA3E963F026096,
	CoinMagnet_GetConsumableType_m066EE107836BC29A086C2E6212D4DF2C40039A9B,
	CoinMagnet_GetPrice_mBABE16458C266DE11E641DF90AACEBAE3FCA207F,
	CoinMagnet_GetPremiumCost_m91F1E7BEDC0AD05C2F0A24C0EFF417281C5F819A,
	CoinMagnet_Tick_mF2DB5370602938FD45F2E6CBEEE8B51EC91ED3FB,
	CoinMagnet__ctor_mE53EF9E42D20C5707050D80C2375BA7B0A0DFD89,
	ExtraLife_GetConsumableName_mF131848FFEAEC044D49E96D69190CA3798807B5D,
	ExtraLife_GetConsumableType_mA57A8CB79B55049DCA612C8C5D6ED9B6CFDF5980,
	ExtraLife_GetPrice_m74417195122AC422C13CBAC29700E6891E71EC80,
	ExtraLife_GetPremiumCost_mA98ECEF49095A390D76505F1BBD977D5EF27181A,
	ExtraLife_CanBeUsed_m35DCDF7D299174D2071BFC05D05A32559E47651E,
	ExtraLife_Started_m7EF4AD4261EDC04DAE7F8EB7986821CA5A72DA50,
	ExtraLife__ctor_mB3DCC1F79E45D78C09749719B8AD67BAC35FCF43,
	ExtraLife_U3CU3En__0_mD7F4F46A4A1EA7E8E75497F878041BBF3E5B3234,
	Invincibility_GetConsumableName_m92BF1C380AB95CFD2C40B458ACFB3067EDDF6680,
	Invincibility_GetConsumableType_m2C7DB2E1C8F20879FF95A0CA87174E141E1702CB,
	Invincibility_GetPrice_m2F2A0248BE8103C448B561D62C78334844CAFF08,
	Invincibility_GetPremiumCost_m51D9B4939272A558EB1EDC572B019EF699E9978D,
	Invincibility_Tick_m61DD4437139F5CA49A0F810D409947F7154E55E1,
	Invincibility_Started_mB345077AEBAE4D21E7FA503F804654AFCDEA0F81,
	Invincibility_Ended_m9429A4F8BFA5A7BF797981E3E7777CA10103CD9F,
	Invincibility__ctor_m29ABFDCF03E7AA70ACF942C162BE505EEBE89C1B,
	Invincibility_U3CU3En__0_mE46F1B9B439B5512A3CAA99F78C22D5ADE77DF01,
	Score2Multiplier_GetConsumableName_m6E3BD93D6000A0CA29CB02E63A6D0C67F94EA00D,
	Score2Multiplier_GetConsumableType_mF5B2DE2A8B9EA24CF8F09AF5D9EC3B20A3BFC32B,
	Score2Multiplier_GetPrice_m4C3D295C3E3BAE3D322E4BD810DDA60A0CA26C17,
	Score2Multiplier_GetPremiumCost_m6AD93F7710BCBD2331F5A86A4C792A8AAF980B16,
	Score2Multiplier_Started_mA87019FA02A9CDB61CB55F4E08471BDB3663E6B5,
	Score2Multiplier_Ended_m90600953DCB74A36FB80F835114E3C23C9A3B587,
	Score2Multiplier_MultiplyModify_mF564041C4522E0465496E14F2B8C25B75309DD61,
	Score2Multiplier__ctor_m336BBDDE4D6CA68070A922351850AA47B03EB8D1,
	Score2Multiplier_U3CU3En__0_mD98F91578AC7A43A24EA7E614E30A51F2FF5E861,
	CoroutineHandler_get_instance_mC30739A5FA9BB17B5885C067029BF17C12537C07,
	CoroutineHandler_OnDisable_m657E1F70FD67D06D5EDD7168B830064BE92B7A08,
	CoroutineHandler_StartStaticCoroutine_m2E6879D6BDC14C945D3379FA8838922AE3714099,
	CoroutineHandler__ctor_m2F086A05A90813039FA5EE301F265E8F3F63A9AE,
	GameManager_get_instance_m01A2DC805F24ED39DAFB4BE28A43CFD69AE0D00B,
	GameManager_get_topState_mDB274A1550D33EFD6B938377D13F6AB89B6312C0,
	GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30,
	GameManager_OnLoginSuccess_m1C192B3BDF92D89AEAEF60BBFA65F47059C516B0,
	GameManager_OnDisplayAuthentication_mC193AB7DDD2BEDF803919DF135DB820721834FA5,
	GameManager_OnPlayFabEnabled_mC54BF508287CB01BC8025D6DD3E1A6B06E826D72,
	GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1,
	GameManager_OnApplicationQuit_m84D45A971E10AF1DE8F25809E66AF69557445E8E,
	GameManager_SwitchState_m4AE5D7157D2BDD220796F870160C0FE4504D81FA,
	GameManager_FindState_m1B489EE6B9437B2DF1DCF153CE5DFFDF779AFD82,
	GameManager_PopState_m34765F910BEB1D9F6CD3556200C596E517716577,
	GameManager_PushState_mB90D8DBD45A3F4565EC885D75B1615BC49387CC8,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	NULL,
	NULL,
	NULL,
	NULL,
	AState__ctor_m72EF28E952C4E3889571AB87DE08A239914CEE13,
	GameOverState_Enter_mB0F5CF2A842DDF8A59E9EFBCD47B16EAEE92B3EE,
	GameOverState_Exit_mF3EE8190CEED49553FB9FB0C96FD5E92D6933CAB,
	GameOverState_GetName_mF07A53A1AC07A5068AA6DC79FBAC9A0FD9619514,
	GameOverState_Tick_m0422DA154B140B6E1A4CCE0280CC64DD192DEDF8,
	GameOverState_OpenLeaderboard_m942B9B675BCEA79ED7C02E5EE9264DF266862762,
	GameOverState_GoToStore_mE82E99460209C6286CFB9E4D68CA121E4EE6DE47,
	GameOverState_GoToLoadout_mEEB859F5492905AD23447D894C765509E30A5780,
	GameOverState_RunAgain_m94ADBFE7B989494A4EDE07F3BE4B9E2AFDC06191,
	GameOverState_CreditCoins_mA4E8EBBCBA2584B43E33415D3542BB88BD50854B,
	GameOverState_FinishRun_mE4573AA25C76AE3DFCDC2CF9C67A04212272FE32,
	GameOverState__ctor_mBE7DA3F9387C25715444A68655E3010A5117F651,
	GameState_Enter_mB5299A6B02EF61E628612A7AD4C4DEB8A021D829,
	GameState_Exit_m43F1D15E53913916EE73E6CD8712B1B86627FC92,
	GameState_StartGame_m1721CD5948A5CD365CAFF93108775A6A19706A37,
	GameState_GetName_m3B44C99B75D40E4DD2D410CB1BA15021C775E408,
	GameState_Tick_m5D400090B65A728BB1548843EA00D341024EA0ED,
	GameState_OnApplicationPause_m72C23784699D91FA9674FC33D8AE8E006B13FAF4,
	GameState_OnApplicationFocus_mBA524788A72F32B49A11BA0D896F96F5DE464F9C,
	GameState_Pause_m4BBA1B9D5F6BD0422D824300A436BC25B7EF1D85,
	GameState_Resume_mE68B46A86029A232BB32351ED4BC5C8A7438703E,
	GameState_QuitToLoadout_m86D5B2A60767C517D7BBF60BBB9C46EE10F30A40,
	GameState_UpdateUI_mF94DAE9FD4E3D150C826044311DD0D9230D9250A,
	GameState_WaitForGameOver_mAC7CAC58332275915F6E5F2368EB63CDEA165465,
	GameState_ClearPowerup_m1D7AF56CB2244ADCBA15B28D424DF1A47A1D4A3B,
	GameState_OpenGameOverPopup_m3ECBDB05AE426EDE9A7DAB1C5D30664AD56F7BC7,
	GameState_GameOver_mDF01C741C890FF323E32124401B0AD4A5B3376FE,
	GameState_PremiumForLife_m2C755DD7C34E1F34D541E7E48157C2194B975A33,
	GameState_SecondWind_m32755DFDC78C85FF5B0B3075967B9F86103EC0BA,
	GameState_ShowRewardedAd_m2AC0069C66EF82C3FA2841CA166E3BBAE1347168,
	GameState__ctor_mA14A91584839FF578B3B751214A2921375FD1578,
	GameState__cctor_m555A98DBA8E01F4099A2E2D12F74D2F3A9C85960,
	LoadoutState_Enter_mE09A2008C430DC89B9BFE41DEBD536EA3723C2B4,
	LoadoutState_Exit_m11726804C21B2A67B8D388A220A7E8D9CB93FB2A,
	LoadoutState_Refresh_m1F3B8D718AE1857C83911DA91828DE5A0820C45D,
	LoadoutState_GetName_m5C5CCC38B793CC1BE8401055C40B606E2069BDAB,
	LoadoutState_Tick_m7C62A386EDDF85D67EFA719CB2C33BDEAB9F05A5,
	LoadoutState_GoToStore_m8914771659E89C17C441E112E1746858B813B796,
	LoadoutState_ChangeCharacter_m29763255FBEFD455603E9B62850A5735037FAAED,
	LoadoutState_ChangeAccessory_m3335A73AAEED201ECC7D69E18D36057A8D628CA6,
	LoadoutState_ChangeTheme_m3F1B3EBA142A07917C97EFCA87135E6EC42F550C,
	LoadoutState_PopulateTheme_m22B65F81A35B1F3F5091DC053BE48EC9129A88B6,
	LoadoutState_PopulateCharacters_m8397B1FD2F99F48E00EBEBA08CA64064BEAD07F3,
	LoadoutState_SetupAccessory_m4550029359668EF4C849F3CAC503A4C183C24CA3,
	LoadoutState_PopulatePowerup_mFC89D079BF7C01492B1068CBFA63DE9BFE42C27D,
	LoadoutState_ChangeConsumable_m8FA06821E9F65D89202408C5CB4D378C5CE76529,
	LoadoutState_UnequipPowerup_mA2F48805147E8845280B14DA689BA236BCBA625C,
	LoadoutState_SetModifier_m4E41DD0C4C78D7DFE62E4F4C3FC9FB21367CEF45,
	LoadoutState_StartGame_m62EBC673F49DC3ED31BBC88D3B9523F98F12FDA5,
	LoadoutState_Openleaderboard_m6995586BC6F036F79D53BE48136851DBC3811AB1,
	LoadoutState__ctor_mB4DA08F5C69BDF13769C2F0AA11AD02C104E662D,
	Modifier_OnRunStart_m9B93F0AF6552ADE96BA79859D380F50796E595FC,
	Modifier_OnRunTick_mDF590DD20D977706508EB7325BCDC91362D6724A,
	Modifier_OnRunEnd_mCAA4ECD15F848A4C5F41CA6EB6371DA36D483C01,
	Modifier__ctor_mEA4613339B8CD37F6DC43A8D7F18CCD448F405B7,
	LimitedLengthRun__ctor_m9D1034526E210A8A249AB2315E6910A3E763AF21,
	LimitedLengthRun_OnRunTick_m15BD7C4686312D8542E1C42A1F86AC7D4E9AAB16,
	LimitedLengthRun_OnRunStart_mEF93A1EBE9F615F22A434C9ED340F6BDF52DC2E9,
	LimitedLengthRun_OnRunEnd_mE58149065198C5312088A8513CAD05AA4309F9A4,
	SeededRun__ctor_mABDBF9C949623B12908B9B6A15AB46DF949BFF6C,
	SeededRun_OnRunStart_m2A3DC411F5DFE0652EDADF5A0AD22CFEB5701DDD,
	SeededRun_OnRunEnd_m39AE67D079D3F8980550F2A7630CC7691A152B9E,
	SingleLifeRun_OnRunTick_mFA020C2F5779BA89EFB884ECCD8B9CEE7A1D2DC8,
	SingleLifeRun_OnRunStart_m6AF5BCA56A90FB79E6F7C076D2384E6ABCF2CAF3,
	SingleLifeRun_OnRunEnd_m7A8BA96050725B35EC7814938561349457E7CF66,
	SingleLifeRun__ctor_m761CE9228F29DE87E31CAE33FBB32510BF85E4A6,
	PlayFabAuthService_add_OnDisplayAuthentication_m09307D1077EFDBA69B680DBC3E4565061E3A3C47,
	PlayFabAuthService_remove_OnDisplayAuthentication_mF91D71B93627024F5084926D6C3C842A8F4AC2B1,
	PlayFabAuthService_add_OnLoginSuccess_m1B44BC59674DA4AEB0F57AD3CA283179AF5EB607,
	PlayFabAuthService_remove_OnLoginSuccess_m6A6AB26D85E1CFEF14668DF1E7338AEF91C54DA0,
	PlayFabAuthService_add_OnPlayFabError_m06D8796B2EC01FCEBFCAAEAECC1FAC972EF83DF4,
	PlayFabAuthService_remove_OnPlayFabError_mF5A068C29D48826E5FB846DC0D38ED5A19DE9397,
	PlayFabAuthService_get_PlayFabId_mFDC42662ECD647411ECE2EA6166E1CEA784E1306,
	PlayFabAuthService_get_SessionTicket_m57EF01A6618572185DEEB280BACAEE512F963250,
	PlayFabAuthService_get_Instance_mA897E191B308C20E8F816061033E07FD9BE92D35,
	PlayFabAuthService__ctor_mE0B53ED52D15B3C9B3CAD84FC3A8CAA911E9BE42,
	PlayFabAuthService_get_RememberMe_m18006A1AEDEB47CA209549E8FBCF17E21B296B69,
	PlayFabAuthService_set_RememberMe_mA43A893BF87ADCC18C816BDE99980EA570A0BD20,
	PlayFabAuthService_get_AuthType_m991DE11CE6DD6432434747062853D0E0A8C65FEB,
	PlayFabAuthService_set_AuthType_m0324E91F4871E9701DF88B4E4A9C56FAF7F1CBEA,
	PlayFabAuthService_get_RememberMeId_m23628575AF9F71CBF48270199BF9FEAF5CD4F0C2,
	PlayFabAuthService_set_RememberMeId_m6B743999F4ACB4BE47E2A3FC102B004792C18C5E,
	PlayFabAuthService_ClearRememberMe_mA008AA2BEF391317EA464A6D21CE349D066818A3,
	PlayFabAuthService_Authenticate_m96B8766549B69AFD2C718B10B6783293D1EA65AD,
	PlayFabAuthService_Authenticate_mFA3335EB389D65B7ABCA8CD104F4A86073FEC8D4,
	PlayFabAuthService_AuthenticateEmailPassword_m302974BB5D0C227928345410E7D8C72DA78FAC28,
	PlayFabAuthService_AddAccountAndPassword_m2842E09E11CAEF5CC46019A3C2EEA3DCB3A0DEF2,
	PlayFabAuthService_AuthenticateFacebook_mA8CD3C30836ECF8E89D3E190EC2E08E38398DC97,
	PlayFabAuthService_AuthenticateGooglePlayGames_m4517338DFEEC6F9326C4E75639315E301F554061,
	PlayFabAuthService_AuthenticateSteam_mAA36F12AE59F7C2F953F3233F3FA673FE5ADF743,
	PlayFabAuthService_SilentlyAuthenticate_mDD6E523EF570A7D7EA5E367B1C45BEB4700A7618,
	PlayFabAuthService_UnlinkSilentAuth_m98A2F30829899450ACC5A3993AD511C7A1AD5912,
	PlayFabAuthService_U3CAuthenticateEmailPasswordU3Eb__43_2_m73A9B7F2804D6DAF65391CC91DE8227C2511B59F,
	PlayFabAuthService_U3CAddAccountAndPasswordU3Eb__44_0_mEBB431D2D3658DEA91D3EAFA612584CE7C5A5F98,
	Helpers_SetRendererLayerRecursive_mB6D8813394E7A1C269B980CBEDBA4A46E86EF68F,
	Helpers__ctor_m1B2219AA037C37F001996A5AB3EA87C908D13924,
	LevelLoader_LoadLevel_mE00D1FE2B6BBF5019DD479F77C311E3946446D52,
	LevelLoader__ctor_m189433ECD8FC8ADD3F871B6C14ECD7F454E38FB5,
	MissionBase_get_isComplete_m1856DBFD7530737E7833976E619FF860C82FF1CF,
	MissionBase_Serialize_mF64493FBDF907597DB062B835C29BE9F4F39AE15,
	MissionBase_Deserialize_m01BFCE90CC1E5EAF6E63A3D6FF7DC4FEE110F694,
	MissionBase_HaveProgressBar_mAF2DDAB2F433B7CCDEDA668E633CADE6000EDA7E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MissionBase_GetNewMissionFromType_m458E73FFCA0E51ECE0B670424274CCA2BB5AE1C0,
	MissionBase__ctor_mEB6F981EE258FDF62FC79CBBD746B09EEF3EC85D,
	SingleRunMission_Created_m112E7397F862F55EC1FEBBEE4895D720B2081B3B,
	SingleRunMission_HaveProgressBar_m9BA84B6BD9AA1AF63979EFBE0912068D201E95CC,
	SingleRunMission_GetMissionDesc_m3FA0FA783C9504859B320D1C3FB32D2BB0A24ACA,
	SingleRunMission_GetMissionType_mB565F81AEFA82A97B235511DD9AD9624349BEFC6,
	SingleRunMission_RunStart_mDEEF0B66457B37341A766ACDD4320F0B8270C908,
	SingleRunMission_Update_m4FA0B557D42FC4263ECC6BB7A0224F6CF9015462,
	SingleRunMission__ctor_mB7F20B0341FAF054716344CE31E6ECB256DB151E,
	PickupMission_Created_m16848F79FE6113C87C0D957A323C01C5468CAEA8,
	PickupMission_GetMissionDesc_m96AA377C275621BED1874802C9C25182194F684E,
	PickupMission_GetMissionType_m76F4B1C6601853AA345CAF8923A97640F5478B4B,
	PickupMission_RunStart_m9A9ADAC867E06E3D0436877BDD7B047E229AF4B7,
	PickupMission_Update_m2476885511EF40CC3748E0FE5700868FA5FBFAEC,
	PickupMission__ctor_mF443916E1BC58541BC90DD6258270A0C2DE605C8,
	BarrierJumpMission_Created_m7930DFC293EF928BD5459B98FFCB91FBD137B55D,
	BarrierJumpMission_GetMissionDesc_m209ECA7AF0E6154090818D18CAAA7C96CE409BDF,
	BarrierJumpMission_GetMissionType_m54F3F27DFDF9C9919FDEC894590084B631F58390,
	BarrierJumpMission_RunStart_mD0F821BB2C94158457674B55745EF865A3FC99F5,
	BarrierJumpMission_Update_m6FE7FE1F1C66159D3383561871920DF5B74C6DFB,
	BarrierJumpMission__ctor_m9945081C8315076BD4223A4FF379F03A80390219,
	SlidingMission_Created_m9605AB9B00BA6BC9BE4C1D826BA6002E6B6A446B,
	SlidingMission_GetMissionDesc_m5785439A42FD3E65C7E088C2E4DBD68A7AC5A6D4,
	SlidingMission_GetMissionType_m3DDE610C348E4968ED5556A70F2082C16B858083,
	SlidingMission_RunStart_m9C16801BDC847044D3EE887764E136D52E3EE0B7,
	SlidingMission_Update_mF90EF7AB4E6C94760AFD5E76380BEC08D108BEBD,
	SlidingMission__ctor_m1E662582E56C6CD05AB9A5FFA4122482F973B9CF,
	MultiplierMission_HaveProgressBar_m9503D2307AE8C4EBF1BFB329346C8EDAD04BE4F1,
	MultiplierMission_Created_mBE6F09FE98BA195DCECD42A8B4FDB82AE3DC5A15,
	MultiplierMission_GetMissionDesc_m3888884232F13A4100BF94F844BED60BD7749D95,
	MultiplierMission_GetMissionType_m99E08F1126B5A324509C99B377AF6F111B1195ED,
	MultiplierMission_RunStart_mC1A8DF7760502476F802E91AEE367D0EEE8BEF6F,
	MultiplierMission_Update_mC569E0AC1CDBF42397BDA80BCA3423FA084A0049,
	MultiplierMission__ctor_m91BD2F77C5663CDE825CF5A106440B00DF36A835,
	AllLaneObstacle_Spawn_mF8A58ED12FC222A217D34C18056902F48CB12682,
	AllLaneObstacle__ctor_mA4D103F1C74740D57AABCC992DBE47A1E801FDF8,
	Missile_get_m_Ready_m69813E8B42B2AE6A82E2930164888C66A28319BB,
	Missile_set_m_Ready_m6E5187A046605EFBB373B5B562ABBB66F08F56A4,
	Missile_Awake_m5C1D3D74A04BC3A74B62462D5545379D8DC62848,
	Missile_Spawn_m70568F98979CB913200F45539DA71B8E644178BC,
	Missile_Setup_m67CAA0BADCC751EE930F7E40706B00FA276FFCB7,
	Missile_Impacted_mBBC8F08D53E5EF1CB51A6F9179FEE32505A70D33,
	Missile_Update_m6CCAF891ABE7958B7A9BF0C719942473B5F6F922,
	Missile__ctor_m31640A83F8A494059B42203A6A8C07B8293C6209,
	Missile__cctor_mF03D8D27B8958DC07639EDC66C5BF3260E0B4B5B,
	Obstacle_Setup_m79556311153491AD55B972F38D498B226E6531F6,
	NULL,
	Obstacle_Impacted_mCD5912D4F042D86063CA0BFFBC66D5F8195AD616,
	Obstacle__ctor_m8DAB1C8ECB2328D09E320C484CC0C76D7C474C96,
	PatrollingObstacle_Spawn_mEE60D0668D6019FEA3CCEE9C8A8EBD7F333D0DF1,
	PatrollingObstacle_Setup_m7B702390BC97E396A2145ADB2C17131A2BD623FC,
	PatrollingObstacle_Impacted_m153BE6D4EFF695CA3D852B829EAC74F8DDD7F86D,
	PatrollingObstacle_Update_m7E1FAEF845B83229C104DAD6B0926988B4AE7045,
	PatrollingObstacle__ctor_m3EFA89EC71E52DC26C423AB24ED317E15C6EAF96,
	PatrollingObstacle__cctor_m014569258BA0243E5A7F23B6B3916EDA5AA3EAFE,
	SimpleBarricade_Spawn_m6BE7D289AB407ADB5BD4B5FEFD3BA5B6B969FED3,
	SimpleBarricade__ctor_m21B41405BBF2E02E3B7E7AB03C9E56F417E828AF,
	OpenURL_OpenURLOnClick_mA48C19DB1E9EE99A1D06FD11ED985D613B804589,
	OpenURL__ctor_mBD893164A0E0FE8CD0999BFF1AE11E07C5244D5D,
	HighscoreEntry_CompareTo_mC9269DA9CFADA1A4989CEFC653F8B5B2D7ADF43B,
	PlayerData_get_instance_m45C5BA3A1E5AD3B6C0641F9F37CF074EBB3A134E,
	PlayerData_Consume_mF763A92F6789868A9DF6841140AB6703BB02F462,
	PlayerData_Add_mD391CE971F57470E2193B901FC3ED909EB948745,
	PlayerData_AddCharacter_mC8CA7E81CCD5A65A49A60A0821D55E2188E6C4A8,
	PlayerData_AddTheme_mF1B4C815537FA5847A089EB88962A37F91E0B9DC,
	PlayerData_AddAccessory_mBEEB16CA19888861452A431325871920E28B1A2C,
	PlayerData_CheckMissionsCount_mF645C8980611A628F1DA09DCA2C957CD5AE8A67B,
	PlayerData_AddMission_m9518AD72ABC04500AF7A5D6EB40AAA88DC0887E8,
	PlayerData_StartRunMissions_m89058D12D3C02E9736D69AF23065F8BF2FD620C1,
	PlayerData_UpdateMissions_m13932B67E425A9B5047094F44E2FB5F4273896D9,
	PlayerData_AnyMissionComplete_mDB0DDE672CE3881426CCB5D61A625D1577152DCF,
	PlayerData_ClaimMission_m9A2B02F4554D5BCAEB2AF5405F984EA20E2A7D0F,
	PlayerData_GetScorePlace_m46A68740A46CEF6910ABC7417F3A3CC5D5A69CF4,
	PlayerData_InsertScore_m9FE02042D5279778E64A0F6EA05F18EB1C749C0C,
	PlayerData_Create_m245A5D8AEB7E119AF5274EEC21782CE625756AA8,
	PlayerData_NewSave_mE50C5BABBD0E75F3CE41BEAD5D226DB38BC300C9,
	PlayerData_Read_mCECA18B90E6F4338609DFE30620D6A44A285DFB3,
	PlayerData_Save_m9B08BB247C2438C0F56CEFB7D7326D573C12CF73,
	PlayerData__ctor_mC2DA6B32832AB92B19E799148968590FF1F28C1B,
	PlayerData__cctor_m329CD40C61128DCA159D2AB563D0676DC3BFF0AF,
	PlayfabLogin_Start_mAC1628871670BC543CDEF3DCE5386937DA853FF4,
	PlayfabLogin_OnClickSubmitToLogin_m86C71391E6894B2D6D72C77E4EC19AA547EDEC38,
	PlayfabLogin_OnLoginSuccess_m7F1079FB561A7AE5371C3406F532EFA5A8130D90,
	PlayfabLogin_OnLoginFailure_m96BACE6CE43F13C799B960BE6A80338585F15ECA,
	PlayfabLogin_OnRegisterFailure_m555629DCDA16C20CDED453E1554979B0B27DCF53,
	PlayfabLogin_OnRegisterSuccess_mE2A369567529BAA5904029B3E6590B25A40D3522,
	PlayfabLogin_SetUIElementActive_m6A04CB18FB2BCBDCA8C18FE097B5ACD0777DE9ED,
	PlayfabLogin_GetEmployeeID_mAD560060AE0D849F5B321491EE8ADFC05DBD001D,
	PlayfabLogin_GetUserName_m695858A15D7086590851E14D4841E50E711F847C,
	PlayfabLogin_GetEmailId_mCB87AA6497730E1A055BC5F7D2425A78C8AF1931,
	PlayfabLogin_IsValidEmail_mF5899A11D6734C31407A1DF171F8A5E8FF5817C6,
	PlayfabLogin__ctor_mFE24C028A6B68E75DC2FDD38B7A906675E786A2F,
	Pooler__ctor_m34F6F1D167C8B66F249899ACADABDBEAA26DBEAF,
	Pooler_Get_m542FB51BE61AC7C7684D9BAA91222F05C2CB566E,
	Pooler_Get_m0BCAD76E4049F5F20212F123946CFCCFD9204647,
	Pooler_Free_m01C46CF77B7FC2B907AE2D1EF0099ABDCB85E114,
	AssignOutputChannel_Awake_mD00B5509094C41B035C39BD7EE2E0F3E4B2EE9D9,
	AssignOutputChannel__ctor_m19DC585EB924A6FBB4CD72D17DA7AA09AA8F2340,
	CountdownSound_OnEnable_mF91F24E5D8AC40DB891F99AE26ECE0CC940F3515,
	CountdownSound_Update_m17455E6FC537451B5B8DC7C7799C1D5FBF65C3D6,
	CountdownSound__ctor_m119DCF565320F4539D786BD74B16027C45744EF0,
	MusicPlayer_get_instance_mDA07A8FACAC1BDD554322313AC3E0247EFE65240,
	MusicPlayer_Awake_mA60904394C1725C664514BD13BA55F8CEC17D6F0,
	MusicPlayer_StartMusicSystem_mA5DC573C43977BB44CFFF9CC0CEDB76E8B03E704,
	MusicPlayer_SetStem_m7E91B576F617E166A7450FE7F065654F6FE2267B,
	MusicPlayer_GetStem_m513DE7A4E2D85486D9FE05740A98B3888154D420,
	MusicPlayer_RestartAllStems_m1A1022BBB009E13E5D9C09DF69C5041BEFEA5BB0,
	MusicPlayer_UpdateVolumes_mB1895EE8AC52F4FA2B868C9EA466276F32B79330,
	MusicPlayer__ctor_mA83600A144210E8586F4618ED75C614BFE6450FC,
	ThemeData__ctor_mE49C6FA2328C48AB61776D3E1B35F1393C4C906E,
	ThemeDatabase_get_dictionnary_mBEDCF05E4CAD3FAFFF4989EAB1B9917937370264,
	ThemeDatabase_get_loaded_m65ACAFFCDE7BC7E25B024EAAB60C6D462E29C72D,
	ThemeDatabase_GetThemeData_m8076E358624D3AF2AD4EB88BA40D8AE0861DDC5A,
	ThemeDatabase_LoadDatabase_m4F775ED28F2F50411A0E7DFF276F651D46836758,
	ThemeDatabase__ctor_mCACE2AF9675ABCB2D9FF6B9A93F87720EC4D69FB,
	ThemeDatabase__cctor_mEF60598DBD220D6EB515C6DBF738AB7D00A701D4,
	TrackManager_get_instance_m6BD53491F54FA37AAE9B7411F874507A5EDB0AD5,
	TrackManager_get_trackSeed_m12145862E2266002296D19E95B615EEDCA45E2D0,
	TrackManager_set_trackSeed_m1AABFE4B7C5E646734DB3ED54316AFE41C200603,
	TrackManager_get_timeToStart_m8C38CB9E7C3C4A7F7D5A55ECA11AE86039B28854,
	TrackManager_get_score_m72FD5C2C3BD1C6F66223A99B58FCB7092E3FAEBC,
	TrackManager_get_multiplier_m245DE2FFD49CC05AB7D7E3ACD5E7FE1782E89B28,
	TrackManager_get_currentSegmentDistance_mAE59532030395D90AD5AEDD384F0CBCF85BDBEF5,
	TrackManager_get_worldDistance_mDA0CF15EA5800D0FEB6FDDD083A410322B2BEE27,
	TrackManager_get_speed_m5151D7EFA3BC17F6A6A7CBA82035CC544E7FA0FA,
	TrackManager_get_speedRatio_mB815CE07EA95B3451B0C362E0612118726CC9812,
	TrackManager_get_currentZone_mA6A8E73AF6212A1C0B2086F222B2192F51CB71F8,
	TrackManager_get_currentSegment_m98BA7068C3E00ED226663B959A8D96C3D042FDE4,
	TrackManager_get_segments_m01AC8381807DE8C1376DB9421ED015EC8B3FAFE7,
	TrackManager_get_currentTheme_m02BA6A1DE64A6416240A8312B29DC6FB0F30EE2C,
	TrackManager_get_isMoving_mE6F3EAEE3A680EE3722D260BC115238B071A17D8,
	TrackManager_get_isRerun_m418889725D618F8A41793C6D7AA8E309D4DA4A8A,
	TrackManager_set_isRerun_m5B23536B5A807325092F18E480BD137E47BE687D,
	TrackManager_get_isLoaded_mDAAB60A07D0AF02B2D53ED140AD71F69EF07A144,
	TrackManager_set_isLoaded_m716AC7275D329E58751D1245126F5F718A1ECD19,
	TrackManager_Awake_m1EBF2B746A7AA6304C35D9E43FB4E458B242259E,
	TrackManager_StartMove_mD127C009DBC45AE7AD17BC7245E776685C82270B,
	TrackManager_StopMove_m3C0652AC6AE61E1DCF99D2506A7E886DF57647A1,
	TrackManager_WaitToStart_m1060A329DAF9089A4F26B2D71621128458A42BE2,
	TrackManager_Begin_m65D7D70E8B8B2AF72ACB9A02DFB5412654A397DF,
	TrackManager_End_m10248F86DAE013A11F592C53E502B42014521F80,
	TrackManager_Update_mDFDCCA9720CEF4EC8F8F62517EF86AAE256C8D0D,
	TrackManager_PowerupSpawnUpdate_m96F701A117BEB2E4B7CC530B4CE7672FCDE6984A,
	TrackManager_ChangeZone_m2403B572099305508A3232F4BF816716FA02F7F9,
	TrackManager_SpawnNewSegment_m1135BFF3841F4A1A83B479EF8775E3D358EBAD71,
	TrackManager_SpawnObstacle_m7BB0901D95E10848E16BC861C2E7D2B5A18BDD6B,
	TrackManager_SpawnFromAssetReference_mD7A7661417A273DEF823C2B338BAB8EACF409C6D,
	TrackManager_SpawnCoinAndPowerup_m5081C041651AE4A620B03B956796D8685AD4687C,
	TrackManager_AddScore_m1FD70C0A4EFE3EFF001A27C90148A40160261503,
	TrackManager__ctor_m88B01DCC563530070B26E8B4744D683634247BCA,
	TrackManager__cctor_m9822F392EF450D5785A73007EFDF21243AF2F9CC,
	TrackSegment_get_worldLength_m589203BAE0A0D7F1E55F824626D902AA4C68D465,
	TrackSegment_OnEnable_mC37DBD3D3D8FFCE6952141182A179207D7D18192,
	TrackSegment_GetPointAtInWorldUnit_mAEB7476D616D234DD5D45A715D6C71DC6BC1D824,
	TrackSegment_GetPointAt_mB820F2F30C51193A7A1DCF689A209F01C87F5CB0,
	TrackSegment_UpdateWorldLength_m2B16323F6BBC848DCC33186C63A6EEE06880BEFC,
	TrackSegment_Cleanup_mA626F785572815F5E054F65CA758BE1AB1FF3289,
	TrackSegment__ctor_m77AB5AFEC598D9311FB639AC2E27B9638618BB0E,
	AdsForMission_OnEnable_mACD73EB2DB1AE1BF593F3D5D6A787798EEAC2EB0,
	AdsForMission_ShowAds_mD891DA3A3B99599F54D3CE68D580B2D5E79E08ED,
	AdsForMission_AddNewMission_m1CB1ADAEC724473380AF24CA125D9FCE27F03173,
	AdsForMission__ctor_mE7BD24C15BD210DC663D80EF6A9F22F4D7ACAE11,
	HighscoreUI__ctor_m92E286AC7802303DC3B98721B6A49291FC8A9DBF,
	Leaderboard_Open_m620A82E8B80B7C2AF37F1F887F341B2CF4CACEC5,
	Leaderboard_Close_m21D21F0E8DCDFDD99A10FA901E5E8A14747F90F0,
	Leaderboard_Populate_mF5BB2E2FE3458457E8C91D75CD9228EACFA3B087,
	Leaderboard_ToggleOpenGlobal_m55DCDC6FDD477966C6108B7659613C98AEBB221A,
	Leaderboard_PopulateGlobal_mF2DCB155C51B5A5F3D190DF228401E04A5AF4FAC,
	Leaderboard__ctor_mC72FE37B9E2BDBA75DDE02AFC34BC5BC06312835,
	Leaderboard_U3CToggleOpenGlobalU3Eb__10_0_m58D1860E8B526AE4B49F21139C57BB0A5FE6A61D,
	LicenceDisplayer_Start_mCF803BDADB209F8B57DC2756792759617355DA1F,
	LicenceDisplayer_Accepted_m08D9D3011611814710D0F2EA143CDA1419A27ED9,
	LicenceDisplayer_Refuse_mDD1F0CF87B86FB2121D27F1780BB283E9502AC5E,
	LicenceDisplayer_Close_m2C81D5CCE03DF167C35165DDA2319D67706E1EEF,
	LicenceDisplayer__ctor_m4AAD7015AFCFE6E922976EF3CB3FD72A93D61E92,
	MainMenu_LoadScene_mAEBC3638BA84257120F4BF0F70082C4EBD9AE43F,
	MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D,
	MissionEntry_FillWithMission_mCD69561490BD716986B049675637430A6F255CD8,
	MissionEntry__ctor_m5FAF3C11563D80BE15E0C8C257BDF4E5AEF67860,
	MissionUI_Open_mC4CDD6194BB5A512C4494663E89FE6F27DC27B2E,
	MissionUI_CallOpen_m1E038EBC531C0D11B1AB026F19E1CB1F4BAE6EE8,
	MissionUI_Claim_m2613F64C41AEB8B3111C19D33FD1A317D4ACE2FB,
	MissionUI_Close_mBEB1556AA5E5386B708A6D9F360A511588FFD35F,
	MissionUI__ctor_mF4A74576D2E28DB29C3D7641398CDF211A163853,
	PowerupIcon_Start_m23C422E43C12F4BC8EF9D5E16855B7FF33302537,
	PowerupIcon_Update_mE33C602BEDEFE693C71D2CF0353571BFA8F60BCC,
	PowerupIcon__ctor_mBD31AE76C34839B0001AC1BBF73D70765A127803,
	SetTransperancy_Start_m0FAFE8C7F83D89FEE9F2D3A1F3FCFA116071C511,
	SetTransperancy__ctor_mE019C9579F12034B3BF6F2F64E6056F89E950E10,
	DataDeleteConfirmation_Open_m4615E1F5AD22EC1131AF78166E3C9C19BD873C7D,
	DataDeleteConfirmation_Close_mAE265625F2B10A2BEE6733E9FAAC4D498D3B9806,
	DataDeleteConfirmation_Confirm_m8187E6A3F69B5FCC3F6CD593A239675E64455767,
	DataDeleteConfirmation_Deny_m1225892E41646760B925EC68F223F6B854853F05,
	DataDeleteConfirmation__ctor_mB1BA72EE7D9895DD7F0781A97F8537146CCD6DBB,
	SettingPopup_Open_mF1E67EBA5FBEA2BC03D823235F94652A8D3BC9ED,
	SettingPopup_Close_m73A914BE59B77805775EE3516C8CC2FA84B55E6E,
	SettingPopup_UpdateUI_m1FF3F9232429701F3378E89AD3A480A3369BACDB,
	SettingPopup_DeleteData_m10C549B0B69EC6C5975B69C3815FAF998DDD01F1,
	SettingPopup_MasterVolumeChangeValue_m267ACE5CE6F6F8EEB701405ABFD9CD6B575ECF8D,
	SettingPopup_MusicVolumeChangeValue_mAFC6866A77F9775F025A18E5D9D06C53637E63C2,
	SettingPopup_MasterSFXVolumeChangeValue_mFFFA1B6C73C9E860F69415865E9195B03EBEEA4D,
	SettingPopup__ctor_m4E2685EEDCED39A2E705C890AA232579F7234D98,
	IAPHandler__ctor_m68F5FCAA077F82208B3B370A4D1C9405B167F42D,
	ShopAccessoriesList_Populate_m58D2943CDCF2DD974C74000A6DB729A67AA68856,
	ShopAccessoriesList_LoadedCharacter_m21304A3591B14D735985AEEBF2A9F91DFBB932DD,
	ShopAccessoriesList_LoadedAccessory_mFCC68D388874EE4EBC7AA388129D3DDB9806499C,
	ShopAccessoriesList_RefreshButton_m38FF72F06217FB7A7F5360AA11478DBD8024587C,
	ShopAccessoriesList_Buy_m5A3FE08FBEAC22CBCAA3849FC9D0746EB6D81BE9,
	ShopAccessoriesList__ctor_mC8E347E1A2F05F468BC4731B66652F955EDA2EB6,
	ShopAccessoriesList_U3CPopulateU3Eb__2_0_mC229749232D40E11CC2DB0E032E9FB417AE22513,
	ShopCharacterList_Populate_m55D7E3316D5436A2CA7F02699C132093891C72A7,
	ShopCharacterList_RefreshButton_mB7704588B7960D246BBE195547FA1839E9401FF1,
	ShopCharacterList_Buy_m543DD69B66C5AD72E92A1E7580D9F2E4B4D64B43,
	ShopCharacterList__ctor_mA3F452AE15FF912D21D3165D9D4F9DEF4B96B05B,
	ShopItemList_Populate_mE7B37EC24953B9A89B7EC0A0C7D51049CB83371E,
	ShopItemList_RefreshButton_m6EDC5E69391E19D56C83369127A002EB455B4FDC,
	ShopItemList_Buy_m63A7CD63583D89F487E4DAB4E26A68A0D5641925,
	ShopItemList__ctor_m38D8145086F26E030AEC5704D4A52AF6A3044E9E,
	ShopItemList__cctor_mEE1B88F89581B80D4F3820475952752FE7FD5CA9,
	ShopItemListItem__ctor_m63A48D5B1DEDADDF59DB56745437DB0096D2372C,
	ShopList_Open_m452847222266C0528884BCE719BBED56CD97397A,
	ShopList_Close_mCEDB5D14E1E7B8B23132AE827C72E62D01C048C7,
	ShopList_Refresh_mDC2607B49700080C3DB0B0F6C871045857BB58E4,
	NULL,
	ShopList__ctor_mAFF39E29493A6B2E2AC2E604D26C90FB8C62C421,
	ShopThemeList_Populate_m63C1A43FB483682A66A3FDC01CE26E1640F8AD44,
	ShopThemeList_RefreshButton_m9D6975394E75C69C1D91E990BED4E6DDD72CA5DE,
	ShopThemeList_Buy_mD03BDCB23F4149DCC44CE3C85E60387DF6D9A9FE,
	ShopThemeList__ctor_mFAC743F06E76927120DABFAB29530194BFC4CBB8,
	ShopUI_Start_mDC3CA06BF11EE6E1CE82E5E7828EA991FC15FA84,
	ShopUI_Update_mCCB762FC4CF3B164B950205AD94C694D0EE8127A,
	ShopUI_OpenItemList_m1A477C2FBFA235B1E24EA05453F3F58745A0FCB7,
	ShopUI_OpenCharacterList_mB0AC77D1E084126CE21DB248F5E761B8638A7919,
	ShopUI_OpenThemeList_m29E8848B71A36A53BD3B4B7347340FDD1D8FA8B5,
	ShopUI_OpenAccessoriesList_m41970EA2577A7FF423E018B53F56B15B4455A174,
	ShopUI_LoadScene_mDF69EF93C0742043FE40DD6C0433E281770BF5C4,
	ShopUI_CloseScene_m0F12FDFC964F452CCD191A76F35FA01AE48E648C,
	ShopUI_CheatCoin_m0BDF7F64CAA040E8B0C24C08F35CA90A86543171,
	ShopUI__ctor_mC1E462240D1846F12A57D4437A01EC35F829384D,
	StartButton_StartGame_mEBB90D61E4FDE822965384E452ABCC9DEE03C9F0,
	StartButton_LoadLevel_mEF5C5953D118A26CA36612300A120B8BB85F8460,
	StartButton_LoadAynchronously_m11F5469D84EDFD0C642BCEBE2FE2F5B5E8C856AF,
	StartButton__ctor_m6DAB2093EB360099E446CA6B746441526C0C7BD9,
	WorldCurver_OnEnable_m278BF41D4EAC8D8436A193225D4172258F5126CE,
	WorldCurver_Update_m755CA3EB5BA64CCDA9A3437C0A41A38B37F08F57,
	WorldCurver__ctor_m8D5FACE2C24857A2415B9863DDCEA6E75C5A5CD5,
	U3CInvincibleTimerU3Ed__34__ctor_m8D5D9230E71664C4F1EB7EEE9B2622E51E2544F1,
	U3CInvincibleTimerU3Ed__34_System_IDisposable_Dispose_mFADBB6E9EA2EF95698FB131E7484A606EDB1BB86,
	U3CInvincibleTimerU3Ed__34_MoveNext_m43BEFB396EC5822C5A904437DE21482356A255CA,
	U3CInvincibleTimerU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAAB39BBB0D3248E1E0AD17A9A10388FAB3D2FAD9,
	U3CInvincibleTimerU3Ed__34_System_Collections_IEnumerator_Reset_m7FBA665B91C3D5A88CC60FAC78EAACBBEE61BD70,
	U3CInvincibleTimerU3Ed__34_System_Collections_IEnumerator_get_Current_mA433FEE9A1864721F75346E1B31F96B527DAAD16,
	U3CU3Ec__cctor_m4D74E3CACAC8A07E1EC4B4934B381775E2F921D7,
	U3CU3Ec__ctor_m56C186B238D4220BB24A536F275BCB1D78939F9F,
	U3CU3Ec_U3CLoadDatabaseU3Eb__7_0_mF442652EB0DA4EDCC3315FE59F2CECE432D1221E,
	U3CLoadDatabaseU3Ed__7__ctor_mC7CDF58302D7B98E7EB22648FAAC2FC4CDE030AD,
	U3CLoadDatabaseU3Ed__7_System_IDisposable_Dispose_m524840546B4184DAFB9505BD13E5FD60665F9838,
	U3CLoadDatabaseU3Ed__7_MoveNext_m49D1AA9A9936BE1977B771D4718C8D2863B56309,
	U3CLoadDatabaseU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE917FBCAB2F35B1DC80946FC5FB59DBE5AE0569E,
	U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_Reset_m342146DBC2100769356803F0C7ECA06B004D92A0,
	U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_get_Current_mC1057775743DC5D326EABE7E00BB68E33A430501,
	U3CStartedU3Ed__19__ctor_mEF42D919739E2A363EF86BC71297EA731B63585B,
	U3CStartedU3Ed__19_System_IDisposable_Dispose_mF4BB5903FDC60C398A1FBCF51F917FAEA78FF26C,
	U3CStartedU3Ed__19_MoveNext_m21B971FA84662D30283C6190DBA896F48F47B71C,
	U3CStartedU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33766A3905DCDAC9404C0B993C384237C3281A12,
	U3CStartedU3Ed__19_System_Collections_IEnumerator_Reset_m2D42E18F669C84497F3B35E7633558E42D7ACBCB,
	U3CStartedU3Ed__19_System_Collections_IEnumerator_get_Current_mCA28224BB04B07A6F7D136CB5986F7534F9F5270,
	U3CTimedReleaseU3Ed__20__ctor_m1F59A14FF7931B4EC7E797731AAFDF24606E8E67,
	U3CTimedReleaseU3Ed__20_System_IDisposable_Dispose_m75E07D756D7211E8CDD6C1F10E9C823DBF382C20,
	U3CTimedReleaseU3Ed__20_MoveNext_m70CDF86A57C887FBE22DFA0D946D4ED7B1CAEFBD,
	U3CTimedReleaseU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99CAAC6AC60A1518342D89A757586121A19C3830,
	U3CTimedReleaseU3Ed__20_System_Collections_IEnumerator_Reset_m5B0ECA825566D38638A998C40993C898EFAE37C5,
	U3CTimedReleaseU3Ed__20_System_Collections_IEnumerator_get_Current_m3D3236E4C725D7CA6CDD038A022E03D87FFCA625,
	U3CStartedU3Ed__7__ctor_mFCE003E9A8AF54E840895F8FCAFAC3A0569FE580,
	U3CStartedU3Ed__7_System_IDisposable_Dispose_mA224EA171282339A50662E2C06D20A13FCB8EEB8,
	U3CStartedU3Ed__7_MoveNext_m2378DC2A5BD1BE1368E33AC22E51C671791EE49E,
	U3CStartedU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18DA349166726DEDC3B52E76B536293ED38E3212,
	U3CStartedU3Ed__7_System_Collections_IEnumerator_Reset_mE90475D8C756EFFBFB2E739183FBEE5AC4808111,
	U3CStartedU3Ed__7_System_Collections_IEnumerator_get_Current_m8E89D7830FEAE1FE57FAEFF1258ADB4C136A4764,
	U3CStartedU3Ed__5__ctor_mFC33F89F8403094329D9B7C5C6DAED5681940F85,
	U3CStartedU3Ed__5_System_IDisposable_Dispose_mA9C7C9600CD551FCC31F1521AE88E77526F2BC72,
	U3CStartedU3Ed__5_MoveNext_m030400298DDD245791EEE3AC6399B9602F11857D,
	U3CStartedU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86FEB0F1746E16D84EC94159AA702C98ACF8F616,
	U3CStartedU3Ed__5_System_Collections_IEnumerator_Reset_mC5A0AA68ACC13F2582248F429790F7C0E41F3368,
	U3CStartedU3Ed__5_System_Collections_IEnumerator_get_Current_m277672D5AC175E17D0776DAB55F13F08455AE19E,
	U3CStartedU3Ed__4__ctor_mA80CFD61C96A66A860A2B9D78DEC1BBF6FDBF35E,
	U3CStartedU3Ed__4_System_IDisposable_Dispose_m6B1A0F182B3D09478075E36E4A3E202456072418,
	U3CStartedU3Ed__4_MoveNext_mFC15AEE93FC22539BFAB9F1F260FE92F182DF8E5,
	U3CStartedU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6290AA1767879126BFB0451959FC7BE49CC0BCD5,
	U3CStartedU3Ed__4_System_Collections_IEnumerator_Reset_mE096394F9BA4B1CEA2ACBF2D68F6C9C97FC27BE5,
	U3CStartedU3Ed__4_System_Collections_IEnumerator_get_Current_mF1A628526F6459B7999E32BA331364F8FB1600FB,
	U3CWaitForGameOverU3Ed__47__ctor_m7E4A5B3AF180B587FB987475A2748DCB7C1D2221,
	U3CWaitForGameOverU3Ed__47_System_IDisposable_Dispose_m20E1D453FE2C885C6F7E3AAD6FCC88CE46FAE8E0,
	U3CWaitForGameOverU3Ed__47_MoveNext_mE86614FE278591285CC15D188EA97CC56566194E,
	U3CWaitForGameOverU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD83D4561CDD0BDE00A1B289E7028A850BA4B319E,
	U3CWaitForGameOverU3Ed__47_System_Collections_IEnumerator_Reset_m92879365CFED874A18B5C23FF6FC651280ECAA03,
	U3CWaitForGameOverU3Ed__47_System_Collections_IEnumerator_get_Current_mD3F369BFA826A6D7DC4DCAD5F60CD6BA4B8B9B20,
	U3CPopulateThemeU3Ed__41__ctor_m3A2B0FD2BD3D5B9799640E208D97DCB2BCA8B9D7,
	U3CPopulateThemeU3Ed__41_System_IDisposable_Dispose_m9A46096A483CD89012F436A25C5CAC19694F49B9,
	U3CPopulateThemeU3Ed__41_MoveNext_mD312B1F408B2FB5E174A9C120381DF6A2C877A5D,
	U3CPopulateThemeU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB13834AC2759BF9DA4A1BF432A3BEB6C9B4E82F,
	U3CPopulateThemeU3Ed__41_System_Collections_IEnumerator_Reset_m13C7C872722C3D5F0E8E69CBD6A7E7BB0C25AF5A,
	U3CPopulateThemeU3Ed__41_System_Collections_IEnumerator_get_Current_mFE6C00DBD349F62EA1C27BC3E2AD348824A469A0,
	U3CPopulateCharactersU3Ed__42__ctor_m1B9F99AA08D99489D2AED553F7213B94D3201241,
	U3CPopulateCharactersU3Ed__42_System_IDisposable_Dispose_m640DE281406BD6BE814BEBE00DF21CA316B4DB3E,
	U3CPopulateCharactersU3Ed__42_MoveNext_m3A2B50E78F7B1ACADBB406A37BC2A007A1D1EC09,
	U3CPopulateCharactersU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A6117B73BA8DA99A0AC10CFAD8498A1D9057462,
	U3CPopulateCharactersU3Ed__42_System_Collections_IEnumerator_Reset_mA612E390F3E7900D2046E6208FCA39AC74BB259E,
	U3CPopulateCharactersU3Ed__42_System_Collections_IEnumerator_get_Current_m67D7CE7A285EA9E670FC0AC6D6C8DA1E0A73E605,
	DisplayAuthenticationEvent__ctor_mE3C811C7EBEA64D5F889266F5987023918C5A5A4,
	DisplayAuthenticationEvent_Invoke_m72C354D6C505453849EA05D96504B7A8AEC68168,
	DisplayAuthenticationEvent_BeginInvoke_m59AE8B7416765C9A1EAF992EEF65189215E3D42E,
	DisplayAuthenticationEvent_EndInvoke_m67B82D0905E1485FEEFE25161C1C09B39D028CE7,
	LoginSuccessEvent__ctor_m21A670CD32D8D4ED39A2CAE8A32EEC1ED0B7C44D,
	LoginSuccessEvent_Invoke_mB83F0BEF94DD4823390DCDCE16735DF3B65058EB,
	LoginSuccessEvent_BeginInvoke_mE2B67040B3876A0B31C48F67AAEDB6549E4722D2,
	LoginSuccessEvent_EndInvoke_m313788711791482E732576CC2675CB8FFD63C0A5,
	PlayFabErrorEvent__ctor_m67E83E7FF37DF0F7974600227E2D8668301C68E8,
	PlayFabErrorEvent_Invoke_mF4D6C92B9E540096382D7E629570490F1A946BCC,
	PlayFabErrorEvent_BeginInvoke_mA1428CC7E4BDDCC334B3AE364D8F2EE0325616C4,
	PlayFabErrorEvent_EndInvoke_mE7779FB47173939D3B762CDB691C422E59953E6F,
	U3CU3Ec__cctor_mC950B412080FFA1441508F56BAA6E23B5D8F02E2,
	U3CU3Ec__ctor_mA0D5D782BB8934706458F9EFCBA88A731E076CB7,
	U3CU3Ec_U3CAuthenticateEmailPasswordU3Eb__43_0_m3DA40107E96FFD438F998C784C199B37DBD6D1B5,
	U3CU3Ec_U3CAuthenticateEmailPasswordU3Eb__43_1_m01C6285C48F2EDCD6D18C5FB5D19E126FF50239A,
	U3CU3Ec_U3CAuthenticateEmailPasswordU3Eb__43_3_m6EE735320F6C13B7098EFA51B6FA403C0B421E20,
	U3CU3Ec_U3CAddAccountAndPasswordU3Eb__44_2_m82BCBEC1F04F922A2822158F1B5D4DBD61116BD9,
	U3CU3Ec_U3CUnlinkSilentAuthU3Eb__49_0_mAC5F5587E8220CB9783E3ADB851011D78E4341DA,
	U3CU3Ec__DisplayClass44_0__ctor_m8822C9CA81364C07314FF210C805364499ED62C1,
	U3CU3Ec__DisplayClass44_0_U3CAddAccountAndPasswordU3Eb__1_mB573E339766BC51F4BCD662932E944B3078925F3,
	U3CU3Ec__DisplayClass48_0__ctor_mA56381A054EE29D689BDA977A2DC3B1865B8CDE8,
	U3CU3Ec__DisplayClass48_0_U3CSilentlyAuthenticateU3Eb__0_m6281BD7FDE47E2C3F9F8DFB89F36F0639A41BFFA,
	U3CU3Ec__DisplayClass48_0_U3CSilentlyAuthenticateU3Eb__1_m7C24F6B2C4D0EDD13ADDFF770B579E27DA8665F3,
	U3CSpawnU3Ed__0__ctor_m3C12D98CEE9084D1104D3AC19E4DB24F14049F5A,
	U3CSpawnU3Ed__0_System_IDisposable_Dispose_m7D173ED73B59596164E3F93AE0B2E81854F5FBD7,
	U3CSpawnU3Ed__0_MoveNext_m2FB9A92FC985CC7A38574673B1B69FDC493AAE13,
	U3CSpawnU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACD1991881DDF201112A85D5567ED427D37AFBF2,
	U3CSpawnU3Ed__0_System_Collections_IEnumerator_Reset_m34B9730116CAEC77500E14E35394DEF368BB6B1A,
	U3CSpawnU3Ed__0_System_Collections_IEnumerator_get_Current_m036F1B038E494E9989082CB4147D70A2657D4FCC,
	U3CSpawnU3Ed__15__ctor_m8B6555F0727267379D91497503A517A14A935396,
	U3CSpawnU3Ed__15_System_IDisposable_Dispose_m90909754072CA037BC9341EBD318B487BDD7F075,
	U3CSpawnU3Ed__15_MoveNext_m6E58BA2883A14563FA3D03A648694163DD6EDAFD,
	U3CSpawnU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEB6FB3ACA001BB1AEE3F356E8AD6242451DFD8F9,
	U3CSpawnU3Ed__15_System_Collections_IEnumerator_Reset_m199A0A54883A81C9D0395A881146EEE0A7200F2F,
	U3CSpawnU3Ed__15_System_Collections_IEnumerator_get_Current_m26E129A15722B49953843E3835FEFAAB4D29DEF3,
	U3CSpawnU3Ed__13__ctor_mC6D26905014FBE3C109464828A997016EA7CA70D,
	U3CSpawnU3Ed__13_System_IDisposable_Dispose_m1DE303152F8C88637C1358770031EF4826E9D74F,
	U3CSpawnU3Ed__13_MoveNext_m62453ABA4265E79EA3810E0CA0EEB6D0A5586913,
	U3CSpawnU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mECAEF1106886D61093B7A6AD5CCA830D8B8655EA,
	U3CSpawnU3Ed__13_System_Collections_IEnumerator_Reset_mF723AA7FEE5B81A88F02DD1007343BF6D75AC0CB,
	U3CSpawnU3Ed__13_System_Collections_IEnumerator_get_Current_m447841FDAE3F33326E46F2162D478C731F852B1E,
	U3CSpawnU3Ed__4__ctor_m9C4DBA85AE3E02D720C6EAECA102687880E5AA9B,
	U3CSpawnU3Ed__4_System_IDisposable_Dispose_mF994CA2235DD73A8DC295DDEF1A35EC4710CCA91,
	U3CSpawnU3Ed__4_MoveNext_mFC40B23E2AF9FE77CA04A60F45C4AA62D4BF0190,
	U3CSpawnU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5EFD4F9B9380DC9E690993D411F37E0164D5B31F,
	U3CSpawnU3Ed__4_System_Collections_IEnumerator_Reset_m6C86770A44474E359195E84BD69DC698AC95B7D9,
	U3CSpawnU3Ed__4_System_Collections_IEnumerator_get_Current_m760CF586EB4B879F894A560953BE3F08CE62A641,
	U3CU3Ec__DisplayClass40_0__ctor_m54D040DF2C9C1F4592476A00AE5124279AD67B83,
	U3CU3Ec__DisplayClass40_0_U3CReadU3Eb__0_mD7721E04436E917C68064A05FDFFA8B0A3A2D0EE,
	U3CU3Ec__DisplayClass41_0__ctor_mF5D6E4AE83DBCEBCAE8BC7F8DCEA60A7429D2D6F,
	U3CU3Ec__DisplayClass41_0_U3CSaveU3Eb__0_mFA6B6CD79896061D92A00D1B366C07F82056E0FC,
	U3CU3Ec__cctor_mE0CD3F1D9EDE692DAD6F8803F57B3CF7A3A98275,
	U3CU3Ec__ctor_m0CBA45D831CFD996668E9C322AFA8139D5557A35,
	U3CU3Ec_U3CSaveU3Eb__41_1_m4594C3DDAEB569B550368E7C40022ABD28F127BF,
	Stem__ctor_mF215D694E31F6E2732FCCBD26582EE43C861868F,
	U3CRestartAllStemsU3Ed__11__ctor_m3F0E2479A180FB3E399F7C5CF8B71BB15DB66295,
	U3CRestartAllStemsU3Ed__11_System_IDisposable_Dispose_m6FEDE0E02A173C3646F429439283D391270E4B65,
	U3CRestartAllStemsU3Ed__11_MoveNext_m6DF8F0E74AD18C8366DA6CF4B9119E116BF17014,
	U3CRestartAllStemsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50388D880EE7347B31D3B2E552933F6F11D19777,
	U3CRestartAllStemsU3Ed__11_System_Collections_IEnumerator_Reset_m79446E16DFB91914B0E767F34FA23F8C74CDC0DD,
	U3CRestartAllStemsU3Ed__11_System_Collections_IEnumerator_get_Current_mA2D3590BA4EA306951314F5F05C52A31C3A8EA1F,
	U3CU3Ec__cctor_mCCFC65D9286B2E0AF39DBC71206A65923CC5F6D0,
	U3CU3Ec__ctor_m93A659164B518E5301095FCC23FB3268E4818C82,
	U3CU3Ec_U3CLoadDatabaseU3Eb__7_0_m62EE6DB7BEFDEB0E3DE6FA3019E625977825BEB8,
	U3CLoadDatabaseU3Ed__7__ctor_mD72AAC64AA5475D057F79B59997BF597BA5DF227,
	U3CLoadDatabaseU3Ed__7_System_IDisposable_Dispose_mE6CA3308E82B8A002709AC9486224960458478FB,
	U3CLoadDatabaseU3Ed__7_MoveNext_mDAFAB7B45306E59E8795D78B1CA14E18EDE1AA30,
	U3CLoadDatabaseU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48B117B0CD0DDB72351D96529C5A0B78E7866475,
	U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_Reset_mC6A56CA03B68D5D83BDF69977C914EC5F710C119,
	U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_get_Current_mDA20F872592A1AFEC3753B785647F92C12CDE482,
	MultiplierModifier__ctor_mC2A9965E5B6239CB5518850CF1FE7BD9964E6442,
	MultiplierModifier_Invoke_m142E206E1CE972E6281BEFF3BB8540C91D24297D,
	MultiplierModifier_BeginInvoke_m0FBD37607A355FE47037B32494061C72CF9A710E,
	MultiplierModifier_EndInvoke_m650AD2D50FAA43B4D1336AA5D55B33DCFF1DD8CD,
	U3CWaitToStartU3Ed__84__ctor_m3D4B56311A798974B7C2A8E3D20E1A8306DAAF2F,
	U3CWaitToStartU3Ed__84_System_IDisposable_Dispose_mA4BA8648817A41A3ED3B3CD9F1E3CEEAAD368241,
	U3CWaitToStartU3Ed__84_MoveNext_m5B30D083902B234CEFBAB0224F5386BB0D23B00B,
	U3CWaitToStartU3Ed__84_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85238BFAE3597C9261B286DEAB50F3C929290B3B,
	U3CWaitToStartU3Ed__84_System_Collections_IEnumerator_Reset_m9384C8A83680182D7B3994AF38C77A948111E2B8,
	U3CWaitToStartU3Ed__84_System_Collections_IEnumerator_get_Current_mE5C8B43BE08F6CFEE838E38084B2ED4832206958,
	U3CBeginU3Ed__85__ctor_mAB58B7584FAD5B4633771C03EA4FFB71685A423E,
	U3CBeginU3Ed__85_System_IDisposable_Dispose_mD03DCDA26327337D788708EEBD8EBD64A7B241A0,
	U3CBeginU3Ed__85_MoveNext_m79F1010515478A29052BE3337F8D65152AC59E0C,
	U3CBeginU3Ed__85_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B38DEBEF616B175340DB85F7FA19B5A9E091E8F,
	U3CBeginU3Ed__85_System_Collections_IEnumerator_Reset_m4795B9DAC83F02487C6DDCAD0AB9251E36630D12,
	U3CBeginU3Ed__85_System_Collections_IEnumerator_get_Current_m14BA90226D7380E67EB837C5A862853928767C31,
	U3CSpawnNewSegmentU3Ed__93__ctor_mC3946A80D2D4BB2E06F0A60CE828B9473567175F,
	U3CSpawnNewSegmentU3Ed__93_System_IDisposable_Dispose_mAE57C25E300472D1292F1AF22404843AF455CDC0,
	U3CSpawnNewSegmentU3Ed__93_MoveNext_mC2149ED255A04730C3273F120252DF80752AC40A,
	U3CSpawnNewSegmentU3Ed__93_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3F18702C42FFE48A79ABD648500AD6DB065AD63B,
	U3CSpawnNewSegmentU3Ed__93_System_Collections_IEnumerator_Reset_mD50E95DA7F2E82EDD8149A3E491986A675D1802A,
	U3CSpawnNewSegmentU3Ed__93_System_Collections_IEnumerator_get_Current_m188CB9FEF972ECC0872CFF268175B3DFAB15C427,
	U3CSpawnFromAssetReferenceU3Ed__95__ctor_m1C51A408EB6060A7133D3180C664171034AD8EC0,
	U3CSpawnFromAssetReferenceU3Ed__95_System_IDisposable_Dispose_m27A19B08B4619C2D7B4DFAA3FD804C5622C1A55A,
	U3CSpawnFromAssetReferenceU3Ed__95_MoveNext_mD7A3C955C10F3FB61ED4A52D4E43261709BCCCE2,
	U3CSpawnFromAssetReferenceU3Ed__95_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A1C8209672A1660F8736CB5C54B72A727BF2217,
	U3CSpawnFromAssetReferenceU3Ed__95_System_Collections_IEnumerator_Reset_mF48630E55DA0DB51C12C225529808B68EFA8EB4F,
	U3CSpawnFromAssetReferenceU3Ed__95_System_Collections_IEnumerator_get_Current_m8F62B25EAA98A90D4BF4C29D773B99F262E649A2,
	U3CSpawnCoinAndPowerupU3Ed__96__ctor_m6919C148252339ED3E1494B186A1BED7A984C7FE,
	U3CSpawnCoinAndPowerupU3Ed__96_System_IDisposable_Dispose_m87B6CB6FF3D58454AC6347C9A812361448BCBC27,
	U3CSpawnCoinAndPowerupU3Ed__96_MoveNext_m71C0FD407CF0082BA5073EF7AEFA7F9C28F24117,
	U3CSpawnCoinAndPowerupU3Ed__96_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5A5E99D6802DA7A1700EEC345C4AB9394D9B6A76,
	U3CSpawnCoinAndPowerupU3Ed__96_System_Collections_IEnumerator_Reset_mCD39A066C923D8FE2C3DF6DA90383819ACD15967,
	U3CSpawnCoinAndPowerupU3Ed__96_System_Collections_IEnumerator_get_Current_m31B94A4C2C7A97578617509C9D5EDB57B9F69C84,
	U3CU3Ec__cctor_mBC82474B30E70C08AE70F5F08AFB90D32E8432B5,
	U3CU3Ec__ctor_m5BCF54269757977342F977D81BE710097ABF3835,
	U3CU3Ec_U3CToggleOpenGlobalU3Eb__10_1_mB254B4DF2B3869BF477D02D2D2031437DA551D86,
	U3CU3Ec__DisplayClass7_0__ctor_m05555B132C79BB709680B66BCA0A6F6C3A7BFCFE,
	U3CU3Ec__DisplayClass7_0_U3CFillWithMissionU3Eb__0_mC4D2BD24F7DD4637B62F01DBF4DE5ADDBDB8901E,
	U3COpenU3Ed__3__ctor_m8AC977975366A5A4ED5DCAB21A1D37D1B95EB264,
	U3COpenU3Ed__3_System_IDisposable_Dispose_m5F0713DDBBA80E241AE5A97F9DEAC6AE128AF282,
	U3COpenU3Ed__3_MoveNext_mA0B5267ED2F901B552003D82F52EB52C45E2ABC4,
	U3COpenU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F4DB819EEEDD4C7D847BE29D85CA72CFB80E644,
	U3COpenU3Ed__3_System_Collections_IEnumerator_Reset_m6D1892BA8644ABC732A24538492BCDB875868DB7,
	U3COpenU3Ed__3_System_Collections_IEnumerator_get_Current_m365CCE1036E1123F495161146CC9A9C1811E761E,
	U3CU3Ec__DisplayClass3_0__ctor_m133663033521ACF2B05795B3D1500462E4A1A171,
	U3CU3Ec__DisplayClass3_0_U3CLoadedCharacterU3Eb__0_m6C4190FF9A08D5739D136EB186BE5D521DBE21E8,
	U3CU3Ec__DisplayClass4_0__ctor_mBCCDBD57B794712410F8CFA0E221A3F5BCE95B27,
	U3CU3Ec__DisplayClass4_0_U3CLoadedAccessoryU3Eb__0_m792BE0AD145BE93015E671F8403DEE3A97533455,
	U3CU3Ec__DisplayClass4_0_U3CLoadedAccessoryU3Eb__1_mAE4993F243D54606CFD81EC649FF9D28973BB43E,
	U3CU3Ec__DisplayClass4_1__ctor_m238FDECC7B3A4EBC30DD7CFB572F9BAC2055B541,
	U3CU3Ec__DisplayClass4_1_U3CLoadedAccessoryU3Eb__2_mBBDDFE3E60237F318764681E78477A4DF0B59FC5,
	U3CU3Ec__DisplayClass4_1_U3CLoadedAccessoryU3Eb__3_mE262E46DF06A5A42228C952873A883927641358F,
	U3CU3Ec__DisplayClass0_0__ctor_m1C1E71C2DF6ACD2838C4837D1AE9E95A345739F3,
	U3CU3Ec__DisplayClass0_0_U3CPopulateU3Eb__0_mABDB22D3482EF4F518205B3DAE814E8543A32F76,
	U3CU3Ec__DisplayClass0_0_U3CPopulateU3Eb__1_m5D2617F6DCD098784C233765ECD9C464F5F93387,
	U3CU3Ec__DisplayClass0_1__ctor_m1CB636D7C24DAFCBA0AFF03619A3977F681ACA6C,
	U3CU3Ec__DisplayClass0_1_U3CPopulateU3Eb__2_m0F8D346E63094845548D190C437E19219A394EFC,
	U3CU3Ec__DisplayClass1_0__ctor_m1C5F66B53AA34D72838A070FA5B87E02BF1A09AB,
	U3CU3Ec__DisplayClass1_0_U3CPopulateU3Eb__0_m0A519017DE6FB73F1ADD1BD360DEDF1DC99DC255,
	U3CU3Ec__DisplayClass1_0_U3CPopulateU3Eb__1_m4FD0B918A1984D030D47CD2FE1E0053A4008A0DA,
	U3CU3Ec__DisplayClass1_1__ctor_m53BAE85CD75D3D00856358AA1FA4B85532E31274,
	U3CU3Ec__DisplayClass1_1_U3CPopulateU3Eb__2_m7DD837D84E621BE1D55F9E4831229FD8A2A66B37,
	RefreshCallback__ctor_mDDC2F95DC59D10F38717870030EEB4A7C6ED3492,
	RefreshCallback_Invoke_mEDAB6D9D59E5E555B75CF56A26F16CAB2DC9F30E,
	RefreshCallback_BeginInvoke_m841EB1E14E62E07B70EDE9B36D364D8483BCF22C,
	RefreshCallback_EndInvoke_mEB4AA4EBC12515BA2D1DE1D720CB7B03CC99719E,
	U3CU3Ec__DisplayClass0_0__ctor_mD1DC0E5473A0A3A3FC4DDC4F7D0CE75676C0CB72,
	U3CU3Ec__DisplayClass0_0_U3CPopulateU3Eb__0_m02FEB2A833AA2C0330231E305FB66E1876592CE9,
	U3CU3Ec__DisplayClass0_0_U3CPopulateU3Eb__1_mDF92A259600292B30791994D12560529A9C5FEED,
	U3CU3Ec__DisplayClass0_1__ctor_mAF1B9B8D6C26975483ED899B0F2177E6FB941048,
	U3CU3Ec__DisplayClass0_1_U3CPopulateU3Eb__2_m4D155329EAA45674CF33E32044288800E9917CB5,
	U3CLoadAynchronouslyU3Ed__4__ctor_m2C80714EAC810CC120B102341525405571574FFF,
	U3CLoadAynchronouslyU3Ed__4_System_IDisposable_Dispose_m42FC8E64428FC380A50BBA8DCE4D27BA73AB23D6,
	U3CLoadAynchronouslyU3Ed__4_MoveNext_m234DC952C4B2F315969BB84611A7E5104BC3A27C,
	U3CLoadAynchronouslyU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5557547C31F7B97F06DB286DA468D7915B50A649,
	U3CLoadAynchronouslyU3Ed__4_System_Collections_IEnumerator_Reset_m57603CABA0EE1D1465CF65D883D6F5CB7A50BC17,
	U3CLoadAynchronouslyU3Ed__4_System_Collections_IEnumerator_get_Current_m6481BEC178F1D855C17F9B03FB18A3D02F57DFFB,
};
extern void HighscoreEntry_CompareTo_mC9269DA9CFADA1A4989CEFC653F8B5B2D7ADF43B_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x06000126, HighscoreEntry_CompareTo_mC9269DA9CFADA1A4989CEFC653F8B5B2D7ADF43B_AdjustorThunk },
};
static const int32_t s_InvokerIndices[692] = 
{
	2439,
	2901,
	2901,
	2907,
	2842,
	2842,
	2901,
	2901,
	2480,
	2901,
	2452,
	2480,
	2488,
	1976,
	2901,
	4376,
	4353,
	4365,
	4223,
	4353,
	2901,
	4376,
	2828,
	2439,
	2828,
	2439,
	2828,
	2439,
	2842,
	2872,
	2872,
	2901,
	2480,
	2872,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2439,
	2901,
	2452,
	2901,
	4376,
	999,
	2901,
	999,
	2901,
	4376,
	2901,
	2872,
	2881,
	2828,
	2842,
	2828,
	2828,
	2901,
	2175,
	1972,
	1207,
	2452,
	2452,
	2901,
	2901,
	4220,
	2901,
	2901,
	2842,
	2828,
	2828,
	2828,
	2452,
	2901,
	2842,
	2828,
	2828,
	2828,
	2175,
	1972,
	2901,
	1972,
	2842,
	2828,
	2828,
	2828,
	2452,
	1972,
	2452,
	2901,
	1972,
	2842,
	2828,
	2828,
	2828,
	1972,
	2452,
	1865,
	2901,
	1972,
	4353,
	2901,
	4223,
	2901,
	4353,
	2842,
	2901,
	2452,
	2901,
	2901,
	2901,
	2901,
	2452,
	1972,
	2901,
	2452,
	2901,
	2452,
	2452,
	2901,
	2842,
	2901,
	2452,
	2452,
	2842,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2452,
	2452,
	2901,
	2842,
	2901,
	2480,
	2480,
	2480,
	2901,
	2901,
	2901,
	2842,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	4376,
	2452,
	2452,
	2901,
	2842,
	2901,
	2901,
	2439,
	2439,
	2439,
	2842,
	2842,
	2901,
	2901,
	2439,
	2901,
	2452,
	2901,
	2901,
	2901,
	2452,
	2452,
	2175,
	2901,
	2488,
	2452,
	2452,
	2175,
	2901,
	2452,
	2175,
	2452,
	2452,
	2175,
	2901,
	4306,
	4306,
	4306,
	4306,
	4306,
	4306,
	4353,
	4353,
	4353,
	2901,
	2872,
	2480,
	2828,
	2439,
	2842,
	2452,
	2901,
	2439,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2452,
	2901,
	2452,
	2452,
	4047,
	2901,
	2452,
	2901,
	2872,
	2452,
	2452,
	2872,
	2901,
	2828,
	2842,
	2452,
	2452,
	4220,
	2901,
	2901,
	2872,
	2842,
	2828,
	2452,
	2452,
	2901,
	2901,
	2842,
	2828,
	2452,
	2452,
	2901,
	2901,
	2842,
	2828,
	2452,
	2452,
	2901,
	2901,
	2842,
	2828,
	2452,
	2452,
	2901,
	2872,
	2901,
	2842,
	2828,
	2452,
	2452,
	2901,
	1207,
	2901,
	2872,
	2480,
	2901,
	1207,
	2901,
	2901,
	2901,
	2901,
	4376,
	2901,
	1207,
	2901,
	2901,
	1207,
	2901,
	2901,
	2901,
	2901,
	4376,
	1207,
	2901,
	2901,
	2901,
	1862,
	4353,
	2439,
	2439,
	2452,
	2452,
	2452,
	2901,
	2901,
	2452,
	2452,
	2872,
	2452,
	1865,
	1451,
	4376,
	4376,
	2901,
	2901,
	2901,
	4376,
	2901,
	2901,
	2452,
	2452,
	2452,
	2452,
	2480,
	2452,
	2452,
	2452,
	2175,
	2901,
	1537,
	2842,
	1216,
	2452,
	2901,
	2901,
	2901,
	2901,
	2901,
	4353,
	2901,
	4376,
	1451,
	1968,
	2842,
	2488,
	2901,
	2901,
	4353,
	4365,
	4223,
	4353,
	2901,
	4376,
	4353,
	2828,
	2439,
	2881,
	2828,
	2828,
	2881,
	2881,
	2881,
	2881,
	2828,
	2842,
	2842,
	2842,
	2872,
	2872,
	2480,
	2872,
	2480,
	2901,
	2480,
	2901,
	2842,
	2842,
	2901,
	2901,
	2901,
	2901,
	2842,
	2452,
	847,
	1972,
	2439,
	2901,
	4376,
	2881,
	2901,
	1061,
	1061,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2452,
	2901,
	2452,
	2901,
	2901,
	2901,
	2901,
	2901,
	2452,
	2901,
	1542,
	2901,
	2842,
	2901,
	2452,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2452,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2488,
	2488,
	2488,
	2901,
	2901,
	2901,
	1331,
	945,
	1021,
	1009,
	2901,
	2328,
	2901,
	1542,
	2452,
	2901,
	2901,
	1542,
	2452,
	2901,
	4376,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	1542,
	2452,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2901,
	2452,
	2901,
	2901,
	2901,
	2901,
	2452,
	1972,
	2901,
	2901,
	2901,
	2901,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	4376,
	2901,
	2452,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	1539,
	2901,
	1205,
	2452,
	1539,
	2452,
	848,
	2452,
	1539,
	2452,
	848,
	2452,
	4376,
	2901,
	2452,
	2452,
	2452,
	2452,
	2452,
	2901,
	2452,
	2901,
	2452,
	2452,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2901,
	2452,
	2901,
	2452,
	4376,
	2901,
	2452,
	2901,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	4376,
	2901,
	2452,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	1539,
	1865,
	830,
	1876,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	4376,
	2901,
	2452,
	2901,
	2901,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
	2901,
	2328,
	2901,
	2328,
	2328,
	2901,
	2901,
	2901,
	2901,
	2328,
	2901,
	2901,
	2901,
	2901,
	2328,
	2901,
	2901,
	2901,
	1539,
	2901,
	1205,
	2452,
	2901,
	2328,
	2901,
	2901,
	2901,
	2439,
	2901,
	2872,
	2842,
	2901,
	2842,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	692,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
