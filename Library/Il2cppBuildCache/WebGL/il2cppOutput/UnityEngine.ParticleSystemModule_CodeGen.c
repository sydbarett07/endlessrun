﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Color32)
extern void ParticleSystem_Emit_mC489C467AAF3C3721AC3315AF78DC4CE469E7AAC (void);
// 0x00000002 System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/Particle)
extern void ParticleSystem_Emit_mF1E108B9BF7E0094C35CF71870B5B0EA72ABB485 (void);
// 0x00000003 System.Single UnityEngine.ParticleSystem::get_startDelay()
extern void ParticleSystem_get_startDelay_m6661DA2328104EAAF1B3EED1A9AD5BA506B3DC57 (void);
// 0x00000004 System.Void UnityEngine.ParticleSystem::set_startDelay(System.Single)
extern void ParticleSystem_set_startDelay_m260AAD1034FB43187FA18D7D82858F7B12827662 (void);
// 0x00000005 System.Boolean UnityEngine.ParticleSystem::get_loop()
extern void ParticleSystem_get_loop_m7D0ED1B08C7503790FC5FD2863728AD281098AB2 (void);
// 0x00000006 System.Void UnityEngine.ParticleSystem::set_loop(System.Boolean)
extern void ParticleSystem_set_loop_mE7536AE1F42CCD5F8C410D719D3D80571C0054C6 (void);
// 0x00000007 System.Boolean UnityEngine.ParticleSystem::get_playOnAwake()
extern void ParticleSystem_get_playOnAwake_mCC6EB49C6C0400E3CC58024E4AEFD6EB81EBBCB6 (void);
// 0x00000008 System.Void UnityEngine.ParticleSystem::set_playOnAwake(System.Boolean)
extern void ParticleSystem_set_playOnAwake_m9AB94C2B3B742F4E3A1C7EFEBF30CDD33195D21D (void);
// 0x00000009 System.Single UnityEngine.ParticleSystem::get_duration()
extern void ParticleSystem_get_duration_m25C0E6C6B0EC084ED59977966AF2C2EDE529FA44 (void);
// 0x0000000A System.Single UnityEngine.ParticleSystem::get_playbackSpeed()
extern void ParticleSystem_get_playbackSpeed_mA03269D76AA3AE6F149A19CCF12528D7EA384D52 (void);
// 0x0000000B System.Void UnityEngine.ParticleSystem::set_playbackSpeed(System.Single)
extern void ParticleSystem_set_playbackSpeed_mCAAD47BDB2A8F83EDE0FA791331FDDA5E29E488C (void);
// 0x0000000C System.Boolean UnityEngine.ParticleSystem::get_enableEmission()
extern void ParticleSystem_get_enableEmission_m4A8F7E3AD120DC00910D5A20B25B90417BF3EB7B (void);
// 0x0000000D System.Void UnityEngine.ParticleSystem::set_enableEmission(System.Boolean)
extern void ParticleSystem_set_enableEmission_m29DCB9B1C7F2E54DCA3D1A046754ACBEFE162FA9 (void);
// 0x0000000E System.Single UnityEngine.ParticleSystem::get_emissionRate()
extern void ParticleSystem_get_emissionRate_m71A71CD1426FC5DC8A0717DABCE413693C412967 (void);
// 0x0000000F System.Void UnityEngine.ParticleSystem::set_emissionRate(System.Single)
extern void ParticleSystem_set_emissionRate_m62D12CC0B786ACD83632326F50A1082BE21EBA90 (void);
// 0x00000010 System.Single UnityEngine.ParticleSystem::get_startSpeed()
extern void ParticleSystem_get_startSpeed_mC6CEF0642452626DEE0687FE57CE96EE9DC65F41 (void);
// 0x00000011 System.Void UnityEngine.ParticleSystem::set_startSpeed(System.Single)
extern void ParticleSystem_set_startSpeed_m969B0FF3E7392F9E027D40C8BAAFF932171D9280 (void);
// 0x00000012 System.Single UnityEngine.ParticleSystem::get_startSize()
extern void ParticleSystem_get_startSize_m7327DF9701486C5476DB9E3296BD5692E2A7CB72 (void);
// 0x00000013 System.Void UnityEngine.ParticleSystem::set_startSize(System.Single)
extern void ParticleSystem_set_startSize_m2175FEE1F0ABA3F69C2DC4EC4D940E19A7FBEC6D (void);
// 0x00000014 UnityEngine.Color UnityEngine.ParticleSystem::get_startColor()
extern void ParticleSystem_get_startColor_mEA5A4B07850804959DE58004AB65FEBFE5067515 (void);
// 0x00000015 System.Void UnityEngine.ParticleSystem::set_startColor(UnityEngine.Color)
extern void ParticleSystem_set_startColor_m58AE61985D5B25B108FB4A5E05D7DF4A0FFECAEC (void);
// 0x00000016 System.Single UnityEngine.ParticleSystem::get_startRotation()
extern void ParticleSystem_get_startRotation_m19EA2F6770518A7E2F9CB529943E23E083DEC5A0 (void);
// 0x00000017 System.Void UnityEngine.ParticleSystem::set_startRotation(System.Single)
extern void ParticleSystem_set_startRotation_mFF30EA836130E5105E8886B27F6051E87EE7DF49 (void);
// 0x00000018 UnityEngine.Vector3 UnityEngine.ParticleSystem::get_startRotation3D()
extern void ParticleSystem_get_startRotation3D_mA9CDEFD28212D4A67F6D3E840BE3A059C361B82F (void);
// 0x00000019 System.Void UnityEngine.ParticleSystem::set_startRotation3D(UnityEngine.Vector3)
extern void ParticleSystem_set_startRotation3D_m801440D1F8A5B0C8268607D7C6CBB1DBE53602AE (void);
// 0x0000001A System.Single UnityEngine.ParticleSystem::get_startLifetime()
extern void ParticleSystem_get_startLifetime_m48D95DC48DB180862C6D921B08D3BB0969AEBC02 (void);
// 0x0000001B System.Void UnityEngine.ParticleSystem::set_startLifetime(System.Single)
extern void ParticleSystem_set_startLifetime_m3574A8F28C124698F4BBC3CBEE7F638CE6AB2552 (void);
// 0x0000001C System.Single UnityEngine.ParticleSystem::get_gravityModifier()
extern void ParticleSystem_get_gravityModifier_m8E0B14851B17FED84102ECC0A4871151C9DAF553 (void);
// 0x0000001D System.Void UnityEngine.ParticleSystem::set_gravityModifier(System.Single)
extern void ParticleSystem_set_gravityModifier_m0E1C7D97DD78C58931FB3E502B43920E4A810F1F (void);
// 0x0000001E System.Int32 UnityEngine.ParticleSystem::get_maxParticles()
extern void ParticleSystem_get_maxParticles_mAF21E5F4E07CC95B46F34FA22E68DE7E1714D596 (void);
// 0x0000001F System.Void UnityEngine.ParticleSystem::set_maxParticles(System.Int32)
extern void ParticleSystem_set_maxParticles_m3B2C177B338CAC418232A25E5F3C0B588B5E4ABA (void);
// 0x00000020 UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem::get_simulationSpace()
extern void ParticleSystem_get_simulationSpace_mF6EED4B63164572C99E8EC7F315108BEC7CBB23D (void);
// 0x00000021 System.Void UnityEngine.ParticleSystem::set_simulationSpace(UnityEngine.ParticleSystemSimulationSpace)
extern void ParticleSystem_set_simulationSpace_mF4A5FBCDCC9F63BB28545FF4FF68BB77C1F6D04E (void);
// 0x00000022 UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem::get_scalingMode()
extern void ParticleSystem_get_scalingMode_mF2EA9887C71A595E98187564E41D94A710504E63 (void);
// 0x00000023 System.Void UnityEngine.ParticleSystem::set_scalingMode(UnityEngine.ParticleSystemScalingMode)
extern void ParticleSystem_set_scalingMode_m4B399177269F81152FAF667684FE84EB44E61E0F (void);
// 0x00000024 System.Boolean UnityEngine.ParticleSystem::get_automaticCullingEnabled()
extern void ParticleSystem_get_automaticCullingEnabled_m3E0F822B082E66D6AB5813BDF96F48E0B54971C4 (void);
// 0x00000025 System.Boolean UnityEngine.ParticleSystem::get_isPlaying()
extern void ParticleSystem_get_isPlaying_m36FD03CBF99EE4C243B01F37D77CB6B1CFA526BA (void);
// 0x00000026 System.Boolean UnityEngine.ParticleSystem::get_isEmitting()
extern void ParticleSystem_get_isEmitting_m1641C8124AE31D556E65164BC5A085C523FE4033 (void);
// 0x00000027 System.Boolean UnityEngine.ParticleSystem::get_isStopped()
extern void ParticleSystem_get_isStopped_mE50B27655AFE6F178C9B3B99F18968A8964D93F8 (void);
// 0x00000028 System.Boolean UnityEngine.ParticleSystem::get_isPaused()
extern void ParticleSystem_get_isPaused_mB8AEA85594606CC49078B39F05ED8BA5C8199C0E (void);
// 0x00000029 System.Int32 UnityEngine.ParticleSystem::get_particleCount()
extern void ParticleSystem_get_particleCount_mAD1793317BD6BBCB0C7A7853A9E82D19703B0A52 (void);
// 0x0000002A System.Single UnityEngine.ParticleSystem::get_time()
extern void ParticleSystem_get_time_m85B483199DA7D9675ABCB194F2CD431C53D4B3B1 (void);
// 0x0000002B System.Void UnityEngine.ParticleSystem::set_time(System.Single)
extern void ParticleSystem_set_time_m014BD2C09A14861186C79BA3FD12E2AC8DE90544 (void);
// 0x0000002C System.UInt32 UnityEngine.ParticleSystem::get_randomSeed()
extern void ParticleSystem_get_randomSeed_m70650D178DEADC42E19B6B22ECE252AE5434F030 (void);
// 0x0000002D System.Void UnityEngine.ParticleSystem::set_randomSeed(System.UInt32)
extern void ParticleSystem_set_randomSeed_m327828D83321B6F734D52EB91E64A45228040EC1 (void);
// 0x0000002E System.Boolean UnityEngine.ParticleSystem::get_useAutoRandomSeed()
extern void ParticleSystem_get_useAutoRandomSeed_m1129F9BC54087CCF6EFE372E9182F7AD298877ED (void);
// 0x0000002F System.Void UnityEngine.ParticleSystem::set_useAutoRandomSeed(System.Boolean)
extern void ParticleSystem_set_useAutoRandomSeed_m1A1B3221AD7BAFF3AA0EB8820D0D6C58BBC2831B (void);
// 0x00000030 System.Boolean UnityEngine.ParticleSystem::get_proceduralSimulationSupported()
extern void ParticleSystem_get_proceduralSimulationSupported_mEF4AEBA182D7DD12ABB6414971A2365236A80A50 (void);
// 0x00000031 System.Single UnityEngine.ParticleSystem::GetParticleCurrentSize(UnityEngine.ParticleSystem/Particle&)
extern void ParticleSystem_GetParticleCurrentSize_mE561E5E2E69A6C357C642444C25AA794833A06BE (void);
// 0x00000032 UnityEngine.Vector3 UnityEngine.ParticleSystem::GetParticleCurrentSize3D(UnityEngine.ParticleSystem/Particle&)
extern void ParticleSystem_GetParticleCurrentSize3D_m989175446F4C6E99635AD8E1FA33BB1146978A9F (void);
// 0x00000033 UnityEngine.Color32 UnityEngine.ParticleSystem::GetParticleCurrentColor(UnityEngine.ParticleSystem/Particle&)
extern void ParticleSystem_GetParticleCurrentColor_mBFD3E4B3A3639F1F59D60C8079F36A9A51E03053 (void);
// 0x00000034 System.Int32 UnityEngine.ParticleSystem::GetParticleMeshIndex(UnityEngine.ParticleSystem/Particle&)
extern void ParticleSystem_GetParticleMeshIndex_m9CBB1891C113D91AA1F0C390EFCA502E1D22C648 (void);
// 0x00000035 System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32,System.Int32)
extern void ParticleSystem_SetParticles_m79867147E65742BE1E1ADF0222C090BD6AF1E20A (void);
// 0x00000036 System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
extern void ParticleSystem_SetParticles_m0658B777D1C6DDA7D244607AC55D5225774CEBFA (void);
// 0x00000037 System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[])
extern void ParticleSystem_SetParticles_m1693B5469E364598DDEAEB1B48724C203E743668 (void);
// 0x00000038 System.Void UnityEngine.ParticleSystem::SetParticlesWithNativeArray(System.IntPtr,System.Int32,System.Int32,System.Int32)
extern void ParticleSystem_SetParticlesWithNativeArray_m163F32606AC234C018E081A3522CCF57C8A2A4FC (void);
// 0x00000039 System.Void UnityEngine.ParticleSystem::SetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>,System.Int32,System.Int32)
extern void ParticleSystem_SetParticles_m380593909B2E9DBB77D8DE09E47C1D0DCCD6ACC2 (void);
// 0x0000003A System.Void UnityEngine.ParticleSystem::SetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>,System.Int32)
extern void ParticleSystem_SetParticles_m648CE8AF7118C73FF5E1D0419E7F6BA52478C13E (void);
// 0x0000003B System.Void UnityEngine.ParticleSystem::SetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>)
extern void ParticleSystem_SetParticles_m75061A9FC1FBA11FBFF903B64762D0B60118F2B9 (void);
// 0x0000003C System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32,System.Int32)
extern void ParticleSystem_GetParticles_mC310C32157EA281ECDC320B63B1E43ED5F6292B3 (void);
// 0x0000003D System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
extern void ParticleSystem_GetParticles_mF6FC6048609DD24AF7F1B8890C44AEC480BDFDEA (void);
// 0x0000003E System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[])
extern void ParticleSystem_GetParticles_mAE8894E2B022EE009C6DDB1390AB331E7D40A344 (void);
// 0x0000003F System.Int32 UnityEngine.ParticleSystem::GetParticlesWithNativeArray(System.IntPtr,System.Int32,System.Int32,System.Int32)
extern void ParticleSystem_GetParticlesWithNativeArray_m4303397CB3084BE1158D854929FE40CA0A6E5253 (void);
// 0x00000040 System.Int32 UnityEngine.ParticleSystem::GetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>,System.Int32,System.Int32)
extern void ParticleSystem_GetParticles_mCD391A836273B8B41035A9F06D5D7ABA83968A95 (void);
// 0x00000041 System.Int32 UnityEngine.ParticleSystem::GetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>,System.Int32)
extern void ParticleSystem_GetParticles_m870578C1C1E37DFF7BC112662BAE81F3CA7AA411 (void);
// 0x00000042 System.Int32 UnityEngine.ParticleSystem::GetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>)
extern void ParticleSystem_GetParticles_mE37AF218E5BF5980DC625491EF3D52756F126BB8 (void);
// 0x00000043 System.Void UnityEngine.ParticleSystem::SetCustomParticleData(System.Collections.Generic.List`1<UnityEngine.Vector4>,UnityEngine.ParticleSystemCustomData)
extern void ParticleSystem_SetCustomParticleData_m0304D334E835ECDA56DBFFA873AA75812143D543 (void);
// 0x00000044 System.Int32 UnityEngine.ParticleSystem::GetCustomParticleData(System.Collections.Generic.List`1<UnityEngine.Vector4>,UnityEngine.ParticleSystemCustomData)
extern void ParticleSystem_GetCustomParticleData_m7EC7DF1370D12E9A5825D8BE1C249672C4794B6B (void);
// 0x00000045 UnityEngine.ParticleSystem/PlaybackState UnityEngine.ParticleSystem::GetPlaybackState()
extern void ParticleSystem_GetPlaybackState_m94CFA6AAEF043DBDEE0C6DC3833B32639AA3BA63 (void);
// 0x00000046 System.Void UnityEngine.ParticleSystem::SetPlaybackState(UnityEngine.ParticleSystem/PlaybackState)
extern void ParticleSystem_SetPlaybackState_m9BE2B963312B05416B540B897C21BAADFCE76E59 (void);
// 0x00000047 System.Void UnityEngine.ParticleSystem::GetTrailDataInternal(UnityEngine.ParticleSystem/Trails&)
extern void ParticleSystem_GetTrailDataInternal_m03273FD85CB69A3D816E91E7D39AAAF58F5BCF78 (void);
// 0x00000048 UnityEngine.ParticleSystem/Trails UnityEngine.ParticleSystem::GetTrails()
extern void ParticleSystem_GetTrails_m99FE1513C3114EA0D1FA05F10B456A8CE6F2610B (void);
// 0x00000049 System.Void UnityEngine.ParticleSystem::SetTrails(UnityEngine.ParticleSystem/Trails)
extern void ParticleSystem_SetTrails_mD865F6A447580EDDCC2568FD900C6027E9F89E66 (void);
// 0x0000004A System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)
extern void ParticleSystem_Simulate_m68A0105281A8BC3F4E9242181586483BA7B796A3 (void);
// 0x0000004B System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean)
extern void ParticleSystem_Simulate_mC2F2E060D7CE94D4936BA995C49827231DF5F1F8 (void);
// 0x0000004C System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean)
extern void ParticleSystem_Simulate_m36BAFFACB58953B2A46AA0B86E1F59C0FD6CD9C7 (void);
// 0x0000004D System.Void UnityEngine.ParticleSystem::Simulate(System.Single)
extern void ParticleSystem_Simulate_m2628543E52E4D8D90A7B592F9C6222626F340F60 (void);
// 0x0000004E System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern void ParticleSystem_Play_m97D70BAF373265E633ACD91833E56D981B19958A (void);
// 0x0000004F System.Void UnityEngine.ParticleSystem::Play()
extern void ParticleSystem_Play_m28D27CC4CDC1D93195C75647E6F6DAECF8B6BC50 (void);
// 0x00000050 System.Void UnityEngine.ParticleSystem::Pause(System.Boolean)
extern void ParticleSystem_Pause_m116581497C92159D3210C733917CD2515399A0CD (void);
// 0x00000051 System.Void UnityEngine.ParticleSystem::Pause()
extern void ParticleSystem_Pause_mA5AE4D5A290E9DD75A0572738CB0910D6A7E2121 (void);
// 0x00000052 System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern void ParticleSystem_Stop_m275B200BC21580C60987EC8FC8B2DD0FEB365C1A (void);
// 0x00000053 System.Void UnityEngine.ParticleSystem::Stop(System.Boolean)
extern void ParticleSystem_Stop_m5EC8B81A8BD1F8C90729E766789EE5D8D4EC64FB (void);
// 0x00000054 System.Void UnityEngine.ParticleSystem::Stop()
extern void ParticleSystem_Stop_m8CBF9268DE7B5A40952B4977462B857B5F5AFB9D (void);
// 0x00000055 System.Void UnityEngine.ParticleSystem::Clear(System.Boolean)
extern void ParticleSystem_Clear_m9E873134B8055ACFEE9DC66F7E4FABE178E27057 (void);
// 0x00000056 System.Void UnityEngine.ParticleSystem::Clear()
extern void ParticleSystem_Clear_mD8C9DCD1267F221B0546E4B9B55DBD9354893797 (void);
// 0x00000057 System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
extern void ParticleSystem_IsAlive_m43E2E84732EBDF5C4C9FD6575051A2A2B135BCF9 (void);
// 0x00000058 System.Boolean UnityEngine.ParticleSystem::IsAlive()
extern void ParticleSystem_IsAlive_m59D28AC79A1A5FD1D97523D83D687ACFA2510198 (void);
// 0x00000059 System.Void UnityEngine.ParticleSystem::Emit(System.Int32)
extern void ParticleSystem_Emit_m07EF0D2DA84EB04814DA7EE6B8618B008DE75F28 (void);
// 0x0000005A System.Void UnityEngine.ParticleSystem::Emit_Internal(System.Int32)
extern void ParticleSystem_Emit_Internal_m7C72C31F7F4875B54B00E255A450B045A4449646 (void);
// 0x0000005B System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/EmitParams,System.Int32)
extern void ParticleSystem_Emit_m1598252E2EF701A5010EFA395A87368495E9F9F7 (void);
// 0x0000005C System.Void UnityEngine.ParticleSystem::EmitOld_Internal(UnityEngine.ParticleSystem/Particle&)
extern void ParticleSystem_EmitOld_Internal_m4F094DC523986298D5626F0F3F2335DFF596C993 (void);
// 0x0000005D System.Void UnityEngine.ParticleSystem::TriggerSubEmitter(System.Int32)
extern void ParticleSystem_TriggerSubEmitter_m9D4541DB830D5C80CD4BF88C3C8B8A8A0C4DFBFA (void);
// 0x0000005E System.Void UnityEngine.ParticleSystem::TriggerSubEmitter(System.Int32,UnityEngine.ParticleSystem/Particle&)
extern void ParticleSystem_TriggerSubEmitter_m54FED6396579982E9C0AF774853E9A2290379A37 (void);
// 0x0000005F System.Void UnityEngine.ParticleSystem::TriggerSubEmitterForParticle(System.Int32,UnityEngine.ParticleSystem/Particle)
extern void ParticleSystem_TriggerSubEmitterForParticle_mCC9F7D09CA9D8DB5576389ABCFC287A21952003A (void);
// 0x00000060 System.Void UnityEngine.ParticleSystem::TriggerSubEmitter(System.Int32,System.Collections.Generic.List`1<UnityEngine.ParticleSystem/Particle>)
extern void ParticleSystem_TriggerSubEmitter_mBEB9C15312BB6AD76AF9B42B2CE184B7149F4C98 (void);
// 0x00000061 System.Void UnityEngine.ParticleSystem::ResetPreMappedBufferMemory()
extern void ParticleSystem_ResetPreMappedBufferMemory_m1B0AEE23A61FA29580513BD4258B263C4F7FAAC3 (void);
// 0x00000062 System.Void UnityEngine.ParticleSystem::SetMaximumPreMappedBufferCounts(System.Int32,System.Int32)
extern void ParticleSystem_SetMaximumPreMappedBufferCounts_m483102DE3EFE86F3E6721C16942E04130B3CAE3F (void);
// 0x00000063 System.Void UnityEngine.ParticleSystem::AllocateAxisOfRotationAttribute()
extern void ParticleSystem_AllocateAxisOfRotationAttribute_mDE6232FD099AD8F1F6F0DF3726C1C32E4729E1D5 (void);
// 0x00000064 System.Void UnityEngine.ParticleSystem::AllocateMeshIndexAttribute()
extern void ParticleSystem_AllocateMeshIndexAttribute_m25F7096688B776F1CAF7F8F6D6F6222B41AF9E18 (void);
// 0x00000065 System.Void UnityEngine.ParticleSystem::AllocateCustomDataAttribute(UnityEngine.ParticleSystemCustomData)
extern void ParticleSystem_AllocateCustomDataAttribute_mAB843E372C9D175730E0730DB54FB34AF7A1DC7F (void);
// 0x00000066 System.Void* UnityEngine.ParticleSystem::GetManagedJobData()
extern void ParticleSystem_GetManagedJobData_mAE6749636F4F3A7EBD7775AE4B8E9BFD24C2CF95 (void);
// 0x00000067 Unity.Jobs.JobHandle UnityEngine.ParticleSystem::GetManagedJobHandle()
extern void ParticleSystem_GetManagedJobHandle_m63494F9028CE37DDF08822AFF19828A2E0A06C6E (void);
// 0x00000068 System.Void UnityEngine.ParticleSystem::SetManagedJobHandle(Unity.Jobs.JobHandle)
extern void ParticleSystem_SetManagedJobHandle_mFD5D834CE36338E182A246668AD58E3CD09A6608 (void);
// 0x00000069 Unity.Jobs.JobHandle UnityEngine.ParticleSystem::ScheduleManagedJob(Unity.Jobs.LowLevel.Unsafe.JobsUtility/JobScheduleParameters&,System.Void*)
extern void ParticleSystem_ScheduleManagedJob_m7B340DC400DAAD69AFC906EA0BAD0D7C3459CB89 (void);
// 0x0000006A System.Void UnityEngine.ParticleSystem::CopyManagedJobData(System.Void*,UnityEngine.ParticleSystemJobs.NativeParticleData&)
extern void ParticleSystem_CopyManagedJobData_mF72E9C4B5DE2228B29D4AB03ADFC3499BA009D95 (void);
// 0x0000006B UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
extern void ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0 (void);
// 0x0000006C UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
extern void ParticleSystem_get_emission_m0BAA1EDF775A8712DA7D62BF3C42A2B1A6C96CA1 (void);
// 0x0000006D UnityEngine.ParticleSystem/ShapeModule UnityEngine.ParticleSystem::get_shape()
extern void ParticleSystem_get_shape_m986023201B5140A525EF34F81DAAF1866D889052 (void);
// 0x0000006E UnityEngine.ParticleSystem/VelocityOverLifetimeModule UnityEngine.ParticleSystem::get_velocityOverLifetime()
extern void ParticleSystem_get_velocityOverLifetime_mA0FEC4CD424A882B27AEBDDB06CBFA61951C2868 (void);
// 0x0000006F UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule UnityEngine.ParticleSystem::get_limitVelocityOverLifetime()
extern void ParticleSystem_get_limitVelocityOverLifetime_m25ABE96DD9F88188874A538AA4992A72EC4AE394 (void);
// 0x00000070 UnityEngine.ParticleSystem/InheritVelocityModule UnityEngine.ParticleSystem::get_inheritVelocity()
extern void ParticleSystem_get_inheritVelocity_mE8C58E2805CB8A7C39F39D7ECE0F3978EAEDBC0A (void);
// 0x00000071 UnityEngine.ParticleSystem/LifetimeByEmitterSpeedModule UnityEngine.ParticleSystem::get_lifetimeByEmitterSpeed()
extern void ParticleSystem_get_lifetimeByEmitterSpeed_m4901408692AC775C037D2D16F55810D9A574BB7B (void);
// 0x00000072 UnityEngine.ParticleSystem/ForceOverLifetimeModule UnityEngine.ParticleSystem::get_forceOverLifetime()
extern void ParticleSystem_get_forceOverLifetime_m044BAE00399D6EB3369C001237E7D8C11BCA3874 (void);
// 0x00000073 UnityEngine.ParticleSystem/ColorOverLifetimeModule UnityEngine.ParticleSystem::get_colorOverLifetime()
extern void ParticleSystem_get_colorOverLifetime_m79598BD17DFAC4CA4C0CAFEA363BA9212000FC31 (void);
// 0x00000074 UnityEngine.ParticleSystem/ColorBySpeedModule UnityEngine.ParticleSystem::get_colorBySpeed()
extern void ParticleSystem_get_colorBySpeed_mF7C88FB024568B5DB6D06DE5C45965FC28633520 (void);
// 0x00000075 UnityEngine.ParticleSystem/SizeOverLifetimeModule UnityEngine.ParticleSystem::get_sizeOverLifetime()
extern void ParticleSystem_get_sizeOverLifetime_m4A532D865E6EA79D745D8C4792CCA4072EC7AA05 (void);
// 0x00000076 UnityEngine.ParticleSystem/SizeBySpeedModule UnityEngine.ParticleSystem::get_sizeBySpeed()
extern void ParticleSystem_get_sizeBySpeed_m0D7FF6F71988E4A1ACF8DD2257E06F5CB6580439 (void);
// 0x00000077 UnityEngine.ParticleSystem/RotationOverLifetimeModule UnityEngine.ParticleSystem::get_rotationOverLifetime()
extern void ParticleSystem_get_rotationOverLifetime_mF5F527E0112C591D297C13CD60A7B888B98E9DEF (void);
// 0x00000078 UnityEngine.ParticleSystem/RotationBySpeedModule UnityEngine.ParticleSystem::get_rotationBySpeed()
extern void ParticleSystem_get_rotationBySpeed_m2ED46A54093DDB496DC8148D54FCB01D0BC6FE2B (void);
// 0x00000079 UnityEngine.ParticleSystem/ExternalForcesModule UnityEngine.ParticleSystem::get_externalForces()
extern void ParticleSystem_get_externalForces_mA09E3DE23F158D76C503A32C3DECBF81FE5FE7E9 (void);
// 0x0000007A UnityEngine.ParticleSystem/NoiseModule UnityEngine.ParticleSystem::get_noise()
extern void ParticleSystem_get_noise_m4059B8305FDEEE3FDE2DA67DBA2B56793D280EF8 (void);
// 0x0000007B UnityEngine.ParticleSystem/CollisionModule UnityEngine.ParticleSystem::get_collision()
extern void ParticleSystem_get_collision_m1914663D5844842893E1F33CF4BFA1D5B3DF4AC1 (void);
// 0x0000007C UnityEngine.ParticleSystem/TriggerModule UnityEngine.ParticleSystem::get_trigger()
extern void ParticleSystem_get_trigger_m0CB1A47823806DA2671918708FEC768C754B5120 (void);
// 0x0000007D UnityEngine.ParticleSystem/SubEmittersModule UnityEngine.ParticleSystem::get_subEmitters()
extern void ParticleSystem_get_subEmitters_mCFE3CAEF76ADEAF3754210EC731A54A9DA836428 (void);
// 0x0000007E UnityEngine.ParticleSystem/TextureSheetAnimationModule UnityEngine.ParticleSystem::get_textureSheetAnimation()
extern void ParticleSystem_get_textureSheetAnimation_mE7D4FF28B018DD7BB94904CE014F5FD56E53AA90 (void);
// 0x0000007F UnityEngine.ParticleSystem/LightsModule UnityEngine.ParticleSystem::get_lights()
extern void ParticleSystem_get_lights_mBAA747B7858C2C39BF4D5502D3026FDA6A9DCE52 (void);
// 0x00000080 UnityEngine.ParticleSystem/TrailModule UnityEngine.ParticleSystem::get_trails()
extern void ParticleSystem_get_trails_m9514134A5A05CCDEF2C901BC0D60FC287959414C (void);
// 0x00000081 UnityEngine.ParticleSystem/CustomDataModule UnityEngine.ParticleSystem::get_customData()
extern void ParticleSystem_get_customData_m7112FE08433F44A3425142B2D4862F8CD889A913 (void);
// 0x00000082 System.Void UnityEngine.ParticleSystem::.ctor()
extern void ParticleSystem__ctor_mEC46AD331B88935F1CFBAA282FD480C53E55ACDC (void);
// 0x00000083 System.Void UnityEngine.ParticleSystem::GetParticleCurrentSize3D_Injected(UnityEngine.ParticleSystem/Particle&,UnityEngine.Vector3&)
extern void ParticleSystem_GetParticleCurrentSize3D_Injected_mE1D7D2457CDC32E35F9D3E06721D773667F2A966 (void);
// 0x00000084 System.Void UnityEngine.ParticleSystem::GetParticleCurrentColor_Injected(UnityEngine.ParticleSystem/Particle&,UnityEngine.Color32&)
extern void ParticleSystem_GetParticleCurrentColor_Injected_m98C468104D268559CF2F06F78F970F1814F7A587 (void);
// 0x00000085 System.Void UnityEngine.ParticleSystem::GetPlaybackState_Injected(UnityEngine.ParticleSystem/PlaybackState&)
extern void ParticleSystem_GetPlaybackState_Injected_m6D3692AF2279B0B9C9FDFF01A9B0D4B21AD19C4D (void);
// 0x00000086 System.Void UnityEngine.ParticleSystem::SetPlaybackState_Injected(UnityEngine.ParticleSystem/PlaybackState&)
extern void ParticleSystem_SetPlaybackState_Injected_m8555985301715DFAAE85761592EEFE881B32EBFC (void);
// 0x00000087 System.Void UnityEngine.ParticleSystem::SetTrails_Injected(UnityEngine.ParticleSystem/Trails&)
extern void ParticleSystem_SetTrails_Injected_m2B3F93DBAD65DB5A22DC136395BA9D329106BC92 (void);
// 0x00000088 System.Void UnityEngine.ParticleSystem::Emit_Injected(UnityEngine.ParticleSystem/EmitParams&,System.Int32)
extern void ParticleSystem_Emit_Injected_mC31E36D577A2D3135436438BFC27B6C76D9ADC72 (void);
// 0x00000089 System.Void UnityEngine.ParticleSystem::TriggerSubEmitterForParticle_Injected(System.Int32,UnityEngine.ParticleSystem/Particle&)
extern void ParticleSystem_TriggerSubEmitterForParticle_Injected_mB9BC0EBDC5CC0D5C392B7287351A8D85337FBBE0 (void);
// 0x0000008A System.Void UnityEngine.ParticleSystem::GetManagedJobHandle_Injected(Unity.Jobs.JobHandle&)
extern void ParticleSystem_GetManagedJobHandle_Injected_m294A930F43B3561DDE1ABD8512F7773C3D7B4318 (void);
// 0x0000008B System.Void UnityEngine.ParticleSystem::SetManagedJobHandle_Injected(Unity.Jobs.JobHandle&)
extern void ParticleSystem_SetManagedJobHandle_Injected_m58056683B09174D75E69BFF45F191CB91542617C (void);
// 0x0000008C System.Void UnityEngine.ParticleSystem::ScheduleManagedJob_Injected(Unity.Jobs.LowLevel.Unsafe.JobsUtility/JobScheduleParameters&,System.Void*,Unity.Jobs.JobHandle&)
extern void ParticleSystem_ScheduleManagedJob_Injected_m2C68F3306AA63CBE2D5F1D2FB6A6571B031255A9 (void);
// 0x0000008D System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern void MainModule__ctor_m34F626B568C6D3C80A036409049761C8316E6A76 (void);
// 0x0000008E System.Single UnityEngine.ParticleSystem/MainModule::get_duration()
extern void MainModule_get_duration_mD516595EFDD66C83A727BDD7EF495069B4EB6132 (void);
// 0x0000008F System.Boolean UnityEngine.ParticleSystem/MainModule::get_loop()
extern void MainModule_get_loop_mE45A1A4A1DF6E2590B7B13A73A9A95197DA346F8 (void);
// 0x00000090 System.Void UnityEngine.ParticleSystem/MainModule::set_loop(System.Boolean)
extern void MainModule_set_loop_m9F43A092EFABBE92662DF9EDA73AF8D9C72DABB2 (void);
// 0x00000091 System.Single UnityEngine.ParticleSystem/MainModule::get_startDelayMultiplier()
extern void MainModule_get_startDelayMultiplier_mC38890C8853463BB25F031BB8884F3D8B0AF581E (void);
// 0x00000092 System.Void UnityEngine.ParticleSystem/MainModule::set_startDelayMultiplier(System.Single)
extern void MainModule_set_startDelayMultiplier_m23FC7BAFE2DCF940C398D6BE53606E23E1715464 (void);
// 0x00000093 System.Single UnityEngine.ParticleSystem/MainModule::get_startLifetimeMultiplier()
extern void MainModule_get_startLifetimeMultiplier_m8D2F788F9A1F5768E64F4511F2158E3BFAEB474D (void);
// 0x00000094 System.Void UnityEngine.ParticleSystem/MainModule::set_startLifetimeMultiplier(System.Single)
extern void MainModule_set_startLifetimeMultiplier_m219938FDA9324C185D998815836B50FEE3AC0D8D (void);
// 0x00000095 System.Single UnityEngine.ParticleSystem/MainModule::get_startSpeedMultiplier()
extern void MainModule_get_startSpeedMultiplier_mE165D2B967AD8A8827D07DBD694F1C7167C80336 (void);
// 0x00000096 System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeedMultiplier(System.Single)
extern void MainModule_set_startSpeedMultiplier_mA3C6B512351C3BFA8E64B40936B3353845A637F2 (void);
// 0x00000097 System.Single UnityEngine.ParticleSystem/MainModule::get_startSizeMultiplier()
extern void MainModule_get_startSizeMultiplier_m820351EE78DF731C1AFA8309ECB5ADF3E1166279 (void);
// 0x00000098 System.Void UnityEngine.ParticleSystem/MainModule::set_startSizeMultiplier(System.Single)
extern void MainModule_set_startSizeMultiplier_m231F400517E7F96EFF7C3C31E59B7B8DD0A1DB64 (void);
// 0x00000099 System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationMultiplier()
extern void MainModule_get_startRotationMultiplier_m72C5648E64889B162553000EF3B7347F47B1F84A (void);
// 0x0000009A System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationMultiplier(System.Single)
extern void MainModule_set_startRotationMultiplier_mB7085FB7FF45BDD4C71E08226C6D5F5635CB47EE (void);
// 0x0000009B System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationXMultiplier()
extern void MainModule_get_startRotationXMultiplier_m15710A32A7861DAF1A0CD55DB1835EC22617F987 (void);
// 0x0000009C System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationXMultiplier(System.Single)
extern void MainModule_set_startRotationXMultiplier_mA489325EDA3D644481BFF44376627B0B6EFB81CD (void);
// 0x0000009D System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationYMultiplier()
extern void MainModule_get_startRotationYMultiplier_mC9C0B9123DDF9D37EF3E5291164E16AB95308011 (void);
// 0x0000009E System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationYMultiplier(System.Single)
extern void MainModule_set_startRotationYMultiplier_m45DC00CC3E94A7CFCEA522F105F93BCE1B727639 (void);
// 0x0000009F System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationZMultiplier()
extern void MainModule_get_startRotationZMultiplier_m736451211917A59B78A3B444963D305BD377898D (void);
// 0x000000A0 System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationZMultiplier(System.Single)
extern void MainModule_set_startRotationZMultiplier_m9D604421CED73B2B110158F1FAF955832FB9A6D5 (void);
// 0x000000A1 UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MainModule::get_startColor()
extern void MainModule_get_startColor_mC8BAD25B55ACF9586BAFEECF047CCE5268123DD4 (void);
// 0x000000A2 System.Void UnityEngine.ParticleSystem/MainModule::set_startColor(UnityEngine.ParticleSystem/MinMaxGradient)
extern void MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42 (void);
// 0x000000A3 System.Single UnityEngine.ParticleSystem/MainModule::get_gravityModifierMultiplier()
extern void MainModule_get_gravityModifierMultiplier_m875B65CAEE457EB12A5CD763A5AA311DE014629A (void);
// 0x000000A4 System.Void UnityEngine.ParticleSystem/MainModule::set_gravityModifierMultiplier(System.Single)
extern void MainModule_set_gravityModifierMultiplier_mBC64B6D0DEF548A94D0C0303855F43931A1F5684 (void);
// 0x000000A5 UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem/MainModule::get_simulationSpace()
extern void MainModule_get_simulationSpace_mD08447602DF2E2AC9790D900A1BD04AB8D4FD0A2 (void);
// 0x000000A6 System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpace(UnityEngine.ParticleSystemSimulationSpace)
extern void MainModule_set_simulationSpace_m59583EC5A1D9D64EC8C0CCE275CE6669FC8E268F (void);
// 0x000000A7 System.Single UnityEngine.ParticleSystem/MainModule::get_simulationSpeed()
extern void MainModule_get_simulationSpeed_m7BCFDA4BD65598CE9CEF34A029E4C03DAB4A3C53 (void);
// 0x000000A8 System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpeed(System.Single)
extern void MainModule_set_simulationSpeed_m9EA9E4BE76B372EF9674568810BF4F5B403B2833 (void);
// 0x000000A9 UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem/MainModule::get_scalingMode()
extern void MainModule_get_scalingMode_m2FEDBC7969A4BD0071B9467AFFD1B98EE64A9C51 (void);
// 0x000000AA System.Void UnityEngine.ParticleSystem/MainModule::set_scalingMode(UnityEngine.ParticleSystemScalingMode)
extern void MainModule_set_scalingMode_m0992C03429ABFBD1021009BF669BF362705D4E23 (void);
// 0x000000AB System.Boolean UnityEngine.ParticleSystem/MainModule::get_playOnAwake()
extern void MainModule_get_playOnAwake_m6E07F3ACCC2797500E70AF4A4A33C5C8425A52D5 (void);
// 0x000000AC System.Void UnityEngine.ParticleSystem/MainModule::set_playOnAwake(System.Boolean)
extern void MainModule_set_playOnAwake_m85E164C05D5C39B0B3C6139BE360BAB41B0D7FCF (void);
// 0x000000AD System.Int32 UnityEngine.ParticleSystem/MainModule::get_maxParticles()
extern void MainModule_get_maxParticles_m4A95D2105C1B414331F15EF86ADBC23CC6C8DD35 (void);
// 0x000000AE System.Void UnityEngine.ParticleSystem/MainModule::set_maxParticles(System.Int32)
extern void MainModule_set_maxParticles_m6FD14B44787B44D38DEDAE4FCBE9596A86A0460D (void);
// 0x000000AF System.Single UnityEngine.ParticleSystem/MainModule::get_duration_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_duration_Injected_mBD7B5FEFFD26DB833052D35A3AC492699B4BEE41 (void);
// 0x000000B0 System.Boolean UnityEngine.ParticleSystem/MainModule::get_loop_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_loop_Injected_mD8FD195A3CF5008FD319636A3945DE2F4F0EEBD2 (void);
// 0x000000B1 System.Void UnityEngine.ParticleSystem/MainModule::set_loop_Injected(UnityEngine.ParticleSystem/MainModule&,System.Boolean)
extern void MainModule_set_loop_Injected_m064E3166CAA11D0842F105E473A231C0C15DE1B3 (void);
// 0x000000B2 System.Single UnityEngine.ParticleSystem/MainModule::get_startDelayMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_startDelayMultiplier_Injected_mEC1750CCDB02393B6FB1E766FDF2C875A9DA5247 (void);
// 0x000000B3 System.Void UnityEngine.ParticleSystem/MainModule::set_startDelayMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
extern void MainModule_set_startDelayMultiplier_Injected_m6C0ADED60F67E9C685F6A4BCAF6E658A498143CC (void);
// 0x000000B4 System.Single UnityEngine.ParticleSystem/MainModule::get_startLifetimeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_startLifetimeMultiplier_Injected_m5DFF410088ECCDC91DB86AEFB2BB1DCB6411666D (void);
// 0x000000B5 System.Void UnityEngine.ParticleSystem/MainModule::set_startLifetimeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
extern void MainModule_set_startLifetimeMultiplier_Injected_m2000D61A48B2CB2AAEE1FCBB141D58299A29446E (void);
// 0x000000B6 System.Single UnityEngine.ParticleSystem/MainModule::get_startSpeedMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_startSpeedMultiplier_Injected_mC59B6818EF7C52D84832165A74B0F5AFD2CC02CA (void);
// 0x000000B7 System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeedMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
extern void MainModule_set_startSpeedMultiplier_Injected_mD454E7C2AF126C2F46265F32E00A7ACA8D632B5C (void);
// 0x000000B8 System.Single UnityEngine.ParticleSystem/MainModule::get_startSizeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_startSizeMultiplier_Injected_m04AAFC24A6357D932E1F36C9FA94CA955FCB051F (void);
// 0x000000B9 System.Void UnityEngine.ParticleSystem/MainModule::set_startSizeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
extern void MainModule_set_startSizeMultiplier_Injected_m09FF5ECF76CB00E810A2EC29926CF5EE12BD94D5 (void);
// 0x000000BA System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_startRotationMultiplier_Injected_mFF1B0ED143B0700ACBC5499AE1EED0F9C2334FD8 (void);
// 0x000000BB System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
extern void MainModule_set_startRotationMultiplier_Injected_m1D25033340DFEC86D7FE7537E0EC7EC9C435A140 (void);
// 0x000000BC System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationXMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_startRotationXMultiplier_Injected_mA907E58F38775A2AD084A95BD11470E9312A0132 (void);
// 0x000000BD System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationXMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
extern void MainModule_set_startRotationXMultiplier_Injected_m68AC8C3218148C2212765904127F87ACB58F6C69 (void);
// 0x000000BE System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationYMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_startRotationYMultiplier_Injected_m4A540D71853039082422C270FC1A14226FDFB43C (void);
// 0x000000BF System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationYMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
extern void MainModule_set_startRotationYMultiplier_Injected_m05F30D5DAFC51E007F1D350A6CB8963EC269901E (void);
// 0x000000C0 System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationZMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_startRotationZMultiplier_Injected_mF08F4933FBA184D447C01ED33E23B0C3B6067735 (void);
// 0x000000C1 System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationZMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
extern void MainModule_set_startRotationZMultiplier_Injected_m3ABFA58E2E34C80CBB9E6BADC32F12CD34B115A0 (void);
// 0x000000C2 System.Void UnityEngine.ParticleSystem/MainModule::get_startColor_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystem/MinMaxGradient&)
extern void MainModule_get_startColor_Injected_mB8F4FB3EAB113FEE09389BED8F41E0F047128276 (void);
// 0x000000C3 System.Void UnityEngine.ParticleSystem/MainModule::set_startColor_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystem/MinMaxGradient&)
extern void MainModule_set_startColor_Injected_mBDC348394A0F1920C4123AF0329DECB2869D6EE6 (void);
// 0x000000C4 System.Single UnityEngine.ParticleSystem/MainModule::get_gravityModifierMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_gravityModifierMultiplier_Injected_mB86E9F34B1B61A23C2F5CB9BFD06703BC8B9972D (void);
// 0x000000C5 System.Void UnityEngine.ParticleSystem/MainModule::set_gravityModifierMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
extern void MainModule_set_gravityModifierMultiplier_Injected_m2AEE5F42B889A4ED22D05AA5E76A87F43680D601 (void);
// 0x000000C6 UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem/MainModule::get_simulationSpace_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_simulationSpace_Injected_mD56565C6B7CDDAFDD418BEB202E8759DCA45D69F (void);
// 0x000000C7 System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpace_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystemSimulationSpace)
extern void MainModule_set_simulationSpace_Injected_mFD6F06EA32114BC5C9D9CF3C10A5A0066E40664C (void);
// 0x000000C8 System.Single UnityEngine.ParticleSystem/MainModule::get_simulationSpeed_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_simulationSpeed_Injected_m12263544CFAE95F37D2A9B4C45AB58452AE963F6 (void);
// 0x000000C9 System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpeed_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
extern void MainModule_set_simulationSpeed_Injected_m86AEA2C2313AC96FC68800A3871248ED2597E26C (void);
// 0x000000CA UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem/MainModule::get_scalingMode_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_scalingMode_Injected_m0DD794410FA56B46A8651B4FC504CFB2484C0A02 (void);
// 0x000000CB System.Void UnityEngine.ParticleSystem/MainModule::set_scalingMode_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystemScalingMode)
extern void MainModule_set_scalingMode_Injected_mEABFC2E8FE084F997A630B8D2A643B2C9249A5A0 (void);
// 0x000000CC System.Boolean UnityEngine.ParticleSystem/MainModule::get_playOnAwake_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_playOnAwake_Injected_m87D4B5B2508D9CF9B3C61F84BC001945B20A06C2 (void);
// 0x000000CD System.Void UnityEngine.ParticleSystem/MainModule::set_playOnAwake_Injected(UnityEngine.ParticleSystem/MainModule&,System.Boolean)
extern void MainModule_set_playOnAwake_Injected_mAE88821DF50EB8AFD7079C587CC7FB633626B5FB (void);
// 0x000000CE System.Int32 UnityEngine.ParticleSystem/MainModule::get_maxParticles_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_maxParticles_Injected_m260F43233BCA02794176F596FF08A96BFCBE0D91 (void);
// 0x000000CF System.Void UnityEngine.ParticleSystem/MainModule::set_maxParticles_Injected(UnityEngine.ParticleSystem/MainModule&,System.Int32)
extern void MainModule_set_maxParticles_Injected_m6D4D611540F53CC4D8F21D9DD7EF3F68FC50F471 (void);
// 0x000000D0 System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern void EmissionModule__ctor_m9DFD2A0BB8BFAD2191C54E6627FC3B0E641EDD51 (void);
// 0x000000D1 System.Boolean UnityEngine.ParticleSystem/EmissionModule::get_enabled()
extern void EmissionModule_get_enabled_m9DB0C4A45B5D04CD88355536DD3E1DD1180DF107 (void);
// 0x000000D2 System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean)
extern void EmissionModule_set_enabled_mD9FD50C9F5EECD04B22D03E5C00C7DD64D2AC1F1 (void);
// 0x000000D3 System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime(UnityEngine.ParticleSystem/MinMaxCurve)
extern void EmissionModule_set_rateOverTime_m4EE4643D8E8B8BEC37BCB16DB76C9B8E4E4C8F48 (void);
// 0x000000D4 System.Single UnityEngine.ParticleSystem/EmissionModule::get_rateOverTimeMultiplier()
extern void EmissionModule_get_rateOverTimeMultiplier_mF8F50D01C0F4ADB44423852CD75A334179456FBA (void);
// 0x000000D5 System.Boolean UnityEngine.ParticleSystem/EmissionModule::get_enabled_Injected(UnityEngine.ParticleSystem/EmissionModule&)
extern void EmissionModule_get_enabled_Injected_m69F97462BF229B56970ABFBA4BCE59AFAF17386C (void);
// 0x000000D6 System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled_Injected(UnityEngine.ParticleSystem/EmissionModule&,System.Boolean)
extern void EmissionModule_set_enabled_Injected_m35208A8279D12A78D5DBCF751BF1760869051C82 (void);
// 0x000000D7 System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime_Injected(UnityEngine.ParticleSystem/EmissionModule&,UnityEngine.ParticleSystem/MinMaxCurve&)
extern void EmissionModule_set_rateOverTime_Injected_m19782FA56F82A6F0A6AFE6454E662F41098B6E3B (void);
// 0x000000D8 System.Single UnityEngine.ParticleSystem/EmissionModule::get_rateOverTimeMultiplier_Injected(UnityEngine.ParticleSystem/EmissionModule&)
extern void EmissionModule_get_rateOverTimeMultiplier_Injected_m1893AB6D1B181596FB7CC1D080313785DB5E5791 (void);
// 0x000000D9 System.Void UnityEngine.ParticleSystem/ShapeModule::.ctor(UnityEngine.ParticleSystem)
extern void ShapeModule__ctor_mB0CE9D41BBE5FFA5B70F465541BA63D9BEEA7264 (void);
// 0x000000DA System.Void UnityEngine.ParticleSystem/CollisionModule::.ctor(UnityEngine.ParticleSystem)
extern void CollisionModule__ctor_m08D1EEAC52F7833F28D4FC2563120FF46A9E4220 (void);
// 0x000000DB System.Void UnityEngine.ParticleSystem/TriggerModule::.ctor(UnityEngine.ParticleSystem)
extern void TriggerModule__ctor_mEB88CDC59DC3AFBC343D3042488445F6E8A5AE9D (void);
// 0x000000DC System.Void UnityEngine.ParticleSystem/SubEmittersModule::.ctor(UnityEngine.ParticleSystem)
extern void SubEmittersModule__ctor_m39DB9FE3DF4B1E15890712EF4E4024818D568A3C (void);
// 0x000000DD System.Void UnityEngine.ParticleSystem/TextureSheetAnimationModule::.ctor(UnityEngine.ParticleSystem)
extern void TextureSheetAnimationModule__ctor_mF02B5303373B93D856A5BDF92B4E4AABC7CB2F65 (void);
// 0x000000DE System.Void UnityEngine.ParticleSystem/Particle::set_lifetime(System.Single)
extern void Particle_set_lifetime_mCE97F9D17D1E660DB9D9F3244150CB6624A74DC3 (void);
// 0x000000DF System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3)
extern void Particle_set_position_mB749E41CD3C4C6DF501C0D68B6211CF7E2217FC2 (void);
// 0x000000E0 System.Void UnityEngine.ParticleSystem/Particle::set_velocity(UnityEngine.Vector3)
extern void Particle_set_velocity_m4894BD6A75E7A1FCD433927F93396AA29A59709B (void);
// 0x000000E1 System.Void UnityEngine.ParticleSystem/Particle::set_remainingLifetime(System.Single)
extern void Particle_set_remainingLifetime_mE521DF9387892C00B3F21D4F76F3A55E77AFC6BE (void);
// 0x000000E2 System.Void UnityEngine.ParticleSystem/Particle::set_startLifetime(System.Single)
extern void Particle_set_startLifetime_m7AC09262BBFE8818EA6B61BDB62264184CD27A82 (void);
// 0x000000E3 System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32)
extern void Particle_set_startColor_mA7B0363E82B7A7CBB1C83F8C5D49FB7B7AF75554 (void);
// 0x000000E4 System.Void UnityEngine.ParticleSystem/Particle::set_randomSeed(System.UInt32)
extern void Particle_set_randomSeed_m8FD7A4DB7F8E7EBDEF2C51A28197F8D9D7CB6E04 (void);
// 0x000000E5 System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single)
extern void Particle_set_startSize_m7CDEE5B620B3D26B4CC5C1DA7C6E24ACCCF64466 (void);
// 0x000000E6 System.Void UnityEngine.ParticleSystem/Particle::set_rotation3D(UnityEngine.Vector3)
extern void Particle_set_rotation3D_m0F402760524A81307FA4940751751C44DF2F77D0 (void);
// 0x000000E7 System.Void UnityEngine.ParticleSystem/Particle::set_angularVelocity3D(UnityEngine.Vector3)
extern void Particle_set_angularVelocity3D_mE9A9544DF33CD0CCF4F1CA14994A2C436E4DF1F8 (void);
// 0x000000E8 System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single)
extern void MinMaxCurve__ctor_mF14A2DBABFDCF1E855911241A555949CAF212AE3 (void);
// 0x000000E9 UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MinMaxCurve::op_Implicit(System.Single)
extern void MinMaxCurve_op_Implicit_m8D746D40E6CCBF5E8C4CE39F23A45712BFC372F5 (void);
// 0x000000EA System.Void UnityEngine.ParticleSystem/MinMaxGradient::.ctor(UnityEngine.Color)
extern void MinMaxGradient__ctor_m8D0DE766F67FF065E4C2B6DCD841752C447977AA (void);
// 0x000000EB UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::get_color()
extern void MinMaxGradient_get_color_m8FA2F2F1E0B9C9E1DFE40CEA325BC5E863C59C90 (void);
// 0x000000EC UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MinMaxGradient::op_Implicit(UnityEngine.Color)
extern void MinMaxGradient_op_Implicit_m5F938B5644AF6BE5DF525839E922B88456B614FA (void);
// 0x000000ED System.Void UnityEngine.ParticleSystem/VelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern void VelocityOverLifetimeModule__ctor_m03EB5FA8BB69309D7DA91CE70EE3A9E65C60AE66 (void);
// 0x000000EE System.Void UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern void LimitVelocityOverLifetimeModule__ctor_mA327985B69E145AAC3FFEE42EE44A81D7800BCCC (void);
// 0x000000EF System.Void UnityEngine.ParticleSystem/InheritVelocityModule::.ctor(UnityEngine.ParticleSystem)
extern void InheritVelocityModule__ctor_m76B46612309334A443F9939A47FBAAF024606892 (void);
// 0x000000F0 System.Void UnityEngine.ParticleSystem/LifetimeByEmitterSpeedModule::.ctor(UnityEngine.ParticleSystem)
extern void LifetimeByEmitterSpeedModule__ctor_m80A22170A2E3047A3B364FE232CC4EB289F87BED (void);
// 0x000000F1 System.Void UnityEngine.ParticleSystem/ForceOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern void ForceOverLifetimeModule__ctor_m4FF722421C5B1449CB900FF0506EF33549A450CD (void);
// 0x000000F2 System.Void UnityEngine.ParticleSystem/ColorOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern void ColorOverLifetimeModule__ctor_m3AFAD44AD8E69F213881272693C23536EEBAB71D (void);
// 0x000000F3 System.Void UnityEngine.ParticleSystem/ColorBySpeedModule::.ctor(UnityEngine.ParticleSystem)
extern void ColorBySpeedModule__ctor_m71C9D80CE70BB41F782B4C262D0FE40477A7D0AF (void);
// 0x000000F4 System.Void UnityEngine.ParticleSystem/SizeOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern void SizeOverLifetimeModule__ctor_mCA80885490FC3B1010887B10BDE21A0DACEFB557 (void);
// 0x000000F5 System.Void UnityEngine.ParticleSystem/SizeBySpeedModule::.ctor(UnityEngine.ParticleSystem)
extern void SizeBySpeedModule__ctor_m479D4802A862D4E07AAF2B47833852A62A17F6DE (void);
// 0x000000F6 System.Void UnityEngine.ParticleSystem/RotationOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern void RotationOverLifetimeModule__ctor_mA0E0BDF5A88582DFEC3FAB42F8B237611C8E0182 (void);
// 0x000000F7 System.Void UnityEngine.ParticleSystem/RotationBySpeedModule::.ctor(UnityEngine.ParticleSystem)
extern void RotationBySpeedModule__ctor_mCEAFF0BBEC6AB5FB3E59FD1584B22079A2142248 (void);
// 0x000000F8 System.Void UnityEngine.ParticleSystem/ExternalForcesModule::.ctor(UnityEngine.ParticleSystem)
extern void ExternalForcesModule__ctor_m2FB0D6776AAAB6731E3E80BA6ADD858458976DF5 (void);
// 0x000000F9 System.Void UnityEngine.ParticleSystem/NoiseModule::.ctor(UnityEngine.ParticleSystem)
extern void NoiseModule__ctor_mE450BC95A9B436FA1627E4D24B7B4E7E723DDB5B (void);
// 0x000000FA System.Void UnityEngine.ParticleSystem/LightsModule::.ctor(UnityEngine.ParticleSystem)
extern void LightsModule__ctor_mD59B002A60EB418DACA4986EC1C4D34B8DAC555F (void);
// 0x000000FB System.Void UnityEngine.ParticleSystem/TrailModule::.ctor(UnityEngine.ParticleSystem)
extern void TrailModule__ctor_m15960ADC2381E58BE70FF2653288599694915412 (void);
// 0x000000FC System.Void UnityEngine.ParticleSystem/CustomDataModule::.ctor(UnityEngine.ParticleSystem)
extern void CustomDataModule__ctor_m8F46AC54F3CC41A63AFE9856B18CC41A9A237277 (void);
// 0x000000FD System.Void UnityEngine.ParticleSystemRenderer::EnableVertexStreams(UnityEngine.ParticleSystemVertexStreams)
extern void ParticleSystemRenderer_EnableVertexStreams_mA2631E5FF8C6CA157E26581BDC47506837AB4696 (void);
// 0x000000FE System.Void UnityEngine.ParticleSystemRenderer::DisableVertexStreams(UnityEngine.ParticleSystemVertexStreams)
extern void ParticleSystemRenderer_DisableVertexStreams_m1317B70228142564B45B3E354615237C312FA9F4 (void);
// 0x000000FF System.Boolean UnityEngine.ParticleSystemRenderer::AreVertexStreamsEnabled(UnityEngine.ParticleSystemVertexStreams)
extern void ParticleSystemRenderer_AreVertexStreamsEnabled_m70D3591D4793FEE8BC879439968884E7889CBD04 (void);
// 0x00000100 UnityEngine.ParticleSystemVertexStreams UnityEngine.ParticleSystemRenderer::GetEnabledVertexStreams(UnityEngine.ParticleSystemVertexStreams)
extern void ParticleSystemRenderer_GetEnabledVertexStreams_mA38CE0B9D50452BE6405ADF18050306C22A29CDC (void);
// 0x00000101 System.Void UnityEngine.ParticleSystemRenderer::Internal_SetVertexStreams(UnityEngine.ParticleSystemVertexStreams,System.Boolean)
extern void ParticleSystemRenderer_Internal_SetVertexStreams_mA00D4C6C868E8309E715015704CCFC12A4CBD8FD (void);
// 0x00000102 UnityEngine.ParticleSystemVertexStreams UnityEngine.ParticleSystemRenderer::Internal_GetEnabledVertexStreams(UnityEngine.ParticleSystemVertexStreams)
extern void ParticleSystemRenderer_Internal_GetEnabledVertexStreams_m1AA436781571C985E87EE8706AF264C977D7A979 (void);
// 0x00000103 UnityEngine.ParticleSystemRenderSpace UnityEngine.ParticleSystemRenderer::get_alignment()
extern void ParticleSystemRenderer_get_alignment_m5D4063743194C66B9671E5CBC3DC5803AAB8D023 (void);
// 0x00000104 System.Void UnityEngine.ParticleSystemRenderer::set_alignment(UnityEngine.ParticleSystemRenderSpace)
extern void ParticleSystemRenderer_set_alignment_m0A6049A94D26A72853875B9A46CA4BE829B439A7 (void);
// 0x00000105 UnityEngine.ParticleSystemRenderMode UnityEngine.ParticleSystemRenderer::get_renderMode()
extern void ParticleSystemRenderer_get_renderMode_mE203DEDF25DAA9E549CECD34F7552E8B39A81576 (void);
// 0x00000106 System.Void UnityEngine.ParticleSystemRenderer::set_renderMode(UnityEngine.ParticleSystemRenderMode)
extern void ParticleSystemRenderer_set_renderMode_mEA05819300408CA36E2D7AA83E15451A22B83D69 (void);
// 0x00000107 UnityEngine.ParticleSystemSortMode UnityEngine.ParticleSystemRenderer::get_sortMode()
extern void ParticleSystemRenderer_get_sortMode_m9BDC333B6BFDBA07EB7254E614B819A35BE54275 (void);
// 0x00000108 System.Void UnityEngine.ParticleSystemRenderer::set_sortMode(UnityEngine.ParticleSystemSortMode)
extern void ParticleSystemRenderer_set_sortMode_mC3736E7991D0D8C55DAF0D40E872BACADF18A560 (void);
// 0x00000109 System.Single UnityEngine.ParticleSystemRenderer::get_lengthScale()
extern void ParticleSystemRenderer_get_lengthScale_m2967DAF0E340DE04D08C03B0895496D90E1F8C20 (void);
// 0x0000010A System.Void UnityEngine.ParticleSystemRenderer::set_lengthScale(System.Single)
extern void ParticleSystemRenderer_set_lengthScale_m082D8A542FC1A7FA8772CE01C98DC44C359FFB8A (void);
// 0x0000010B System.Single UnityEngine.ParticleSystemRenderer::get_velocityScale()
extern void ParticleSystemRenderer_get_velocityScale_mF549344ED73B7231CE97A0C6E9D76DFA74E0CBBB (void);
// 0x0000010C System.Void UnityEngine.ParticleSystemRenderer::set_velocityScale(System.Single)
extern void ParticleSystemRenderer_set_velocityScale_mB3BADC3EB04FE4BDD4BDE4E2D4725371BD0AA034 (void);
// 0x0000010D System.Single UnityEngine.ParticleSystemRenderer::get_cameraVelocityScale()
extern void ParticleSystemRenderer_get_cameraVelocityScale_m4EFFB41B99BAD7CC6CBBC1E8E56BE91B459B386C (void);
// 0x0000010E System.Void UnityEngine.ParticleSystemRenderer::set_cameraVelocityScale(System.Single)
extern void ParticleSystemRenderer_set_cameraVelocityScale_m05772225B1824D2AE906FF68CA45C5901C4F9E95 (void);
// 0x0000010F System.Single UnityEngine.ParticleSystemRenderer::get_normalDirection()
extern void ParticleSystemRenderer_get_normalDirection_m4F7D8E0376239DD36E1526BBA511B76133B57C9D (void);
// 0x00000110 System.Void UnityEngine.ParticleSystemRenderer::set_normalDirection(System.Single)
extern void ParticleSystemRenderer_set_normalDirection_mFC60F14E941A1C4939F977F22B0D081A740AB5F2 (void);
// 0x00000111 System.Single UnityEngine.ParticleSystemRenderer::get_shadowBias()
extern void ParticleSystemRenderer_get_shadowBias_m8324464177725CD2C3A4FEB227F7FD00394FF53D (void);
// 0x00000112 System.Void UnityEngine.ParticleSystemRenderer::set_shadowBias(System.Single)
extern void ParticleSystemRenderer_set_shadowBias_mCB3BD9449A66F4F20077BFB970D1A6E99D6EDA2A (void);
// 0x00000113 System.Single UnityEngine.ParticleSystemRenderer::get_sortingFudge()
extern void ParticleSystemRenderer_get_sortingFudge_m2F9B4EFF4F196E685AECB9462D69B13E340C43EA (void);
// 0x00000114 System.Void UnityEngine.ParticleSystemRenderer::set_sortingFudge(System.Single)
extern void ParticleSystemRenderer_set_sortingFudge_m619B3A624EF266C7DE36BE083085E327033ADBAF (void);
// 0x00000115 System.Single UnityEngine.ParticleSystemRenderer::get_minParticleSize()
extern void ParticleSystemRenderer_get_minParticleSize_m1BDB16135B182231F359499E811BF5274664E140 (void);
// 0x00000116 System.Void UnityEngine.ParticleSystemRenderer::set_minParticleSize(System.Single)
extern void ParticleSystemRenderer_set_minParticleSize_m1FDD2EEFBF60703A7AC154FC91DD5015F65A34F4 (void);
// 0x00000117 System.Single UnityEngine.ParticleSystemRenderer::get_maxParticleSize()
extern void ParticleSystemRenderer_get_maxParticleSize_mA9131C2FE7C8784D02EC94E20E7F5734C06C8EB4 (void);
// 0x00000118 System.Void UnityEngine.ParticleSystemRenderer::set_maxParticleSize(System.Single)
extern void ParticleSystemRenderer_set_maxParticleSize_m7D19DF8E661235A19910F216DFDBBD447940CD06 (void);
// 0x00000119 UnityEngine.Vector3 UnityEngine.ParticleSystemRenderer::get_pivot()
extern void ParticleSystemRenderer_get_pivot_m19CA73F7EE2F456F16E9E4753FC197A10649A6CE (void);
// 0x0000011A System.Void UnityEngine.ParticleSystemRenderer::set_pivot(UnityEngine.Vector3)
extern void ParticleSystemRenderer_set_pivot_mF7A808C4BD4258937742A26C4E993902A4AA5C80 (void);
// 0x0000011B UnityEngine.Vector3 UnityEngine.ParticleSystemRenderer::get_flip()
extern void ParticleSystemRenderer_get_flip_m5499AACC6A0BF4AD6ECB0919121D219BDEE2D0BC (void);
// 0x0000011C System.Void UnityEngine.ParticleSystemRenderer::set_flip(UnityEngine.Vector3)
extern void ParticleSystemRenderer_set_flip_m6148ED3103BBC6999C1121DFEEF355B139CEA051 (void);
// 0x0000011D UnityEngine.SpriteMaskInteraction UnityEngine.ParticleSystemRenderer::get_maskInteraction()
extern void ParticleSystemRenderer_get_maskInteraction_mAE4B3D48137AA10DEA775FD30A2D161D54747AE8 (void);
// 0x0000011E System.Void UnityEngine.ParticleSystemRenderer::set_maskInteraction(UnityEngine.SpriteMaskInteraction)
extern void ParticleSystemRenderer_set_maskInteraction_mFC9DD6C5CDADD003946F4CDA508D5A814992F2BB (void);
// 0x0000011F UnityEngine.Material UnityEngine.ParticleSystemRenderer::get_trailMaterial()
extern void ParticleSystemRenderer_get_trailMaterial_mB7C8AE9C0D9B9A2092E2FCF55AA6CFE3B0B3A400 (void);
// 0x00000120 System.Void UnityEngine.ParticleSystemRenderer::set_trailMaterial(UnityEngine.Material)
extern void ParticleSystemRenderer_set_trailMaterial_mB28936A342D1805B3F8246DC5DD531970452D016 (void);
// 0x00000121 System.Boolean UnityEngine.ParticleSystemRenderer::get_enableGPUInstancing()
extern void ParticleSystemRenderer_get_enableGPUInstancing_mAC82F715FF3D827AB2EE7AF70FF0CC828DD71B34 (void);
// 0x00000122 System.Void UnityEngine.ParticleSystemRenderer::set_enableGPUInstancing(System.Boolean)
extern void ParticleSystemRenderer_set_enableGPUInstancing_m643A1F548832BECBB84384F2A85E724E5FBE3D0E (void);
// 0x00000123 System.Boolean UnityEngine.ParticleSystemRenderer::get_allowRoll()
extern void ParticleSystemRenderer_get_allowRoll_m8D1E22AA8E4426C79CB995899196CF22FBF17023 (void);
// 0x00000124 System.Void UnityEngine.ParticleSystemRenderer::set_allowRoll(System.Boolean)
extern void ParticleSystemRenderer_set_allowRoll_mB321729C721EBFCA6688F9AC4DBAF38A166D8CE7 (void);
// 0x00000125 System.Boolean UnityEngine.ParticleSystemRenderer::get_freeformStretching()
extern void ParticleSystemRenderer_get_freeformStretching_m93B2D5A92718928D43E185DF0B0F2B13CFF4889E (void);
// 0x00000126 System.Void UnityEngine.ParticleSystemRenderer::set_freeformStretching(System.Boolean)
extern void ParticleSystemRenderer_set_freeformStretching_m4388C3A17F73D42B6A3951BA30D4805E9869F14B (void);
// 0x00000127 System.Boolean UnityEngine.ParticleSystemRenderer::get_rotateWithStretchDirection()
extern void ParticleSystemRenderer_get_rotateWithStretchDirection_m1E966F0539AA2A9C806652E59158EC939DBF5000 (void);
// 0x00000128 System.Void UnityEngine.ParticleSystemRenderer::set_rotateWithStretchDirection(System.Boolean)
extern void ParticleSystemRenderer_set_rotateWithStretchDirection_m027CFD4D235694227FA2FA0F01BA663E8392E2BC (void);
// 0x00000129 UnityEngine.Mesh UnityEngine.ParticleSystemRenderer::get_mesh()
extern void ParticleSystemRenderer_get_mesh_m32A036955DAEA0FDDA2D4B8EE3D5AEE7FD67A92F (void);
// 0x0000012A System.Void UnityEngine.ParticleSystemRenderer::set_mesh(UnityEngine.Mesh)
extern void ParticleSystemRenderer_set_mesh_m5AEE1784648604DA92FD5B99759D366EA7C76C65 (void);
// 0x0000012B System.Int32 UnityEngine.ParticleSystemRenderer::GetMeshes(UnityEngine.Mesh[])
extern void ParticleSystemRenderer_GetMeshes_m1B36A6BFF152AAE5520D727976E3DA26722C3A75 (void);
// 0x0000012C System.Void UnityEngine.ParticleSystemRenderer::SetMeshes(UnityEngine.Mesh[],System.Int32)
extern void ParticleSystemRenderer_SetMeshes_mAD7FCBD6E24A447D5304058EA04F751F85AF5A99 (void);
// 0x0000012D System.Void UnityEngine.ParticleSystemRenderer::SetMeshes(UnityEngine.Mesh[])
extern void ParticleSystemRenderer_SetMeshes_m1528C9A3211F04578ECBC89E84125ABFE82796DC (void);
// 0x0000012E System.Int32 UnityEngine.ParticleSystemRenderer::get_meshCount()
extern void ParticleSystemRenderer_get_meshCount_mCB20BE0EC5F2671EF6F6119227130F3473C3302D (void);
// 0x0000012F System.Void UnityEngine.ParticleSystemRenderer::BakeMesh(UnityEngine.Mesh,System.Boolean)
extern void ParticleSystemRenderer_BakeMesh_m0833E4AFD66E76A995B877E9759F3497EECFC3A6 (void);
// 0x00000130 System.Void UnityEngine.ParticleSystemRenderer::BakeMesh(UnityEngine.Mesh,UnityEngine.Camera,System.Boolean)
extern void ParticleSystemRenderer_BakeMesh_m1A53F569D7DFCBE964E66D5E3ACA11D85ED56C9E (void);
// 0x00000131 System.Void UnityEngine.ParticleSystemRenderer::BakeTrailsMesh(UnityEngine.Mesh,System.Boolean)
extern void ParticleSystemRenderer_BakeTrailsMesh_m07C7C29BB8DBA0621E414DC443838F287AF58DFE (void);
// 0x00000132 System.Void UnityEngine.ParticleSystemRenderer::BakeTrailsMesh(UnityEngine.Mesh,UnityEngine.Camera,System.Boolean)
extern void ParticleSystemRenderer_BakeTrailsMesh_m7BE3F730731BBFA0C6A9399BF91813236AC14E3F (void);
// 0x00000133 System.Int32 UnityEngine.ParticleSystemRenderer::get_activeVertexStreamsCount()
extern void ParticleSystemRenderer_get_activeVertexStreamsCount_m948DD12ED56E758FF7F55DF8D1D9B6D644A3D3FE (void);
// 0x00000134 System.Void UnityEngine.ParticleSystemRenderer::SetActiveVertexStreams(System.Collections.Generic.List`1<UnityEngine.ParticleSystemVertexStream>)
extern void ParticleSystemRenderer_SetActiveVertexStreams_m6915A0D7D1E0DCEBEA77A31DC78BE0CE8DBEAD22 (void);
// 0x00000135 System.Void UnityEngine.ParticleSystemRenderer::GetActiveVertexStreams(System.Collections.Generic.List`1<UnityEngine.ParticleSystemVertexStream>)
extern void ParticleSystemRenderer_GetActiveVertexStreams_m32C738BAD8EAB54463B9BBCA2CAA0324BC702014 (void);
// 0x00000136 System.Void UnityEngine.ParticleSystemRenderer::.ctor()
extern void ParticleSystemRenderer__ctor_mD6D0E9A92CF103E60163821DCC7C3D6DF3AA838B (void);
// 0x00000137 System.Void UnityEngine.ParticleSystemRenderer::get_pivot_Injected(UnityEngine.Vector3&)
extern void ParticleSystemRenderer_get_pivot_Injected_mB393D1D0F0C69BD2F52A282FD0868E3E2585488C (void);
// 0x00000138 System.Void UnityEngine.ParticleSystemRenderer::set_pivot_Injected(UnityEngine.Vector3&)
extern void ParticleSystemRenderer_set_pivot_Injected_m8CE9A3B8B0EB136F3E658CFB2B00616D7D8A8AB3 (void);
// 0x00000139 System.Void UnityEngine.ParticleSystemRenderer::get_flip_Injected(UnityEngine.Vector3&)
extern void ParticleSystemRenderer_get_flip_Injected_m08B63FCDBAB2C235981432A80901FE97A29DA985 (void);
// 0x0000013A System.Void UnityEngine.ParticleSystemRenderer::set_flip_Injected(UnityEngine.Vector3&)
extern void ParticleSystemRenderer_set_flip_Injected_mE957C7B1BCD98C9D2D19900F1CC8EE50ED1DD688 (void);
static Il2CppMethodPointer s_methodPointers[314] = 
{
	ParticleSystem_Emit_mC489C467AAF3C3721AC3315AF78DC4CE469E7AAC,
	ParticleSystem_Emit_mF1E108B9BF7E0094C35CF71870B5B0EA72ABB485,
	ParticleSystem_get_startDelay_m6661DA2328104EAAF1B3EED1A9AD5BA506B3DC57,
	ParticleSystem_set_startDelay_m260AAD1034FB43187FA18D7D82858F7B12827662,
	ParticleSystem_get_loop_m7D0ED1B08C7503790FC5FD2863728AD281098AB2,
	ParticleSystem_set_loop_mE7536AE1F42CCD5F8C410D719D3D80571C0054C6,
	ParticleSystem_get_playOnAwake_mCC6EB49C6C0400E3CC58024E4AEFD6EB81EBBCB6,
	ParticleSystem_set_playOnAwake_m9AB94C2B3B742F4E3A1C7EFEBF30CDD33195D21D,
	ParticleSystem_get_duration_m25C0E6C6B0EC084ED59977966AF2C2EDE529FA44,
	ParticleSystem_get_playbackSpeed_mA03269D76AA3AE6F149A19CCF12528D7EA384D52,
	ParticleSystem_set_playbackSpeed_mCAAD47BDB2A8F83EDE0FA791331FDDA5E29E488C,
	ParticleSystem_get_enableEmission_m4A8F7E3AD120DC00910D5A20B25B90417BF3EB7B,
	ParticleSystem_set_enableEmission_m29DCB9B1C7F2E54DCA3D1A046754ACBEFE162FA9,
	ParticleSystem_get_emissionRate_m71A71CD1426FC5DC8A0717DABCE413693C412967,
	ParticleSystem_set_emissionRate_m62D12CC0B786ACD83632326F50A1082BE21EBA90,
	ParticleSystem_get_startSpeed_mC6CEF0642452626DEE0687FE57CE96EE9DC65F41,
	ParticleSystem_set_startSpeed_m969B0FF3E7392F9E027D40C8BAAFF932171D9280,
	ParticleSystem_get_startSize_m7327DF9701486C5476DB9E3296BD5692E2A7CB72,
	ParticleSystem_set_startSize_m2175FEE1F0ABA3F69C2DC4EC4D940E19A7FBEC6D,
	ParticleSystem_get_startColor_mEA5A4B07850804959DE58004AB65FEBFE5067515,
	ParticleSystem_set_startColor_m58AE61985D5B25B108FB4A5E05D7DF4A0FFECAEC,
	ParticleSystem_get_startRotation_m19EA2F6770518A7E2F9CB529943E23E083DEC5A0,
	ParticleSystem_set_startRotation_mFF30EA836130E5105E8886B27F6051E87EE7DF49,
	ParticleSystem_get_startRotation3D_mA9CDEFD28212D4A67F6D3E840BE3A059C361B82F,
	ParticleSystem_set_startRotation3D_m801440D1F8A5B0C8268607D7C6CBB1DBE53602AE,
	ParticleSystem_get_startLifetime_m48D95DC48DB180862C6D921B08D3BB0969AEBC02,
	ParticleSystem_set_startLifetime_m3574A8F28C124698F4BBC3CBEE7F638CE6AB2552,
	ParticleSystem_get_gravityModifier_m8E0B14851B17FED84102ECC0A4871151C9DAF553,
	ParticleSystem_set_gravityModifier_m0E1C7D97DD78C58931FB3E502B43920E4A810F1F,
	ParticleSystem_get_maxParticles_mAF21E5F4E07CC95B46F34FA22E68DE7E1714D596,
	ParticleSystem_set_maxParticles_m3B2C177B338CAC418232A25E5F3C0B588B5E4ABA,
	ParticleSystem_get_simulationSpace_mF6EED4B63164572C99E8EC7F315108BEC7CBB23D,
	ParticleSystem_set_simulationSpace_mF4A5FBCDCC9F63BB28545FF4FF68BB77C1F6D04E,
	ParticleSystem_get_scalingMode_mF2EA9887C71A595E98187564E41D94A710504E63,
	ParticleSystem_set_scalingMode_m4B399177269F81152FAF667684FE84EB44E61E0F,
	ParticleSystem_get_automaticCullingEnabled_m3E0F822B082E66D6AB5813BDF96F48E0B54971C4,
	ParticleSystem_get_isPlaying_m36FD03CBF99EE4C243B01F37D77CB6B1CFA526BA,
	ParticleSystem_get_isEmitting_m1641C8124AE31D556E65164BC5A085C523FE4033,
	ParticleSystem_get_isStopped_mE50B27655AFE6F178C9B3B99F18968A8964D93F8,
	ParticleSystem_get_isPaused_mB8AEA85594606CC49078B39F05ED8BA5C8199C0E,
	ParticleSystem_get_particleCount_mAD1793317BD6BBCB0C7A7853A9E82D19703B0A52,
	ParticleSystem_get_time_m85B483199DA7D9675ABCB194F2CD431C53D4B3B1,
	ParticleSystem_set_time_m014BD2C09A14861186C79BA3FD12E2AC8DE90544,
	ParticleSystem_get_randomSeed_m70650D178DEADC42E19B6B22ECE252AE5434F030,
	ParticleSystem_set_randomSeed_m327828D83321B6F734D52EB91E64A45228040EC1,
	ParticleSystem_get_useAutoRandomSeed_m1129F9BC54087CCF6EFE372E9182F7AD298877ED,
	ParticleSystem_set_useAutoRandomSeed_m1A1B3221AD7BAFF3AA0EB8820D0D6C58BBC2831B,
	ParticleSystem_get_proceduralSimulationSupported_mEF4AEBA182D7DD12ABB6414971A2365236A80A50,
	ParticleSystem_GetParticleCurrentSize_mE561E5E2E69A6C357C642444C25AA794833A06BE,
	ParticleSystem_GetParticleCurrentSize3D_m989175446F4C6E99635AD8E1FA33BB1146978A9F,
	ParticleSystem_GetParticleCurrentColor_mBFD3E4B3A3639F1F59D60C8079F36A9A51E03053,
	ParticleSystem_GetParticleMeshIndex_m9CBB1891C113D91AA1F0C390EFCA502E1D22C648,
	ParticleSystem_SetParticles_m79867147E65742BE1E1ADF0222C090BD6AF1E20A,
	ParticleSystem_SetParticles_m0658B777D1C6DDA7D244607AC55D5225774CEBFA,
	ParticleSystem_SetParticles_m1693B5469E364598DDEAEB1B48724C203E743668,
	ParticleSystem_SetParticlesWithNativeArray_m163F32606AC234C018E081A3522CCF57C8A2A4FC,
	ParticleSystem_SetParticles_m380593909B2E9DBB77D8DE09E47C1D0DCCD6ACC2,
	ParticleSystem_SetParticles_m648CE8AF7118C73FF5E1D0419E7F6BA52478C13E,
	ParticleSystem_SetParticles_m75061A9FC1FBA11FBFF903B64762D0B60118F2B9,
	ParticleSystem_GetParticles_mC310C32157EA281ECDC320B63B1E43ED5F6292B3,
	ParticleSystem_GetParticles_mF6FC6048609DD24AF7F1B8890C44AEC480BDFDEA,
	ParticleSystem_GetParticles_mAE8894E2B022EE009C6DDB1390AB331E7D40A344,
	ParticleSystem_GetParticlesWithNativeArray_m4303397CB3084BE1158D854929FE40CA0A6E5253,
	ParticleSystem_GetParticles_mCD391A836273B8B41035A9F06D5D7ABA83968A95,
	ParticleSystem_GetParticles_m870578C1C1E37DFF7BC112662BAE81F3CA7AA411,
	ParticleSystem_GetParticles_mE37AF218E5BF5980DC625491EF3D52756F126BB8,
	ParticleSystem_SetCustomParticleData_m0304D334E835ECDA56DBFFA873AA75812143D543,
	ParticleSystem_GetCustomParticleData_m7EC7DF1370D12E9A5825D8BE1C249672C4794B6B,
	ParticleSystem_GetPlaybackState_m94CFA6AAEF043DBDEE0C6DC3833B32639AA3BA63,
	ParticleSystem_SetPlaybackState_m9BE2B963312B05416B540B897C21BAADFCE76E59,
	ParticleSystem_GetTrailDataInternal_m03273FD85CB69A3D816E91E7D39AAAF58F5BCF78,
	ParticleSystem_GetTrails_m99FE1513C3114EA0D1FA05F10B456A8CE6F2610B,
	ParticleSystem_SetTrails_mD865F6A447580EDDCC2568FD900C6027E9F89E66,
	ParticleSystem_Simulate_m68A0105281A8BC3F4E9242181586483BA7B796A3,
	ParticleSystem_Simulate_mC2F2E060D7CE94D4936BA995C49827231DF5F1F8,
	ParticleSystem_Simulate_m36BAFFACB58953B2A46AA0B86E1F59C0FD6CD9C7,
	ParticleSystem_Simulate_m2628543E52E4D8D90A7B592F9C6222626F340F60,
	ParticleSystem_Play_m97D70BAF373265E633ACD91833E56D981B19958A,
	ParticleSystem_Play_m28D27CC4CDC1D93195C75647E6F6DAECF8B6BC50,
	ParticleSystem_Pause_m116581497C92159D3210C733917CD2515399A0CD,
	ParticleSystem_Pause_mA5AE4D5A290E9DD75A0572738CB0910D6A7E2121,
	ParticleSystem_Stop_m275B200BC21580C60987EC8FC8B2DD0FEB365C1A,
	ParticleSystem_Stop_m5EC8B81A8BD1F8C90729E766789EE5D8D4EC64FB,
	ParticleSystem_Stop_m8CBF9268DE7B5A40952B4977462B857B5F5AFB9D,
	ParticleSystem_Clear_m9E873134B8055ACFEE9DC66F7E4FABE178E27057,
	ParticleSystem_Clear_mD8C9DCD1267F221B0546E4B9B55DBD9354893797,
	ParticleSystem_IsAlive_m43E2E84732EBDF5C4C9FD6575051A2A2B135BCF9,
	ParticleSystem_IsAlive_m59D28AC79A1A5FD1D97523D83D687ACFA2510198,
	ParticleSystem_Emit_m07EF0D2DA84EB04814DA7EE6B8618B008DE75F28,
	ParticleSystem_Emit_Internal_m7C72C31F7F4875B54B00E255A450B045A4449646,
	ParticleSystem_Emit_m1598252E2EF701A5010EFA395A87368495E9F9F7,
	ParticleSystem_EmitOld_Internal_m4F094DC523986298D5626F0F3F2335DFF596C993,
	ParticleSystem_TriggerSubEmitter_m9D4541DB830D5C80CD4BF88C3C8B8A8A0C4DFBFA,
	ParticleSystem_TriggerSubEmitter_m54FED6396579982E9C0AF774853E9A2290379A37,
	ParticleSystem_TriggerSubEmitterForParticle_mCC9F7D09CA9D8DB5576389ABCFC287A21952003A,
	ParticleSystem_TriggerSubEmitter_mBEB9C15312BB6AD76AF9B42B2CE184B7149F4C98,
	ParticleSystem_ResetPreMappedBufferMemory_m1B0AEE23A61FA29580513BD4258B263C4F7FAAC3,
	ParticleSystem_SetMaximumPreMappedBufferCounts_m483102DE3EFE86F3E6721C16942E04130B3CAE3F,
	ParticleSystem_AllocateAxisOfRotationAttribute_mDE6232FD099AD8F1F6F0DF3726C1C32E4729E1D5,
	ParticleSystem_AllocateMeshIndexAttribute_m25F7096688B776F1CAF7F8F6D6F6222B41AF9E18,
	ParticleSystem_AllocateCustomDataAttribute_mAB843E372C9D175730E0730DB54FB34AF7A1DC7F,
	ParticleSystem_GetManagedJobData_mAE6749636F4F3A7EBD7775AE4B8E9BFD24C2CF95,
	ParticleSystem_GetManagedJobHandle_m63494F9028CE37DDF08822AFF19828A2E0A06C6E,
	ParticleSystem_SetManagedJobHandle_mFD5D834CE36338E182A246668AD58E3CD09A6608,
	ParticleSystem_ScheduleManagedJob_m7B340DC400DAAD69AFC906EA0BAD0D7C3459CB89,
	ParticleSystem_CopyManagedJobData_mF72E9C4B5DE2228B29D4AB03ADFC3499BA009D95,
	ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0,
	ParticleSystem_get_emission_m0BAA1EDF775A8712DA7D62BF3C42A2B1A6C96CA1,
	ParticleSystem_get_shape_m986023201B5140A525EF34F81DAAF1866D889052,
	ParticleSystem_get_velocityOverLifetime_mA0FEC4CD424A882B27AEBDDB06CBFA61951C2868,
	ParticleSystem_get_limitVelocityOverLifetime_m25ABE96DD9F88188874A538AA4992A72EC4AE394,
	ParticleSystem_get_inheritVelocity_mE8C58E2805CB8A7C39F39D7ECE0F3978EAEDBC0A,
	ParticleSystem_get_lifetimeByEmitterSpeed_m4901408692AC775C037D2D16F55810D9A574BB7B,
	ParticleSystem_get_forceOverLifetime_m044BAE00399D6EB3369C001237E7D8C11BCA3874,
	ParticleSystem_get_colorOverLifetime_m79598BD17DFAC4CA4C0CAFEA363BA9212000FC31,
	ParticleSystem_get_colorBySpeed_mF7C88FB024568B5DB6D06DE5C45965FC28633520,
	ParticleSystem_get_sizeOverLifetime_m4A532D865E6EA79D745D8C4792CCA4072EC7AA05,
	ParticleSystem_get_sizeBySpeed_m0D7FF6F71988E4A1ACF8DD2257E06F5CB6580439,
	ParticleSystem_get_rotationOverLifetime_mF5F527E0112C591D297C13CD60A7B888B98E9DEF,
	ParticleSystem_get_rotationBySpeed_m2ED46A54093DDB496DC8148D54FCB01D0BC6FE2B,
	ParticleSystem_get_externalForces_mA09E3DE23F158D76C503A32C3DECBF81FE5FE7E9,
	ParticleSystem_get_noise_m4059B8305FDEEE3FDE2DA67DBA2B56793D280EF8,
	ParticleSystem_get_collision_m1914663D5844842893E1F33CF4BFA1D5B3DF4AC1,
	ParticleSystem_get_trigger_m0CB1A47823806DA2671918708FEC768C754B5120,
	ParticleSystem_get_subEmitters_mCFE3CAEF76ADEAF3754210EC731A54A9DA836428,
	ParticleSystem_get_textureSheetAnimation_mE7D4FF28B018DD7BB94904CE014F5FD56E53AA90,
	ParticleSystem_get_lights_mBAA747B7858C2C39BF4D5502D3026FDA6A9DCE52,
	ParticleSystem_get_trails_m9514134A5A05CCDEF2C901BC0D60FC287959414C,
	ParticleSystem_get_customData_m7112FE08433F44A3425142B2D4862F8CD889A913,
	ParticleSystem__ctor_mEC46AD331B88935F1CFBAA282FD480C53E55ACDC,
	ParticleSystem_GetParticleCurrentSize3D_Injected_mE1D7D2457CDC32E35F9D3E06721D773667F2A966,
	ParticleSystem_GetParticleCurrentColor_Injected_m98C468104D268559CF2F06F78F970F1814F7A587,
	ParticleSystem_GetPlaybackState_Injected_m6D3692AF2279B0B9C9FDFF01A9B0D4B21AD19C4D,
	ParticleSystem_SetPlaybackState_Injected_m8555985301715DFAAE85761592EEFE881B32EBFC,
	ParticleSystem_SetTrails_Injected_m2B3F93DBAD65DB5A22DC136395BA9D329106BC92,
	ParticleSystem_Emit_Injected_mC31E36D577A2D3135436438BFC27B6C76D9ADC72,
	ParticleSystem_TriggerSubEmitterForParticle_Injected_mB9BC0EBDC5CC0D5C392B7287351A8D85337FBBE0,
	ParticleSystem_GetManagedJobHandle_Injected_m294A930F43B3561DDE1ABD8512F7773C3D7B4318,
	ParticleSystem_SetManagedJobHandle_Injected_m58056683B09174D75E69BFF45F191CB91542617C,
	ParticleSystem_ScheduleManagedJob_Injected_m2C68F3306AA63CBE2D5F1D2FB6A6571B031255A9,
	MainModule__ctor_m34F626B568C6D3C80A036409049761C8316E6A76,
	MainModule_get_duration_mD516595EFDD66C83A727BDD7EF495069B4EB6132,
	MainModule_get_loop_mE45A1A4A1DF6E2590B7B13A73A9A95197DA346F8,
	MainModule_set_loop_m9F43A092EFABBE92662DF9EDA73AF8D9C72DABB2,
	MainModule_get_startDelayMultiplier_mC38890C8853463BB25F031BB8884F3D8B0AF581E,
	MainModule_set_startDelayMultiplier_m23FC7BAFE2DCF940C398D6BE53606E23E1715464,
	MainModule_get_startLifetimeMultiplier_m8D2F788F9A1F5768E64F4511F2158E3BFAEB474D,
	MainModule_set_startLifetimeMultiplier_m219938FDA9324C185D998815836B50FEE3AC0D8D,
	MainModule_get_startSpeedMultiplier_mE165D2B967AD8A8827D07DBD694F1C7167C80336,
	MainModule_set_startSpeedMultiplier_mA3C6B512351C3BFA8E64B40936B3353845A637F2,
	MainModule_get_startSizeMultiplier_m820351EE78DF731C1AFA8309ECB5ADF3E1166279,
	MainModule_set_startSizeMultiplier_m231F400517E7F96EFF7C3C31E59B7B8DD0A1DB64,
	MainModule_get_startRotationMultiplier_m72C5648E64889B162553000EF3B7347F47B1F84A,
	MainModule_set_startRotationMultiplier_mB7085FB7FF45BDD4C71E08226C6D5F5635CB47EE,
	MainModule_get_startRotationXMultiplier_m15710A32A7861DAF1A0CD55DB1835EC22617F987,
	MainModule_set_startRotationXMultiplier_mA489325EDA3D644481BFF44376627B0B6EFB81CD,
	MainModule_get_startRotationYMultiplier_mC9C0B9123DDF9D37EF3E5291164E16AB95308011,
	MainModule_set_startRotationYMultiplier_m45DC00CC3E94A7CFCEA522F105F93BCE1B727639,
	MainModule_get_startRotationZMultiplier_m736451211917A59B78A3B444963D305BD377898D,
	MainModule_set_startRotationZMultiplier_m9D604421CED73B2B110158F1FAF955832FB9A6D5,
	MainModule_get_startColor_mC8BAD25B55ACF9586BAFEECF047CCE5268123DD4,
	MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42,
	MainModule_get_gravityModifierMultiplier_m875B65CAEE457EB12A5CD763A5AA311DE014629A,
	MainModule_set_gravityModifierMultiplier_mBC64B6D0DEF548A94D0C0303855F43931A1F5684,
	MainModule_get_simulationSpace_mD08447602DF2E2AC9790D900A1BD04AB8D4FD0A2,
	MainModule_set_simulationSpace_m59583EC5A1D9D64EC8C0CCE275CE6669FC8E268F,
	MainModule_get_simulationSpeed_m7BCFDA4BD65598CE9CEF34A029E4C03DAB4A3C53,
	MainModule_set_simulationSpeed_m9EA9E4BE76B372EF9674568810BF4F5B403B2833,
	MainModule_get_scalingMode_m2FEDBC7969A4BD0071B9467AFFD1B98EE64A9C51,
	MainModule_set_scalingMode_m0992C03429ABFBD1021009BF669BF362705D4E23,
	MainModule_get_playOnAwake_m6E07F3ACCC2797500E70AF4A4A33C5C8425A52D5,
	MainModule_set_playOnAwake_m85E164C05D5C39B0B3C6139BE360BAB41B0D7FCF,
	MainModule_get_maxParticles_m4A95D2105C1B414331F15EF86ADBC23CC6C8DD35,
	MainModule_set_maxParticles_m6FD14B44787B44D38DEDAE4FCBE9596A86A0460D,
	MainModule_get_duration_Injected_mBD7B5FEFFD26DB833052D35A3AC492699B4BEE41,
	MainModule_get_loop_Injected_mD8FD195A3CF5008FD319636A3945DE2F4F0EEBD2,
	MainModule_set_loop_Injected_m064E3166CAA11D0842F105E473A231C0C15DE1B3,
	MainModule_get_startDelayMultiplier_Injected_mEC1750CCDB02393B6FB1E766FDF2C875A9DA5247,
	MainModule_set_startDelayMultiplier_Injected_m6C0ADED60F67E9C685F6A4BCAF6E658A498143CC,
	MainModule_get_startLifetimeMultiplier_Injected_m5DFF410088ECCDC91DB86AEFB2BB1DCB6411666D,
	MainModule_set_startLifetimeMultiplier_Injected_m2000D61A48B2CB2AAEE1FCBB141D58299A29446E,
	MainModule_get_startSpeedMultiplier_Injected_mC59B6818EF7C52D84832165A74B0F5AFD2CC02CA,
	MainModule_set_startSpeedMultiplier_Injected_mD454E7C2AF126C2F46265F32E00A7ACA8D632B5C,
	MainModule_get_startSizeMultiplier_Injected_m04AAFC24A6357D932E1F36C9FA94CA955FCB051F,
	MainModule_set_startSizeMultiplier_Injected_m09FF5ECF76CB00E810A2EC29926CF5EE12BD94D5,
	MainModule_get_startRotationMultiplier_Injected_mFF1B0ED143B0700ACBC5499AE1EED0F9C2334FD8,
	MainModule_set_startRotationMultiplier_Injected_m1D25033340DFEC86D7FE7537E0EC7EC9C435A140,
	MainModule_get_startRotationXMultiplier_Injected_mA907E58F38775A2AD084A95BD11470E9312A0132,
	MainModule_set_startRotationXMultiplier_Injected_m68AC8C3218148C2212765904127F87ACB58F6C69,
	MainModule_get_startRotationYMultiplier_Injected_m4A540D71853039082422C270FC1A14226FDFB43C,
	MainModule_set_startRotationYMultiplier_Injected_m05F30D5DAFC51E007F1D350A6CB8963EC269901E,
	MainModule_get_startRotationZMultiplier_Injected_mF08F4933FBA184D447C01ED33E23B0C3B6067735,
	MainModule_set_startRotationZMultiplier_Injected_m3ABFA58E2E34C80CBB9E6BADC32F12CD34B115A0,
	MainModule_get_startColor_Injected_mB8F4FB3EAB113FEE09389BED8F41E0F047128276,
	MainModule_set_startColor_Injected_mBDC348394A0F1920C4123AF0329DECB2869D6EE6,
	MainModule_get_gravityModifierMultiplier_Injected_mB86E9F34B1B61A23C2F5CB9BFD06703BC8B9972D,
	MainModule_set_gravityModifierMultiplier_Injected_m2AEE5F42B889A4ED22D05AA5E76A87F43680D601,
	MainModule_get_simulationSpace_Injected_mD56565C6B7CDDAFDD418BEB202E8759DCA45D69F,
	MainModule_set_simulationSpace_Injected_mFD6F06EA32114BC5C9D9CF3C10A5A0066E40664C,
	MainModule_get_simulationSpeed_Injected_m12263544CFAE95F37D2A9B4C45AB58452AE963F6,
	MainModule_set_simulationSpeed_Injected_m86AEA2C2313AC96FC68800A3871248ED2597E26C,
	MainModule_get_scalingMode_Injected_m0DD794410FA56B46A8651B4FC504CFB2484C0A02,
	MainModule_set_scalingMode_Injected_mEABFC2E8FE084F997A630B8D2A643B2C9249A5A0,
	MainModule_get_playOnAwake_Injected_m87D4B5B2508D9CF9B3C61F84BC001945B20A06C2,
	MainModule_set_playOnAwake_Injected_mAE88821DF50EB8AFD7079C587CC7FB633626B5FB,
	MainModule_get_maxParticles_Injected_m260F43233BCA02794176F596FF08A96BFCBE0D91,
	MainModule_set_maxParticles_Injected_m6D4D611540F53CC4D8F21D9DD7EF3F68FC50F471,
	EmissionModule__ctor_m9DFD2A0BB8BFAD2191C54E6627FC3B0E641EDD51,
	EmissionModule_get_enabled_m9DB0C4A45B5D04CD88355536DD3E1DD1180DF107,
	EmissionModule_set_enabled_mD9FD50C9F5EECD04B22D03E5C00C7DD64D2AC1F1,
	EmissionModule_set_rateOverTime_m4EE4643D8E8B8BEC37BCB16DB76C9B8E4E4C8F48,
	EmissionModule_get_rateOverTimeMultiplier_mF8F50D01C0F4ADB44423852CD75A334179456FBA,
	EmissionModule_get_enabled_Injected_m69F97462BF229B56970ABFBA4BCE59AFAF17386C,
	EmissionModule_set_enabled_Injected_m35208A8279D12A78D5DBCF751BF1760869051C82,
	EmissionModule_set_rateOverTime_Injected_m19782FA56F82A6F0A6AFE6454E662F41098B6E3B,
	EmissionModule_get_rateOverTimeMultiplier_Injected_m1893AB6D1B181596FB7CC1D080313785DB5E5791,
	ShapeModule__ctor_mB0CE9D41BBE5FFA5B70F465541BA63D9BEEA7264,
	CollisionModule__ctor_m08D1EEAC52F7833F28D4FC2563120FF46A9E4220,
	TriggerModule__ctor_mEB88CDC59DC3AFBC343D3042488445F6E8A5AE9D,
	SubEmittersModule__ctor_m39DB9FE3DF4B1E15890712EF4E4024818D568A3C,
	TextureSheetAnimationModule__ctor_mF02B5303373B93D856A5BDF92B4E4AABC7CB2F65,
	Particle_set_lifetime_mCE97F9D17D1E660DB9D9F3244150CB6624A74DC3,
	Particle_set_position_mB749E41CD3C4C6DF501C0D68B6211CF7E2217FC2,
	Particle_set_velocity_m4894BD6A75E7A1FCD433927F93396AA29A59709B,
	Particle_set_remainingLifetime_mE521DF9387892C00B3F21D4F76F3A55E77AFC6BE,
	Particle_set_startLifetime_m7AC09262BBFE8818EA6B61BDB62264184CD27A82,
	Particle_set_startColor_mA7B0363E82B7A7CBB1C83F8C5D49FB7B7AF75554,
	Particle_set_randomSeed_m8FD7A4DB7F8E7EBDEF2C51A28197F8D9D7CB6E04,
	Particle_set_startSize_m7CDEE5B620B3D26B4CC5C1DA7C6E24ACCCF64466,
	Particle_set_rotation3D_m0F402760524A81307FA4940751751C44DF2F77D0,
	Particle_set_angularVelocity3D_mE9A9544DF33CD0CCF4F1CA14994A2C436E4DF1F8,
	MinMaxCurve__ctor_mF14A2DBABFDCF1E855911241A555949CAF212AE3,
	MinMaxCurve_op_Implicit_m8D746D40E6CCBF5E8C4CE39F23A45712BFC372F5,
	MinMaxGradient__ctor_m8D0DE766F67FF065E4C2B6DCD841752C447977AA,
	MinMaxGradient_get_color_m8FA2F2F1E0B9C9E1DFE40CEA325BC5E863C59C90,
	MinMaxGradient_op_Implicit_m5F938B5644AF6BE5DF525839E922B88456B614FA,
	VelocityOverLifetimeModule__ctor_m03EB5FA8BB69309D7DA91CE70EE3A9E65C60AE66,
	LimitVelocityOverLifetimeModule__ctor_mA327985B69E145AAC3FFEE42EE44A81D7800BCCC,
	InheritVelocityModule__ctor_m76B46612309334A443F9939A47FBAAF024606892,
	LifetimeByEmitterSpeedModule__ctor_m80A22170A2E3047A3B364FE232CC4EB289F87BED,
	ForceOverLifetimeModule__ctor_m4FF722421C5B1449CB900FF0506EF33549A450CD,
	ColorOverLifetimeModule__ctor_m3AFAD44AD8E69F213881272693C23536EEBAB71D,
	ColorBySpeedModule__ctor_m71C9D80CE70BB41F782B4C262D0FE40477A7D0AF,
	SizeOverLifetimeModule__ctor_mCA80885490FC3B1010887B10BDE21A0DACEFB557,
	SizeBySpeedModule__ctor_m479D4802A862D4E07AAF2B47833852A62A17F6DE,
	RotationOverLifetimeModule__ctor_mA0E0BDF5A88582DFEC3FAB42F8B237611C8E0182,
	RotationBySpeedModule__ctor_mCEAFF0BBEC6AB5FB3E59FD1584B22079A2142248,
	ExternalForcesModule__ctor_m2FB0D6776AAAB6731E3E80BA6ADD858458976DF5,
	NoiseModule__ctor_mE450BC95A9B436FA1627E4D24B7B4E7E723DDB5B,
	LightsModule__ctor_mD59B002A60EB418DACA4986EC1C4D34B8DAC555F,
	TrailModule__ctor_m15960ADC2381E58BE70FF2653288599694915412,
	CustomDataModule__ctor_m8F46AC54F3CC41A63AFE9856B18CC41A9A237277,
	ParticleSystemRenderer_EnableVertexStreams_mA2631E5FF8C6CA157E26581BDC47506837AB4696,
	ParticleSystemRenderer_DisableVertexStreams_m1317B70228142564B45B3E354615237C312FA9F4,
	ParticleSystemRenderer_AreVertexStreamsEnabled_m70D3591D4793FEE8BC879439968884E7889CBD04,
	ParticleSystemRenderer_GetEnabledVertexStreams_mA38CE0B9D50452BE6405ADF18050306C22A29CDC,
	ParticleSystemRenderer_Internal_SetVertexStreams_mA00D4C6C868E8309E715015704CCFC12A4CBD8FD,
	ParticleSystemRenderer_Internal_GetEnabledVertexStreams_m1AA436781571C985E87EE8706AF264C977D7A979,
	ParticleSystemRenderer_get_alignment_m5D4063743194C66B9671E5CBC3DC5803AAB8D023,
	ParticleSystemRenderer_set_alignment_m0A6049A94D26A72853875B9A46CA4BE829B439A7,
	ParticleSystemRenderer_get_renderMode_mE203DEDF25DAA9E549CECD34F7552E8B39A81576,
	ParticleSystemRenderer_set_renderMode_mEA05819300408CA36E2D7AA83E15451A22B83D69,
	ParticleSystemRenderer_get_sortMode_m9BDC333B6BFDBA07EB7254E614B819A35BE54275,
	ParticleSystemRenderer_set_sortMode_mC3736E7991D0D8C55DAF0D40E872BACADF18A560,
	ParticleSystemRenderer_get_lengthScale_m2967DAF0E340DE04D08C03B0895496D90E1F8C20,
	ParticleSystemRenderer_set_lengthScale_m082D8A542FC1A7FA8772CE01C98DC44C359FFB8A,
	ParticleSystemRenderer_get_velocityScale_mF549344ED73B7231CE97A0C6E9D76DFA74E0CBBB,
	ParticleSystemRenderer_set_velocityScale_mB3BADC3EB04FE4BDD4BDE4E2D4725371BD0AA034,
	ParticleSystemRenderer_get_cameraVelocityScale_m4EFFB41B99BAD7CC6CBBC1E8E56BE91B459B386C,
	ParticleSystemRenderer_set_cameraVelocityScale_m05772225B1824D2AE906FF68CA45C5901C4F9E95,
	ParticleSystemRenderer_get_normalDirection_m4F7D8E0376239DD36E1526BBA511B76133B57C9D,
	ParticleSystemRenderer_set_normalDirection_mFC60F14E941A1C4939F977F22B0D081A740AB5F2,
	ParticleSystemRenderer_get_shadowBias_m8324464177725CD2C3A4FEB227F7FD00394FF53D,
	ParticleSystemRenderer_set_shadowBias_mCB3BD9449A66F4F20077BFB970D1A6E99D6EDA2A,
	ParticleSystemRenderer_get_sortingFudge_m2F9B4EFF4F196E685AECB9462D69B13E340C43EA,
	ParticleSystemRenderer_set_sortingFudge_m619B3A624EF266C7DE36BE083085E327033ADBAF,
	ParticleSystemRenderer_get_minParticleSize_m1BDB16135B182231F359499E811BF5274664E140,
	ParticleSystemRenderer_set_minParticleSize_m1FDD2EEFBF60703A7AC154FC91DD5015F65A34F4,
	ParticleSystemRenderer_get_maxParticleSize_mA9131C2FE7C8784D02EC94E20E7F5734C06C8EB4,
	ParticleSystemRenderer_set_maxParticleSize_m7D19DF8E661235A19910F216DFDBBD447940CD06,
	ParticleSystemRenderer_get_pivot_m19CA73F7EE2F456F16E9E4753FC197A10649A6CE,
	ParticleSystemRenderer_set_pivot_mF7A808C4BD4258937742A26C4E993902A4AA5C80,
	ParticleSystemRenderer_get_flip_m5499AACC6A0BF4AD6ECB0919121D219BDEE2D0BC,
	ParticleSystemRenderer_set_flip_m6148ED3103BBC6999C1121DFEEF355B139CEA051,
	ParticleSystemRenderer_get_maskInteraction_mAE4B3D48137AA10DEA775FD30A2D161D54747AE8,
	ParticleSystemRenderer_set_maskInteraction_mFC9DD6C5CDADD003946F4CDA508D5A814992F2BB,
	ParticleSystemRenderer_get_trailMaterial_mB7C8AE9C0D9B9A2092E2FCF55AA6CFE3B0B3A400,
	ParticleSystemRenderer_set_trailMaterial_mB28936A342D1805B3F8246DC5DD531970452D016,
	ParticleSystemRenderer_get_enableGPUInstancing_mAC82F715FF3D827AB2EE7AF70FF0CC828DD71B34,
	ParticleSystemRenderer_set_enableGPUInstancing_m643A1F548832BECBB84384F2A85E724E5FBE3D0E,
	ParticleSystemRenderer_get_allowRoll_m8D1E22AA8E4426C79CB995899196CF22FBF17023,
	ParticleSystemRenderer_set_allowRoll_mB321729C721EBFCA6688F9AC4DBAF38A166D8CE7,
	ParticleSystemRenderer_get_freeformStretching_m93B2D5A92718928D43E185DF0B0F2B13CFF4889E,
	ParticleSystemRenderer_set_freeformStretching_m4388C3A17F73D42B6A3951BA30D4805E9869F14B,
	ParticleSystemRenderer_get_rotateWithStretchDirection_m1E966F0539AA2A9C806652E59158EC939DBF5000,
	ParticleSystemRenderer_set_rotateWithStretchDirection_m027CFD4D235694227FA2FA0F01BA663E8392E2BC,
	ParticleSystemRenderer_get_mesh_m32A036955DAEA0FDDA2D4B8EE3D5AEE7FD67A92F,
	ParticleSystemRenderer_set_mesh_m5AEE1784648604DA92FD5B99759D366EA7C76C65,
	ParticleSystemRenderer_GetMeshes_m1B36A6BFF152AAE5520D727976E3DA26722C3A75,
	ParticleSystemRenderer_SetMeshes_mAD7FCBD6E24A447D5304058EA04F751F85AF5A99,
	ParticleSystemRenderer_SetMeshes_m1528C9A3211F04578ECBC89E84125ABFE82796DC,
	ParticleSystemRenderer_get_meshCount_mCB20BE0EC5F2671EF6F6119227130F3473C3302D,
	ParticleSystemRenderer_BakeMesh_m0833E4AFD66E76A995B877E9759F3497EECFC3A6,
	ParticleSystemRenderer_BakeMesh_m1A53F569D7DFCBE964E66D5E3ACA11D85ED56C9E,
	ParticleSystemRenderer_BakeTrailsMesh_m07C7C29BB8DBA0621E414DC443838F287AF58DFE,
	ParticleSystemRenderer_BakeTrailsMesh_m7BE3F730731BBFA0C6A9399BF91813236AC14E3F,
	ParticleSystemRenderer_get_activeVertexStreamsCount_m948DD12ED56E758FF7F55DF8D1D9B6D644A3D3FE,
	ParticleSystemRenderer_SetActiveVertexStreams_m6915A0D7D1E0DCEBEA77A31DC78BE0CE8DBEAD22,
	ParticleSystemRenderer_GetActiveVertexStreams_m32C738BAD8EAB54463B9BBCA2CAA0324BC702014,
	ParticleSystemRenderer__ctor_mD6D0E9A92CF103E60163821DCC7C3D6DF3AA838B,
	ParticleSystemRenderer_get_pivot_Injected_mB393D1D0F0C69BD2F52A282FD0868E3E2585488C,
	ParticleSystemRenderer_set_pivot_Injected_m8CE9A3B8B0EB136F3E658CFB2B00616D7D8A8AB3,
	ParticleSystemRenderer_get_flip_Injected_m08B63FCDBAB2C235981432A80901FE97A29DA985,
	ParticleSystemRenderer_set_flip_Injected_mE957C7B1BCD98C9D2D19900F1CC8EE50ED1DD688,
};
extern void MainModule__ctor_m34F626B568C6D3C80A036409049761C8316E6A76_AdjustorThunk (void);
extern void MainModule_get_duration_mD516595EFDD66C83A727BDD7EF495069B4EB6132_AdjustorThunk (void);
extern void MainModule_get_loop_mE45A1A4A1DF6E2590B7B13A73A9A95197DA346F8_AdjustorThunk (void);
extern void MainModule_set_loop_m9F43A092EFABBE92662DF9EDA73AF8D9C72DABB2_AdjustorThunk (void);
extern void MainModule_get_startDelayMultiplier_mC38890C8853463BB25F031BB8884F3D8B0AF581E_AdjustorThunk (void);
extern void MainModule_set_startDelayMultiplier_m23FC7BAFE2DCF940C398D6BE53606E23E1715464_AdjustorThunk (void);
extern void MainModule_get_startLifetimeMultiplier_m8D2F788F9A1F5768E64F4511F2158E3BFAEB474D_AdjustorThunk (void);
extern void MainModule_set_startLifetimeMultiplier_m219938FDA9324C185D998815836B50FEE3AC0D8D_AdjustorThunk (void);
extern void MainModule_get_startSpeedMultiplier_mE165D2B967AD8A8827D07DBD694F1C7167C80336_AdjustorThunk (void);
extern void MainModule_set_startSpeedMultiplier_mA3C6B512351C3BFA8E64B40936B3353845A637F2_AdjustorThunk (void);
extern void MainModule_get_startSizeMultiplier_m820351EE78DF731C1AFA8309ECB5ADF3E1166279_AdjustorThunk (void);
extern void MainModule_set_startSizeMultiplier_m231F400517E7F96EFF7C3C31E59B7B8DD0A1DB64_AdjustorThunk (void);
extern void MainModule_get_startRotationMultiplier_m72C5648E64889B162553000EF3B7347F47B1F84A_AdjustorThunk (void);
extern void MainModule_set_startRotationMultiplier_mB7085FB7FF45BDD4C71E08226C6D5F5635CB47EE_AdjustorThunk (void);
extern void MainModule_get_startRotationXMultiplier_m15710A32A7861DAF1A0CD55DB1835EC22617F987_AdjustorThunk (void);
extern void MainModule_set_startRotationXMultiplier_mA489325EDA3D644481BFF44376627B0B6EFB81CD_AdjustorThunk (void);
extern void MainModule_get_startRotationYMultiplier_mC9C0B9123DDF9D37EF3E5291164E16AB95308011_AdjustorThunk (void);
extern void MainModule_set_startRotationYMultiplier_m45DC00CC3E94A7CFCEA522F105F93BCE1B727639_AdjustorThunk (void);
extern void MainModule_get_startRotationZMultiplier_m736451211917A59B78A3B444963D305BD377898D_AdjustorThunk (void);
extern void MainModule_set_startRotationZMultiplier_m9D604421CED73B2B110158F1FAF955832FB9A6D5_AdjustorThunk (void);
extern void MainModule_get_startColor_mC8BAD25B55ACF9586BAFEECF047CCE5268123DD4_AdjustorThunk (void);
extern void MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42_AdjustorThunk (void);
extern void MainModule_get_gravityModifierMultiplier_m875B65CAEE457EB12A5CD763A5AA311DE014629A_AdjustorThunk (void);
extern void MainModule_set_gravityModifierMultiplier_mBC64B6D0DEF548A94D0C0303855F43931A1F5684_AdjustorThunk (void);
extern void MainModule_get_simulationSpace_mD08447602DF2E2AC9790D900A1BD04AB8D4FD0A2_AdjustorThunk (void);
extern void MainModule_set_simulationSpace_m59583EC5A1D9D64EC8C0CCE275CE6669FC8E268F_AdjustorThunk (void);
extern void MainModule_get_simulationSpeed_m7BCFDA4BD65598CE9CEF34A029E4C03DAB4A3C53_AdjustorThunk (void);
extern void MainModule_set_simulationSpeed_m9EA9E4BE76B372EF9674568810BF4F5B403B2833_AdjustorThunk (void);
extern void MainModule_get_scalingMode_m2FEDBC7969A4BD0071B9467AFFD1B98EE64A9C51_AdjustorThunk (void);
extern void MainModule_set_scalingMode_m0992C03429ABFBD1021009BF669BF362705D4E23_AdjustorThunk (void);
extern void MainModule_get_playOnAwake_m6E07F3ACCC2797500E70AF4A4A33C5C8425A52D5_AdjustorThunk (void);
extern void MainModule_set_playOnAwake_m85E164C05D5C39B0B3C6139BE360BAB41B0D7FCF_AdjustorThunk (void);
extern void MainModule_get_maxParticles_m4A95D2105C1B414331F15EF86ADBC23CC6C8DD35_AdjustorThunk (void);
extern void MainModule_set_maxParticles_m6FD14B44787B44D38DEDAE4FCBE9596A86A0460D_AdjustorThunk (void);
extern void EmissionModule__ctor_m9DFD2A0BB8BFAD2191C54E6627FC3B0E641EDD51_AdjustorThunk (void);
extern void EmissionModule_get_enabled_m9DB0C4A45B5D04CD88355536DD3E1DD1180DF107_AdjustorThunk (void);
extern void EmissionModule_set_enabled_mD9FD50C9F5EECD04B22D03E5C00C7DD64D2AC1F1_AdjustorThunk (void);
extern void EmissionModule_set_rateOverTime_m4EE4643D8E8B8BEC37BCB16DB76C9B8E4E4C8F48_AdjustorThunk (void);
extern void EmissionModule_get_rateOverTimeMultiplier_mF8F50D01C0F4ADB44423852CD75A334179456FBA_AdjustorThunk (void);
extern void ShapeModule__ctor_mB0CE9D41BBE5FFA5B70F465541BA63D9BEEA7264_AdjustorThunk (void);
extern void CollisionModule__ctor_m08D1EEAC52F7833F28D4FC2563120FF46A9E4220_AdjustorThunk (void);
extern void TriggerModule__ctor_mEB88CDC59DC3AFBC343D3042488445F6E8A5AE9D_AdjustorThunk (void);
extern void SubEmittersModule__ctor_m39DB9FE3DF4B1E15890712EF4E4024818D568A3C_AdjustorThunk (void);
extern void TextureSheetAnimationModule__ctor_mF02B5303373B93D856A5BDF92B4E4AABC7CB2F65_AdjustorThunk (void);
extern void Particle_set_lifetime_mCE97F9D17D1E660DB9D9F3244150CB6624A74DC3_AdjustorThunk (void);
extern void Particle_set_position_mB749E41CD3C4C6DF501C0D68B6211CF7E2217FC2_AdjustorThunk (void);
extern void Particle_set_velocity_m4894BD6A75E7A1FCD433927F93396AA29A59709B_AdjustorThunk (void);
extern void Particle_set_remainingLifetime_mE521DF9387892C00B3F21D4F76F3A55E77AFC6BE_AdjustorThunk (void);
extern void Particle_set_startLifetime_m7AC09262BBFE8818EA6B61BDB62264184CD27A82_AdjustorThunk (void);
extern void Particle_set_startColor_mA7B0363E82B7A7CBB1C83F8C5D49FB7B7AF75554_AdjustorThunk (void);
extern void Particle_set_randomSeed_m8FD7A4DB7F8E7EBDEF2C51A28197F8D9D7CB6E04_AdjustorThunk (void);
extern void Particle_set_startSize_m7CDEE5B620B3D26B4CC5C1DA7C6E24ACCCF64466_AdjustorThunk (void);
extern void Particle_set_rotation3D_m0F402760524A81307FA4940751751C44DF2F77D0_AdjustorThunk (void);
extern void Particle_set_angularVelocity3D_mE9A9544DF33CD0CCF4F1CA14994A2C436E4DF1F8_AdjustorThunk (void);
extern void MinMaxCurve__ctor_mF14A2DBABFDCF1E855911241A555949CAF212AE3_AdjustorThunk (void);
extern void MinMaxGradient__ctor_m8D0DE766F67FF065E4C2B6DCD841752C447977AA_AdjustorThunk (void);
extern void MinMaxGradient_get_color_m8FA2F2F1E0B9C9E1DFE40CEA325BC5E863C59C90_AdjustorThunk (void);
extern void VelocityOverLifetimeModule__ctor_m03EB5FA8BB69309D7DA91CE70EE3A9E65C60AE66_AdjustorThunk (void);
extern void LimitVelocityOverLifetimeModule__ctor_mA327985B69E145AAC3FFEE42EE44A81D7800BCCC_AdjustorThunk (void);
extern void InheritVelocityModule__ctor_m76B46612309334A443F9939A47FBAAF024606892_AdjustorThunk (void);
extern void LifetimeByEmitterSpeedModule__ctor_m80A22170A2E3047A3B364FE232CC4EB289F87BED_AdjustorThunk (void);
extern void ForceOverLifetimeModule__ctor_m4FF722421C5B1449CB900FF0506EF33549A450CD_AdjustorThunk (void);
extern void ColorOverLifetimeModule__ctor_m3AFAD44AD8E69F213881272693C23536EEBAB71D_AdjustorThunk (void);
extern void ColorBySpeedModule__ctor_m71C9D80CE70BB41F782B4C262D0FE40477A7D0AF_AdjustorThunk (void);
extern void SizeOverLifetimeModule__ctor_mCA80885490FC3B1010887B10BDE21A0DACEFB557_AdjustorThunk (void);
extern void SizeBySpeedModule__ctor_m479D4802A862D4E07AAF2B47833852A62A17F6DE_AdjustorThunk (void);
extern void RotationOverLifetimeModule__ctor_mA0E0BDF5A88582DFEC3FAB42F8B237611C8E0182_AdjustorThunk (void);
extern void RotationBySpeedModule__ctor_mCEAFF0BBEC6AB5FB3E59FD1584B22079A2142248_AdjustorThunk (void);
extern void ExternalForcesModule__ctor_m2FB0D6776AAAB6731E3E80BA6ADD858458976DF5_AdjustorThunk (void);
extern void NoiseModule__ctor_mE450BC95A9B436FA1627E4D24B7B4E7E723DDB5B_AdjustorThunk (void);
extern void LightsModule__ctor_mD59B002A60EB418DACA4986EC1C4D34B8DAC555F_AdjustorThunk (void);
extern void TrailModule__ctor_m15960ADC2381E58BE70FF2653288599694915412_AdjustorThunk (void);
extern void CustomDataModule__ctor_m8F46AC54F3CC41A63AFE9856B18CC41A9A237277_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[73] = 
{
	{ 0x0600008D, MainModule__ctor_m34F626B568C6D3C80A036409049761C8316E6A76_AdjustorThunk },
	{ 0x0600008E, MainModule_get_duration_mD516595EFDD66C83A727BDD7EF495069B4EB6132_AdjustorThunk },
	{ 0x0600008F, MainModule_get_loop_mE45A1A4A1DF6E2590B7B13A73A9A95197DA346F8_AdjustorThunk },
	{ 0x06000090, MainModule_set_loop_m9F43A092EFABBE92662DF9EDA73AF8D9C72DABB2_AdjustorThunk },
	{ 0x06000091, MainModule_get_startDelayMultiplier_mC38890C8853463BB25F031BB8884F3D8B0AF581E_AdjustorThunk },
	{ 0x06000092, MainModule_set_startDelayMultiplier_m23FC7BAFE2DCF940C398D6BE53606E23E1715464_AdjustorThunk },
	{ 0x06000093, MainModule_get_startLifetimeMultiplier_m8D2F788F9A1F5768E64F4511F2158E3BFAEB474D_AdjustorThunk },
	{ 0x06000094, MainModule_set_startLifetimeMultiplier_m219938FDA9324C185D998815836B50FEE3AC0D8D_AdjustorThunk },
	{ 0x06000095, MainModule_get_startSpeedMultiplier_mE165D2B967AD8A8827D07DBD694F1C7167C80336_AdjustorThunk },
	{ 0x06000096, MainModule_set_startSpeedMultiplier_mA3C6B512351C3BFA8E64B40936B3353845A637F2_AdjustorThunk },
	{ 0x06000097, MainModule_get_startSizeMultiplier_m820351EE78DF731C1AFA8309ECB5ADF3E1166279_AdjustorThunk },
	{ 0x06000098, MainModule_set_startSizeMultiplier_m231F400517E7F96EFF7C3C31E59B7B8DD0A1DB64_AdjustorThunk },
	{ 0x06000099, MainModule_get_startRotationMultiplier_m72C5648E64889B162553000EF3B7347F47B1F84A_AdjustorThunk },
	{ 0x0600009A, MainModule_set_startRotationMultiplier_mB7085FB7FF45BDD4C71E08226C6D5F5635CB47EE_AdjustorThunk },
	{ 0x0600009B, MainModule_get_startRotationXMultiplier_m15710A32A7861DAF1A0CD55DB1835EC22617F987_AdjustorThunk },
	{ 0x0600009C, MainModule_set_startRotationXMultiplier_mA489325EDA3D644481BFF44376627B0B6EFB81CD_AdjustorThunk },
	{ 0x0600009D, MainModule_get_startRotationYMultiplier_mC9C0B9123DDF9D37EF3E5291164E16AB95308011_AdjustorThunk },
	{ 0x0600009E, MainModule_set_startRotationYMultiplier_m45DC00CC3E94A7CFCEA522F105F93BCE1B727639_AdjustorThunk },
	{ 0x0600009F, MainModule_get_startRotationZMultiplier_m736451211917A59B78A3B444963D305BD377898D_AdjustorThunk },
	{ 0x060000A0, MainModule_set_startRotationZMultiplier_m9D604421CED73B2B110158F1FAF955832FB9A6D5_AdjustorThunk },
	{ 0x060000A1, MainModule_get_startColor_mC8BAD25B55ACF9586BAFEECF047CCE5268123DD4_AdjustorThunk },
	{ 0x060000A2, MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42_AdjustorThunk },
	{ 0x060000A3, MainModule_get_gravityModifierMultiplier_m875B65CAEE457EB12A5CD763A5AA311DE014629A_AdjustorThunk },
	{ 0x060000A4, MainModule_set_gravityModifierMultiplier_mBC64B6D0DEF548A94D0C0303855F43931A1F5684_AdjustorThunk },
	{ 0x060000A5, MainModule_get_simulationSpace_mD08447602DF2E2AC9790D900A1BD04AB8D4FD0A2_AdjustorThunk },
	{ 0x060000A6, MainModule_set_simulationSpace_m59583EC5A1D9D64EC8C0CCE275CE6669FC8E268F_AdjustorThunk },
	{ 0x060000A7, MainModule_get_simulationSpeed_m7BCFDA4BD65598CE9CEF34A029E4C03DAB4A3C53_AdjustorThunk },
	{ 0x060000A8, MainModule_set_simulationSpeed_m9EA9E4BE76B372EF9674568810BF4F5B403B2833_AdjustorThunk },
	{ 0x060000A9, MainModule_get_scalingMode_m2FEDBC7969A4BD0071B9467AFFD1B98EE64A9C51_AdjustorThunk },
	{ 0x060000AA, MainModule_set_scalingMode_m0992C03429ABFBD1021009BF669BF362705D4E23_AdjustorThunk },
	{ 0x060000AB, MainModule_get_playOnAwake_m6E07F3ACCC2797500E70AF4A4A33C5C8425A52D5_AdjustorThunk },
	{ 0x060000AC, MainModule_set_playOnAwake_m85E164C05D5C39B0B3C6139BE360BAB41B0D7FCF_AdjustorThunk },
	{ 0x060000AD, MainModule_get_maxParticles_m4A95D2105C1B414331F15EF86ADBC23CC6C8DD35_AdjustorThunk },
	{ 0x060000AE, MainModule_set_maxParticles_m6FD14B44787B44D38DEDAE4FCBE9596A86A0460D_AdjustorThunk },
	{ 0x060000D0, EmissionModule__ctor_m9DFD2A0BB8BFAD2191C54E6627FC3B0E641EDD51_AdjustorThunk },
	{ 0x060000D1, EmissionModule_get_enabled_m9DB0C4A45B5D04CD88355536DD3E1DD1180DF107_AdjustorThunk },
	{ 0x060000D2, EmissionModule_set_enabled_mD9FD50C9F5EECD04B22D03E5C00C7DD64D2AC1F1_AdjustorThunk },
	{ 0x060000D3, EmissionModule_set_rateOverTime_m4EE4643D8E8B8BEC37BCB16DB76C9B8E4E4C8F48_AdjustorThunk },
	{ 0x060000D4, EmissionModule_get_rateOverTimeMultiplier_mF8F50D01C0F4ADB44423852CD75A334179456FBA_AdjustorThunk },
	{ 0x060000D9, ShapeModule__ctor_mB0CE9D41BBE5FFA5B70F465541BA63D9BEEA7264_AdjustorThunk },
	{ 0x060000DA, CollisionModule__ctor_m08D1EEAC52F7833F28D4FC2563120FF46A9E4220_AdjustorThunk },
	{ 0x060000DB, TriggerModule__ctor_mEB88CDC59DC3AFBC343D3042488445F6E8A5AE9D_AdjustorThunk },
	{ 0x060000DC, SubEmittersModule__ctor_m39DB9FE3DF4B1E15890712EF4E4024818D568A3C_AdjustorThunk },
	{ 0x060000DD, TextureSheetAnimationModule__ctor_mF02B5303373B93D856A5BDF92B4E4AABC7CB2F65_AdjustorThunk },
	{ 0x060000DE, Particle_set_lifetime_mCE97F9D17D1E660DB9D9F3244150CB6624A74DC3_AdjustorThunk },
	{ 0x060000DF, Particle_set_position_mB749E41CD3C4C6DF501C0D68B6211CF7E2217FC2_AdjustorThunk },
	{ 0x060000E0, Particle_set_velocity_m4894BD6A75E7A1FCD433927F93396AA29A59709B_AdjustorThunk },
	{ 0x060000E1, Particle_set_remainingLifetime_mE521DF9387892C00B3F21D4F76F3A55E77AFC6BE_AdjustorThunk },
	{ 0x060000E2, Particle_set_startLifetime_m7AC09262BBFE8818EA6B61BDB62264184CD27A82_AdjustorThunk },
	{ 0x060000E3, Particle_set_startColor_mA7B0363E82B7A7CBB1C83F8C5D49FB7B7AF75554_AdjustorThunk },
	{ 0x060000E4, Particle_set_randomSeed_m8FD7A4DB7F8E7EBDEF2C51A28197F8D9D7CB6E04_AdjustorThunk },
	{ 0x060000E5, Particle_set_startSize_m7CDEE5B620B3D26B4CC5C1DA7C6E24ACCCF64466_AdjustorThunk },
	{ 0x060000E6, Particle_set_rotation3D_m0F402760524A81307FA4940751751C44DF2F77D0_AdjustorThunk },
	{ 0x060000E7, Particle_set_angularVelocity3D_mE9A9544DF33CD0CCF4F1CA14994A2C436E4DF1F8_AdjustorThunk },
	{ 0x060000E8, MinMaxCurve__ctor_mF14A2DBABFDCF1E855911241A555949CAF212AE3_AdjustorThunk },
	{ 0x060000EA, MinMaxGradient__ctor_m8D0DE766F67FF065E4C2B6DCD841752C447977AA_AdjustorThunk },
	{ 0x060000EB, MinMaxGradient_get_color_m8FA2F2F1E0B9C9E1DFE40CEA325BC5E863C59C90_AdjustorThunk },
	{ 0x060000ED, VelocityOverLifetimeModule__ctor_m03EB5FA8BB69309D7DA91CE70EE3A9E65C60AE66_AdjustorThunk },
	{ 0x060000EE, LimitVelocityOverLifetimeModule__ctor_mA327985B69E145AAC3FFEE42EE44A81D7800BCCC_AdjustorThunk },
	{ 0x060000EF, InheritVelocityModule__ctor_m76B46612309334A443F9939A47FBAAF024606892_AdjustorThunk },
	{ 0x060000F0, LifetimeByEmitterSpeedModule__ctor_m80A22170A2E3047A3B364FE232CC4EB289F87BED_AdjustorThunk },
	{ 0x060000F1, ForceOverLifetimeModule__ctor_m4FF722421C5B1449CB900FF0506EF33549A450CD_AdjustorThunk },
	{ 0x060000F2, ColorOverLifetimeModule__ctor_m3AFAD44AD8E69F213881272693C23536EEBAB71D_AdjustorThunk },
	{ 0x060000F3, ColorBySpeedModule__ctor_m71C9D80CE70BB41F782B4C262D0FE40477A7D0AF_AdjustorThunk },
	{ 0x060000F4, SizeOverLifetimeModule__ctor_mCA80885490FC3B1010887B10BDE21A0DACEFB557_AdjustorThunk },
	{ 0x060000F5, SizeBySpeedModule__ctor_m479D4802A862D4E07AAF2B47833852A62A17F6DE_AdjustorThunk },
	{ 0x060000F6, RotationOverLifetimeModule__ctor_mA0E0BDF5A88582DFEC3FAB42F8B237611C8E0182_AdjustorThunk },
	{ 0x060000F7, RotationBySpeedModule__ctor_mCEAFF0BBEC6AB5FB3E59FD1584B22079A2142248_AdjustorThunk },
	{ 0x060000F8, ExternalForcesModule__ctor_m2FB0D6776AAAB6731E3E80BA6ADD858458976DF5_AdjustorThunk },
	{ 0x060000F9, NoiseModule__ctor_mE450BC95A9B436FA1627E4D24B7B4E7E723DDB5B_AdjustorThunk },
	{ 0x060000FA, LightsModule__ctor_mD59B002A60EB418DACA4986EC1C4D34B8DAC555F_AdjustorThunk },
	{ 0x060000FB, TrailModule__ctor_m15960ADC2381E58BE70FF2653288599694915412_AdjustorThunk },
	{ 0x060000FC, CustomDataModule__ctor_m8F46AC54F3CC41A63AFE9856B18CC41A9A237277_AdjustorThunk },
};
static const int32_t s_InvokerIndices[314] = 
{
	354,
	2526,
	2881,
	2488,
	2872,
	2480,
	2872,
	2480,
	2881,
	2881,
	2488,
	2872,
	2480,
	2881,
	2488,
	2881,
	2488,
	2881,
	2488,
	2800,
	2406,
	2881,
	2488,
	2897,
	2507,
	2881,
	2488,
	2881,
	2488,
	2828,
	2439,
	2828,
	2439,
	2828,
	2439,
	2872,
	2872,
	2872,
	2872,
	2872,
	2828,
	2881,
	2488,
	2828,
	2439,
	2872,
	2480,
	2872,
	2272,
	2308,
	1724,
	1832,
	1009,
	1537,
	2452,
	660,
	946,
	1336,
	2373,
	786,
	1146,
	1876,
	417,
	771,
	1116,
	1810,
	1537,
	1146,
	2935,
	2527,
	2394,
	2944,
	2528,
	739,
	1062,
	1573,
	2488,
	2480,
	2901,
	2480,
	2901,
	1562,
	2480,
	2901,
	2480,
	2901,
	2203,
	2872,
	2439,
	2439,
	1590,
	2394,
	2439,
	1407,
	1499,
	1451,
	4376,
	4031,
	2901,
	2901,
	2439,
	2789,
	2833,
	2444,
	3852,
	4019,
	2931,
	2924,
	2938,
	2946,
	2930,
	2927,
	2928,
	2926,
	2922,
	2921,
	2940,
	2939,
	2937,
	2936,
	2925,
	2933,
	2920,
	2945,
	2941,
	2942,
	2929,
	2943,
	2923,
	2901,
	1338,
	1338,
	2394,
	2394,
	2394,
	1339,
	1407,
	2394,
	2394,
	3678,
	2452,
	2881,
	2872,
	2480,
	2881,
	2488,
	2881,
	2488,
	2881,
	2488,
	2881,
	2488,
	2881,
	2488,
	2881,
	2488,
	2881,
	2488,
	2881,
	2488,
	2932,
	2525,
	2881,
	2488,
	2828,
	2439,
	2881,
	2488,
	2828,
	2439,
	2872,
	2480,
	2828,
	2439,
	4266,
	4249,
	4024,
	4266,
	4025,
	4266,
	4025,
	4266,
	4025,
	4266,
	4025,
	4266,
	4025,
	4266,
	4025,
	4266,
	4025,
	4266,
	4025,
	4019,
	4019,
	4266,
	4025,
	4156,
	4020,
	4266,
	4025,
	4156,
	4020,
	4249,
	4024,
	4156,
	4020,
	2452,
	2872,
	2480,
	2524,
	2881,
	4249,
	4024,
	4019,
	4266,
	2452,
	2452,
	2452,
	2452,
	2452,
	2488,
	2507,
	2507,
	2488,
	2488,
	2407,
	2439,
	2488,
	2507,
	2507,
	2488,
	4319,
	2406,
	2800,
	4320,
	2452,
	2452,
	2452,
	2452,
	2452,
	2452,
	2452,
	2452,
	2452,
	2452,
	2452,
	2452,
	2452,
	2452,
	2452,
	2452,
	2439,
	2439,
	2160,
	1865,
	1468,
	1865,
	2828,
	2439,
	2828,
	2439,
	2828,
	2439,
	2881,
	2488,
	2881,
	2488,
	2881,
	2488,
	2881,
	2488,
	2881,
	2488,
	2881,
	2488,
	2881,
	2488,
	2881,
	2488,
	2897,
	2507,
	2897,
	2507,
	2828,
	2439,
	2842,
	2452,
	2872,
	2480,
	2872,
	2480,
	2872,
	2480,
	2872,
	2480,
	2842,
	2452,
	1876,
	1537,
	2452,
	2828,
	1547,
	1022,
	1547,
	1022,
	2828,
	2452,
	2452,
	2901,
	2394,
	2394,
	2394,
	2394,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_ParticleSystemModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_ParticleSystemModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_ParticleSystemModule_CodeGenModule = 
{
	"UnityEngine.ParticleSystemModule.dll",
	314,
	s_methodPointers,
	73,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_ParticleSystemModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
