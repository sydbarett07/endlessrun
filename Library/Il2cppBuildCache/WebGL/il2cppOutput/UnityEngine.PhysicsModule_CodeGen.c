﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern void RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E (void);
// 0x00000002 UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern void RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E (void);
// 0x00000003 UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern void RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674 (void);
// 0x00000004 System.Single UnityEngine.RaycastHit::get_distance()
extern void RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA (void);
// 0x00000005 UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern void Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C (void);
// 0x00000006 System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern void Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D (void);
// 0x00000007 UnityEngine.Vector3 UnityEngine.Rigidbody::get_angularVelocity()
extern void Rigidbody_get_angularVelocity_m6737340DF546452900D199246279231D80A21DCF (void);
// 0x00000008 System.Void UnityEngine.Rigidbody::set_angularVelocity(UnityEngine.Vector3)
extern void Rigidbody_set_angularVelocity_m3A40B7F195E9E217AE29A0964D7E7540E2E23080 (void);
// 0x00000009 System.Single UnityEngine.Rigidbody::get_drag()
extern void Rigidbody_get_drag_m0C617963D9BBBC4018D3A8B2DB5D6190615F4A64 (void);
// 0x0000000A System.Void UnityEngine.Rigidbody::set_drag(System.Single)
extern void Rigidbody_set_drag_m60E39BE31529DE5163116785A69FACC77C52DA98 (void);
// 0x0000000B System.Single UnityEngine.Rigidbody::get_angularDrag()
extern void Rigidbody_get_angularDrag_m0E53FD8F8A09DFA941C52C868288DBBC030C5082 (void);
// 0x0000000C System.Void UnityEngine.Rigidbody::set_angularDrag(System.Single)
extern void Rigidbody_set_angularDrag_m8BF3771789B32FB09FDD8066BAFA0A0B661372A4 (void);
// 0x0000000D System.Single UnityEngine.Rigidbody::get_mass()
extern void Rigidbody_get_mass_mB7B19406DAC6336A8244E98BE271BDA8B5C26223 (void);
// 0x0000000E System.Void UnityEngine.Rigidbody::set_mass(System.Single)
extern void Rigidbody_set_mass_m54FCACE073F5E7742DB1D7C0BA19CD0C0F3DDA3F (void);
// 0x0000000F System.Void UnityEngine.Rigidbody::SetDensity(System.Single)
extern void Rigidbody_SetDensity_m794AA2513F044BCFC3BC87AB39387131BA1EA282 (void);
// 0x00000010 System.Boolean UnityEngine.Rigidbody::get_useGravity()
extern void Rigidbody_get_useGravity_mDA0FB6F456377840E6E46C42B9210F93264E2B28 (void);
// 0x00000011 System.Void UnityEngine.Rigidbody::set_useGravity(System.Boolean)
extern void Rigidbody_set_useGravity_m1057292FB3199E87664F40B8BCBA7A7E64D1A096 (void);
// 0x00000012 System.Single UnityEngine.Rigidbody::get_maxDepenetrationVelocity()
extern void Rigidbody_get_maxDepenetrationVelocity_mE2F07995501D4EFAB39C3B5E6A91ECD6D899272E (void);
// 0x00000013 System.Void UnityEngine.Rigidbody::set_maxDepenetrationVelocity(System.Single)
extern void Rigidbody_set_maxDepenetrationVelocity_mF2D2B5F040B69E32E48544EBA5F6D43656798A02 (void);
// 0x00000014 System.Boolean UnityEngine.Rigidbody::get_isKinematic()
extern void Rigidbody_get_isKinematic_m597B48C45021313B6C6C4B126E405EF566C5C80C (void);
// 0x00000015 System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
extern void Rigidbody_set_isKinematic_mCF74D680205544826F2DE2CAB929C9F25409A311 (void);
// 0x00000016 System.Boolean UnityEngine.Rigidbody::get_freezeRotation()
extern void Rigidbody_get_freezeRotation_m52850F7355488269E7026A1E9C6AEB69EA1A1CCE (void);
// 0x00000017 System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
extern void Rigidbody_set_freezeRotation_mE08A39E98D46F82D6DD86CC389D86D242C694D52 (void);
// 0x00000018 UnityEngine.RigidbodyConstraints UnityEngine.Rigidbody::get_constraints()
extern void Rigidbody_get_constraints_m5EBE3E9C0A2084BAAE0C783E212C368A1CB1DD04 (void);
// 0x00000019 System.Void UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)
extern void Rigidbody_set_constraints_mA76F562D16D3BE8889E095D0309C8FE38DA914F1 (void);
// 0x0000001A UnityEngine.CollisionDetectionMode UnityEngine.Rigidbody::get_collisionDetectionMode()
extern void Rigidbody_get_collisionDetectionMode_m1328BE903DD8B2DBD3A67B64FCECFEE91DC49FCE (void);
// 0x0000001B System.Void UnityEngine.Rigidbody::set_collisionDetectionMode(UnityEngine.CollisionDetectionMode)
extern void Rigidbody_set_collisionDetectionMode_m4BC31A84B26540ACD15273774C47F31CDF5381C2 (void);
// 0x0000001C UnityEngine.Vector3 UnityEngine.Rigidbody::get_centerOfMass()
extern void Rigidbody_get_centerOfMass_mEF7838582F7977AAD2952F6A09E2203D9B0ABC53 (void);
// 0x0000001D System.Void UnityEngine.Rigidbody::set_centerOfMass(UnityEngine.Vector3)
extern void Rigidbody_set_centerOfMass_m3B13BE412D99CE5133606643F14501CF5C63CCEC (void);
// 0x0000001E UnityEngine.Vector3 UnityEngine.Rigidbody::get_worldCenterOfMass()
extern void Rigidbody_get_worldCenterOfMass_mF825B0D5110903BD0A3FDC1DC4317902305DE143 (void);
// 0x0000001F UnityEngine.Quaternion UnityEngine.Rigidbody::get_inertiaTensorRotation()
extern void Rigidbody_get_inertiaTensorRotation_m9EFA1F67FF15DC3E996146737149C69A5003D3C1 (void);
// 0x00000020 System.Void UnityEngine.Rigidbody::set_inertiaTensorRotation(UnityEngine.Quaternion)
extern void Rigidbody_set_inertiaTensorRotation_m18E191AC25EE29C24DE6A862A26633A7A21A9D9B (void);
// 0x00000021 UnityEngine.Vector3 UnityEngine.Rigidbody::get_inertiaTensor()
extern void Rigidbody_get_inertiaTensor_m55F35467FD2088D883060D57595AD5BF14BC5C6F (void);
// 0x00000022 System.Void UnityEngine.Rigidbody::set_inertiaTensor(UnityEngine.Vector3)
extern void Rigidbody_set_inertiaTensor_mCDE8A0CCFD0B6464B5F99CC1A54DF5300652D5D1 (void);
// 0x00000023 System.Boolean UnityEngine.Rigidbody::get_detectCollisions()
extern void Rigidbody_get_detectCollisions_m6741D1A8C1942766F95258677352A75451D99D21 (void);
// 0x00000024 System.Void UnityEngine.Rigidbody::set_detectCollisions(System.Boolean)
extern void Rigidbody_set_detectCollisions_mB94256836724071B1EFE622A5E9BA435B6572E9A (void);
// 0x00000025 UnityEngine.Vector3 UnityEngine.Rigidbody::get_position()
extern void Rigidbody_get_position_m5F429382F610E324F39F33E8498A29D0828AD8E8 (void);
// 0x00000026 System.Void UnityEngine.Rigidbody::set_position(UnityEngine.Vector3)
extern void Rigidbody_set_position_mD1A5B0DB2FD8861778CAA575E1D7B72E2AEF4024 (void);
// 0x00000027 UnityEngine.Quaternion UnityEngine.Rigidbody::get_rotation()
extern void Rigidbody_get_rotation_mEB90F9D223B0BA32A1962971E3A93DEE1670D18A (void);
// 0x00000028 System.Void UnityEngine.Rigidbody::set_rotation(UnityEngine.Quaternion)
extern void Rigidbody_set_rotation_m3024C151FEC9BB75735DE9B4BA64F16AA779C5D6 (void);
// 0x00000029 UnityEngine.RigidbodyInterpolation UnityEngine.Rigidbody::get_interpolation()
extern void Rigidbody_get_interpolation_m17422952D016117813B4520CB65842D2537C8E12 (void);
// 0x0000002A System.Void UnityEngine.Rigidbody::set_interpolation(UnityEngine.RigidbodyInterpolation)
extern void Rigidbody_set_interpolation_mEBAF9DF8DE317E2D58848735D26ED9E69CC425CE (void);
// 0x0000002B System.Int32 UnityEngine.Rigidbody::get_solverIterations()
extern void Rigidbody_get_solverIterations_m9A62E73E257401EFDC44E7338CD36E9E006E8CA1 (void);
// 0x0000002C System.Void UnityEngine.Rigidbody::set_solverIterations(System.Int32)
extern void Rigidbody_set_solverIterations_m71644E473B3DB97C10199A7D5AEBAD790DD586EC (void);
// 0x0000002D System.Single UnityEngine.Rigidbody::get_sleepThreshold()
extern void Rigidbody_get_sleepThreshold_m1469D01B7634195511EEC913BA69617618DE5EEC (void);
// 0x0000002E System.Void UnityEngine.Rigidbody::set_sleepThreshold(System.Single)
extern void Rigidbody_set_sleepThreshold_mA71700BF6225A4366FE6BE1365E26B0BF832C00B (void);
// 0x0000002F System.Single UnityEngine.Rigidbody::get_maxAngularVelocity()
extern void Rigidbody_get_maxAngularVelocity_m8804A10C23EF0C073D5D829ED6BBF5253B0A2DFE (void);
// 0x00000030 System.Void UnityEngine.Rigidbody::set_maxAngularVelocity(System.Single)
extern void Rigidbody_set_maxAngularVelocity_m55AB6C34E2C33E6EABB07EFCB7AE443F5D3BD060 (void);
// 0x00000031 System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
extern void Rigidbody_MovePosition_mB3CBBF21FD0ABB88BC6C004B993DED25673001C7 (void);
// 0x00000032 System.Void UnityEngine.Rigidbody::MoveRotation(UnityEngine.Quaternion)
extern void Rigidbody_MoveRotation_m08A1449DC0D514A70065CD80D067597765BDA5B2 (void);
// 0x00000033 System.Void UnityEngine.Rigidbody::Sleep()
extern void Rigidbody_Sleep_m60350AEF3E52D57FBE448CADBC06BA98DAEA2115 (void);
// 0x00000034 System.Boolean UnityEngine.Rigidbody::IsSleeping()
extern void Rigidbody_IsSleeping_mD41EDC4429411110D1FE36101C89B0277F8D62BA (void);
// 0x00000035 System.Void UnityEngine.Rigidbody::WakeUp()
extern void Rigidbody_WakeUp_m89308E6756834CF3705D9CDE89D636EEEE409316 (void);
// 0x00000036 System.Void UnityEngine.Rigidbody::ResetCenterOfMass()
extern void Rigidbody_ResetCenterOfMass_m6F34341B309D3AAE2A8B3CC963A1E38383EB0FD9 (void);
// 0x00000037 System.Void UnityEngine.Rigidbody::ResetInertiaTensor()
extern void Rigidbody_ResetInertiaTensor_m8888FABEC8D40828D8F594915AF20CB01E9511D7 (void);
// 0x00000038 UnityEngine.Vector3 UnityEngine.Rigidbody::GetRelativePointVelocity(UnityEngine.Vector3)
extern void Rigidbody_GetRelativePointVelocity_mBE38822FEE8B1D4ECBDCD65AA20D13D0423B1FCF (void);
// 0x00000039 UnityEngine.Vector3 UnityEngine.Rigidbody::GetPointVelocity(UnityEngine.Vector3)
extern void Rigidbody_GetPointVelocity_m48BA0D6C61636CADE0764CC2BF5B02E1B61F1273 (void);
// 0x0000003A System.Int32 UnityEngine.Rigidbody::get_solverVelocityIterations()
extern void Rigidbody_get_solverVelocityIterations_m94EF00A61F9CD356D0E56D7285D06EAC4FD2ABEA (void);
// 0x0000003B System.Void UnityEngine.Rigidbody::set_solverVelocityIterations(System.Int32)
extern void Rigidbody_set_solverVelocityIterations_mA345A4D3722FFFEDD9391720F9296A9721647AED (void);
// 0x0000003C System.Single UnityEngine.Rigidbody::get_sleepVelocity()
extern void Rigidbody_get_sleepVelocity_m9907C2F2A20D66AC6091E166E89EF93E1C7C630A (void);
// 0x0000003D System.Void UnityEngine.Rigidbody::set_sleepVelocity(System.Single)
extern void Rigidbody_set_sleepVelocity_m1D805ECFA84DCC1E4FB952D9831F9BA64DC1DA07 (void);
// 0x0000003E System.Single UnityEngine.Rigidbody::get_sleepAngularVelocity()
extern void Rigidbody_get_sleepAngularVelocity_mFD5A89F4453A180761602A5464372C776C62097F (void);
// 0x0000003F System.Void UnityEngine.Rigidbody::set_sleepAngularVelocity(System.Single)
extern void Rigidbody_set_sleepAngularVelocity_m6585DD80CB21ED9C256D994144587355C3ED2DFA (void);
// 0x00000040 System.Void UnityEngine.Rigidbody::SetMaxAngularVelocity(System.Single)
extern void Rigidbody_SetMaxAngularVelocity_mDAB13E69D76596245D830FAE2763FEEB8FA06BEA (void);
// 0x00000041 System.Boolean UnityEngine.Rigidbody::get_useConeFriction()
extern void Rigidbody_get_useConeFriction_mED6C377EDCC693043A6A1954C248F2F23F634536 (void);
// 0x00000042 System.Void UnityEngine.Rigidbody::set_useConeFriction(System.Boolean)
extern void Rigidbody_set_useConeFriction_m8D46BEAEF34497ED930DFE237EBA066DC4704EEA (void);
// 0x00000043 System.Int32 UnityEngine.Rigidbody::get_solverIterationCount()
extern void Rigidbody_get_solverIterationCount_mC0EE8B9789EC02A9471E1FDDB6A480E1B393A2F9 (void);
// 0x00000044 System.Void UnityEngine.Rigidbody::set_solverIterationCount(System.Int32)
extern void Rigidbody_set_solverIterationCount_mD46FF60DA43935C791BA3C377AB59474BF7E5275 (void);
// 0x00000045 System.Int32 UnityEngine.Rigidbody::get_solverVelocityIterationCount()
extern void Rigidbody_get_solverVelocityIterationCount_mE9CCC9ABBAED831FFB6C97C9B77653D1F5504CA9 (void);
// 0x00000046 System.Void UnityEngine.Rigidbody::set_solverVelocityIterationCount(System.Int32)
extern void Rigidbody_set_solverVelocityIterationCount_m68E2D9D094E6DC605C9C541A20067C74D19701CA (void);
// 0x00000047 System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700 (void);
// 0x00000048 System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
extern void Rigidbody_AddForce_mDFB0D57C25682B826999B0074F5C0FD399C6401D (void);
// 0x00000049 System.Void UnityEngine.Rigidbody::AddForce(System.Single,System.Single,System.Single,UnityEngine.ForceMode)
extern void Rigidbody_AddForce_mB32FE7E26D05833A284C6CB9CF7E941A8570934E (void);
// 0x0000004A System.Void UnityEngine.Rigidbody::AddForce(System.Single,System.Single,System.Single)
extern void Rigidbody_AddForce_mFF44DF454FDA13ADFD3CAA4958C4265E45BCF773 (void);
// 0x0000004B System.Void UnityEngine.Rigidbody::AddRelativeForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddRelativeForce_mD1AA911C950F80F697130C0A3DEDEECC8B532752 (void);
// 0x0000004C System.Void UnityEngine.Rigidbody::AddRelativeForce(UnityEngine.Vector3)
extern void Rigidbody_AddRelativeForce_m45E6C06CE742C72905BB126945B454FBA54D84E1 (void);
// 0x0000004D System.Void UnityEngine.Rigidbody::AddRelativeForce(System.Single,System.Single,System.Single,UnityEngine.ForceMode)
extern void Rigidbody_AddRelativeForce_m60FECB22FDEA509AF396844051F9953C25C92B55 (void);
// 0x0000004E System.Void UnityEngine.Rigidbody::AddRelativeForce(System.Single,System.Single,System.Single)
extern void Rigidbody_AddRelativeForce_m3997E5306302262D5CB451DA1FEFE687BF42B042 (void);
// 0x0000004F System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddTorque_mEDE3483056FB07222A4D096F22D45C7D8A6E2552 (void);
// 0x00000050 System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3)
extern void Rigidbody_AddTorque_mAEB5758FA773B1A0ECDD328934BB3A7202D21EB3 (void);
// 0x00000051 System.Void UnityEngine.Rigidbody::AddTorque(System.Single,System.Single,System.Single,UnityEngine.ForceMode)
extern void Rigidbody_AddTorque_m181DAF83E45841384AC7E2E7DB83EC1747623DA4 (void);
// 0x00000052 System.Void UnityEngine.Rigidbody::AddTorque(System.Single,System.Single,System.Single)
extern void Rigidbody_AddTorque_m37159A78400CF4E39F460871437FC3E60CB60E48 (void);
// 0x00000053 System.Void UnityEngine.Rigidbody::AddRelativeTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddRelativeTorque_m1C03D022550534C7C08BF4F8D7983EE8823A785B (void);
// 0x00000054 System.Void UnityEngine.Rigidbody::AddRelativeTorque(UnityEngine.Vector3)
extern void Rigidbody_AddRelativeTorque_m02D008355A3626183BF1B6D4451324FA045F3887 (void);
// 0x00000055 System.Void UnityEngine.Rigidbody::AddRelativeTorque(System.Single,System.Single,System.Single,UnityEngine.ForceMode)
extern void Rigidbody_AddRelativeTorque_m4C699C552995284B52445F59C398D55D4E2AE9E8 (void);
// 0x00000056 System.Void UnityEngine.Rigidbody::AddRelativeTorque(System.Single,System.Single,System.Single)
extern void Rigidbody_AddRelativeTorque_m088537EE9F7B75DC820FDFDE1CDCFCAAF7214CD9 (void);
// 0x00000057 System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddForceAtPosition_mEE49C058A6D57C1D5A78207494BFED5906D80D0F (void);
// 0x00000058 System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Rigidbody_AddForceAtPosition_m5190249D95CE1882B37481C5BFD2ABF201902BA5 (void);
// 0x00000059 System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.ForceMode)
extern void Rigidbody_AddExplosionForce_mA81BFBF84914CEA89D18047ADE14B47D171280DD (void);
// 0x0000005A System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single,System.Single)
extern void Rigidbody_AddExplosionForce_m857BD26BDE42BFDC503AADE97899F94E92F6A6FC (void);
// 0x0000005B System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single)
extern void Rigidbody_AddExplosionForce_m8B6FFD506CCCDD31C8C018A4B635E82B5499648F (void);
// 0x0000005C System.Void UnityEngine.Rigidbody::Internal_ClosestPointOnBounds(UnityEngine.Vector3,UnityEngine.Vector3&,System.Single&)
extern void Rigidbody_Internal_ClosestPointOnBounds_mA448D126F605336DC36AAA31237742A267A923D7 (void);
// 0x0000005D UnityEngine.Vector3 UnityEngine.Rigidbody::ClosestPointOnBounds(UnityEngine.Vector3)
extern void Rigidbody_ClosestPointOnBounds_m449FB43B807BDD87CB528311EA0E9EE7E46CFCAD (void);
// 0x0000005E UnityEngine.RaycastHit UnityEngine.Rigidbody::SweepTest(UnityEngine.Vector3,System.Single,UnityEngine.QueryTriggerInteraction,System.Boolean&)
extern void Rigidbody_SweepTest_m3CD0217FAA41AB8B51B7AA8CF4CBE6CFAFBBD9E2 (void);
// 0x0000005F System.Boolean UnityEngine.Rigidbody::SweepTest(UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,UnityEngine.QueryTriggerInteraction)
extern void Rigidbody_SweepTest_mB96D6E932712DCABA13A2FF341511F2A525BD559 (void);
// 0x00000060 System.Boolean UnityEngine.Rigidbody::SweepTest(UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern void Rigidbody_SweepTest_m10E90C88C57072C4D093CD75909D3B0B9116D911 (void);
// 0x00000061 System.Boolean UnityEngine.Rigidbody::SweepTest(UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern void Rigidbody_SweepTest_mDC7248518295568AD6FC8ED49168242EABD04918 (void);
// 0x00000062 UnityEngine.RaycastHit[] UnityEngine.Rigidbody::Internal_SweepTestAll(UnityEngine.Vector3,System.Single,UnityEngine.QueryTriggerInteraction)
extern void Rigidbody_Internal_SweepTestAll_mAF90D437ED68D4E1FCE3C993E45BCBBC7C1DD66A (void);
// 0x00000063 UnityEngine.RaycastHit[] UnityEngine.Rigidbody::SweepTestAll(UnityEngine.Vector3,System.Single,UnityEngine.QueryTriggerInteraction)
extern void Rigidbody_SweepTestAll_m1D341AB79E8097404255AC95C3C1F5AF8AB83EA5 (void);
// 0x00000064 UnityEngine.RaycastHit[] UnityEngine.Rigidbody::SweepTestAll(UnityEngine.Vector3,System.Single)
extern void Rigidbody_SweepTestAll_mB45AA1BEF24E98CF8D9150C2E1E107A6C81851D8 (void);
// 0x00000065 UnityEngine.RaycastHit[] UnityEngine.Rigidbody::SweepTestAll(UnityEngine.Vector3)
extern void Rigidbody_SweepTestAll_m50B229C454076D4CC2085E8F053B2ED419B314DC (void);
// 0x00000066 System.Void UnityEngine.Rigidbody::.ctor()
extern void Rigidbody__ctor_m0E43BA3B0E70E71B2CA62B165EE5B7CFAEFACDE9 (void);
// 0x00000067 System.Void UnityEngine.Rigidbody::get_velocity_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_velocity_Injected_m79C6BB8C054D0B5F03F3F13325910BC068E5B796 (void);
// 0x00000068 System.Void UnityEngine.Rigidbody::set_velocity_Injected(UnityEngine.Vector3&)
extern void Rigidbody_set_velocity_Injected_mBFBC7681B33942F69A5CD908941D399777F66ADD (void);
// 0x00000069 System.Void UnityEngine.Rigidbody::get_angularVelocity_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_angularVelocity_Injected_mD00F8790DFF2A31A033487AC67C4C018F28D0D13 (void);
// 0x0000006A System.Void UnityEngine.Rigidbody::set_angularVelocity_Injected(UnityEngine.Vector3&)
extern void Rigidbody_set_angularVelocity_Injected_m5ED47D0F131F6B3788B8B736DA7854FD63C13D56 (void);
// 0x0000006B System.Void UnityEngine.Rigidbody::get_centerOfMass_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_centerOfMass_Injected_m4350E78A3E7EC4B0B426A87824EC735462B9946E (void);
// 0x0000006C System.Void UnityEngine.Rigidbody::set_centerOfMass_Injected(UnityEngine.Vector3&)
extern void Rigidbody_set_centerOfMass_Injected_m60EC1BE2558EC9A9BD40ED144A1C848E6A4BCD9D (void);
// 0x0000006D System.Void UnityEngine.Rigidbody::get_worldCenterOfMass_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_worldCenterOfMass_Injected_m6FADF4899BDD58E674C0CDF09811505E42218E44 (void);
// 0x0000006E System.Void UnityEngine.Rigidbody::get_inertiaTensorRotation_Injected(UnityEngine.Quaternion&)
extern void Rigidbody_get_inertiaTensorRotation_Injected_m4147A057A803E19787F39B4B44F237D4E3D7B5F4 (void);
// 0x0000006F System.Void UnityEngine.Rigidbody::set_inertiaTensorRotation_Injected(UnityEngine.Quaternion&)
extern void Rigidbody_set_inertiaTensorRotation_Injected_m3EF9205426B41821EFEE2E9902792EEDC498A0C2 (void);
// 0x00000070 System.Void UnityEngine.Rigidbody::get_inertiaTensor_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_inertiaTensor_Injected_mF43CDE1538A2E5FCD06552A971D3828C12D45480 (void);
// 0x00000071 System.Void UnityEngine.Rigidbody::set_inertiaTensor_Injected(UnityEngine.Vector3&)
extern void Rigidbody_set_inertiaTensor_Injected_mB625F9A0523C4FF7AB51334B095B3284D6FF062C (void);
// 0x00000072 System.Void UnityEngine.Rigidbody::get_position_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_position_Injected_m16D551CF5A925BD26F4E77116483B2B36115A079 (void);
// 0x00000073 System.Void UnityEngine.Rigidbody::set_position_Injected(UnityEngine.Vector3&)
extern void Rigidbody_set_position_Injected_mC4BF2A57546741F3E26E82E988646AD1BB22D44E (void);
// 0x00000074 System.Void UnityEngine.Rigidbody::get_rotation_Injected(UnityEngine.Quaternion&)
extern void Rigidbody_get_rotation_Injected_mA9DA175CC81C9D6D4D7098C34CF5378C4C2955D8 (void);
// 0x00000075 System.Void UnityEngine.Rigidbody::set_rotation_Injected(UnityEngine.Quaternion&)
extern void Rigidbody_set_rotation_Injected_mBB08A477E5E134A4E6FFF381BFDE9E2959844EE9 (void);
// 0x00000076 System.Void UnityEngine.Rigidbody::MovePosition_Injected(UnityEngine.Vector3&)
extern void Rigidbody_MovePosition_Injected_m06454253A0DF550B2EAD47F545734E8735BA0732 (void);
// 0x00000077 System.Void UnityEngine.Rigidbody::MoveRotation_Injected(UnityEngine.Quaternion&)
extern void Rigidbody_MoveRotation_Injected_mB730FBD0786AE2DEC454E76326E08ED79CEEF440 (void);
// 0x00000078 System.Void UnityEngine.Rigidbody::GetRelativePointVelocity_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Rigidbody_GetRelativePointVelocity_Injected_mAFF00EBA1B693EBFFF2E66480C87E40675C9482C (void);
// 0x00000079 System.Void UnityEngine.Rigidbody::GetPointVelocity_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Rigidbody_GetPointVelocity_Injected_m24C6296C3FB436843956E904B02F907181867118 (void);
// 0x0000007A System.Void UnityEngine.Rigidbody::AddForce_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddForce_Injected_m233C3E22C3FE9D2BCBBC510132B82CE26057370C (void);
// 0x0000007B System.Void UnityEngine.Rigidbody::AddRelativeForce_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddRelativeForce_Injected_m2807F26D600ACF197910A90301FFD74A384775FD (void);
// 0x0000007C System.Void UnityEngine.Rigidbody::AddTorque_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddTorque_Injected_mFFD31FDF8F82D3D740EA83348BBC0D0D5EB0DB3A (void);
// 0x0000007D System.Void UnityEngine.Rigidbody::AddRelativeTorque_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddRelativeTorque_Injected_m0B2D9B655EE6D105CB658C788E52C8228EE195B0 (void);
// 0x0000007E System.Void UnityEngine.Rigidbody::AddForceAtPosition_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddForceAtPosition_Injected_m60ED364228EF475F97B0FF1D0F4EFCAB97382DDB (void);
// 0x0000007F System.Void UnityEngine.Rigidbody::AddExplosionForce_Injected(System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)
extern void Rigidbody_AddExplosionForce_Injected_m8CCDC7FDD8F5F1BC231094105B3076815A16E22D (void);
// 0x00000080 System.Void UnityEngine.Rigidbody::Internal_ClosestPointOnBounds_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single&)
extern void Rigidbody_Internal_ClosestPointOnBounds_Injected_m717C820FF2668DB17C27A6EE470660BB8531D4A7 (void);
// 0x00000081 System.Void UnityEngine.Rigidbody::SweepTest_Injected(UnityEngine.Vector3&,System.Single,UnityEngine.QueryTriggerInteraction,System.Boolean&,UnityEngine.RaycastHit&)
extern void Rigidbody_SweepTest_Injected_mD4F788EFA83E0082B497F8FEA1A2985E058F3A8E (void);
// 0x00000082 UnityEngine.RaycastHit[] UnityEngine.Rigidbody::Internal_SweepTestAll_Injected(UnityEngine.Vector3&,System.Single,UnityEngine.QueryTriggerInteraction)
extern void Rigidbody_Internal_SweepTestAll_Injected_m8C341BB6B39648FDEC63B3743F45E4955877ABCC (void);
// 0x00000083 System.Boolean UnityEngine.Collider::get_enabled()
extern void Collider_get_enabled_m03B73B5C97033F939387D1785BDF2619CADAEEB0 (void);
// 0x00000084 System.Void UnityEngine.Collider::set_enabled(System.Boolean)
extern void Collider_set_enabled_m047B4D830755CD36671F7A60BFAA9C0D61F6C4A1 (void);
// 0x00000085 UnityEngine.Vector3 UnityEngine.Collider::ClosestPoint(UnityEngine.Vector3)
extern void Collider_ClosestPoint_m7777917E298B31796DEE906B54F0102F6ED76676 (void);
// 0x00000086 UnityEngine.Bounds UnityEngine.Collider::get_bounds()
extern void Collider_get_bounds_mE341D29E1DA184ADD53A474D57D9082A3550EACB (void);
// 0x00000087 System.Void UnityEngine.Collider::.ctor()
extern void Collider__ctor_m09D7A9B985D74FD50346DA08D88EB1874E968B69 (void);
// 0x00000088 System.Void UnityEngine.Collider::ClosestPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Collider_ClosestPoint_Injected_m6D72FF73D51838EE47234CB4D6234521C08B780D (void);
// 0x00000089 System.Void UnityEngine.Collider::get_bounds_Injected(UnityEngine.Bounds&)
extern void Collider_get_bounds_Injected_m9BA8C3BC133BC241D571849C4F10F67A951CA962 (void);
// 0x0000008A UnityEngine.Mesh UnityEngine.MeshCollider::get_sharedMesh()
extern void MeshCollider_get_sharedMesh_m77D27894F87619BD2AF868BC1156583EEC0D8CB4 (void);
// 0x0000008B System.Void UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)
extern void MeshCollider_set_sharedMesh_m5E39BE3C85A9D21D846E8B7DBA1ED14820ED0406 (void);
// 0x0000008C System.Boolean UnityEngine.MeshCollider::get_convex()
extern void MeshCollider_get_convex_m115EB1C243F7EFD1EBAB80350E98EB3DE1A4C44C (void);
// 0x0000008D System.Void UnityEngine.MeshCollider::set_convex(System.Boolean)
extern void MeshCollider_set_convex_mE94F2BE7760587C1944992B7AF9959ED425F631A (void);
// 0x0000008E System.Boolean UnityEngine.MeshCollider::get_inflateMesh()
extern void MeshCollider_get_inflateMesh_m626D03F100B290ED636AA9AC8C0F433BAF8154FE (void);
// 0x0000008F System.Void UnityEngine.MeshCollider::set_inflateMesh(System.Boolean)
extern void MeshCollider_set_inflateMesh_m3E3876DDE7EEA4EE1B65A1ABC2E01A5323306325 (void);
// 0x00000090 UnityEngine.MeshColliderCookingOptions UnityEngine.MeshCollider::get_cookingOptions()
extern void MeshCollider_get_cookingOptions_m5B11CB587B76A0BE610DDC7BE94EF5043ABB969F (void);
// 0x00000091 System.Void UnityEngine.MeshCollider::set_cookingOptions(UnityEngine.MeshColliderCookingOptions)
extern void MeshCollider_set_cookingOptions_mAC05C5B70D29C228BDDB5C85CA76C7292311DA68 (void);
// 0x00000092 System.Single UnityEngine.MeshCollider::get_skinWidth()
extern void MeshCollider_get_skinWidth_m13581A3A6C9EFCB1CB1C2024F4F59DE843937288 (void);
// 0x00000093 System.Void UnityEngine.MeshCollider::set_skinWidth(System.Single)
extern void MeshCollider_set_skinWidth_mE0E723C7FF74EAA1232F4312FA25A3B59A9EDB27 (void);
// 0x00000094 System.Boolean UnityEngine.MeshCollider::get_smoothSphereCollisions()
extern void MeshCollider_get_smoothSphereCollisions_mA2DD4935C279325854799447857CAC5831E32CA4 (void);
// 0x00000095 System.Void UnityEngine.MeshCollider::set_smoothSphereCollisions(System.Boolean)
extern void MeshCollider_set_smoothSphereCollisions_m7F27EE71FF46F14BDA138B3C259273B217EFA92B (void);
// 0x00000096 System.Void UnityEngine.MeshCollider::.ctor()
extern void MeshCollider__ctor_m30E265B3A2F241F9BF4663BAC38F3D917273D8CB (void);
// 0x00000097 UnityEngine.Vector3 UnityEngine.CapsuleCollider::get_center()
extern void CapsuleCollider_get_center_m6374F7457A9450CAFFAD2DF0C9D1419BF9E304CB (void);
// 0x00000098 System.Void UnityEngine.CapsuleCollider::set_center(UnityEngine.Vector3)
extern void CapsuleCollider_set_center_m36F35F070DFC2CBFC87532004073CA8D56F3678F (void);
// 0x00000099 System.Single UnityEngine.CapsuleCollider::get_radius()
extern void CapsuleCollider_get_radius_m5746DDE5E6A099269FC9DAD253887C58E8A064D0 (void);
// 0x0000009A System.Void UnityEngine.CapsuleCollider::set_radius(System.Single)
extern void CapsuleCollider_set_radius_mD4502A8676AAC093F1E9958FB7D5D765EA206432 (void);
// 0x0000009B System.Single UnityEngine.CapsuleCollider::get_height()
extern void CapsuleCollider_get_height_mD6CF93CB2C222F8E5B77D65B0356F8FD8005B526 (void);
// 0x0000009C System.Void UnityEngine.CapsuleCollider::set_height(System.Single)
extern void CapsuleCollider_set_height_m728C9AF3772EEC1DA9845E19F3C2899CDD2D9496 (void);
// 0x0000009D System.Int32 UnityEngine.CapsuleCollider::get_direction()
extern void CapsuleCollider_get_direction_m2C5110BBCA2CC1C63183CDBD73B6D11CC89B0918 (void);
// 0x0000009E System.Void UnityEngine.CapsuleCollider::set_direction(System.Int32)
extern void CapsuleCollider_set_direction_mE5E7B7BB7FBBBA97148E8CFCDC46D1BB14673984 (void);
// 0x0000009F UnityEngine.Vector2 UnityEngine.CapsuleCollider::GetGlobalExtents()
extern void CapsuleCollider_GetGlobalExtents_m96E63F24BCAF34DDBC49E743C3203D19638501AF (void);
// 0x000000A0 UnityEngine.Matrix4x4 UnityEngine.CapsuleCollider::CalculateTransform()
extern void CapsuleCollider_CalculateTransform_m5DA33B8D319E92F1FBE72447A8CDE5223DB69258 (void);
// 0x000000A1 System.Void UnityEngine.CapsuleCollider::.ctor()
extern void CapsuleCollider__ctor_mB993913A662CF20B09C4A7475B66E820C87879A4 (void);
// 0x000000A2 System.Void UnityEngine.CapsuleCollider::get_center_Injected(UnityEngine.Vector3&)
extern void CapsuleCollider_get_center_Injected_mBDF07E6DE932FBCAF0AA6F167EB993BE03BE6872 (void);
// 0x000000A3 System.Void UnityEngine.CapsuleCollider::set_center_Injected(UnityEngine.Vector3&)
extern void CapsuleCollider_set_center_Injected_mB43C17AC81236FB74E61E21215D2145663A7DEEE (void);
// 0x000000A4 System.Void UnityEngine.CapsuleCollider::GetGlobalExtents_Injected(UnityEngine.Vector2&)
extern void CapsuleCollider_GetGlobalExtents_Injected_m3604AFE1AB8E8F50E203CF50E7A484346DC72C18 (void);
// 0x000000A5 System.Void UnityEngine.CapsuleCollider::CalculateTransform_Injected(UnityEngine.Matrix4x4&)
extern void CapsuleCollider_CalculateTransform_Injected_mA1A6CB7A01489F8442B921163D0149EE6AE329EC (void);
// 0x000000A6 UnityEngine.Vector3 UnityEngine.BoxCollider::get_center()
extern void BoxCollider_get_center_m832B93439717C72D4A3B427C6E8F5B54E2DBD669 (void);
// 0x000000A7 System.Void UnityEngine.BoxCollider::set_center(UnityEngine.Vector3)
extern void BoxCollider_set_center_m02D745E759A66BBEF405D33CE4ACE34B7E064178 (void);
// 0x000000A8 UnityEngine.Vector3 UnityEngine.BoxCollider::get_size()
extern void BoxCollider_get_size_mBC38D4926D4BE54A6532F6E1D642F363CA3A58A1 (void);
// 0x000000A9 System.Void UnityEngine.BoxCollider::set_size(UnityEngine.Vector3)
extern void BoxCollider_set_size_mD9153B4AE4C366ACAB9E5F49075D919A89168B2E (void);
// 0x000000AA UnityEngine.Vector3 UnityEngine.BoxCollider::get_extents()
extern void BoxCollider_get_extents_mAF61D471B8E2ABE9C41D44F647BB0EED74DA329B (void);
// 0x000000AB System.Void UnityEngine.BoxCollider::set_extents(UnityEngine.Vector3)
extern void BoxCollider_set_extents_m8F23EDB911DCFD1440281E7E9B11D0C1CA23FA4F (void);
// 0x000000AC System.Void UnityEngine.BoxCollider::.ctor()
extern void BoxCollider__ctor_mB54A88CED24F4C03899850888C7C11483EA3D645 (void);
// 0x000000AD System.Void UnityEngine.BoxCollider::get_center_Injected(UnityEngine.Vector3&)
extern void BoxCollider_get_center_Injected_m5F060E93AE651FB756B9371C7A716BDAA2A96F51 (void);
// 0x000000AE System.Void UnityEngine.BoxCollider::set_center_Injected(UnityEngine.Vector3&)
extern void BoxCollider_set_center_Injected_m54F8740514B14E3B53063FB4C7623BF0F2A48F0A (void);
// 0x000000AF System.Void UnityEngine.BoxCollider::get_size_Injected(UnityEngine.Vector3&)
extern void BoxCollider_get_size_Injected_mC9ABFED6FEC27351BC116B44021578CB39CBCB22 (void);
// 0x000000B0 System.Void UnityEngine.BoxCollider::set_size_Injected(UnityEngine.Vector3&)
extern void BoxCollider_set_size_Injected_m7A8B807F4AB573BF955610F55AAD372FF868C967 (void);
// 0x000000B1 UnityEngine.Vector3 UnityEngine.SphereCollider::get_center()
extern void SphereCollider_get_center_mBFAE4FFFC76B8FD8F1B2B2F12C52A30470443D3A (void);
// 0x000000B2 System.Void UnityEngine.SphereCollider::set_center(UnityEngine.Vector3)
extern void SphereCollider_set_center_mD5E898A2FED304A82BC67ABB11B60BB0F612CED7 (void);
// 0x000000B3 System.Single UnityEngine.SphereCollider::get_radius()
extern void SphereCollider_get_radius_m403989140BDAD513299276953B481167CF08D02F (void);
// 0x000000B4 System.Void UnityEngine.SphereCollider::set_radius(System.Single)
extern void SphereCollider_set_radius_m55A0D144B32871AECC2A83FBCF423FBE1E5A63A0 (void);
// 0x000000B5 System.Void UnityEngine.SphereCollider::.ctor()
extern void SphereCollider__ctor_m81B7D07301D49F1E0229B92068559F0A2FDE0962 (void);
// 0x000000B6 System.Void UnityEngine.SphereCollider::get_center_Injected(UnityEngine.Vector3&)
extern void SphereCollider_get_center_Injected_mF082CC62A7E8DE3DD5B86518C210D85139DD93CB (void);
// 0x000000B7 System.Void UnityEngine.SphereCollider::set_center_Injected(UnityEngine.Vector3&)
extern void SphereCollider_set_center_Injected_mB213D6CB194E150BBEF8EC2AA31EA391C2F4A641 (void);
// 0x000000B8 UnityEngine.Vector3 UnityEngine.ConstantForce::get_force()
extern void ConstantForce_get_force_mC597105F67388B4FB6443E72981ECB09129BFBF5 (void);
// 0x000000B9 System.Void UnityEngine.ConstantForce::set_force(UnityEngine.Vector3)
extern void ConstantForce_set_force_mE7AFBCB9EBFB6A94312C9D72DE160C12503147E9 (void);
// 0x000000BA UnityEngine.Vector3 UnityEngine.ConstantForce::get_relativeForce()
extern void ConstantForce_get_relativeForce_mEBA2646D393A1D6DF7F5574033B10C71CDE616C3 (void);
// 0x000000BB System.Void UnityEngine.ConstantForce::set_relativeForce(UnityEngine.Vector3)
extern void ConstantForce_set_relativeForce_m9B0113E65E5B48F399C889441C75158B7E2F2BB6 (void);
// 0x000000BC UnityEngine.Vector3 UnityEngine.ConstantForce::get_torque()
extern void ConstantForce_get_torque_m09E929B93FB47DF28BEBED8B3D1B94180D99A890 (void);
// 0x000000BD System.Void UnityEngine.ConstantForce::set_torque(UnityEngine.Vector3)
extern void ConstantForce_set_torque_m1568C91C8C2BA02C57DE457550CE19AF0B3A78FD (void);
// 0x000000BE UnityEngine.Vector3 UnityEngine.ConstantForce::get_relativeTorque()
extern void ConstantForce_get_relativeTorque_m130283B736FD47C2E07223E34E7176886DEAF7DA (void);
// 0x000000BF System.Void UnityEngine.ConstantForce::set_relativeTorque(UnityEngine.Vector3)
extern void ConstantForce_set_relativeTorque_m613F980B2371177D134F200776CEE314AB128FE5 (void);
// 0x000000C0 System.Void UnityEngine.ConstantForce::.ctor()
extern void ConstantForce__ctor_m91080857146412E848B415BB4DE1B7C86D1F69A4 (void);
// 0x000000C1 System.Void UnityEngine.ConstantForce::get_force_Injected(UnityEngine.Vector3&)
extern void ConstantForce_get_force_Injected_mD97072761D01F77BE93C9FA66666B732AEBE39B7 (void);
// 0x000000C2 System.Void UnityEngine.ConstantForce::set_force_Injected(UnityEngine.Vector3&)
extern void ConstantForce_set_force_Injected_mDD654BAA10488ABF81D98B5129F6AD0BDF82D578 (void);
// 0x000000C3 System.Void UnityEngine.ConstantForce::get_relativeForce_Injected(UnityEngine.Vector3&)
extern void ConstantForce_get_relativeForce_Injected_m83CEC6A7E2AFD77B29B53473331E2165606389A4 (void);
// 0x000000C4 System.Void UnityEngine.ConstantForce::set_relativeForce_Injected(UnityEngine.Vector3&)
extern void ConstantForce_set_relativeForce_Injected_m1EBC38E3BBE1020E80C4ECCFB345960FDC3423E8 (void);
// 0x000000C5 System.Void UnityEngine.ConstantForce::get_torque_Injected(UnityEngine.Vector3&)
extern void ConstantForce_get_torque_Injected_mDD968C22228ED81AAAA203E9944CEBF61E767853 (void);
// 0x000000C6 System.Void UnityEngine.ConstantForce::set_torque_Injected(UnityEngine.Vector3&)
extern void ConstantForce_set_torque_Injected_m581F2B41182D65BC4311693EB0AF705FA1FC7578 (void);
// 0x000000C7 System.Void UnityEngine.ConstantForce::get_relativeTorque_Injected(UnityEngine.Vector3&)
extern void ConstantForce_get_relativeTorque_Injected_m6DA015FF7ED6B661C348DCE4EC1844B9EFD1409B (void);
// 0x000000C8 System.Void UnityEngine.ConstantForce::set_relativeTorque_Injected(UnityEngine.Vector3&)
extern void ConstantForce_set_relativeTorque_Injected_m82778B9E054E5FC2BDB38BD50BAAD4E55C3E1D41 (void);
// 0x000000C9 System.String UnityEngine.PhysicsScene::ToString()
extern void PhysicsScene_ToString_mBB6D0AC1E3E2EDC34CB4A7A34485B24B6271903F (void);
// 0x000000CA System.Int32 UnityEngine.PhysicsScene::GetHashCode()
extern void PhysicsScene_GetHashCode_m5344CC6CCCB1CA6EBB149775EE028089DD74B8A2 (void);
// 0x000000CB System.Boolean UnityEngine.PhysicsScene::Equals(System.Object)
extern void PhysicsScene_Equals_m2645ABB96C598F197F734EB712494795928CBCEC (void);
// 0x000000CC System.Boolean UnityEngine.PhysicsScene::Equals(UnityEngine.PhysicsScene)
extern void PhysicsScene_Equals_m8C948B86D105177D519A3608816CDC698C8686B8 (void);
// 0x000000CD System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_m8C3E61EB7C3DB8AAC6FEB098D815062BEF821187 (void);
// 0x000000CE System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastTest_m6715CAD8D0330195269AA7FCCD91613BC564E3EB (void);
// 0x000000CF System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_mFC0D59C4439EE9DA6E8AA5F6891915132D587208 (void);
// 0x000000D0 System.Boolean UnityEngine.PhysicsScene::Internal_Raycast(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_Raycast_m1E630AA11805114B090FC50741AEF64D677AF2C7 (void);
// 0x000000D1 System.Int32 UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_m95CAB68EB9165E3AB2A4D441F7DF9767AD4B7F73 (void);
// 0x000000D2 System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc(UnityEngine.PhysicsScene,UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastNonAlloc_mE7833EE5F1082431A4C2D29362F5DCDEFBF6D2BD (void);
// 0x000000D3 System.Int32 UnityEngine.PhysicsScene::OverlapBoxNonAlloc_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Collider[],UnityEngine.Quaternion,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_OverlapBoxNonAlloc_Internal_mEE4323DAE1A464A56CA1DD205D622617059AEA30 (void);
// 0x000000D4 System.Int32 UnityEngine.PhysicsScene::OverlapBox(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Collider[],UnityEngine.Quaternion,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_OverlapBox_mCC9E06BCEE764C2C0A9F2C544A69B996BB37CF27 (void);
// 0x000000D5 System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastTest_Injected_m956474EFEA507F82ABE0BCB75DDB3CC8EFF4D12B (void);
// 0x000000D6 System.Boolean UnityEngine.PhysicsScene::Internal_Raycast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_Raycast_Injected_m8868E0BF89F8C42D11F42F94F9CF767647433BB1 (void);
// 0x000000D7 System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastNonAlloc_Injected_m59FB44C18E62D21E9320F30229C0AA9F9B971211 (void);
// 0x000000D8 System.Int32 UnityEngine.PhysicsScene::OverlapBoxNonAlloc_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Collider[],UnityEngine.Quaternion&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_OverlapBoxNonAlloc_Internal_Injected_mA30EFF675823E14B22919E34D5D9641F06199010 (void);
// 0x000000D9 UnityEngine.PhysicsScene UnityEngine.Physics::get_defaultPhysicsScene()
extern void Physics_get_defaultPhysicsScene_mA6361488FBAC110DA8397E5E4CB473EBF4191010 (void);
// 0x000000DA System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m9DE0EEA1CF8DEF7D06216225F19E2958D341F7BA (void);
// 0x000000DB System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_Raycast_m09F21E13465121A73F19C07FC5F5314CF15ACD15 (void);
// 0x000000DC System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Physics_Raycast_m284670765E1627E43B7B0F5EC811A688EE595091 (void);
// 0x000000DD System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Physics_Raycast_mDA2EB8C7692308A7178222D31CBA4C7A1C7DC915 (void);
// 0x000000DE System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m2EA572B4613E1BD7DBAA299511CFD2DBA179A163 (void);
// 0x000000DF System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C (void);
// 0x000000E0 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern void Physics_Raycast_m18E12C65F127D1AA50D196623F04F81CB138FD12 (void);
// 0x000000E1 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern void Physics_Raycast_mE84D30EEFE59DA28DA172342068F092A35B2BE4A (void);
// 0x000000E2 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m77560CCAA49DC821E51FDDD1570B921D7704646F (void);
// 0x000000E3 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics_Raycast_mFDC4B8E7979495E3C22D0E3CEA4BCAB271EEC25A (void);
// 0x000000E4 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single)
extern void Physics_Raycast_mD7A711D3A790AC505F7229A4FFFA2E389008176B (void);
// 0x000000E5 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray)
extern void Physics_Raycast_m111AFC36A136A67BE4776670E356E99A82010BFE (void);
// 0x000000E6 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_mCA3F2DD1DC08199AAD8466BB857318CD454AC774 (void);
// 0x000000E7 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void Physics_Raycast_m46E12070D6996F4C8C91D49ADC27C74AC1D6A3D8 (void);
// 0x000000E8 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern void Physics_Raycast_mA64F8C30681E3A6A8F2B7EDE592FE7BBC0D354F4 (void);
// 0x000000E9 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern void Physics_Raycast_m80EC8EEDA0ABA8B01838BA9054834CD1A381916E (void);
// 0x000000EA UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Internal_RaycastAll_m39EE7CF896F9D8BA58CC623F14E4DB0641480523 (void);
// 0x000000EB UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastAll_m5103E7C60CC66BAFA6534D1736138A92EB1EF2B8 (void);
// 0x000000EC UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_RaycastAll_m2C977C8B022672F42B5E18F40B529C0A74B7E471 (void);
// 0x000000ED UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Physics_RaycastAll_m2BBC8D2731B38EE0B704A5C6A09D0F8BBA074A4D (void);
// 0x000000EE UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Physics_RaycastAll_m83F28CB671653C07995FB1BCDC561121DE3D9CA6 (void);
// 0x000000EF UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastAll_m55EB4478198ED6EF838500257FA3BE1A871D3605 (void);
// 0x000000F0 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics_RaycastAll_mA24B9B922C98C5D18397FAE55C04AEAFDD47F029 (void);
// 0x000000F1 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single)
extern void Physics_RaycastAll_m72947571EFB0EFB34E48340AA2EC0C8030D27C50 (void);
// 0x000000F2 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray)
extern void Physics_RaycastAll_m529EE59D6D03E4CFAECE4016C8CC4181BEC2D26D (void);
// 0x000000F3 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastNonAlloc_m7C81125AD7D5891EBC3AB48C6DED7B60F53DF099 (void);
// 0x000000F4 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern void Physics_RaycastNonAlloc_m8F62EE5A33E81A02E4983A171023B7BC4AC700CA (void);
// 0x000000F5 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single)
extern void Physics_RaycastNonAlloc_m41BB7BB8755B09700C10F59A29C3541D45AB9CCD (void);
// 0x000000F6 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[])
extern void Physics_RaycastNonAlloc_mBC5BE514E55B8D98A8F4752DED6850E02EE8AD98 (void);
// 0x000000F7 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastNonAlloc_m2C90D14E472DE7929EFD54FDA7BC512D3FBDE4ED (void);
// 0x000000F8 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern void Physics_RaycastNonAlloc_m316F597067055C9F923F57CC68D68FF917C3B4D1 (void);
// 0x000000F9 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single)
extern void Physics_RaycastNonAlloc_m1B8F31BF41F756F561F6AC3A2E0736F2BC9C4DB4 (void);
// 0x000000FA System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[])
extern void Physics_RaycastNonAlloc_m9076DDE1A65F87DB3D2824DAD4E5B8B534159F1F (void);
// 0x000000FB System.Boolean UnityEngine.Physics::CheckSphere_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_CheckSphere_Internal_m2139E9BBEC789DCDCA7A729F13A0AB6FD7E4ADAA (void);
// 0x000000FC System.Boolean UnityEngine.Physics::CheckSphere(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_CheckSphere_mF4AE4778A415A4E9C7C15BA21A0E402909AD3472 (void);
// 0x000000FD System.Boolean UnityEngine.Physics::CheckSphere(UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_CheckSphere_m6DFD61C841CEBFDE6645689279AA6E31297B002B (void);
// 0x000000FE System.Int32 UnityEngine.Physics::OverlapBoxNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Collider[],UnityEngine.Quaternion,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapBoxNonAlloc_m79F2597E253FCDD8D03253FAC692FF2191D0232A (void);
// 0x000000FF System.Int32 UnityEngine.Physics::OverlapBoxNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Collider[],UnityEngine.Quaternion,System.Int32)
extern void Physics_OverlapBoxNonAlloc_m243E935F534B3E08DDF43049C81DD30214B7DD97 (void);
// 0x00000100 System.Int32 UnityEngine.Physics::OverlapBoxNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Collider[])
extern void Physics_OverlapBoxNonAlloc_m95BFA939738463ED735E9EC43A7C1A2D83C9DECD (void);
// 0x00000101 System.Void UnityEngine.Physics::get_defaultPhysicsScene_Injected(UnityEngine.PhysicsScene&)
extern void Physics_get_defaultPhysicsScene_Injected_m0B71068269B3C8499A258BBCAEA7E3D9D020BADA (void);
// 0x00000102 UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Internal_RaycastAll_Injected_mD4C3E8762D9FCEE654CE0415E636EB7DBEC92A5B (void);
// 0x00000103 System.Boolean UnityEngine.Physics::CheckSphere_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_CheckSphere_Internal_Injected_mEF8DD496E66AB1FDBF6EE8E10831E795B7B77A00 (void);
static Il2CppMethodPointer s_methodPointers[259] = 
{
	RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E,
	RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E,
	RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674,
	RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA,
	Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C,
	Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D,
	Rigidbody_get_angularVelocity_m6737340DF546452900D199246279231D80A21DCF,
	Rigidbody_set_angularVelocity_m3A40B7F195E9E217AE29A0964D7E7540E2E23080,
	Rigidbody_get_drag_m0C617963D9BBBC4018D3A8B2DB5D6190615F4A64,
	Rigidbody_set_drag_m60E39BE31529DE5163116785A69FACC77C52DA98,
	Rigidbody_get_angularDrag_m0E53FD8F8A09DFA941C52C868288DBBC030C5082,
	Rigidbody_set_angularDrag_m8BF3771789B32FB09FDD8066BAFA0A0B661372A4,
	Rigidbody_get_mass_mB7B19406DAC6336A8244E98BE271BDA8B5C26223,
	Rigidbody_set_mass_m54FCACE073F5E7742DB1D7C0BA19CD0C0F3DDA3F,
	Rigidbody_SetDensity_m794AA2513F044BCFC3BC87AB39387131BA1EA282,
	Rigidbody_get_useGravity_mDA0FB6F456377840E6E46C42B9210F93264E2B28,
	Rigidbody_set_useGravity_m1057292FB3199E87664F40B8BCBA7A7E64D1A096,
	Rigidbody_get_maxDepenetrationVelocity_mE2F07995501D4EFAB39C3B5E6A91ECD6D899272E,
	Rigidbody_set_maxDepenetrationVelocity_mF2D2B5F040B69E32E48544EBA5F6D43656798A02,
	Rigidbody_get_isKinematic_m597B48C45021313B6C6C4B126E405EF566C5C80C,
	Rigidbody_set_isKinematic_mCF74D680205544826F2DE2CAB929C9F25409A311,
	Rigidbody_get_freezeRotation_m52850F7355488269E7026A1E9C6AEB69EA1A1CCE,
	Rigidbody_set_freezeRotation_mE08A39E98D46F82D6DD86CC389D86D242C694D52,
	Rigidbody_get_constraints_m5EBE3E9C0A2084BAAE0C783E212C368A1CB1DD04,
	Rigidbody_set_constraints_mA76F562D16D3BE8889E095D0309C8FE38DA914F1,
	Rigidbody_get_collisionDetectionMode_m1328BE903DD8B2DBD3A67B64FCECFEE91DC49FCE,
	Rigidbody_set_collisionDetectionMode_m4BC31A84B26540ACD15273774C47F31CDF5381C2,
	Rigidbody_get_centerOfMass_mEF7838582F7977AAD2952F6A09E2203D9B0ABC53,
	Rigidbody_set_centerOfMass_m3B13BE412D99CE5133606643F14501CF5C63CCEC,
	Rigidbody_get_worldCenterOfMass_mF825B0D5110903BD0A3FDC1DC4317902305DE143,
	Rigidbody_get_inertiaTensorRotation_m9EFA1F67FF15DC3E996146737149C69A5003D3C1,
	Rigidbody_set_inertiaTensorRotation_m18E191AC25EE29C24DE6A862A26633A7A21A9D9B,
	Rigidbody_get_inertiaTensor_m55F35467FD2088D883060D57595AD5BF14BC5C6F,
	Rigidbody_set_inertiaTensor_mCDE8A0CCFD0B6464B5F99CC1A54DF5300652D5D1,
	Rigidbody_get_detectCollisions_m6741D1A8C1942766F95258677352A75451D99D21,
	Rigidbody_set_detectCollisions_mB94256836724071B1EFE622A5E9BA435B6572E9A,
	Rigidbody_get_position_m5F429382F610E324F39F33E8498A29D0828AD8E8,
	Rigidbody_set_position_mD1A5B0DB2FD8861778CAA575E1D7B72E2AEF4024,
	Rigidbody_get_rotation_mEB90F9D223B0BA32A1962971E3A93DEE1670D18A,
	Rigidbody_set_rotation_m3024C151FEC9BB75735DE9B4BA64F16AA779C5D6,
	Rigidbody_get_interpolation_m17422952D016117813B4520CB65842D2537C8E12,
	Rigidbody_set_interpolation_mEBAF9DF8DE317E2D58848735D26ED9E69CC425CE,
	Rigidbody_get_solverIterations_m9A62E73E257401EFDC44E7338CD36E9E006E8CA1,
	Rigidbody_set_solverIterations_m71644E473B3DB97C10199A7D5AEBAD790DD586EC,
	Rigidbody_get_sleepThreshold_m1469D01B7634195511EEC913BA69617618DE5EEC,
	Rigidbody_set_sleepThreshold_mA71700BF6225A4366FE6BE1365E26B0BF832C00B,
	Rigidbody_get_maxAngularVelocity_m8804A10C23EF0C073D5D829ED6BBF5253B0A2DFE,
	Rigidbody_set_maxAngularVelocity_m55AB6C34E2C33E6EABB07EFCB7AE443F5D3BD060,
	Rigidbody_MovePosition_mB3CBBF21FD0ABB88BC6C004B993DED25673001C7,
	Rigidbody_MoveRotation_m08A1449DC0D514A70065CD80D067597765BDA5B2,
	Rigidbody_Sleep_m60350AEF3E52D57FBE448CADBC06BA98DAEA2115,
	Rigidbody_IsSleeping_mD41EDC4429411110D1FE36101C89B0277F8D62BA,
	Rigidbody_WakeUp_m89308E6756834CF3705D9CDE89D636EEEE409316,
	Rigidbody_ResetCenterOfMass_m6F34341B309D3AAE2A8B3CC963A1E38383EB0FD9,
	Rigidbody_ResetInertiaTensor_m8888FABEC8D40828D8F594915AF20CB01E9511D7,
	Rigidbody_GetRelativePointVelocity_mBE38822FEE8B1D4ECBDCD65AA20D13D0423B1FCF,
	Rigidbody_GetPointVelocity_m48BA0D6C61636CADE0764CC2BF5B02E1B61F1273,
	Rigidbody_get_solverVelocityIterations_m94EF00A61F9CD356D0E56D7285D06EAC4FD2ABEA,
	Rigidbody_set_solverVelocityIterations_mA345A4D3722FFFEDD9391720F9296A9721647AED,
	Rigidbody_get_sleepVelocity_m9907C2F2A20D66AC6091E166E89EF93E1C7C630A,
	Rigidbody_set_sleepVelocity_m1D805ECFA84DCC1E4FB952D9831F9BA64DC1DA07,
	Rigidbody_get_sleepAngularVelocity_mFD5A89F4453A180761602A5464372C776C62097F,
	Rigidbody_set_sleepAngularVelocity_m6585DD80CB21ED9C256D994144587355C3ED2DFA,
	Rigidbody_SetMaxAngularVelocity_mDAB13E69D76596245D830FAE2763FEEB8FA06BEA,
	Rigidbody_get_useConeFriction_mED6C377EDCC693043A6A1954C248F2F23F634536,
	Rigidbody_set_useConeFriction_m8D46BEAEF34497ED930DFE237EBA066DC4704EEA,
	Rigidbody_get_solverIterationCount_mC0EE8B9789EC02A9471E1FDDB6A480E1B393A2F9,
	Rigidbody_set_solverIterationCount_mD46FF60DA43935C791BA3C377AB59474BF7E5275,
	Rigidbody_get_solverVelocityIterationCount_mE9CCC9ABBAED831FFB6C97C9B77653D1F5504CA9,
	Rigidbody_set_solverVelocityIterationCount_m68E2D9D094E6DC605C9C541A20067C74D19701CA,
	Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700,
	Rigidbody_AddForce_mDFB0D57C25682B826999B0074F5C0FD399C6401D,
	Rigidbody_AddForce_mB32FE7E26D05833A284C6CB9CF7E941A8570934E,
	Rigidbody_AddForce_mFF44DF454FDA13ADFD3CAA4958C4265E45BCF773,
	Rigidbody_AddRelativeForce_mD1AA911C950F80F697130C0A3DEDEECC8B532752,
	Rigidbody_AddRelativeForce_m45E6C06CE742C72905BB126945B454FBA54D84E1,
	Rigidbody_AddRelativeForce_m60FECB22FDEA509AF396844051F9953C25C92B55,
	Rigidbody_AddRelativeForce_m3997E5306302262D5CB451DA1FEFE687BF42B042,
	Rigidbody_AddTorque_mEDE3483056FB07222A4D096F22D45C7D8A6E2552,
	Rigidbody_AddTorque_mAEB5758FA773B1A0ECDD328934BB3A7202D21EB3,
	Rigidbody_AddTorque_m181DAF83E45841384AC7E2E7DB83EC1747623DA4,
	Rigidbody_AddTorque_m37159A78400CF4E39F460871437FC3E60CB60E48,
	Rigidbody_AddRelativeTorque_m1C03D022550534C7C08BF4F8D7983EE8823A785B,
	Rigidbody_AddRelativeTorque_m02D008355A3626183BF1B6D4451324FA045F3887,
	Rigidbody_AddRelativeTorque_m4C699C552995284B52445F59C398D55D4E2AE9E8,
	Rigidbody_AddRelativeTorque_m088537EE9F7B75DC820FDFDE1CDCFCAAF7214CD9,
	Rigidbody_AddForceAtPosition_mEE49C058A6D57C1D5A78207494BFED5906D80D0F,
	Rigidbody_AddForceAtPosition_m5190249D95CE1882B37481C5BFD2ABF201902BA5,
	Rigidbody_AddExplosionForce_mA81BFBF84914CEA89D18047ADE14B47D171280DD,
	Rigidbody_AddExplosionForce_m857BD26BDE42BFDC503AADE97899F94E92F6A6FC,
	Rigidbody_AddExplosionForce_m8B6FFD506CCCDD31C8C018A4B635E82B5499648F,
	Rigidbody_Internal_ClosestPointOnBounds_mA448D126F605336DC36AAA31237742A267A923D7,
	Rigidbody_ClosestPointOnBounds_m449FB43B807BDD87CB528311EA0E9EE7E46CFCAD,
	Rigidbody_SweepTest_m3CD0217FAA41AB8B51B7AA8CF4CBE6CFAFBBD9E2,
	Rigidbody_SweepTest_mB96D6E932712DCABA13A2FF341511F2A525BD559,
	Rigidbody_SweepTest_m10E90C88C57072C4D093CD75909D3B0B9116D911,
	Rigidbody_SweepTest_mDC7248518295568AD6FC8ED49168242EABD04918,
	Rigidbody_Internal_SweepTestAll_mAF90D437ED68D4E1FCE3C993E45BCBBC7C1DD66A,
	Rigidbody_SweepTestAll_m1D341AB79E8097404255AC95C3C1F5AF8AB83EA5,
	Rigidbody_SweepTestAll_mB45AA1BEF24E98CF8D9150C2E1E107A6C81851D8,
	Rigidbody_SweepTestAll_m50B229C454076D4CC2085E8F053B2ED419B314DC,
	Rigidbody__ctor_m0E43BA3B0E70E71B2CA62B165EE5B7CFAEFACDE9,
	Rigidbody_get_velocity_Injected_m79C6BB8C054D0B5F03F3F13325910BC068E5B796,
	Rigidbody_set_velocity_Injected_mBFBC7681B33942F69A5CD908941D399777F66ADD,
	Rigidbody_get_angularVelocity_Injected_mD00F8790DFF2A31A033487AC67C4C018F28D0D13,
	Rigidbody_set_angularVelocity_Injected_m5ED47D0F131F6B3788B8B736DA7854FD63C13D56,
	Rigidbody_get_centerOfMass_Injected_m4350E78A3E7EC4B0B426A87824EC735462B9946E,
	Rigidbody_set_centerOfMass_Injected_m60EC1BE2558EC9A9BD40ED144A1C848E6A4BCD9D,
	Rigidbody_get_worldCenterOfMass_Injected_m6FADF4899BDD58E674C0CDF09811505E42218E44,
	Rigidbody_get_inertiaTensorRotation_Injected_m4147A057A803E19787F39B4B44F237D4E3D7B5F4,
	Rigidbody_set_inertiaTensorRotation_Injected_m3EF9205426B41821EFEE2E9902792EEDC498A0C2,
	Rigidbody_get_inertiaTensor_Injected_mF43CDE1538A2E5FCD06552A971D3828C12D45480,
	Rigidbody_set_inertiaTensor_Injected_mB625F9A0523C4FF7AB51334B095B3284D6FF062C,
	Rigidbody_get_position_Injected_m16D551CF5A925BD26F4E77116483B2B36115A079,
	Rigidbody_set_position_Injected_mC4BF2A57546741F3E26E82E988646AD1BB22D44E,
	Rigidbody_get_rotation_Injected_mA9DA175CC81C9D6D4D7098C34CF5378C4C2955D8,
	Rigidbody_set_rotation_Injected_mBB08A477E5E134A4E6FFF381BFDE9E2959844EE9,
	Rigidbody_MovePosition_Injected_m06454253A0DF550B2EAD47F545734E8735BA0732,
	Rigidbody_MoveRotation_Injected_mB730FBD0786AE2DEC454E76326E08ED79CEEF440,
	Rigidbody_GetRelativePointVelocity_Injected_mAFF00EBA1B693EBFFF2E66480C87E40675C9482C,
	Rigidbody_GetPointVelocity_Injected_m24C6296C3FB436843956E904B02F907181867118,
	Rigidbody_AddForce_Injected_m233C3E22C3FE9D2BCBBC510132B82CE26057370C,
	Rigidbody_AddRelativeForce_Injected_m2807F26D600ACF197910A90301FFD74A384775FD,
	Rigidbody_AddTorque_Injected_mFFD31FDF8F82D3D740EA83348BBC0D0D5EB0DB3A,
	Rigidbody_AddRelativeTorque_Injected_m0B2D9B655EE6D105CB658C788E52C8228EE195B0,
	Rigidbody_AddForceAtPosition_Injected_m60ED364228EF475F97B0FF1D0F4EFCAB97382DDB,
	Rigidbody_AddExplosionForce_Injected_m8CCDC7FDD8F5F1BC231094105B3076815A16E22D,
	Rigidbody_Internal_ClosestPointOnBounds_Injected_m717C820FF2668DB17C27A6EE470660BB8531D4A7,
	Rigidbody_SweepTest_Injected_mD4F788EFA83E0082B497F8FEA1A2985E058F3A8E,
	Rigidbody_Internal_SweepTestAll_Injected_m8C341BB6B39648FDEC63B3743F45E4955877ABCC,
	Collider_get_enabled_m03B73B5C97033F939387D1785BDF2619CADAEEB0,
	Collider_set_enabled_m047B4D830755CD36671F7A60BFAA9C0D61F6C4A1,
	Collider_ClosestPoint_m7777917E298B31796DEE906B54F0102F6ED76676,
	Collider_get_bounds_mE341D29E1DA184ADD53A474D57D9082A3550EACB,
	Collider__ctor_m09D7A9B985D74FD50346DA08D88EB1874E968B69,
	Collider_ClosestPoint_Injected_m6D72FF73D51838EE47234CB4D6234521C08B780D,
	Collider_get_bounds_Injected_m9BA8C3BC133BC241D571849C4F10F67A951CA962,
	MeshCollider_get_sharedMesh_m77D27894F87619BD2AF868BC1156583EEC0D8CB4,
	MeshCollider_set_sharedMesh_m5E39BE3C85A9D21D846E8B7DBA1ED14820ED0406,
	MeshCollider_get_convex_m115EB1C243F7EFD1EBAB80350E98EB3DE1A4C44C,
	MeshCollider_set_convex_mE94F2BE7760587C1944992B7AF9959ED425F631A,
	MeshCollider_get_inflateMesh_m626D03F100B290ED636AA9AC8C0F433BAF8154FE,
	MeshCollider_set_inflateMesh_m3E3876DDE7EEA4EE1B65A1ABC2E01A5323306325,
	MeshCollider_get_cookingOptions_m5B11CB587B76A0BE610DDC7BE94EF5043ABB969F,
	MeshCollider_set_cookingOptions_mAC05C5B70D29C228BDDB5C85CA76C7292311DA68,
	MeshCollider_get_skinWidth_m13581A3A6C9EFCB1CB1C2024F4F59DE843937288,
	MeshCollider_set_skinWidth_mE0E723C7FF74EAA1232F4312FA25A3B59A9EDB27,
	MeshCollider_get_smoothSphereCollisions_mA2DD4935C279325854799447857CAC5831E32CA4,
	MeshCollider_set_smoothSphereCollisions_m7F27EE71FF46F14BDA138B3C259273B217EFA92B,
	MeshCollider__ctor_m30E265B3A2F241F9BF4663BAC38F3D917273D8CB,
	CapsuleCollider_get_center_m6374F7457A9450CAFFAD2DF0C9D1419BF9E304CB,
	CapsuleCollider_set_center_m36F35F070DFC2CBFC87532004073CA8D56F3678F,
	CapsuleCollider_get_radius_m5746DDE5E6A099269FC9DAD253887C58E8A064D0,
	CapsuleCollider_set_radius_mD4502A8676AAC093F1E9958FB7D5D765EA206432,
	CapsuleCollider_get_height_mD6CF93CB2C222F8E5B77D65B0356F8FD8005B526,
	CapsuleCollider_set_height_m728C9AF3772EEC1DA9845E19F3C2899CDD2D9496,
	CapsuleCollider_get_direction_m2C5110BBCA2CC1C63183CDBD73B6D11CC89B0918,
	CapsuleCollider_set_direction_mE5E7B7BB7FBBBA97148E8CFCDC46D1BB14673984,
	CapsuleCollider_GetGlobalExtents_m96E63F24BCAF34DDBC49E743C3203D19638501AF,
	CapsuleCollider_CalculateTransform_m5DA33B8D319E92F1FBE72447A8CDE5223DB69258,
	CapsuleCollider__ctor_mB993913A662CF20B09C4A7475B66E820C87879A4,
	CapsuleCollider_get_center_Injected_mBDF07E6DE932FBCAF0AA6F167EB993BE03BE6872,
	CapsuleCollider_set_center_Injected_mB43C17AC81236FB74E61E21215D2145663A7DEEE,
	CapsuleCollider_GetGlobalExtents_Injected_m3604AFE1AB8E8F50E203CF50E7A484346DC72C18,
	CapsuleCollider_CalculateTransform_Injected_mA1A6CB7A01489F8442B921163D0149EE6AE329EC,
	BoxCollider_get_center_m832B93439717C72D4A3B427C6E8F5B54E2DBD669,
	BoxCollider_set_center_m02D745E759A66BBEF405D33CE4ACE34B7E064178,
	BoxCollider_get_size_mBC38D4926D4BE54A6532F6E1D642F363CA3A58A1,
	BoxCollider_set_size_mD9153B4AE4C366ACAB9E5F49075D919A89168B2E,
	BoxCollider_get_extents_mAF61D471B8E2ABE9C41D44F647BB0EED74DA329B,
	BoxCollider_set_extents_m8F23EDB911DCFD1440281E7E9B11D0C1CA23FA4F,
	BoxCollider__ctor_mB54A88CED24F4C03899850888C7C11483EA3D645,
	BoxCollider_get_center_Injected_m5F060E93AE651FB756B9371C7A716BDAA2A96F51,
	BoxCollider_set_center_Injected_m54F8740514B14E3B53063FB4C7623BF0F2A48F0A,
	BoxCollider_get_size_Injected_mC9ABFED6FEC27351BC116B44021578CB39CBCB22,
	BoxCollider_set_size_Injected_m7A8B807F4AB573BF955610F55AAD372FF868C967,
	SphereCollider_get_center_mBFAE4FFFC76B8FD8F1B2B2F12C52A30470443D3A,
	SphereCollider_set_center_mD5E898A2FED304A82BC67ABB11B60BB0F612CED7,
	SphereCollider_get_radius_m403989140BDAD513299276953B481167CF08D02F,
	SphereCollider_set_radius_m55A0D144B32871AECC2A83FBCF423FBE1E5A63A0,
	SphereCollider__ctor_m81B7D07301D49F1E0229B92068559F0A2FDE0962,
	SphereCollider_get_center_Injected_mF082CC62A7E8DE3DD5B86518C210D85139DD93CB,
	SphereCollider_set_center_Injected_mB213D6CB194E150BBEF8EC2AA31EA391C2F4A641,
	ConstantForce_get_force_mC597105F67388B4FB6443E72981ECB09129BFBF5,
	ConstantForce_set_force_mE7AFBCB9EBFB6A94312C9D72DE160C12503147E9,
	ConstantForce_get_relativeForce_mEBA2646D393A1D6DF7F5574033B10C71CDE616C3,
	ConstantForce_set_relativeForce_m9B0113E65E5B48F399C889441C75158B7E2F2BB6,
	ConstantForce_get_torque_m09E929B93FB47DF28BEBED8B3D1B94180D99A890,
	ConstantForce_set_torque_m1568C91C8C2BA02C57DE457550CE19AF0B3A78FD,
	ConstantForce_get_relativeTorque_m130283B736FD47C2E07223E34E7176886DEAF7DA,
	ConstantForce_set_relativeTorque_m613F980B2371177D134F200776CEE314AB128FE5,
	ConstantForce__ctor_m91080857146412E848B415BB4DE1B7C86D1F69A4,
	ConstantForce_get_force_Injected_mD97072761D01F77BE93C9FA66666B732AEBE39B7,
	ConstantForce_set_force_Injected_mDD654BAA10488ABF81D98B5129F6AD0BDF82D578,
	ConstantForce_get_relativeForce_Injected_m83CEC6A7E2AFD77B29B53473331E2165606389A4,
	ConstantForce_set_relativeForce_Injected_m1EBC38E3BBE1020E80C4ECCFB345960FDC3423E8,
	ConstantForce_get_torque_Injected_mDD968C22228ED81AAAA203E9944CEBF61E767853,
	ConstantForce_set_torque_Injected_m581F2B41182D65BC4311693EB0AF705FA1FC7578,
	ConstantForce_get_relativeTorque_Injected_m6DA015FF7ED6B661C348DCE4EC1844B9EFD1409B,
	ConstantForce_set_relativeTorque_Injected_m82778B9E054E5FC2BDB38BD50BAAD4E55C3E1D41,
	PhysicsScene_ToString_mBB6D0AC1E3E2EDC34CB4A7A34485B24B6271903F,
	PhysicsScene_GetHashCode_m5344CC6CCCB1CA6EBB149775EE028089DD74B8A2,
	PhysicsScene_Equals_m2645ABB96C598F197F734EB712494795928CBCEC,
	PhysicsScene_Equals_m8C948B86D105177D519A3608816CDC698C8686B8,
	PhysicsScene_Raycast_m8C3E61EB7C3DB8AAC6FEB098D815062BEF821187,
	PhysicsScene_Internal_RaycastTest_m6715CAD8D0330195269AA7FCCD91613BC564E3EB,
	PhysicsScene_Raycast_mFC0D59C4439EE9DA6E8AA5F6891915132D587208,
	PhysicsScene_Internal_Raycast_m1E630AA11805114B090FC50741AEF64D677AF2C7,
	PhysicsScene_Raycast_m95CAB68EB9165E3AB2A4D441F7DF9767AD4B7F73,
	PhysicsScene_Internal_RaycastNonAlloc_mE7833EE5F1082431A4C2D29362F5DCDEFBF6D2BD,
	PhysicsScene_OverlapBoxNonAlloc_Internal_mEE4323DAE1A464A56CA1DD205D622617059AEA30,
	PhysicsScene_OverlapBox_mCC9E06BCEE764C2C0A9F2C544A69B996BB37CF27,
	PhysicsScene_Internal_RaycastTest_Injected_m956474EFEA507F82ABE0BCB75DDB3CC8EFF4D12B,
	PhysicsScene_Internal_Raycast_Injected_m8868E0BF89F8C42D11F42F94F9CF767647433BB1,
	PhysicsScene_Internal_RaycastNonAlloc_Injected_m59FB44C18E62D21E9320F30229C0AA9F9B971211,
	PhysicsScene_OverlapBoxNonAlloc_Internal_Injected_mA30EFF675823E14B22919E34D5D9641F06199010,
	Physics_get_defaultPhysicsScene_mA6361488FBAC110DA8397E5E4CB473EBF4191010,
	Physics_Raycast_m9DE0EEA1CF8DEF7D06216225F19E2958D341F7BA,
	Physics_Raycast_m09F21E13465121A73F19C07FC5F5314CF15ACD15,
	Physics_Raycast_m284670765E1627E43B7B0F5EC811A688EE595091,
	Physics_Raycast_mDA2EB8C7692308A7178222D31CBA4C7A1C7DC915,
	Physics_Raycast_m2EA572B4613E1BD7DBAA299511CFD2DBA179A163,
	Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C,
	Physics_Raycast_m18E12C65F127D1AA50D196623F04F81CB138FD12,
	Physics_Raycast_mE84D30EEFE59DA28DA172342068F092A35B2BE4A,
	Physics_Raycast_m77560CCAA49DC821E51FDDD1570B921D7704646F,
	Physics_Raycast_mFDC4B8E7979495E3C22D0E3CEA4BCAB271EEC25A,
	Physics_Raycast_mD7A711D3A790AC505F7229A4FFFA2E389008176B,
	Physics_Raycast_m111AFC36A136A67BE4776670E356E99A82010BFE,
	Physics_Raycast_mCA3F2DD1DC08199AAD8466BB857318CD454AC774,
	Physics_Raycast_m46E12070D6996F4C8C91D49ADC27C74AC1D6A3D8,
	Physics_Raycast_mA64F8C30681E3A6A8F2B7EDE592FE7BBC0D354F4,
	Physics_Raycast_m80EC8EEDA0ABA8B01838BA9054834CD1A381916E,
	Physics_Internal_RaycastAll_m39EE7CF896F9D8BA58CC623F14E4DB0641480523,
	Physics_RaycastAll_m5103E7C60CC66BAFA6534D1736138A92EB1EF2B8,
	Physics_RaycastAll_m2C977C8B022672F42B5E18F40B529C0A74B7E471,
	Physics_RaycastAll_m2BBC8D2731B38EE0B704A5C6A09D0F8BBA074A4D,
	Physics_RaycastAll_m83F28CB671653C07995FB1BCDC561121DE3D9CA6,
	Physics_RaycastAll_m55EB4478198ED6EF838500257FA3BE1A871D3605,
	Physics_RaycastAll_mA24B9B922C98C5D18397FAE55C04AEAFDD47F029,
	Physics_RaycastAll_m72947571EFB0EFB34E48340AA2EC0C8030D27C50,
	Physics_RaycastAll_m529EE59D6D03E4CFAECE4016C8CC4181BEC2D26D,
	Physics_RaycastNonAlloc_m7C81125AD7D5891EBC3AB48C6DED7B60F53DF099,
	Physics_RaycastNonAlloc_m8F62EE5A33E81A02E4983A171023B7BC4AC700CA,
	Physics_RaycastNonAlloc_m41BB7BB8755B09700C10F59A29C3541D45AB9CCD,
	Physics_RaycastNonAlloc_mBC5BE514E55B8D98A8F4752DED6850E02EE8AD98,
	Physics_RaycastNonAlloc_m2C90D14E472DE7929EFD54FDA7BC512D3FBDE4ED,
	Physics_RaycastNonAlloc_m316F597067055C9F923F57CC68D68FF917C3B4D1,
	Physics_RaycastNonAlloc_m1B8F31BF41F756F561F6AC3A2E0736F2BC9C4DB4,
	Physics_RaycastNonAlloc_m9076DDE1A65F87DB3D2824DAD4E5B8B534159F1F,
	Physics_CheckSphere_Internal_m2139E9BBEC789DCDCA7A729F13A0AB6FD7E4ADAA,
	Physics_CheckSphere_mF4AE4778A415A4E9C7C15BA21A0E402909AD3472,
	Physics_CheckSphere_m6DFD61C841CEBFDE6645689279AA6E31297B002B,
	Physics_OverlapBoxNonAlloc_m79F2597E253FCDD8D03253FAC692FF2191D0232A,
	Physics_OverlapBoxNonAlloc_m243E935F534B3E08DDF43049C81DD30214B7DD97,
	Physics_OverlapBoxNonAlloc_m95BFA939738463ED735E9EC43A7C1A2D83C9DECD,
	Physics_get_defaultPhysicsScene_Injected_m0B71068269B3C8499A258BBCAEA7E3D9D020BADA,
	Physics_Internal_RaycastAll_Injected_mD4C3E8762D9FCEE654CE0415E636EB7DBEC92A5B,
	Physics_CheckSphere_Internal_Injected_mEF8DD496E66AB1FDBF6EE8E10831E795B7B77A00,
};
extern void RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E_AdjustorThunk (void);
extern void RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E_AdjustorThunk (void);
extern void RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674_AdjustorThunk (void);
extern void RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA_AdjustorThunk (void);
extern void PhysicsScene_ToString_mBB6D0AC1E3E2EDC34CB4A7A34485B24B6271903F_AdjustorThunk (void);
extern void PhysicsScene_GetHashCode_m5344CC6CCCB1CA6EBB149775EE028089DD74B8A2_AdjustorThunk (void);
extern void PhysicsScene_Equals_m2645ABB96C598F197F734EB712494795928CBCEC_AdjustorThunk (void);
extern void PhysicsScene_Equals_m8C948B86D105177D519A3608816CDC698C8686B8_AdjustorThunk (void);
extern void PhysicsScene_Raycast_m8C3E61EB7C3DB8AAC6FEB098D815062BEF821187_AdjustorThunk (void);
extern void PhysicsScene_Raycast_mFC0D59C4439EE9DA6E8AA5F6891915132D587208_AdjustorThunk (void);
extern void PhysicsScene_Raycast_m95CAB68EB9165E3AB2A4D441F7DF9767AD4B7F73_AdjustorThunk (void);
extern void PhysicsScene_OverlapBox_mCC9E06BCEE764C2C0A9F2C544A69B996BB37CF27_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[12] = 
{
	{ 0x06000001, RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E_AdjustorThunk },
	{ 0x06000002, RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E_AdjustorThunk },
	{ 0x06000003, RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674_AdjustorThunk },
	{ 0x06000004, RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA_AdjustorThunk },
	{ 0x060000C9, PhysicsScene_ToString_mBB6D0AC1E3E2EDC34CB4A7A34485B24B6271903F_AdjustorThunk },
	{ 0x060000CA, PhysicsScene_GetHashCode_m5344CC6CCCB1CA6EBB149775EE028089DD74B8A2_AdjustorThunk },
	{ 0x060000CB, PhysicsScene_Equals_m2645ABB96C598F197F734EB712494795928CBCEC_AdjustorThunk },
	{ 0x060000CC, PhysicsScene_Equals_m8C948B86D105177D519A3608816CDC698C8686B8_AdjustorThunk },
	{ 0x060000CD, PhysicsScene_Raycast_m8C3E61EB7C3DB8AAC6FEB098D815062BEF821187_AdjustorThunk },
	{ 0x060000CF, PhysicsScene_Raycast_mFC0D59C4439EE9DA6E8AA5F6891915132D587208_AdjustorThunk },
	{ 0x060000D1, PhysicsScene_Raycast_m95CAB68EB9165E3AB2A4D441F7DF9767AD4B7F73_AdjustorThunk },
	{ 0x060000D4, PhysicsScene_OverlapBox_mCC9E06BCEE764C2C0A9F2C544A69B996BB37CF27_AdjustorThunk },
};
static const int32_t s_InvokerIndices[259] = 
{
	2842,
	2897,
	2897,
	2881,
	2897,
	2507,
	2897,
	2507,
	2881,
	2488,
	2881,
	2488,
	2881,
	2488,
	2488,
	2872,
	2480,
	2881,
	2488,
	2872,
	2480,
	2872,
	2480,
	2828,
	2439,
	2828,
	2439,
	2897,
	2507,
	2897,
	2852,
	2464,
	2897,
	2507,
	2872,
	2480,
	2897,
	2507,
	2852,
	2464,
	2828,
	2439,
	2828,
	2439,
	2881,
	2488,
	2881,
	2488,
	2507,
	2464,
	2901,
	2872,
	2901,
	2901,
	2901,
	2313,
	2313,
	2828,
	2439,
	2881,
	2488,
	2881,
	2488,
	2488,
	2872,
	2480,
	2828,
	2439,
	2828,
	2439,
	1581,
	2507,
	740,
	1064,
	1581,
	2507,
	740,
	1064,
	1581,
	2507,
	740,
	1064,
	1581,
	2507,
	740,
	1064,
	1072,
	1586,
	352,
	744,
	1065,
	1068,
	2313,
	585,
	609,
	942,
	1305,
	882,
	882,
	1217,
	1978,
	2901,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	1338,
	1338,
	1339,
	1339,
	1339,
	1339,
	948,
	348,
	947,
	275,
	809,
	2872,
	2480,
	2313,
	2796,
	2901,
	1338,
	2394,
	2842,
	2452,
	2872,
	2480,
	2872,
	2480,
	2828,
	2439,
	2881,
	2488,
	2872,
	2480,
	2901,
	2897,
	2507,
	2881,
	2488,
	2881,
	2488,
	2828,
	2439,
	2895,
	2839,
	2901,
	2394,
	2394,
	2394,
	2394,
	2897,
	2507,
	2897,
	2507,
	2897,
	2507,
	2901,
	2394,
	2394,
	2394,
	2394,
	2897,
	2507,
	2881,
	2488,
	2901,
	2394,
	2394,
	2897,
	2507,
	2897,
	2507,
	2897,
	2507,
	2897,
	2507,
	2901,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	2394,
	2842,
	2828,
	2175,
	2178,
	269,
	3242,
	157,
	3096,
	125,
	3063,
	3018,
	124,
	3224,
	3086,
	3054,
	3013,
	4354,
	3246,
	3455,
	3669,
	3974,
	3098,
	3245,
	3454,
	3668,
	3449,
	3665,
	3959,
	4259,
	3244,
	3448,
	3664,
	3958,
	3218,
	3220,
	3411,
	3630,
	3888,
	3408,
	3623,
	3880,
	4224,
	3189,
	3352,
	3565,
	3831,
	3067,
	3192,
	3354,
	3567,
	3243,
	3453,
	3667,
	3066,
	3191,
	3567,
	4299,
	3196,
	3224,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_PhysicsModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_PhysicsModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_PhysicsModule_CodeGenModule = 
{
	"UnityEngine.PhysicsModule.dll",
	259,
	s_methodPointers,
	12,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_PhysicsModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
