﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<System.Int32Enum>
struct List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystemVertexStream>
struct List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem/Particle>
struct List_1_tA1D5161C2AD941530756A2128C933F0D76F48E95;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// UnityEngine.Mesh[]
struct MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8;
// UnityEngine.ParticleSystemVertexStream[]
struct ParticleSystemVertexStreamU5BU5D_t1F1B124649A46CC13C2ED94699A10FFE344D8C2C;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Gradient
struct Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.ParticleSystem
struct ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E;
// UnityEngine.ParticleSystemRenderer
struct ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E;
// System.String
struct String_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;

IL2CPP_EXTERN_C RuntimeClass* List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m50FEC7E4E5C76F2F1388964CF2AF151F6BC4216C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m8C48C3F6EF447031F54430F3EF5AEB57666345E6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeArrayUnsafeUtility_GetUnsafePtr_TisParticle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1_mB8C1C175F09CE57B87C863A2827A9F50D1DCEEB7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeArrayUnsafeUtility_GetUnsafeReadOnlyPtr_TisParticle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1_m6DB36AF31E5460B987A006F70D2DD82D2FB350C1_RuntimeMethod_var;

struct MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8;
struct ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t0349A77A3F13355590A8D42950AB18125A07DF5D 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____items_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.ParticleSystemVertexStream>
struct List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ParticleSystemVertexStreamU5BU5D_t1F1B124649A46CC13C2ED94699A10FFE344D8C2C* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0, ____items_1)); }
	inline ParticleSystemVertexStreamU5BU5D_t1F1B124649A46CC13C2ED94699A10FFE344D8C2C* get__items_1() const { return ____items_1; }
	inline ParticleSystemVertexStreamU5BU5D_t1F1B124649A46CC13C2ED94699A10FFE344D8C2C** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ParticleSystemVertexStreamU5BU5D_t1F1B124649A46CC13C2ED94699A10FFE344D8C2C* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ParticleSystemVertexStreamU5BU5D_t1F1B124649A46CC13C2ED94699A10FFE344D8C2C* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0_StaticFields, ____emptyArray_5)); }
	inline ParticleSystemVertexStreamU5BU5D_t1F1B124649A46CC13C2ED94699A10FFE344D8C2C* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ParticleSystemVertexStreamU5BU5D_t1F1B124649A46CC13C2ED94699A10FFE344D8C2C** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ParticleSystemVertexStreamU5BU5D_t1F1B124649A46CC13C2ED94699A10FFE344D8C2C* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A, ____items_1)); }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* get__items_1() const { return ____items_1; }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A_StaticFields, ____emptyArray_5)); }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.ParticleSystem/Particle>
struct List_1_tA1D5161C2AD941530756A2128C933F0D76F48E95  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tA1D5161C2AD941530756A2128C933F0D76F48E95, ____items_1)); }
	inline ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* get__items_1() const { return ____items_1; }
	inline ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tA1D5161C2AD941530756A2128C933F0D76F48E95, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tA1D5161C2AD941530756A2128C933F0D76F48E95, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tA1D5161C2AD941530756A2128C933F0D76F48E95, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tA1D5161C2AD941530756A2128C933F0D76F48E95_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tA1D5161C2AD941530756A2128C933F0D76F48E95_StaticFields, ____emptyArray_5)); }
	inline ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Color32
struct Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.UInt32
struct UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.ParticleSystemJobs.NativeParticleData/Array3
struct Array3_t8C453D8814FA7AFB47B96022121D442000295666 
{
public:
	// System.Single* UnityEngine.ParticleSystemJobs.NativeParticleData/Array3::x
	float* ___x_0;
	// System.Single* UnityEngine.ParticleSystemJobs.NativeParticleData/Array3::y
	float* ___y_1;
	// System.Single* UnityEngine.ParticleSystemJobs.NativeParticleData/Array3::z
	float* ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Array3_t8C453D8814FA7AFB47B96022121D442000295666, ___x_0)); }
	inline float* get_x_0() const { return ___x_0; }
	inline float** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float* value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Array3_t8C453D8814FA7AFB47B96022121D442000295666, ___y_1)); }
	inline float* get_y_1() const { return ___y_1; }
	inline float** get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float* value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Array3_t8C453D8814FA7AFB47B96022121D442000295666, ___z_2)); }
	inline float* get_z_2() const { return ___z_2; }
	inline float** get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float* value)
	{
		___z_2 = value;
	}
};


// UnityEngine.ParticleSystemJobs.NativeParticleData/Array4
struct Array4_tE3091E6F5D6BC5875DCFDB6A632F375F3D64B5B0 
{
public:
	// System.Single* UnityEngine.ParticleSystemJobs.NativeParticleData/Array4::x
	float* ___x_0;
	// System.Single* UnityEngine.ParticleSystemJobs.NativeParticleData/Array4::y
	float* ___y_1;
	// System.Single* UnityEngine.ParticleSystemJobs.NativeParticleData/Array4::z
	float* ___z_2;
	// System.Single* UnityEngine.ParticleSystemJobs.NativeParticleData/Array4::w
	float* ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Array4_tE3091E6F5D6BC5875DCFDB6A632F375F3D64B5B0, ___x_0)); }
	inline float* get_x_0() const { return ___x_0; }
	inline float** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float* value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Array4_tE3091E6F5D6BC5875DCFDB6A632F375F3D64B5B0, ___y_1)); }
	inline float* get_y_1() const { return ___y_1; }
	inline float** get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float* value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Array4_tE3091E6F5D6BC5875DCFDB6A632F375F3D64B5B0, ___z_2)); }
	inline float* get_z_2() const { return ___z_2; }
	inline float** get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float* value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Array4_tE3091E6F5D6BC5875DCFDB6A632F375F3D64B5B0, ___w_3)); }
	inline float* get_w_3() const { return ___w_3; }
	inline float** get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float* value)
	{
		___w_3 = value;
	}
};


// UnityEngine.ParticleSystem/CollisionModule
struct CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/CollisionModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/CollisionModule
struct CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/CollisionModule
struct CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/ColorBySpeedModule
struct ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/ColorBySpeedModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/ColorBySpeedModule
struct ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/ColorBySpeedModule
struct ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/ColorOverLifetimeModule
struct ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/ColorOverLifetimeModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/ColorOverLifetimeModule
struct ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/ColorOverLifetimeModule
struct ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/CustomDataModule
struct CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/CustomDataModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/CustomDataModule
struct CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/CustomDataModule
struct CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/EmissionModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/ExternalForcesModule
struct ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/ExternalForcesModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/ExternalForcesModule
struct ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/ExternalForcesModule
struct ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/ForceOverLifetimeModule
struct ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/ForceOverLifetimeModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/ForceOverLifetimeModule
struct ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/ForceOverLifetimeModule
struct ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/InheritVelocityModule
struct InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/InheritVelocityModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/InheritVelocityModule
struct InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/InheritVelocityModule
struct InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/LifetimeByEmitterSpeedModule
struct LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/LifetimeByEmitterSpeedModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/LifetimeByEmitterSpeedModule
struct LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/LifetimeByEmitterSpeedModule
struct LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/LightsModule
struct LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/LightsModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/LightsModule
struct LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/LightsModule
struct LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule
struct LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule
struct LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule
struct LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/MainModule
struct MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/NoiseModule
struct NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/NoiseModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/NoiseModule
struct NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/NoiseModule
struct NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/RotationBySpeedModule
struct RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/RotationBySpeedModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/RotationBySpeedModule
struct RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/RotationBySpeedModule
struct RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/RotationOverLifetimeModule
struct RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/RotationOverLifetimeModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/RotationOverLifetimeModule
struct RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/RotationOverLifetimeModule
struct RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/ShapeModule
struct ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/ShapeModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/ShapeModule
struct ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/ShapeModule
struct ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/SizeBySpeedModule
struct SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/SizeBySpeedModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/SizeBySpeedModule
struct SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/SizeBySpeedModule
struct SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/SizeOverLifetimeModule
struct SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/SizeOverLifetimeModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/SizeOverLifetimeModule
struct SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/SizeOverLifetimeModule
struct SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/SubEmittersModule
struct SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/SubEmittersModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/SubEmittersModule
struct SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/SubEmittersModule
struct SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/TextureSheetAnimationModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/TextureSheetAnimationModule
struct TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/TrailModule
struct TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/TrailModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/TrailModule
struct TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/TrailModule
struct TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/Trails
struct Trails_tE9352AAF21D22D5007179B507019C44C823013F2 
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.ParticleSystem/Trails::positions
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___positions_0;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.ParticleSystem/Trails::frontPositions
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___frontPositions_1;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.ParticleSystem/Trails::backPositions
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___backPositions_2;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.ParticleSystem/Trails::positionCounts
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___positionCounts_3;
	// System.Int32 UnityEngine.ParticleSystem/Trails::maxTrailCount
	int32_t ___maxTrailCount_4;
	// System.Int32 UnityEngine.ParticleSystem/Trails::maxPositionsPerTrailCount
	int32_t ___maxPositionsPerTrailCount_5;

public:
	inline static int32_t get_offset_of_positions_0() { return static_cast<int32_t>(offsetof(Trails_tE9352AAF21D22D5007179B507019C44C823013F2, ___positions_0)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_positions_0() const { return ___positions_0; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_positions_0() { return &___positions_0; }
	inline void set_positions_0(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___positions_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___positions_0), (void*)value);
	}

	inline static int32_t get_offset_of_frontPositions_1() { return static_cast<int32_t>(offsetof(Trails_tE9352AAF21D22D5007179B507019C44C823013F2, ___frontPositions_1)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_frontPositions_1() const { return ___frontPositions_1; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_frontPositions_1() { return &___frontPositions_1; }
	inline void set_frontPositions_1(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___frontPositions_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frontPositions_1), (void*)value);
	}

	inline static int32_t get_offset_of_backPositions_2() { return static_cast<int32_t>(offsetof(Trails_tE9352AAF21D22D5007179B507019C44C823013F2, ___backPositions_2)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_backPositions_2() const { return ___backPositions_2; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_backPositions_2() { return &___backPositions_2; }
	inline void set_backPositions_2(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___backPositions_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___backPositions_2), (void*)value);
	}

	inline static int32_t get_offset_of_positionCounts_3() { return static_cast<int32_t>(offsetof(Trails_tE9352AAF21D22D5007179B507019C44C823013F2, ___positionCounts_3)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_positionCounts_3() const { return ___positionCounts_3; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_positionCounts_3() { return &___positionCounts_3; }
	inline void set_positionCounts_3(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___positionCounts_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___positionCounts_3), (void*)value);
	}

	inline static int32_t get_offset_of_maxTrailCount_4() { return static_cast<int32_t>(offsetof(Trails_tE9352AAF21D22D5007179B507019C44C823013F2, ___maxTrailCount_4)); }
	inline int32_t get_maxTrailCount_4() const { return ___maxTrailCount_4; }
	inline int32_t* get_address_of_maxTrailCount_4() { return &___maxTrailCount_4; }
	inline void set_maxTrailCount_4(int32_t value)
	{
		___maxTrailCount_4 = value;
	}

	inline static int32_t get_offset_of_maxPositionsPerTrailCount_5() { return static_cast<int32_t>(offsetof(Trails_tE9352AAF21D22D5007179B507019C44C823013F2, ___maxPositionsPerTrailCount_5)); }
	inline int32_t get_maxPositionsPerTrailCount_5() const { return ___maxPositionsPerTrailCount_5; }
	inline int32_t* get_address_of_maxPositionsPerTrailCount_5() { return &___maxPositionsPerTrailCount_5; }
	inline void set_maxPositionsPerTrailCount_5(int32_t value)
	{
		___maxPositionsPerTrailCount_5 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/Trails
struct Trails_tE9352AAF21D22D5007179B507019C44C823013F2_marshaled_pinvoke
{
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___positions_0;
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___frontPositions_1;
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___backPositions_2;
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___positionCounts_3;
	int32_t ___maxTrailCount_4;
	int32_t ___maxPositionsPerTrailCount_5;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/Trails
struct Trails_tE9352AAF21D22D5007179B507019C44C823013F2_marshaled_com
{
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___positions_0;
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___frontPositions_1;
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___backPositions_2;
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___positionCounts_3;
	int32_t ___maxTrailCount_4;
	int32_t ___maxPositionsPerTrailCount_5;
};

// UnityEngine.ParticleSystem/TriggerModule
struct TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/TriggerModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/TriggerModule
struct TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/TriggerModule
struct TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/VelocityOverLifetimeModule
struct VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/VelocityOverLifetimeModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/VelocityOverLifetimeModule
struct VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/VelocityOverLifetimeModule
struct VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/PlaybackState/Noise
struct Noise_tAE62462FCFB05E417C3A8A8BE90183952CEAF93B 
{
public:
	// System.Single UnityEngine.ParticleSystem/PlaybackState/Noise::m_ScrollOffset
	float ___m_ScrollOffset_0;

public:
	inline static int32_t get_offset_of_m_ScrollOffset_0() { return static_cast<int32_t>(offsetof(Noise_tAE62462FCFB05E417C3A8A8BE90183952CEAF93B, ___m_ScrollOffset_0)); }
	inline float get_m_ScrollOffset_0() const { return ___m_ScrollOffset_0; }
	inline float* get_address_of_m_ScrollOffset_0() { return &___m_ScrollOffset_0; }
	inline void set_m_ScrollOffset_0(float value)
	{
		___m_ScrollOffset_0 = value;
	}
};


// UnityEngine.ParticleSystem/PlaybackState/Seed
struct Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949 
{
public:
	// System.UInt32 UnityEngine.ParticleSystem/PlaybackState/Seed::x
	uint32_t ___x_0;
	// System.UInt32 UnityEngine.ParticleSystem/PlaybackState/Seed::y
	uint32_t ___y_1;
	// System.UInt32 UnityEngine.ParticleSystem/PlaybackState/Seed::z
	uint32_t ___z_2;
	// System.UInt32 UnityEngine.ParticleSystem/PlaybackState/Seed::w
	uint32_t ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949, ___x_0)); }
	inline uint32_t get_x_0() const { return ___x_0; }
	inline uint32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(uint32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949, ___y_1)); }
	inline uint32_t get_y_1() const { return ___y_1; }
	inline uint32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(uint32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949, ___z_2)); }
	inline uint32_t get_z_2() const { return ___z_2; }
	inline uint32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(uint32_t value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949, ___w_3)); }
	inline uint32_t get_w_3() const { return ___w_3; }
	inline uint32_t* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(uint32_t value)
	{
		___w_3 = value;
	}
};


// UnityEngine.ParticleSystem/PlaybackState/Trail
struct Trail_t82FA0A8F8A1B2507F5CCDE83B9B520BAD2F713B5 
{
public:
	// System.Single UnityEngine.ParticleSystem/PlaybackState/Trail::m_Timer
	float ___m_Timer_0;

public:
	inline static int32_t get_offset_of_m_Timer_0() { return static_cast<int32_t>(offsetof(Trail_t82FA0A8F8A1B2507F5CCDE83B9B520BAD2F713B5, ___m_Timer_0)); }
	inline float get_m_Timer_0() const { return ___m_Timer_0; }
	inline float* get_address_of_m_Timer_0() { return &___m_Timer_0; }
	inline void set_m_Timer_0(float value)
	{
		___m_Timer_0 = value;
	}
};


// Unity.Collections.Allocator
struct Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05 
{
public:
	// System.Int32 Unity.Collections.Allocator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Gradient
struct Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Gradient::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Gradient
struct Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Gradient
struct Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Int32Enum
struct Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Jobs.JobHandle
struct JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847 
{
public:
	// System.IntPtr Unity.Jobs.JobHandle::jobGroup
	intptr_t ___jobGroup_0;
	// System.Int32 Unity.Jobs.JobHandle::version
	int32_t ___version_1;

public:
	inline static int32_t get_offset_of_jobGroup_0() { return static_cast<int32_t>(offsetof(JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847, ___jobGroup_0)); }
	inline intptr_t get_jobGroup_0() const { return ___jobGroup_0; }
	inline intptr_t* get_address_of_jobGroup_0() { return &___jobGroup_0; }
	inline void set_jobGroup_0(intptr_t value)
	{
		___jobGroup_0 = value;
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}
};


// UnityEngine.ParticleSystemJobs.NativeParticleData
struct NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD 
{
public:
	// System.Int32 UnityEngine.ParticleSystemJobs.NativeParticleData::count
	int32_t ___count_0;
	// UnityEngine.ParticleSystemJobs.NativeParticleData/Array3 UnityEngine.ParticleSystemJobs.NativeParticleData::positions
	Array3_t8C453D8814FA7AFB47B96022121D442000295666  ___positions_1;
	// UnityEngine.ParticleSystemJobs.NativeParticleData/Array3 UnityEngine.ParticleSystemJobs.NativeParticleData::velocities
	Array3_t8C453D8814FA7AFB47B96022121D442000295666  ___velocities_2;
	// UnityEngine.ParticleSystemJobs.NativeParticleData/Array3 UnityEngine.ParticleSystemJobs.NativeParticleData::axisOfRotations
	Array3_t8C453D8814FA7AFB47B96022121D442000295666  ___axisOfRotations_3;
	// UnityEngine.ParticleSystemJobs.NativeParticleData/Array3 UnityEngine.ParticleSystemJobs.NativeParticleData::rotations
	Array3_t8C453D8814FA7AFB47B96022121D442000295666  ___rotations_4;
	// UnityEngine.ParticleSystemJobs.NativeParticleData/Array3 UnityEngine.ParticleSystemJobs.NativeParticleData::rotationalSpeeds
	Array3_t8C453D8814FA7AFB47B96022121D442000295666  ___rotationalSpeeds_5;
	// UnityEngine.ParticleSystemJobs.NativeParticleData/Array3 UnityEngine.ParticleSystemJobs.NativeParticleData::sizes
	Array3_t8C453D8814FA7AFB47B96022121D442000295666  ___sizes_6;
	// System.Void* UnityEngine.ParticleSystemJobs.NativeParticleData::startColors
	void* ___startColors_7;
	// System.Void* UnityEngine.ParticleSystemJobs.NativeParticleData::aliveTimePercent
	void* ___aliveTimePercent_8;
	// System.Void* UnityEngine.ParticleSystemJobs.NativeParticleData::inverseStartLifetimes
	void* ___inverseStartLifetimes_9;
	// System.Void* UnityEngine.ParticleSystemJobs.NativeParticleData::randomSeeds
	void* ___randomSeeds_10;
	// UnityEngine.ParticleSystemJobs.NativeParticleData/Array4 UnityEngine.ParticleSystemJobs.NativeParticleData::customData1
	Array4_tE3091E6F5D6BC5875DCFDB6A632F375F3D64B5B0  ___customData1_11;
	// UnityEngine.ParticleSystemJobs.NativeParticleData/Array4 UnityEngine.ParticleSystemJobs.NativeParticleData::customData2
	Array4_tE3091E6F5D6BC5875DCFDB6A632F375F3D64B5B0  ___customData2_12;
	// System.Void* UnityEngine.ParticleSystemJobs.NativeParticleData::meshIndices
	void* ___meshIndices_13;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_positions_1() { return static_cast<int32_t>(offsetof(NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD, ___positions_1)); }
	inline Array3_t8C453D8814FA7AFB47B96022121D442000295666  get_positions_1() const { return ___positions_1; }
	inline Array3_t8C453D8814FA7AFB47B96022121D442000295666 * get_address_of_positions_1() { return &___positions_1; }
	inline void set_positions_1(Array3_t8C453D8814FA7AFB47B96022121D442000295666  value)
	{
		___positions_1 = value;
	}

	inline static int32_t get_offset_of_velocities_2() { return static_cast<int32_t>(offsetof(NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD, ___velocities_2)); }
	inline Array3_t8C453D8814FA7AFB47B96022121D442000295666  get_velocities_2() const { return ___velocities_2; }
	inline Array3_t8C453D8814FA7AFB47B96022121D442000295666 * get_address_of_velocities_2() { return &___velocities_2; }
	inline void set_velocities_2(Array3_t8C453D8814FA7AFB47B96022121D442000295666  value)
	{
		___velocities_2 = value;
	}

	inline static int32_t get_offset_of_axisOfRotations_3() { return static_cast<int32_t>(offsetof(NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD, ___axisOfRotations_3)); }
	inline Array3_t8C453D8814FA7AFB47B96022121D442000295666  get_axisOfRotations_3() const { return ___axisOfRotations_3; }
	inline Array3_t8C453D8814FA7AFB47B96022121D442000295666 * get_address_of_axisOfRotations_3() { return &___axisOfRotations_3; }
	inline void set_axisOfRotations_3(Array3_t8C453D8814FA7AFB47B96022121D442000295666  value)
	{
		___axisOfRotations_3 = value;
	}

	inline static int32_t get_offset_of_rotations_4() { return static_cast<int32_t>(offsetof(NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD, ___rotations_4)); }
	inline Array3_t8C453D8814FA7AFB47B96022121D442000295666  get_rotations_4() const { return ___rotations_4; }
	inline Array3_t8C453D8814FA7AFB47B96022121D442000295666 * get_address_of_rotations_4() { return &___rotations_4; }
	inline void set_rotations_4(Array3_t8C453D8814FA7AFB47B96022121D442000295666  value)
	{
		___rotations_4 = value;
	}

	inline static int32_t get_offset_of_rotationalSpeeds_5() { return static_cast<int32_t>(offsetof(NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD, ___rotationalSpeeds_5)); }
	inline Array3_t8C453D8814FA7AFB47B96022121D442000295666  get_rotationalSpeeds_5() const { return ___rotationalSpeeds_5; }
	inline Array3_t8C453D8814FA7AFB47B96022121D442000295666 * get_address_of_rotationalSpeeds_5() { return &___rotationalSpeeds_5; }
	inline void set_rotationalSpeeds_5(Array3_t8C453D8814FA7AFB47B96022121D442000295666  value)
	{
		___rotationalSpeeds_5 = value;
	}

	inline static int32_t get_offset_of_sizes_6() { return static_cast<int32_t>(offsetof(NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD, ___sizes_6)); }
	inline Array3_t8C453D8814FA7AFB47B96022121D442000295666  get_sizes_6() const { return ___sizes_6; }
	inline Array3_t8C453D8814FA7AFB47B96022121D442000295666 * get_address_of_sizes_6() { return &___sizes_6; }
	inline void set_sizes_6(Array3_t8C453D8814FA7AFB47B96022121D442000295666  value)
	{
		___sizes_6 = value;
	}

	inline static int32_t get_offset_of_startColors_7() { return static_cast<int32_t>(offsetof(NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD, ___startColors_7)); }
	inline void* get_startColors_7() const { return ___startColors_7; }
	inline void** get_address_of_startColors_7() { return &___startColors_7; }
	inline void set_startColors_7(void* value)
	{
		___startColors_7 = value;
	}

	inline static int32_t get_offset_of_aliveTimePercent_8() { return static_cast<int32_t>(offsetof(NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD, ___aliveTimePercent_8)); }
	inline void* get_aliveTimePercent_8() const { return ___aliveTimePercent_8; }
	inline void** get_address_of_aliveTimePercent_8() { return &___aliveTimePercent_8; }
	inline void set_aliveTimePercent_8(void* value)
	{
		___aliveTimePercent_8 = value;
	}

	inline static int32_t get_offset_of_inverseStartLifetimes_9() { return static_cast<int32_t>(offsetof(NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD, ___inverseStartLifetimes_9)); }
	inline void* get_inverseStartLifetimes_9() const { return ___inverseStartLifetimes_9; }
	inline void** get_address_of_inverseStartLifetimes_9() { return &___inverseStartLifetimes_9; }
	inline void set_inverseStartLifetimes_9(void* value)
	{
		___inverseStartLifetimes_9 = value;
	}

	inline static int32_t get_offset_of_randomSeeds_10() { return static_cast<int32_t>(offsetof(NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD, ___randomSeeds_10)); }
	inline void* get_randomSeeds_10() const { return ___randomSeeds_10; }
	inline void** get_address_of_randomSeeds_10() { return &___randomSeeds_10; }
	inline void set_randomSeeds_10(void* value)
	{
		___randomSeeds_10 = value;
	}

	inline static int32_t get_offset_of_customData1_11() { return static_cast<int32_t>(offsetof(NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD, ___customData1_11)); }
	inline Array4_tE3091E6F5D6BC5875DCFDB6A632F375F3D64B5B0  get_customData1_11() const { return ___customData1_11; }
	inline Array4_tE3091E6F5D6BC5875DCFDB6A632F375F3D64B5B0 * get_address_of_customData1_11() { return &___customData1_11; }
	inline void set_customData1_11(Array4_tE3091E6F5D6BC5875DCFDB6A632F375F3D64B5B0  value)
	{
		___customData1_11 = value;
	}

	inline static int32_t get_offset_of_customData2_12() { return static_cast<int32_t>(offsetof(NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD, ___customData2_12)); }
	inline Array4_tE3091E6F5D6BC5875DCFDB6A632F375F3D64B5B0  get_customData2_12() const { return ___customData2_12; }
	inline Array4_tE3091E6F5D6BC5875DCFDB6A632F375F3D64B5B0 * get_address_of_customData2_12() { return &___customData2_12; }
	inline void set_customData2_12(Array4_tE3091E6F5D6BC5875DCFDB6A632F375F3D64B5B0  value)
	{
		___customData2_12 = value;
	}

	inline static int32_t get_offset_of_meshIndices_13() { return static_cast<int32_t>(offsetof(NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD, ___meshIndices_13)); }
	inline void* get_meshIndices_13() const { return ___meshIndices_13; }
	inline void** get_address_of_meshIndices_13() { return &___meshIndices_13; }
	inline void set_meshIndices_13(void* value)
	{
		___meshIndices_13 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.ParticleSystemCurveMode
struct ParticleSystemCurveMode_t1B9D50590BC22BDD142A21664B8E2F9475409342 
{
public:
	// System.Int32 UnityEngine.ParticleSystemCurveMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemCurveMode_t1B9D50590BC22BDD142A21664B8E2F9475409342, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ParticleSystemCustomData
struct ParticleSystemCustomData_t515AF0B8658ED14A2098625A0EF89F9963B84350 
{
public:
	// System.Int32 UnityEngine.ParticleSystemCustomData::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemCustomData_t515AF0B8658ED14A2098625A0EF89F9963B84350, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ParticleSystemGradientMode
struct ParticleSystemGradientMode_tCF15644F35B8D166D1A9C073E758D24794895497 
{
public:
	// System.Int32 UnityEngine.ParticleSystemGradientMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemGradientMode_tCF15644F35B8D166D1A9C073E758D24794895497, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ParticleSystemRenderMode
struct ParticleSystemRenderMode_t59F40974ED8FD0F14A4D20136E551AAFFDC0999D 
{
public:
	// System.Int32 UnityEngine.ParticleSystemRenderMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemRenderMode_t59F40974ED8FD0F14A4D20136E551AAFFDC0999D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ParticleSystemRenderSpace
struct ParticleSystemRenderSpace_t5EBC57C8A93BD4A79DDACA2D9EDB9C274E02191A 
{
public:
	// System.Int32 UnityEngine.ParticleSystemRenderSpace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemRenderSpace_t5EBC57C8A93BD4A79DDACA2D9EDB9C274E02191A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ParticleSystemScalingMode
struct ParticleSystemScalingMode_t7412EA1EC1652B7889578A8B3F5A26739AB270C2 
{
public:
	// System.Int32 UnityEngine.ParticleSystemScalingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemScalingMode_t7412EA1EC1652B7889578A8B3F5A26739AB270C2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ParticleSystemSimulationSpace
struct ParticleSystemSimulationSpace_tC10E7C116E400697EDF9E5C81AD5BAAFD08F4C11 
{
public:
	// System.Int32 UnityEngine.ParticleSystemSimulationSpace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemSimulationSpace_tC10E7C116E400697EDF9E5C81AD5BAAFD08F4C11, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ParticleSystemSortMode
struct ParticleSystemSortMode_tC14A9A574CCC272A3043FE207288706BF4A7807C 
{
public:
	// System.Int32 UnityEngine.ParticleSystemSortMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemSortMode_tC14A9A574CCC272A3043FE207288706BF4A7807C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ParticleSystemStopBehavior
struct ParticleSystemStopBehavior_tD9B009574B0315B09EC10AA1EAF8836DA87DD925 
{
public:
	// System.Int32 UnityEngine.ParticleSystemStopBehavior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemStopBehavior_tD9B009574B0315B09EC10AA1EAF8836DA87DD925, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ParticleSystemVertexStream
struct ParticleSystemVertexStream_t776E91F18B36FC089B11ABD6D3D51A77C06EFACD 
{
public:
	// System.Int32 UnityEngine.ParticleSystemVertexStream::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemVertexStream_t776E91F18B36FC089B11ABD6D3D51A77C06EFACD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ParticleSystemVertexStreams
struct ParticleSystemVertexStreams_tAA5817E7AEC7041426A506D2B5630498A0D03EE6 
{
public:
	// System.Int32 UnityEngine.ParticleSystemVertexStreams::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemVertexStreams_tAA5817E7AEC7041426A506D2B5630498A0D03EE6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.SpriteMaskInteraction
struct SpriteMaskInteraction_t35C03F1CE86D053B455FAB3EFFCB429CA9C6A74C 
{
public:
	// System.Int32 UnityEngine.SpriteMaskInteraction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpriteMaskInteraction_t35C03F1CE86D053B455FAB3EFFCB429CA9C6A74C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ParticleSystem/Particle
struct Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 
{
public:
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Position_0;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Velocity
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Velocity_1;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AnimatedVelocity
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_AnimatedVelocity_2;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_InitialVelocity
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_InitialVelocity_3;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AxisOfRotation
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_AxisOfRotation_4;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Rotation
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Rotation_5;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AngularVelocity
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_AngularVelocity_6;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_StartSize
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_StartSize_7;
	// UnityEngine.Color32 UnityEngine.ParticleSystem/Particle::m_StartColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___m_StartColor_8;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_RandomSeed
	uint32_t ___m_RandomSeed_9;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_ParentRandomSeed
	uint32_t ___m_ParentRandomSeed_10;
	// System.Single UnityEngine.ParticleSystem/Particle::m_Lifetime
	float ___m_Lifetime_11;
	// System.Single UnityEngine.ParticleSystem/Particle::m_StartLifetime
	float ___m_StartLifetime_12;
	// System.Int32 UnityEngine.ParticleSystem/Particle::m_MeshIndex
	int32_t ___m_MeshIndex_13;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator0
	float ___m_EmitAccumulator0_14;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator1
	float ___m_EmitAccumulator1_15;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_Flags
	uint32_t ___m_Flags_16;

public:
	inline static int32_t get_offset_of_m_Position_0() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_Position_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Position_0() const { return ___m_Position_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Position_0() { return &___m_Position_0; }
	inline void set_m_Position_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Position_0 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_1() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_Velocity_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Velocity_1() const { return ___m_Velocity_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Velocity_1() { return &___m_Velocity_1; }
	inline void set_m_Velocity_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Velocity_1 = value;
	}

	inline static int32_t get_offset_of_m_AnimatedVelocity_2() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_AnimatedVelocity_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_AnimatedVelocity_2() const { return ___m_AnimatedVelocity_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_AnimatedVelocity_2() { return &___m_AnimatedVelocity_2; }
	inline void set_m_AnimatedVelocity_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_AnimatedVelocity_2 = value;
	}

	inline static int32_t get_offset_of_m_InitialVelocity_3() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_InitialVelocity_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_InitialVelocity_3() const { return ___m_InitialVelocity_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_InitialVelocity_3() { return &___m_InitialVelocity_3; }
	inline void set_m_InitialVelocity_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_InitialVelocity_3 = value;
	}

	inline static int32_t get_offset_of_m_AxisOfRotation_4() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_AxisOfRotation_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_AxisOfRotation_4() const { return ___m_AxisOfRotation_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_AxisOfRotation_4() { return &___m_AxisOfRotation_4; }
	inline void set_m_AxisOfRotation_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_AxisOfRotation_4 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_5() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_Rotation_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Rotation_5() const { return ___m_Rotation_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Rotation_5() { return &___m_Rotation_5; }
	inline void set_m_Rotation_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Rotation_5 = value;
	}

	inline static int32_t get_offset_of_m_AngularVelocity_6() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_AngularVelocity_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_AngularVelocity_6() const { return ___m_AngularVelocity_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_AngularVelocity_6() { return &___m_AngularVelocity_6; }
	inline void set_m_AngularVelocity_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_AngularVelocity_6 = value;
	}

	inline static int32_t get_offset_of_m_StartSize_7() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_StartSize_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_StartSize_7() const { return ___m_StartSize_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_StartSize_7() { return &___m_StartSize_7; }
	inline void set_m_StartSize_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_StartSize_7 = value;
	}

	inline static int32_t get_offset_of_m_StartColor_8() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_StartColor_8)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_m_StartColor_8() const { return ___m_StartColor_8; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_m_StartColor_8() { return &___m_StartColor_8; }
	inline void set_m_StartColor_8(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___m_StartColor_8 = value;
	}

	inline static int32_t get_offset_of_m_RandomSeed_9() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_RandomSeed_9)); }
	inline uint32_t get_m_RandomSeed_9() const { return ___m_RandomSeed_9; }
	inline uint32_t* get_address_of_m_RandomSeed_9() { return &___m_RandomSeed_9; }
	inline void set_m_RandomSeed_9(uint32_t value)
	{
		___m_RandomSeed_9 = value;
	}

	inline static int32_t get_offset_of_m_ParentRandomSeed_10() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_ParentRandomSeed_10)); }
	inline uint32_t get_m_ParentRandomSeed_10() const { return ___m_ParentRandomSeed_10; }
	inline uint32_t* get_address_of_m_ParentRandomSeed_10() { return &___m_ParentRandomSeed_10; }
	inline void set_m_ParentRandomSeed_10(uint32_t value)
	{
		___m_ParentRandomSeed_10 = value;
	}

	inline static int32_t get_offset_of_m_Lifetime_11() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_Lifetime_11)); }
	inline float get_m_Lifetime_11() const { return ___m_Lifetime_11; }
	inline float* get_address_of_m_Lifetime_11() { return &___m_Lifetime_11; }
	inline void set_m_Lifetime_11(float value)
	{
		___m_Lifetime_11 = value;
	}

	inline static int32_t get_offset_of_m_StartLifetime_12() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_StartLifetime_12)); }
	inline float get_m_StartLifetime_12() const { return ___m_StartLifetime_12; }
	inline float* get_address_of_m_StartLifetime_12() { return &___m_StartLifetime_12; }
	inline void set_m_StartLifetime_12(float value)
	{
		___m_StartLifetime_12 = value;
	}

	inline static int32_t get_offset_of_m_MeshIndex_13() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_MeshIndex_13)); }
	inline int32_t get_m_MeshIndex_13() const { return ___m_MeshIndex_13; }
	inline int32_t* get_address_of_m_MeshIndex_13() { return &___m_MeshIndex_13; }
	inline void set_m_MeshIndex_13(int32_t value)
	{
		___m_MeshIndex_13 = value;
	}

	inline static int32_t get_offset_of_m_EmitAccumulator0_14() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_EmitAccumulator0_14)); }
	inline float get_m_EmitAccumulator0_14() const { return ___m_EmitAccumulator0_14; }
	inline float* get_address_of_m_EmitAccumulator0_14() { return &___m_EmitAccumulator0_14; }
	inline void set_m_EmitAccumulator0_14(float value)
	{
		___m_EmitAccumulator0_14 = value;
	}

	inline static int32_t get_offset_of_m_EmitAccumulator1_15() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_EmitAccumulator1_15)); }
	inline float get_m_EmitAccumulator1_15() const { return ___m_EmitAccumulator1_15; }
	inline float* get_address_of_m_EmitAccumulator1_15() { return &___m_EmitAccumulator1_15; }
	inline void set_m_EmitAccumulator1_15(float value)
	{
		___m_EmitAccumulator1_15 = value;
	}

	inline static int32_t get_offset_of_m_Flags_16() { return static_cast<int32_t>(offsetof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1, ___m_Flags_16)); }
	inline uint32_t get_m_Flags_16() const { return ___m_Flags_16; }
	inline uint32_t* get_address_of_m_Flags_16() { return &___m_Flags_16; }
	inline void set_m_Flags_16(uint32_t value)
	{
		___m_Flags_16 = value;
	}
};


// UnityEngine.ParticleSystem/PlaybackState/Emission
struct Emission_tC1BDB748E761EC23ADC2610C67638B7336F0FDCC 
{
public:
	// System.Single UnityEngine.ParticleSystem/PlaybackState/Emission::m_ParticleSpacing
	float ___m_ParticleSpacing_0;
	// System.Single UnityEngine.ParticleSystem/PlaybackState/Emission::m_ToEmitAccumulator
	float ___m_ToEmitAccumulator_1;
	// UnityEngine.ParticleSystem/PlaybackState/Seed UnityEngine.ParticleSystem/PlaybackState/Emission::m_Random
	Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  ___m_Random_2;

public:
	inline static int32_t get_offset_of_m_ParticleSpacing_0() { return static_cast<int32_t>(offsetof(Emission_tC1BDB748E761EC23ADC2610C67638B7336F0FDCC, ___m_ParticleSpacing_0)); }
	inline float get_m_ParticleSpacing_0() const { return ___m_ParticleSpacing_0; }
	inline float* get_address_of_m_ParticleSpacing_0() { return &___m_ParticleSpacing_0; }
	inline void set_m_ParticleSpacing_0(float value)
	{
		___m_ParticleSpacing_0 = value;
	}

	inline static int32_t get_offset_of_m_ToEmitAccumulator_1() { return static_cast<int32_t>(offsetof(Emission_tC1BDB748E761EC23ADC2610C67638B7336F0FDCC, ___m_ToEmitAccumulator_1)); }
	inline float get_m_ToEmitAccumulator_1() const { return ___m_ToEmitAccumulator_1; }
	inline float* get_address_of_m_ToEmitAccumulator_1() { return &___m_ToEmitAccumulator_1; }
	inline void set_m_ToEmitAccumulator_1(float value)
	{
		___m_ToEmitAccumulator_1 = value;
	}

	inline static int32_t get_offset_of_m_Random_2() { return static_cast<int32_t>(offsetof(Emission_tC1BDB748E761EC23ADC2610C67638B7336F0FDCC, ___m_Random_2)); }
	inline Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  get_m_Random_2() const { return ___m_Random_2; }
	inline Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949 * get_address_of_m_Random_2() { return &___m_Random_2; }
	inline void set_m_Random_2(Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  value)
	{
		___m_Random_2 = value;
	}
};


// UnityEngine.ParticleSystem/PlaybackState/Lights
struct Lights_tB301DBBA05EFE2536E58FB693C88B9924C6F91CE 
{
public:
	// UnityEngine.ParticleSystem/PlaybackState/Seed UnityEngine.ParticleSystem/PlaybackState/Lights::m_Random
	Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  ___m_Random_0;
	// System.Single UnityEngine.ParticleSystem/PlaybackState/Lights::m_ParticleEmissionCounter
	float ___m_ParticleEmissionCounter_1;

public:
	inline static int32_t get_offset_of_m_Random_0() { return static_cast<int32_t>(offsetof(Lights_tB301DBBA05EFE2536E58FB693C88B9924C6F91CE, ___m_Random_0)); }
	inline Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  get_m_Random_0() const { return ___m_Random_0; }
	inline Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949 * get_address_of_m_Random_0() { return &___m_Random_0; }
	inline void set_m_Random_0(Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  value)
	{
		___m_Random_0 = value;
	}

	inline static int32_t get_offset_of_m_ParticleEmissionCounter_1() { return static_cast<int32_t>(offsetof(Lights_tB301DBBA05EFE2536E58FB693C88B9924C6F91CE, ___m_ParticleEmissionCounter_1)); }
	inline float get_m_ParticleEmissionCounter_1() const { return ___m_ParticleEmissionCounter_1; }
	inline float* get_address_of_m_ParticleEmissionCounter_1() { return &___m_ParticleEmissionCounter_1; }
	inline void set_m_ParticleEmissionCounter_1(float value)
	{
		___m_ParticleEmissionCounter_1 = value;
	}
};


// UnityEngine.ParticleSystem/PlaybackState/Seed4
struct Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A 
{
public:
	// UnityEngine.ParticleSystem/PlaybackState/Seed UnityEngine.ParticleSystem/PlaybackState/Seed4::x
	Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  ___x_0;
	// UnityEngine.ParticleSystem/PlaybackState/Seed UnityEngine.ParticleSystem/PlaybackState/Seed4::y
	Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  ___y_1;
	// UnityEngine.ParticleSystem/PlaybackState/Seed UnityEngine.ParticleSystem/PlaybackState/Seed4::z
	Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  ___z_2;
	// UnityEngine.ParticleSystem/PlaybackState/Seed UnityEngine.ParticleSystem/PlaybackState/Seed4::w
	Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A, ___x_0)); }
	inline Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  get_x_0() const { return ___x_0; }
	inline Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949 * get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A, ___y_1)); }
	inline Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  get_y_1() const { return ___y_1; }
	inline Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949 * get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A, ___z_2)); }
	inline Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  get_z_2() const { return ___z_2; }
	inline Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949 * get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A, ___w_3)); }
	inline Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  get_w_3() const { return ___w_3; }
	inline Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949 * get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(Seed_t4E79E046B125E666C7E150C3A0B08DDE0338B949  value)
	{
		___w_3 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>
struct NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// Unity.Jobs.LowLevel.Unsafe.JobsUtility/JobScheduleParameters
struct JobScheduleParameters_tA29064729507A938ACB05D9F98B2C4A6977E3EF8 
{
public:
	// Unity.Jobs.JobHandle Unity.Jobs.LowLevel.Unsafe.JobsUtility/JobScheduleParameters::Dependency
	JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847  ___Dependency_0;
	// System.Int32 Unity.Jobs.LowLevel.Unsafe.JobsUtility/JobScheduleParameters::ScheduleMode
	int32_t ___ScheduleMode_1;
	// System.IntPtr Unity.Jobs.LowLevel.Unsafe.JobsUtility/JobScheduleParameters::ReflectionData
	intptr_t ___ReflectionData_2;
	// System.IntPtr Unity.Jobs.LowLevel.Unsafe.JobsUtility/JobScheduleParameters::JobDataPtr
	intptr_t ___JobDataPtr_3;

public:
	inline static int32_t get_offset_of_Dependency_0() { return static_cast<int32_t>(offsetof(JobScheduleParameters_tA29064729507A938ACB05D9F98B2C4A6977E3EF8, ___Dependency_0)); }
	inline JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847  get_Dependency_0() const { return ___Dependency_0; }
	inline JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847 * get_address_of_Dependency_0() { return &___Dependency_0; }
	inline void set_Dependency_0(JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847  value)
	{
		___Dependency_0 = value;
	}

	inline static int32_t get_offset_of_ScheduleMode_1() { return static_cast<int32_t>(offsetof(JobScheduleParameters_tA29064729507A938ACB05D9F98B2C4A6977E3EF8, ___ScheduleMode_1)); }
	inline int32_t get_ScheduleMode_1() const { return ___ScheduleMode_1; }
	inline int32_t* get_address_of_ScheduleMode_1() { return &___ScheduleMode_1; }
	inline void set_ScheduleMode_1(int32_t value)
	{
		___ScheduleMode_1 = value;
	}

	inline static int32_t get_offset_of_ReflectionData_2() { return static_cast<int32_t>(offsetof(JobScheduleParameters_tA29064729507A938ACB05D9F98B2C4A6977E3EF8, ___ReflectionData_2)); }
	inline intptr_t get_ReflectionData_2() const { return ___ReflectionData_2; }
	inline intptr_t* get_address_of_ReflectionData_2() { return &___ReflectionData_2; }
	inline void set_ReflectionData_2(intptr_t value)
	{
		___ReflectionData_2 = value;
	}

	inline static int32_t get_offset_of_JobDataPtr_3() { return static_cast<int32_t>(offsetof(JobScheduleParameters_tA29064729507A938ACB05D9F98B2C4A6977E3EF8, ___JobDataPtr_3)); }
	inline intptr_t get_JobDataPtr_3() const { return ___JobDataPtr_3; }
	inline intptr_t* get_address_of_JobDataPtr_3() { return &___JobDataPtr_3; }
	inline void set_JobDataPtr_3(intptr_t value)
	{
		___JobDataPtr_3 = value;
	}
};


// UnityEngine.ParticleSystem/EmitParams
struct EmitParams_t4F6429654653488A5D430701CD0743D011807CCC 
{
public:
	// UnityEngine.ParticleSystem/Particle UnityEngine.ParticleSystem/EmitParams::m_Particle
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  ___m_Particle_0;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_PositionSet
	bool ___m_PositionSet_1;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_VelocitySet
	bool ___m_VelocitySet_2;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_AxisOfRotationSet
	bool ___m_AxisOfRotationSet_3;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_RotationSet
	bool ___m_RotationSet_4;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_AngularVelocitySet
	bool ___m_AngularVelocitySet_5;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartSizeSet
	bool ___m_StartSizeSet_6;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartColorSet
	bool ___m_StartColorSet_7;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_RandomSeedSet
	bool ___m_RandomSeedSet_8;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_StartLifetimeSet
	bool ___m_StartLifetimeSet_9;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_MeshIndexSet
	bool ___m_MeshIndexSet_10;
	// System.Boolean UnityEngine.ParticleSystem/EmitParams::m_ApplyShapeToPosition
	bool ___m_ApplyShapeToPosition_11;

public:
	inline static int32_t get_offset_of_m_Particle_0() { return static_cast<int32_t>(offsetof(EmitParams_t4F6429654653488A5D430701CD0743D011807CCC, ___m_Particle_0)); }
	inline Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  get_m_Particle_0() const { return ___m_Particle_0; }
	inline Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * get_address_of_m_Particle_0() { return &___m_Particle_0; }
	inline void set_m_Particle_0(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  value)
	{
		___m_Particle_0 = value;
	}

	inline static int32_t get_offset_of_m_PositionSet_1() { return static_cast<int32_t>(offsetof(EmitParams_t4F6429654653488A5D430701CD0743D011807CCC, ___m_PositionSet_1)); }
	inline bool get_m_PositionSet_1() const { return ___m_PositionSet_1; }
	inline bool* get_address_of_m_PositionSet_1() { return &___m_PositionSet_1; }
	inline void set_m_PositionSet_1(bool value)
	{
		___m_PositionSet_1 = value;
	}

	inline static int32_t get_offset_of_m_VelocitySet_2() { return static_cast<int32_t>(offsetof(EmitParams_t4F6429654653488A5D430701CD0743D011807CCC, ___m_VelocitySet_2)); }
	inline bool get_m_VelocitySet_2() const { return ___m_VelocitySet_2; }
	inline bool* get_address_of_m_VelocitySet_2() { return &___m_VelocitySet_2; }
	inline void set_m_VelocitySet_2(bool value)
	{
		___m_VelocitySet_2 = value;
	}

	inline static int32_t get_offset_of_m_AxisOfRotationSet_3() { return static_cast<int32_t>(offsetof(EmitParams_t4F6429654653488A5D430701CD0743D011807CCC, ___m_AxisOfRotationSet_3)); }
	inline bool get_m_AxisOfRotationSet_3() const { return ___m_AxisOfRotationSet_3; }
	inline bool* get_address_of_m_AxisOfRotationSet_3() { return &___m_AxisOfRotationSet_3; }
	inline void set_m_AxisOfRotationSet_3(bool value)
	{
		___m_AxisOfRotationSet_3 = value;
	}

	inline static int32_t get_offset_of_m_RotationSet_4() { return static_cast<int32_t>(offsetof(EmitParams_t4F6429654653488A5D430701CD0743D011807CCC, ___m_RotationSet_4)); }
	inline bool get_m_RotationSet_4() const { return ___m_RotationSet_4; }
	inline bool* get_address_of_m_RotationSet_4() { return &___m_RotationSet_4; }
	inline void set_m_RotationSet_4(bool value)
	{
		___m_RotationSet_4 = value;
	}

	inline static int32_t get_offset_of_m_AngularVelocitySet_5() { return static_cast<int32_t>(offsetof(EmitParams_t4F6429654653488A5D430701CD0743D011807CCC, ___m_AngularVelocitySet_5)); }
	inline bool get_m_AngularVelocitySet_5() const { return ___m_AngularVelocitySet_5; }
	inline bool* get_address_of_m_AngularVelocitySet_5() { return &___m_AngularVelocitySet_5; }
	inline void set_m_AngularVelocitySet_5(bool value)
	{
		___m_AngularVelocitySet_5 = value;
	}

	inline static int32_t get_offset_of_m_StartSizeSet_6() { return static_cast<int32_t>(offsetof(EmitParams_t4F6429654653488A5D430701CD0743D011807CCC, ___m_StartSizeSet_6)); }
	inline bool get_m_StartSizeSet_6() const { return ___m_StartSizeSet_6; }
	inline bool* get_address_of_m_StartSizeSet_6() { return &___m_StartSizeSet_6; }
	inline void set_m_StartSizeSet_6(bool value)
	{
		___m_StartSizeSet_6 = value;
	}

	inline static int32_t get_offset_of_m_StartColorSet_7() { return static_cast<int32_t>(offsetof(EmitParams_t4F6429654653488A5D430701CD0743D011807CCC, ___m_StartColorSet_7)); }
	inline bool get_m_StartColorSet_7() const { return ___m_StartColorSet_7; }
	inline bool* get_address_of_m_StartColorSet_7() { return &___m_StartColorSet_7; }
	inline void set_m_StartColorSet_7(bool value)
	{
		___m_StartColorSet_7 = value;
	}

	inline static int32_t get_offset_of_m_RandomSeedSet_8() { return static_cast<int32_t>(offsetof(EmitParams_t4F6429654653488A5D430701CD0743D011807CCC, ___m_RandomSeedSet_8)); }
	inline bool get_m_RandomSeedSet_8() const { return ___m_RandomSeedSet_8; }
	inline bool* get_address_of_m_RandomSeedSet_8() { return &___m_RandomSeedSet_8; }
	inline void set_m_RandomSeedSet_8(bool value)
	{
		___m_RandomSeedSet_8 = value;
	}

	inline static int32_t get_offset_of_m_StartLifetimeSet_9() { return static_cast<int32_t>(offsetof(EmitParams_t4F6429654653488A5D430701CD0743D011807CCC, ___m_StartLifetimeSet_9)); }
	inline bool get_m_StartLifetimeSet_9() const { return ___m_StartLifetimeSet_9; }
	inline bool* get_address_of_m_StartLifetimeSet_9() { return &___m_StartLifetimeSet_9; }
	inline void set_m_StartLifetimeSet_9(bool value)
	{
		___m_StartLifetimeSet_9 = value;
	}

	inline static int32_t get_offset_of_m_MeshIndexSet_10() { return static_cast<int32_t>(offsetof(EmitParams_t4F6429654653488A5D430701CD0743D011807CCC, ___m_MeshIndexSet_10)); }
	inline bool get_m_MeshIndexSet_10() const { return ___m_MeshIndexSet_10; }
	inline bool* get_address_of_m_MeshIndexSet_10() { return &___m_MeshIndexSet_10; }
	inline void set_m_MeshIndexSet_10(bool value)
	{
		___m_MeshIndexSet_10 = value;
	}

	inline static int32_t get_offset_of_m_ApplyShapeToPosition_11() { return static_cast<int32_t>(offsetof(EmitParams_t4F6429654653488A5D430701CD0743D011807CCC, ___m_ApplyShapeToPosition_11)); }
	inline bool get_m_ApplyShapeToPosition_11() const { return ___m_ApplyShapeToPosition_11; }
	inline bool* get_address_of_m_ApplyShapeToPosition_11() { return &___m_ApplyShapeToPosition_11; }
	inline void set_m_ApplyShapeToPosition_11(bool value)
	{
		___m_ApplyShapeToPosition_11 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/EmitParams
struct EmitParams_t4F6429654653488A5D430701CD0743D011807CCC_marshaled_pinvoke
{
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  ___m_Particle_0;
	int32_t ___m_PositionSet_1;
	int32_t ___m_VelocitySet_2;
	int32_t ___m_AxisOfRotationSet_3;
	int32_t ___m_RotationSet_4;
	int32_t ___m_AngularVelocitySet_5;
	int32_t ___m_StartSizeSet_6;
	int32_t ___m_StartColorSet_7;
	int32_t ___m_RandomSeedSet_8;
	int32_t ___m_StartLifetimeSet_9;
	int32_t ___m_MeshIndexSet_10;
	int32_t ___m_ApplyShapeToPosition_11;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/EmitParams
struct EmitParams_t4F6429654653488A5D430701CD0743D011807CCC_marshaled_com
{
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  ___m_Particle_0;
	int32_t ___m_PositionSet_1;
	int32_t ___m_VelocitySet_2;
	int32_t ___m_AxisOfRotationSet_3;
	int32_t ___m_RotationSet_4;
	int32_t ___m_AngularVelocitySet_5;
	int32_t ___m_StartSizeSet_6;
	int32_t ___m_StartColorSet_7;
	int32_t ___m_RandomSeedSet_8;
	int32_t ___m_StartLifetimeSet_9;
	int32_t ___m_MeshIndexSet_10;
	int32_t ___m_ApplyShapeToPosition_11;
};

// UnityEngine.ParticleSystem/MinMaxCurve
struct MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD 
{
public:
	// UnityEngine.ParticleSystemCurveMode UnityEngine.ParticleSystem/MinMaxCurve::m_Mode
	int32_t ___m_Mode_0;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMultiplier
	float ___m_CurveMultiplier_1;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMin
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___m_CurveMin_2;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem/MinMaxCurve::m_CurveMax
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___m_CurveMax_3;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMin
	float ___m_ConstantMin_4;
	// System.Single UnityEngine.ParticleSystem/MinMaxCurve::m_ConstantMax
	float ___m_ConstantMax_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_CurveMultiplier_1() { return static_cast<int32_t>(offsetof(MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD, ___m_CurveMultiplier_1)); }
	inline float get_m_CurveMultiplier_1() const { return ___m_CurveMultiplier_1; }
	inline float* get_address_of_m_CurveMultiplier_1() { return &___m_CurveMultiplier_1; }
	inline void set_m_CurveMultiplier_1(float value)
	{
		___m_CurveMultiplier_1 = value;
	}

	inline static int32_t get_offset_of_m_CurveMin_2() { return static_cast<int32_t>(offsetof(MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD, ___m_CurveMin_2)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_m_CurveMin_2() const { return ___m_CurveMin_2; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_m_CurveMin_2() { return &___m_CurveMin_2; }
	inline void set_m_CurveMin_2(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___m_CurveMin_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurveMin_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurveMax_3() { return static_cast<int32_t>(offsetof(MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD, ___m_CurveMax_3)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_m_CurveMax_3() const { return ___m_CurveMax_3; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_m_CurveMax_3() { return &___m_CurveMax_3; }
	inline void set_m_CurveMax_3(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___m_CurveMax_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurveMax_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_ConstantMin_4() { return static_cast<int32_t>(offsetof(MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD, ___m_ConstantMin_4)); }
	inline float get_m_ConstantMin_4() const { return ___m_ConstantMin_4; }
	inline float* get_address_of_m_ConstantMin_4() { return &___m_ConstantMin_4; }
	inline void set_m_ConstantMin_4(float value)
	{
		___m_ConstantMin_4 = value;
	}

	inline static int32_t get_offset_of_m_ConstantMax_5() { return static_cast<int32_t>(offsetof(MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD, ___m_ConstantMax_5)); }
	inline float get_m_ConstantMax_5() const { return ___m_ConstantMax_5; }
	inline float* get_address_of_m_ConstantMax_5() { return &___m_ConstantMax_5; }
	inline void set_m_ConstantMax_5(float value)
	{
		___m_ConstantMax_5 = value;
	}
};


// UnityEngine.ParticleSystem/MinMaxGradient
struct MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 
{
public:
	// UnityEngine.ParticleSystemGradientMode UnityEngine.ParticleSystem/MinMaxGradient::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMin
	Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * ___m_GradientMin_1;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMax
	Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * ___m_GradientMax_2;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMin
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_ColorMin_3;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMax
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_ColorMax_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_GradientMin_1() { return static_cast<int32_t>(offsetof(MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7, ___m_GradientMin_1)); }
	inline Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * get_m_GradientMin_1() const { return ___m_GradientMin_1; }
	inline Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 ** get_address_of_m_GradientMin_1() { return &___m_GradientMin_1; }
	inline void set_m_GradientMin_1(Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * value)
	{
		___m_GradientMin_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GradientMin_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_GradientMax_2() { return static_cast<int32_t>(offsetof(MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7, ___m_GradientMax_2)); }
	inline Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * get_m_GradientMax_2() const { return ___m_GradientMax_2; }
	inline Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 ** get_address_of_m_GradientMax_2() { return &___m_GradientMax_2; }
	inline void set_m_GradientMax_2(Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * value)
	{
		___m_GradientMax_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GradientMax_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorMin_3() { return static_cast<int32_t>(offsetof(MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7, ___m_ColorMin_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_ColorMin_3() const { return ___m_ColorMin_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_ColorMin_3() { return &___m_ColorMin_3; }
	inline void set_m_ColorMin_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_ColorMin_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMax_4() { return static_cast<int32_t>(offsetof(MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7, ___m_ColorMax_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_ColorMax_4() const { return ___m_ColorMax_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_ColorMax_4() { return &___m_ColorMax_4; }
	inline void set_m_ColorMax_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_ColorMax_4 = value;
	}
};


// UnityEngine.ParticleSystem/PlaybackState/Collision
struct Collision_tE21C1E427BE7AAD867521023A7C9AB49CB967163 
{
public:
	// UnityEngine.ParticleSystem/PlaybackState/Seed4 UnityEngine.ParticleSystem/PlaybackState/Collision::m_Random
	Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A  ___m_Random_0;

public:
	inline static int32_t get_offset_of_m_Random_0() { return static_cast<int32_t>(offsetof(Collision_tE21C1E427BE7AAD867521023A7C9AB49CB967163, ___m_Random_0)); }
	inline Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A  get_m_Random_0() const { return ___m_Random_0; }
	inline Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A * get_address_of_m_Random_0() { return &___m_Random_0; }
	inline void set_m_Random_0(Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A  value)
	{
		___m_Random_0 = value;
	}
};


// UnityEngine.ParticleSystem/PlaybackState/Force
struct Force_t1677BF777F3B926125CBB1D71E97B31F02D7E12E 
{
public:
	// UnityEngine.ParticleSystem/PlaybackState/Seed4 UnityEngine.ParticleSystem/PlaybackState/Force::m_Random
	Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A  ___m_Random_0;

public:
	inline static int32_t get_offset_of_m_Random_0() { return static_cast<int32_t>(offsetof(Force_t1677BF777F3B926125CBB1D71E97B31F02D7E12E, ___m_Random_0)); }
	inline Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A  get_m_Random_0() const { return ___m_Random_0; }
	inline Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A * get_address_of_m_Random_0() { return &___m_Random_0; }
	inline void set_m_Random_0(Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A  value)
	{
		___m_Random_0 = value;
	}
};


// UnityEngine.ParticleSystem/PlaybackState/Initial
struct Initial_t3D5093029F4DEE656AAFF89308B480BF0782A15B 
{
public:
	// UnityEngine.ParticleSystem/PlaybackState/Seed4 UnityEngine.ParticleSystem/PlaybackState/Initial::m_Random
	Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A  ___m_Random_0;

public:
	inline static int32_t get_offset_of_m_Random_0() { return static_cast<int32_t>(offsetof(Initial_t3D5093029F4DEE656AAFF89308B480BF0782A15B, ___m_Random_0)); }
	inline Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A  get_m_Random_0() const { return ___m_Random_0; }
	inline Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A * get_address_of_m_Random_0() { return &___m_Random_0; }
	inline void set_m_Random_0(Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A  value)
	{
		___m_Random_0 = value;
	}
};


// UnityEngine.ParticleSystem/PlaybackState/Shape
struct Shape_tC8009C4789BC8B081E0368159AB94F03FDDDD3A5 
{
public:
	// UnityEngine.ParticleSystem/PlaybackState/Seed4 UnityEngine.ParticleSystem/PlaybackState/Shape::m_Random
	Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A  ___m_Random_0;
	// System.Single UnityEngine.ParticleSystem/PlaybackState/Shape::m_RadiusTimer
	float ___m_RadiusTimer_1;
	// System.Single UnityEngine.ParticleSystem/PlaybackState/Shape::m_RadiusTimerPrev
	float ___m_RadiusTimerPrev_2;
	// System.Single UnityEngine.ParticleSystem/PlaybackState/Shape::m_ArcTimer
	float ___m_ArcTimer_3;
	// System.Single UnityEngine.ParticleSystem/PlaybackState/Shape::m_ArcTimerPrev
	float ___m_ArcTimerPrev_4;
	// System.Single UnityEngine.ParticleSystem/PlaybackState/Shape::m_MeshSpawnTimer
	float ___m_MeshSpawnTimer_5;
	// System.Single UnityEngine.ParticleSystem/PlaybackState/Shape::m_MeshSpawnTimerPrev
	float ___m_MeshSpawnTimerPrev_6;
	// System.Int32 UnityEngine.ParticleSystem/PlaybackState/Shape::m_OrderedMeshVertexIndex
	int32_t ___m_OrderedMeshVertexIndex_7;

public:
	inline static int32_t get_offset_of_m_Random_0() { return static_cast<int32_t>(offsetof(Shape_tC8009C4789BC8B081E0368159AB94F03FDDDD3A5, ___m_Random_0)); }
	inline Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A  get_m_Random_0() const { return ___m_Random_0; }
	inline Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A * get_address_of_m_Random_0() { return &___m_Random_0; }
	inline void set_m_Random_0(Seed4_t5412E4443D1C6EE04020A80A48CFA8A247AE519A  value)
	{
		___m_Random_0 = value;
	}

	inline static int32_t get_offset_of_m_RadiusTimer_1() { return static_cast<int32_t>(offsetof(Shape_tC8009C4789BC8B081E0368159AB94F03FDDDD3A5, ___m_RadiusTimer_1)); }
	inline float get_m_RadiusTimer_1() const { return ___m_RadiusTimer_1; }
	inline float* get_address_of_m_RadiusTimer_1() { return &___m_RadiusTimer_1; }
	inline void set_m_RadiusTimer_1(float value)
	{
		___m_RadiusTimer_1 = value;
	}

	inline static int32_t get_offset_of_m_RadiusTimerPrev_2() { return static_cast<int32_t>(offsetof(Shape_tC8009C4789BC8B081E0368159AB94F03FDDDD3A5, ___m_RadiusTimerPrev_2)); }
	inline float get_m_RadiusTimerPrev_2() const { return ___m_RadiusTimerPrev_2; }
	inline float* get_address_of_m_RadiusTimerPrev_2() { return &___m_RadiusTimerPrev_2; }
	inline void set_m_RadiusTimerPrev_2(float value)
	{
		___m_RadiusTimerPrev_2 = value;
	}

	inline static int32_t get_offset_of_m_ArcTimer_3() { return static_cast<int32_t>(offsetof(Shape_tC8009C4789BC8B081E0368159AB94F03FDDDD3A5, ___m_ArcTimer_3)); }
	inline float get_m_ArcTimer_3() const { return ___m_ArcTimer_3; }
	inline float* get_address_of_m_ArcTimer_3() { return &___m_ArcTimer_3; }
	inline void set_m_ArcTimer_3(float value)
	{
		___m_ArcTimer_3 = value;
	}

	inline static int32_t get_offset_of_m_ArcTimerPrev_4() { return static_cast<int32_t>(offsetof(Shape_tC8009C4789BC8B081E0368159AB94F03FDDDD3A5, ___m_ArcTimerPrev_4)); }
	inline float get_m_ArcTimerPrev_4() const { return ___m_ArcTimerPrev_4; }
	inline float* get_address_of_m_ArcTimerPrev_4() { return &___m_ArcTimerPrev_4; }
	inline void set_m_ArcTimerPrev_4(float value)
	{
		___m_ArcTimerPrev_4 = value;
	}

	inline static int32_t get_offset_of_m_MeshSpawnTimer_5() { return static_cast<int32_t>(offsetof(Shape_tC8009C4789BC8B081E0368159AB94F03FDDDD3A5, ___m_MeshSpawnTimer_5)); }
	inline float get_m_MeshSpawnTimer_5() const { return ___m_MeshSpawnTimer_5; }
	inline float* get_address_of_m_MeshSpawnTimer_5() { return &___m_MeshSpawnTimer_5; }
	inline void set_m_MeshSpawnTimer_5(float value)
	{
		___m_MeshSpawnTimer_5 = value;
	}

	inline static int32_t get_offset_of_m_MeshSpawnTimerPrev_6() { return static_cast<int32_t>(offsetof(Shape_tC8009C4789BC8B081E0368159AB94F03FDDDD3A5, ___m_MeshSpawnTimerPrev_6)); }
	inline float get_m_MeshSpawnTimerPrev_6() const { return ___m_MeshSpawnTimerPrev_6; }
	inline float* get_address_of_m_MeshSpawnTimerPrev_6() { return &___m_MeshSpawnTimerPrev_6; }
	inline void set_m_MeshSpawnTimerPrev_6(float value)
	{
		___m_MeshSpawnTimerPrev_6 = value;
	}

	inline static int32_t get_offset_of_m_OrderedMeshVertexIndex_7() { return static_cast<int32_t>(offsetof(Shape_tC8009C4789BC8B081E0368159AB94F03FDDDD3A5, ___m_OrderedMeshVertexIndex_7)); }
	inline int32_t get_m_OrderedMeshVertexIndex_7() const { return ___m_OrderedMeshVertexIndex_7; }
	inline int32_t* get_address_of_m_OrderedMeshVertexIndex_7() { return &___m_OrderedMeshVertexIndex_7; }
	inline void set_m_OrderedMeshVertexIndex_7(int32_t value)
	{
		___m_OrderedMeshVertexIndex_7 = value;
	}
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.ParticleSystem
struct ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.ParticleSystem/PlaybackState
struct PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F 
{
public:
	// System.Single UnityEngine.ParticleSystem/PlaybackState::m_AccumulatedDt
	float ___m_AccumulatedDt_0;
	// System.Single UnityEngine.ParticleSystem/PlaybackState::m_StartDelay
	float ___m_StartDelay_1;
	// System.Single UnityEngine.ParticleSystem/PlaybackState::m_PlaybackTime
	float ___m_PlaybackTime_2;
	// System.Int32 UnityEngine.ParticleSystem/PlaybackState::m_RingBufferIndex
	int32_t ___m_RingBufferIndex_3;
	// UnityEngine.ParticleSystem/PlaybackState/Emission UnityEngine.ParticleSystem/PlaybackState::m_Emission
	Emission_tC1BDB748E761EC23ADC2610C67638B7336F0FDCC  ___m_Emission_4;
	// UnityEngine.ParticleSystem/PlaybackState/Initial UnityEngine.ParticleSystem/PlaybackState::m_Initial
	Initial_t3D5093029F4DEE656AAFF89308B480BF0782A15B  ___m_Initial_5;
	// UnityEngine.ParticleSystem/PlaybackState/Shape UnityEngine.ParticleSystem/PlaybackState::m_Shape
	Shape_tC8009C4789BC8B081E0368159AB94F03FDDDD3A5  ___m_Shape_6;
	// UnityEngine.ParticleSystem/PlaybackState/Force UnityEngine.ParticleSystem/PlaybackState::m_Force
	Force_t1677BF777F3B926125CBB1D71E97B31F02D7E12E  ___m_Force_7;
	// UnityEngine.ParticleSystem/PlaybackState/Collision UnityEngine.ParticleSystem/PlaybackState::m_Collision
	Collision_tE21C1E427BE7AAD867521023A7C9AB49CB967163  ___m_Collision_8;
	// UnityEngine.ParticleSystem/PlaybackState/Noise UnityEngine.ParticleSystem/PlaybackState::m_Noise
	Noise_tAE62462FCFB05E417C3A8A8BE90183952CEAF93B  ___m_Noise_9;
	// UnityEngine.ParticleSystem/PlaybackState/Lights UnityEngine.ParticleSystem/PlaybackState::m_Lights
	Lights_tB301DBBA05EFE2536E58FB693C88B9924C6F91CE  ___m_Lights_10;
	// UnityEngine.ParticleSystem/PlaybackState/Trail UnityEngine.ParticleSystem/PlaybackState::m_Trail
	Trail_t82FA0A8F8A1B2507F5CCDE83B9B520BAD2F713B5  ___m_Trail_11;

public:
	inline static int32_t get_offset_of_m_AccumulatedDt_0() { return static_cast<int32_t>(offsetof(PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F, ___m_AccumulatedDt_0)); }
	inline float get_m_AccumulatedDt_0() const { return ___m_AccumulatedDt_0; }
	inline float* get_address_of_m_AccumulatedDt_0() { return &___m_AccumulatedDt_0; }
	inline void set_m_AccumulatedDt_0(float value)
	{
		___m_AccumulatedDt_0 = value;
	}

	inline static int32_t get_offset_of_m_StartDelay_1() { return static_cast<int32_t>(offsetof(PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F, ___m_StartDelay_1)); }
	inline float get_m_StartDelay_1() const { return ___m_StartDelay_1; }
	inline float* get_address_of_m_StartDelay_1() { return &___m_StartDelay_1; }
	inline void set_m_StartDelay_1(float value)
	{
		___m_StartDelay_1 = value;
	}

	inline static int32_t get_offset_of_m_PlaybackTime_2() { return static_cast<int32_t>(offsetof(PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F, ___m_PlaybackTime_2)); }
	inline float get_m_PlaybackTime_2() const { return ___m_PlaybackTime_2; }
	inline float* get_address_of_m_PlaybackTime_2() { return &___m_PlaybackTime_2; }
	inline void set_m_PlaybackTime_2(float value)
	{
		___m_PlaybackTime_2 = value;
	}

	inline static int32_t get_offset_of_m_RingBufferIndex_3() { return static_cast<int32_t>(offsetof(PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F, ___m_RingBufferIndex_3)); }
	inline int32_t get_m_RingBufferIndex_3() const { return ___m_RingBufferIndex_3; }
	inline int32_t* get_address_of_m_RingBufferIndex_3() { return &___m_RingBufferIndex_3; }
	inline void set_m_RingBufferIndex_3(int32_t value)
	{
		___m_RingBufferIndex_3 = value;
	}

	inline static int32_t get_offset_of_m_Emission_4() { return static_cast<int32_t>(offsetof(PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F, ___m_Emission_4)); }
	inline Emission_tC1BDB748E761EC23ADC2610C67638B7336F0FDCC  get_m_Emission_4() const { return ___m_Emission_4; }
	inline Emission_tC1BDB748E761EC23ADC2610C67638B7336F0FDCC * get_address_of_m_Emission_4() { return &___m_Emission_4; }
	inline void set_m_Emission_4(Emission_tC1BDB748E761EC23ADC2610C67638B7336F0FDCC  value)
	{
		___m_Emission_4 = value;
	}

	inline static int32_t get_offset_of_m_Initial_5() { return static_cast<int32_t>(offsetof(PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F, ___m_Initial_5)); }
	inline Initial_t3D5093029F4DEE656AAFF89308B480BF0782A15B  get_m_Initial_5() const { return ___m_Initial_5; }
	inline Initial_t3D5093029F4DEE656AAFF89308B480BF0782A15B * get_address_of_m_Initial_5() { return &___m_Initial_5; }
	inline void set_m_Initial_5(Initial_t3D5093029F4DEE656AAFF89308B480BF0782A15B  value)
	{
		___m_Initial_5 = value;
	}

	inline static int32_t get_offset_of_m_Shape_6() { return static_cast<int32_t>(offsetof(PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F, ___m_Shape_6)); }
	inline Shape_tC8009C4789BC8B081E0368159AB94F03FDDDD3A5  get_m_Shape_6() const { return ___m_Shape_6; }
	inline Shape_tC8009C4789BC8B081E0368159AB94F03FDDDD3A5 * get_address_of_m_Shape_6() { return &___m_Shape_6; }
	inline void set_m_Shape_6(Shape_tC8009C4789BC8B081E0368159AB94F03FDDDD3A5  value)
	{
		___m_Shape_6 = value;
	}

	inline static int32_t get_offset_of_m_Force_7() { return static_cast<int32_t>(offsetof(PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F, ___m_Force_7)); }
	inline Force_t1677BF777F3B926125CBB1D71E97B31F02D7E12E  get_m_Force_7() const { return ___m_Force_7; }
	inline Force_t1677BF777F3B926125CBB1D71E97B31F02D7E12E * get_address_of_m_Force_7() { return &___m_Force_7; }
	inline void set_m_Force_7(Force_t1677BF777F3B926125CBB1D71E97B31F02D7E12E  value)
	{
		___m_Force_7 = value;
	}

	inline static int32_t get_offset_of_m_Collision_8() { return static_cast<int32_t>(offsetof(PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F, ___m_Collision_8)); }
	inline Collision_tE21C1E427BE7AAD867521023A7C9AB49CB967163  get_m_Collision_8() const { return ___m_Collision_8; }
	inline Collision_tE21C1E427BE7AAD867521023A7C9AB49CB967163 * get_address_of_m_Collision_8() { return &___m_Collision_8; }
	inline void set_m_Collision_8(Collision_tE21C1E427BE7AAD867521023A7C9AB49CB967163  value)
	{
		___m_Collision_8 = value;
	}

	inline static int32_t get_offset_of_m_Noise_9() { return static_cast<int32_t>(offsetof(PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F, ___m_Noise_9)); }
	inline Noise_tAE62462FCFB05E417C3A8A8BE90183952CEAF93B  get_m_Noise_9() const { return ___m_Noise_9; }
	inline Noise_tAE62462FCFB05E417C3A8A8BE90183952CEAF93B * get_address_of_m_Noise_9() { return &___m_Noise_9; }
	inline void set_m_Noise_9(Noise_tAE62462FCFB05E417C3A8A8BE90183952CEAF93B  value)
	{
		___m_Noise_9 = value;
	}

	inline static int32_t get_offset_of_m_Lights_10() { return static_cast<int32_t>(offsetof(PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F, ___m_Lights_10)); }
	inline Lights_tB301DBBA05EFE2536E58FB693C88B9924C6F91CE  get_m_Lights_10() const { return ___m_Lights_10; }
	inline Lights_tB301DBBA05EFE2536E58FB693C88B9924C6F91CE * get_address_of_m_Lights_10() { return &___m_Lights_10; }
	inline void set_m_Lights_10(Lights_tB301DBBA05EFE2536E58FB693C88B9924C6F91CE  value)
	{
		___m_Lights_10 = value;
	}

	inline static int32_t get_offset_of_m_Trail_11() { return static_cast<int32_t>(offsetof(PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F, ___m_Trail_11)); }
	inline Trail_t82FA0A8F8A1B2507F5CCDE83B9B520BAD2F713B5  get_m_Trail_11() const { return ___m_Trail_11; }
	inline Trail_t82FA0A8F8A1B2507F5CCDE83B9B520BAD2F713B5 * get_address_of_m_Trail_11() { return &___m_Trail_11; }
	inline void set_m_Trail_11(Trail_t82FA0A8F8A1B2507F5CCDE83B9B520BAD2F713B5  value)
	{
		___m_Trail_11 = value;
	}
};


// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.ParticleSystemRenderer
struct ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  m_Items[1];

public:
	inline Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Mesh[]
struct MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * m_Items[1];

public:
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void* Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::GetUnsafeReadOnlyPtr<UnityEngine.ParticleSystem/Particle>(Unity.Collections.NativeArray`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void* NativeArrayUnsafeUtility_GetUnsafeReadOnlyPtr_TisParticle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1_m6DB36AF31E5460B987A006F70D2DD82D2FB350C1_gshared (NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  ___nativeArray0, const RuntimeMethod* method);
// System.Void* Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::GetUnsafePtr<UnityEngine.ParticleSystem/Particle>(Unity.Collections.NativeArray`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void* NativeArrayUnsafeUtility_GetUnsafePtr_TisParticle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1_mB8C1C175F09CE57B87C863A2827A9F50D1DCEEB7_gshared (NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  ___nativeArray0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m8C48C3F6EF447031F54430F3EF5AEB57666345E6_gshared (List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32Enum>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mA4B6A2C2E6A818890DB03FBE417C0FEB7E1E4451_gshared (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * __this, int32_t ___capacity0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Int32Enum>::Contains(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Contains_m793EB605F2702C16DE665C690BFA9B9675529D94_gshared (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * __this, int32_t ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32Enum>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mBA0FDF41792A78B3EB9E395D711706E268313F0F_gshared (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * __this, int32_t ___item0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Int32Enum>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m33506664531C256A7C5EE365E9E994AF9289F43A_gshared (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * __this, int32_t ___item0, const RuntimeMethod* method);

// System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_position_mB749E41CD3C4C6DF501C0D68B6211CF7E2217FC2 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_velocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_velocity_m4894BD6A75E7A1FCD433927F93396AA29A59709B (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_lifetime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_lifetime_mCE97F9D17D1E660DB9D9F3244150CB6624A74DC3 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_startLifetime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_startLifetime_m7AC09262BBFE8818EA6B61BDB62264184CD27A82 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_startSize_m7CDEE5B620B3D26B4CC5C1DA7C6E24ACCCF64466 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, float ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_rotation3D(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_rotation3D_m0F402760524A81307FA4940751751C44DF2F77D0 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_angularVelocity3D(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_angularVelocity3D_mE9A9544DF33CD0CCF4F1CA14994A2C436E4DF1F8 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_startColor_mA7B0363E82B7A7CBB1C83F8C5D49FB7B7AF75554 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_randomSeed(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_randomSeed_m8FD7A4DB7F8E7EBDEF2C51A28197F8D9D7CB6E04 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, uint32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::EmitOld_Internal(UnityEngine.ParticleSystem/Particle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_EmitOld_Internal_m4F094DC523986298D5626F0F3F2335DFF596C993 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * ___particle0, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startDelayMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startDelayMultiplier_mC38890C8853463BB25F031BB8884F3D8B0AF581E (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startDelayMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startDelayMultiplier_m23FC7BAFE2DCF940C398D6BE53606E23E1715464 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.ParticleSystem/MainModule::get_loop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MainModule_get_loop_mE45A1A4A1DF6E2590B7B13A73A9A95197DA346F8 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_loop(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_loop_m9F43A092EFABBE92662DF9EDA73AF8D9C72DABB2 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.ParticleSystem/MainModule::get_playOnAwake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MainModule_get_playOnAwake_m6E07F3ACCC2797500E70AF4A4A33C5C8425A52D5 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_playOnAwake(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_playOnAwake_m85E164C05D5C39B0B3C6139BE360BAB41B0D7FCF (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, bool ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_duration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_duration_mD516595EFDD66C83A727BDD7EF495069B4EB6132 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_simulationSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_simulationSpeed_m7BCFDA4BD65598CE9CEF34A029E4C03DAB4A3C53 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_simulationSpeed_m9EA9E4BE76B372EF9674568810BF4F5B403B2833 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  ParticleSystem_get_emission_m0BAA1EDF775A8712DA7D62BF3C42A2B1A6C96CA1 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.ParticleSystem/EmissionModule::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EmissionModule_get_enabled_m9DB0C4A45B5D04CD88355536DD3E1DD1180DF107 (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmissionModule_set_enabled_mD9FD50C9F5EECD04B22D03E5C00C7DD64D2AC1F1 (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * __this, bool ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/EmissionModule::get_rateOverTimeMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float EmissionModule_get_rateOverTimeMultiplier_mF8F50D01C0F4ADB44423852CD75A334179456FBA (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * __this, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MinMaxCurve::op_Implicit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD  MinMaxCurve_op_Implicit_m8D746D40E6CCBF5E8C4CE39F23A45712BFC372F5 (float ___constant0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime(UnityEngine.ParticleSystem/MinMaxCurve)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmissionModule_set_rateOverTime_m4EE4643D8E8B8BEC37BCB16DB76C9B8E4E4C8F48 (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * __this, MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startSpeedMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startSpeedMultiplier_mE165D2B967AD8A8827D07DBD694F1C7167C80336 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeedMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startSpeedMultiplier_mA3C6B512351C3BFA8E64B40936B3353845A637F2 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startSizeMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startSizeMultiplier_m820351EE78DF731C1AFA8309ECB5ADF3E1166279 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSizeMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startSizeMultiplier_m231F400517E7F96EFF7C3C31E59B7B8DD0A1DB64 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MainModule::get_startColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  MainModule_get_startColor_mC8BAD25B55ACF9586BAFEECF047CCE5268123DD4 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::get_color()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  MinMaxGradient_get_color_m8FA2F2F1E0B9C9E1DFE40CEA325BC5E863C59C90 (MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 * __this, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MinMaxGradient::op_Implicit(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  MinMaxGradient_op_Implicit_m5F938B5644AF6BE5DF525839E922B88456B614FA (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startColor(UnityEngine.ParticleSystem/MinMaxGradient)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationMultiplier_m72C5648E64889B162553000EF3B7347F47B1F84A (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationMultiplier_mB7085FB7FF45BDD4C71E08226C6D5F5635CB47EE (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationXMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationXMultiplier_m15710A32A7861DAF1A0CD55DB1835EC22617F987 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationYMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationYMultiplier_mC9C0B9123DDF9D37EF3E5291164E16AB95308011 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationZMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationZMultiplier_m736451211917A59B78A3B444963D305BD377898D (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationXMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationXMultiplier_mA489325EDA3D644481BFF44376627B0B6EFB81CD (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationYMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationYMultiplier_m45DC00CC3E94A7CFCEA522F105F93BCE1B727639 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationZMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationZMultiplier_m9D604421CED73B2B110158F1FAF955832FB9A6D5 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startLifetimeMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startLifetimeMultiplier_m8D2F788F9A1F5768E64F4511F2158E3BFAEB474D (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startLifetimeMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startLifetimeMultiplier_m219938FDA9324C185D998815836B50FEE3AC0D8D (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_gravityModifierMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_gravityModifierMultiplier_m875B65CAEE457EB12A5CD763A5AA311DE014629A (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_gravityModifierMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_gravityModifierMultiplier_mBC64B6D0DEF548A94D0C0303855F43931A1F5684 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.ParticleSystem/MainModule::get_maxParticles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MainModule_get_maxParticles_m4A95D2105C1B414331F15EF86ADBC23CC6C8DD35 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_maxParticles(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_maxParticles_m6FD14B44787B44D38DEDAE4FCBE9596A86A0460D (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, int32_t ___value0, const RuntimeMethod* method);
// UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem/MainModule::get_simulationSpace()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MainModule_get_simulationSpace_mD08447602DF2E2AC9790D900A1BD04AB8D4FD0A2 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpace(UnityEngine.ParticleSystemSimulationSpace)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_simulationSpace_m59583EC5A1D9D64EC8C0CCE275CE6669FC8E268F (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, int32_t ___value0, const RuntimeMethod* method);
// UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem/MainModule::get_scalingMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MainModule_get_scalingMode_m2FEDBC7969A4BD0071B9467AFFD1B98EE64A9C51 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_scalingMode(UnityEngine.ParticleSystemScalingMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_scalingMode_m0992C03429ABFBD1021009BF669BF362705D4E23 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.ParticleSystem::get_proceduralSimulationSupported()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_get_proceduralSimulationSupported_mEF4AEBA182D7DD12ABB6414971A2365236A80A50 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::GetParticleCurrentSize3D_Injected(UnityEngine.ParticleSystem/Particle&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_GetParticleCurrentSize3D_Injected_mE1D7D2457CDC32E35F9D3E06721D773667F2A966 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * ___particle0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___ret1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::GetParticleCurrentColor_Injected(UnityEngine.ParticleSystem/Particle&,UnityEngine.Color32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_GetParticleCurrentColor_Injected_m98C468104D268559CF2F06F78F970F1814F7A587 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * ___particle0, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * ___ret1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetParticles_m79867147E65742BE1E1ADF0222C090BD6AF1E20A (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* ___particles0, int32_t ___size1, int32_t ___offset2, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetParticles_m0658B777D1C6DDA7D244607AC55D5225774CEBFA (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* ___particles0, int32_t ___size1, const RuntimeMethod* method);
// System.Void* Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::GetUnsafeReadOnlyPtr<UnityEngine.ParticleSystem/Particle>(Unity.Collections.NativeArray`1<!!0>)
inline void* NativeArrayUnsafeUtility_GetUnsafeReadOnlyPtr_TisParticle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1_m6DB36AF31E5460B987A006F70D2DD82D2FB350C1 (NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  ___nativeArray0, const RuntimeMethod* method)
{
	return ((  void* (*) (NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0 , const RuntimeMethod*))NativeArrayUnsafeUtility_GetUnsafeReadOnlyPtr_TisParticle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1_m6DB36AF31E5460B987A006F70D2DD82D2FB350C1_gshared)(___nativeArray0, method);
}
// System.IntPtr System.IntPtr::op_Explicit(System.Void*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t IntPtr_op_Explicit_mBD40223EE90BDDF40A24C0F321D3398DEA300495 (void* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::SetParticlesWithNativeArray(System.IntPtr,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetParticlesWithNativeArray_m163F32606AC234C018E081A3522CCF57C8A2A4FC (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, intptr_t ___particles0, int32_t ___particlesLength1, int32_t ___size2, int32_t ___offset3, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::SetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetParticles_m380593909B2E9DBB77D8DE09E47C1D0DCCD6ACC2 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  ___particles0, int32_t ___size1, int32_t ___offset2, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::SetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetParticles_m648CE8AF7118C73FF5E1D0419E7F6BA52478C13E (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  ___particles0, int32_t ___size1, const RuntimeMethod* method);
// System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetParticles_mC310C32157EA281ECDC320B63B1E43ED5F6292B3 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* ___particles0, int32_t ___size1, int32_t ___offset2, const RuntimeMethod* method);
// System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetParticles_mF6FC6048609DD24AF7F1B8890C44AEC480BDFDEA (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* ___particles0, int32_t ___size1, const RuntimeMethod* method);
// System.Void* Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::GetUnsafePtr<UnityEngine.ParticleSystem/Particle>(Unity.Collections.NativeArray`1<!!0>)
inline void* NativeArrayUnsafeUtility_GetUnsafePtr_TisParticle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1_mB8C1C175F09CE57B87C863A2827A9F50D1DCEEB7 (NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  ___nativeArray0, const RuntimeMethod* method)
{
	return ((  void* (*) (NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0 , const RuntimeMethod*))NativeArrayUnsafeUtility_GetUnsafePtr_TisParticle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1_mB8C1C175F09CE57B87C863A2827A9F50D1DCEEB7_gshared)(___nativeArray0, method);
}
// System.Int32 UnityEngine.ParticleSystem::GetParticlesWithNativeArray(System.IntPtr,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetParticlesWithNativeArray_m4303397CB3084BE1158D854929FE40CA0A6E5253 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, intptr_t ___particles0, int32_t ___particlesLength1, int32_t ___size2, int32_t ___offset3, const RuntimeMethod* method);
// System.Int32 UnityEngine.ParticleSystem::GetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetParticles_mCD391A836273B8B41035A9F06D5D7ABA83968A95 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  ___particles0, int32_t ___size1, int32_t ___offset2, const RuntimeMethod* method);
// System.Int32 UnityEngine.ParticleSystem::GetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetParticles_m870578C1C1E37DFF7BC112662BAE81F3CA7AA411 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  ___particles0, int32_t ___size1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::GetPlaybackState_Injected(UnityEngine.ParticleSystem/PlaybackState&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_GetPlaybackState_Injected_m6D3692AF2279B0B9C9FDFF01A9B0D4B21AD19C4D (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::SetPlaybackState_Injected(UnityEngine.ParticleSystem/PlaybackState&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetPlaybackState_Injected_m8555985301715DFAAE85761592EEFE881B32EBFC (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F * ___playbackState0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor()
inline void List_1__ctor_m8C48C3F6EF447031F54430F3EF5AEB57666345E6 (List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *, const RuntimeMethod*))List_1__ctor_m8C48C3F6EF447031F54430F3EF5AEB57666345E6_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
inline void List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_gshared)(__this, method);
}
// System.Void UnityEngine.ParticleSystem::GetTrailDataInternal(UnityEngine.ParticleSystem/Trails&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_GetTrailDataInternal_m03273FD85CB69A3D816E91E7D39AAAF58F5BCF78 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Trails_tE9352AAF21D22D5007179B507019C44C823013F2 * ___trailData0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::SetTrails_Injected(UnityEngine.ParticleSystem/Trails&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetTrails_Injected_m2B3F93DBAD65DB5A22DC136395BA9D329106BC92 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Trails_tE9352AAF21D22D5007179B507019C44C823013F2 * ___trailData0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Simulate_m68A0105281A8BC3F4E9242181586483BA7B796A3 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___t0, bool ___withChildren1, bool ___restart2, bool ___fixedTimeStep3, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Simulate_mC2F2E060D7CE94D4936BA995C49827231DF5F1F8 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___t0, bool ___withChildren1, bool ___restart2, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Simulate_m36BAFFACB58953B2A46AA0B86E1F59C0FD6CD9C7 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___t0, bool ___withChildren1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Play_m97D70BAF373265E633ACD91833E56D981B19958A (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___withChildren0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Pause(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Pause_m116581497C92159D3210C733917CD2515399A0CD (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___withChildren0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Stop_m275B200BC21580C60987EC8FC8B2DD0FEB365C1A (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___withChildren0, int32_t ___stopBehavior1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Stop_m5EC8B81A8BD1F8C90729E766789EE5D8D4EC64FB (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___withChildren0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Clear(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Clear_m9E873134B8055ACFEE9DC66F7E4FABE178E27057 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___withChildren0, const RuntimeMethod* method);
// System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_IsAlive_m43E2E84732EBDF5C4C9FD6575051A2A2B135BCF9 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___withChildren0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Emit_Internal(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Emit_Internal_m7C72C31F7F4875B54B00E255A450B045A4449646 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___count0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Emit_Injected(UnityEngine.ParticleSystem/EmitParams&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Emit_Injected_mC31E36D577A2D3135436438BFC27B6C76D9ADC72 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, EmitParams_t4F6429654653488A5D430701CD0743D011807CCC * ___emitParams0, int32_t ___count1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::TriggerSubEmitter(System.Int32,System.Collections.Generic.List`1<UnityEngine.ParticleSystem/Particle>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_TriggerSubEmitter_mBEB9C15312BB6AD76AF9B42B2CE184B7149F4C98 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___subEmitterIndex0, List_1_tA1D5161C2AD941530756A2128C933F0D76F48E95 * ___particles1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::TriggerSubEmitterForParticle(System.Int32,UnityEngine.ParticleSystem/Particle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_TriggerSubEmitterForParticle_mCC9F7D09CA9D8DB5576389ABCFC287A21952003A (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___subEmitterIndex0, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  ___particle1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::TriggerSubEmitterForParticle_Injected(System.Int32,UnityEngine.ParticleSystem/Particle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_TriggerSubEmitterForParticle_Injected_mB9BC0EBDC5CC0D5C392B7287351A8D85337FBBE0 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___subEmitterIndex0, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * ___particle1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::GetManagedJobHandle_Injected(Unity.Jobs.JobHandle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_GetManagedJobHandle_Injected_m294A930F43B3561DDE1ABD8512F7773C3D7B4318 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::SetManagedJobHandle_Injected(Unity.Jobs.JobHandle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetManagedJobHandle_Injected_m58056683B09174D75E69BFF45F191CB91542617C (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847 * ___handle0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::ScheduleManagedJob_Injected(Unity.Jobs.LowLevel.Unsafe.JobsUtility/JobScheduleParameters&,System.Void*,Unity.Jobs.JobHandle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_ScheduleManagedJob_Injected_m2C68F3306AA63CBE2D5F1D2FB6A6571B031255A9 (JobScheduleParameters_tA29064729507A938ACB05D9F98B2C4A6977E3EF8 * ___parameters0, void* ___additionalData1, JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847 * ___ret2, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule__ctor_m34F626B568C6D3C80A036409049761C8316E6A76 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmissionModule__ctor_m9DFD2A0BB8BFAD2191C54E6627FC3B0E641EDD51 (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/ShapeModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShapeModule__ctor_mB0CE9D41BBE5FFA5B70F465541BA63D9BEEA7264 (ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/VelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VelocityOverLifetimeModule__ctor_m03EB5FA8BB69309D7DA91CE70EE3A9E65C60AE66 (VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LimitVelocityOverLifetimeModule__ctor_mA327985B69E145AAC3FFEE42EE44A81D7800BCCC (LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/InheritVelocityModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InheritVelocityModule__ctor_m76B46612309334A443F9939A47FBAAF024606892 (InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/LifetimeByEmitterSpeedModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LifetimeByEmitterSpeedModule__ctor_m80A22170A2E3047A3B364FE232CC4EB289F87BED (LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/ForceOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ForceOverLifetimeModule__ctor_m4FF722421C5B1449CB900FF0506EF33549A450CD (ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/ColorOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorOverLifetimeModule__ctor_m3AFAD44AD8E69F213881272693C23536EEBAB71D (ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/ColorBySpeedModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorBySpeedModule__ctor_m71C9D80CE70BB41F782B4C262D0FE40477A7D0AF (ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/SizeOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SizeOverLifetimeModule__ctor_mCA80885490FC3B1010887B10BDE21A0DACEFB557 (SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/SizeBySpeedModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SizeBySpeedModule__ctor_m479D4802A862D4E07AAF2B47833852A62A17F6DE (SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/RotationOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RotationOverLifetimeModule__ctor_mA0E0BDF5A88582DFEC3FAB42F8B237611C8E0182 (RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/RotationBySpeedModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RotationBySpeedModule__ctor_mCEAFF0BBEC6AB5FB3E59FD1584B22079A2142248 (RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/ExternalForcesModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExternalForcesModule__ctor_m2FB0D6776AAAB6731E3E80BA6ADD858458976DF5 (ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/NoiseModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NoiseModule__ctor_mE450BC95A9B436FA1627E4D24B7B4E7E723DDB5B (NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/CollisionModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollisionModule__ctor_m08D1EEAC52F7833F28D4FC2563120FF46A9E4220 (CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/TriggerModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TriggerModule__ctor_mEB88CDC59DC3AFBC343D3042488445F6E8A5AE9D (TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/SubEmittersModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubEmittersModule__ctor_m39DB9FE3DF4B1E15890712EF4E4024818D568A3C (SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/TextureSheetAnimationModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextureSheetAnimationModule__ctor_mF02B5303373B93D856A5BDF92B4E4AABC7CB2F65 (TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/LightsModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LightsModule__ctor_mD59B002A60EB418DACA4986EC1C4D34B8DAC555F (LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/TrailModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrailModule__ctor_m15960ADC2381E58BE70FF2653288599694915412 (TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/CustomDataModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomDataModule__ctor_m8F46AC54F3CC41A63AFE9856B18CC41A9A237277 (CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method);
// System.Void UnityEngine.Component::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Component__ctor_m0B00FA207EB3E560B78938D8AD877DB2BC1E3722 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystemRenderer::Internal_SetVertexStreams(UnityEngine.ParticleSystemVertexStreams,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_Internal_SetVertexStreams_mA00D4C6C868E8309E715015704CCFC12A4CBD8FD (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, int32_t ___streams0, bool ___enabled1, const RuntimeMethod* method);
// UnityEngine.ParticleSystemVertexStreams UnityEngine.ParticleSystemRenderer::Internal_GetEnabledVertexStreams(UnityEngine.ParticleSystemVertexStreams)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystemRenderer_Internal_GetEnabledVertexStreams_m1AA436781571C985E87EE8706AF264C977D7A979 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, int32_t ___streams0, const RuntimeMethod* method);
// System.Int32 UnityEngine.ParticleSystemRenderer::get_activeVertexStreamsCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystemRenderer_get_activeVertexStreamsCount_m948DD12ED56E758FF7F55DF8D1D9B6D644A3D3FE (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystemVertexStream>::.ctor(System.Int32)
inline void List_1__ctor_m50FEC7E4E5C76F2F1388964CF2AF151F6BC4216C (List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 *, int32_t, const RuntimeMethod*))List_1__ctor_mA4B6A2C2E6A818890DB03FBE417C0FEB7E1E4451_gshared)(__this, ___capacity0, method);
}
// System.Void UnityEngine.ParticleSystemRenderer::GetActiveVertexStreams(System.Collections.Generic.List`1<UnityEngine.ParticleSystemVertexStream>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_GetActiveVertexStreams_m32C738BAD8EAB54463B9BBCA2CAA0324BC702014 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * ___streams0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<UnityEngine.ParticleSystemVertexStream>::Contains(!0)
inline bool List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86 (List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 *, int32_t, const RuntimeMethod*))List_1_Contains_m793EB605F2702C16DE665C690BFA9B9675529D94_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.ParticleSystemVertexStream>::Add(!0)
inline void List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93 (List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 *, int32_t, const RuntimeMethod*))List_1_Add_mBA0FDF41792A78B3EB9E395D711706E268313F0F_gshared)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1<UnityEngine.ParticleSystemVertexStream>::Remove(!0)
inline bool List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F (List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 *, int32_t, const RuntimeMethod*))List_1_Remove_m33506664531C256A7C5EE365E9E994AF9289F43A_gshared)(__this, ___item0, method);
}
// System.Void UnityEngine.ParticleSystemRenderer::SetActiveVertexStreams(System.Collections.Generic.List`1<UnityEngine.ParticleSystemVertexStream>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_SetActiveVertexStreams_m6915A0D7D1E0DCEBEA77A31DC78BE0CE8DBEAD22 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * ___streams0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystemRenderer::get_pivot_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_get_pivot_Injected_mB393D1D0F0C69BD2F52A282FD0868E3E2585488C (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystemRenderer::set_pivot_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_pivot_Injected_m8CE9A3B8B0EB136F3E658CFB2B00616D7D8A8AB3 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystemRenderer::get_flip_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_get_flip_Injected_m08B63FCDBAB2C235981432A80901FE97A29DA985 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystemRenderer::set_flip_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_flip_Injected_mE957C7B1BCD98C9D2D19900F1CC8EE50ED1DD688 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystemRenderer::SetMeshes(UnityEngine.Mesh[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_SetMeshes_mAD7FCBD6E24A447D5304058EA04F751F85AF5A99 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8* ___meshes0, int32_t ___size1, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C (const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystemRenderer::BakeMesh(UnityEngine.Mesh,UnityEngine.Camera,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_BakeMesh_m1A53F569D7DFCBE964E66D5E3ACA11D85ED56C9E (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___mesh0, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___camera1, bool ___useTransform2, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystemRenderer::BakeTrailsMesh(UnityEngine.Mesh,UnityEngine.Camera,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_BakeTrailsMesh_m7BE3F730731BBFA0C6A9399BF91813236AC14E3F (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___mesh0, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___camera1, bool ___useTransform2, const RuntimeMethod* method);
// System.Void UnityEngine.Renderer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer__ctor_m998612F51A0E95E387FC2032AB5FEE1304E346EE (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.ParticleSystem/EmissionModule::get_enabled_Injected(UnityEngine.ParticleSystem/EmissionModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EmissionModule_get_enabled_Injected_m69F97462BF229B56970ABFBA4BCE59AFAF17386C (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled_Injected(UnityEngine.ParticleSystem/EmissionModule&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmissionModule_set_enabled_Injected_m35208A8279D12A78D5DBCF751BF1760869051C82 (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * ____unity_self0, bool ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime_Injected(UnityEngine.ParticleSystem/EmissionModule&,UnityEngine.ParticleSystem/MinMaxCurve&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmissionModule_set_rateOverTime_Injected_m19782FA56F82A6F0A6AFE6454E662F41098B6E3B (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * ____unity_self0, MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD * ___value1, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/EmissionModule::get_rateOverTimeMultiplier_Injected(UnityEngine.ParticleSystem/EmissionModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float EmissionModule_get_rateOverTimeMultiplier_Injected_m1893AB6D1B181596FB7CC1D080313785DB5E5791 (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * ____unity_self0, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_duration_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_duration_Injected_mBD7B5FEFFD26DB833052D35A3AC492699B4BEE41 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Boolean UnityEngine.ParticleSystem/MainModule::get_loop_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MainModule_get_loop_Injected_mD8FD195A3CF5008FD319636A3945DE2F4F0EEBD2 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_loop_Injected(UnityEngine.ParticleSystem/MainModule&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_loop_Injected_m064E3166CAA11D0842F105E473A231C0C15DE1B3 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, bool ___value1, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startDelayMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startDelayMultiplier_Injected_mEC1750CCDB02393B6FB1E766FDF2C875A9DA5247 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startDelayMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startDelayMultiplier_Injected_m6C0ADED60F67E9C685F6A4BCAF6E658A498143CC (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startLifetimeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startLifetimeMultiplier_Injected_m5DFF410088ECCDC91DB86AEFB2BB1DCB6411666D (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startLifetimeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startLifetimeMultiplier_Injected_m2000D61A48B2CB2AAEE1FCBB141D58299A29446E (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startSpeedMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startSpeedMultiplier_Injected_mC59B6818EF7C52D84832165A74B0F5AFD2CC02CA (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeedMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startSpeedMultiplier_Injected_mD454E7C2AF126C2F46265F32E00A7ACA8D632B5C (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startSizeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startSizeMultiplier_Injected_m04AAFC24A6357D932E1F36C9FA94CA955FCB051F (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSizeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startSizeMultiplier_Injected_m09FF5ECF76CB00E810A2EC29926CF5EE12BD94D5 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationMultiplier_Injected_mFF1B0ED143B0700ACBC5499AE1EED0F9C2334FD8 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationMultiplier_Injected_m1D25033340DFEC86D7FE7537E0EC7EC9C435A140 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationXMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationXMultiplier_Injected_mA907E58F38775A2AD084A95BD11470E9312A0132 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationXMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationXMultiplier_Injected_m68AC8C3218148C2212765904127F87ACB58F6C69 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationYMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationYMultiplier_Injected_m4A540D71853039082422C270FC1A14226FDFB43C (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationYMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationYMultiplier_Injected_m05F30D5DAFC51E007F1D350A6CB8963EC269901E (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationZMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationZMultiplier_Injected_mF08F4933FBA184D447C01ED33E23B0C3B6067735 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationZMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationZMultiplier_Injected_m3ABFA58E2E34C80CBB9E6BADC32F12CD34B115A0 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::get_startColor_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystem/MinMaxGradient&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_get_startColor_Injected_mB8F4FB3EAB113FEE09389BED8F41E0F047128276 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 * ___ret1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startColor_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystem/MinMaxGradient&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startColor_Injected_mBDC348394A0F1920C4123AF0329DECB2869D6EE6 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 * ___value1, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_gravityModifierMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_gravityModifierMultiplier_Injected_mB86E9F34B1B61A23C2F5CB9BFD06703BC8B9972D (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_gravityModifierMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_gravityModifierMultiplier_Injected_m2AEE5F42B889A4ED22D05AA5E76A87F43680D601 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method);
// UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem/MainModule::get_simulationSpace_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MainModule_get_simulationSpace_Injected_mD56565C6B7CDDAFDD418BEB202E8759DCA45D69F (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpace_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystemSimulationSpace)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_simulationSpace_Injected_mFD6F06EA32114BC5C9D9CF3C10A5A0066E40664C (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, int32_t ___value1, const RuntimeMethod* method);
// System.Single UnityEngine.ParticleSystem/MainModule::get_simulationSpeed_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_simulationSpeed_Injected_m12263544CFAE95F37D2A9B4C45AB58452AE963F6 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpeed_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_simulationSpeed_Injected_m86AEA2C2313AC96FC68800A3871248ED2597E26C (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method);
// UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem/MainModule::get_scalingMode_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MainModule_get_scalingMode_Injected_m0DD794410FA56B46A8651B4FC504CFB2484C0A02 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_scalingMode_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystemScalingMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_scalingMode_Injected_mEABFC2E8FE084F997A630B8D2A643B2C9249A5A0 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, int32_t ___value1, const RuntimeMethod* method);
// System.Boolean UnityEngine.ParticleSystem/MainModule::get_playOnAwake_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MainModule_get_playOnAwake_Injected_m87D4B5B2508D9CF9B3C61F84BC001945B20A06C2 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_playOnAwake_Injected(UnityEngine.ParticleSystem/MainModule&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_playOnAwake_Injected_mAE88821DF50EB8AFD7079C587CC7FB633626B5FB (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, bool ___value1, const RuntimeMethod* method);
// System.Int32 UnityEngine.ParticleSystem/MainModule::get_maxParticles_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MainModule_get_maxParticles_Injected_m260F43233BCA02794176F596FF08A96BFCBE0D91 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_maxParticles_Injected(UnityEngine.ParticleSystem/MainModule&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_maxParticles_Injected_m6D4D611540F53CC4D8F21D9DD7EF3F68FC50F471 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, int32_t ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxCurve__ctor_mF14A2DBABFDCF1E855911241A555949CAF212AE3 (MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD * __this, float ___constant0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_black()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_black_m67E91EB7017FC74D9AB5ADEF6B6929B7EFC9A982 (const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MinMaxGradient::.ctor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxGradient__ctor_m8D0DE766F67FF065E4C2B6DCD841752C447977AA (MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/Particle::set_remainingLifetime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_remainingLifetime_mE521DF9387892C00B3F21D4F76F3A55E77AFC6BE (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, float ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Emit_mC489C467AAF3C3721AC3315AF78DC4CE469E7AAC (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___velocity1, float ___size2, float ___lifetime3, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___color4, const RuntimeMethod* method)
{
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 ));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___position0;
		Particle_set_position_mB749E41CD3C4C6DF501C0D68B6211CF7E2217FC2((Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)(&V_0), L_0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = ___velocity1;
		Particle_set_velocity_m4894BD6A75E7A1FCD433927F93396AA29A59709B((Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)(&V_0), L_1, /*hidden argument*/NULL);
		float L_2 = ___lifetime3;
		Particle_set_lifetime_mCE97F9D17D1E660DB9D9F3244150CB6624A74DC3((Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)(&V_0), L_2, /*hidden argument*/NULL);
		float L_3 = ___lifetime3;
		Particle_set_startLifetime_m7AC09262BBFE8818EA6B61BDB62264184CD27A82((Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)(&V_0), L_3, /*hidden argument*/NULL);
		float L_4 = ___size2;
		Particle_set_startSize_m7CDEE5B620B3D26B4CC5C1DA7C6E24ACCCF64466((Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)(&V_0), L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		Particle_set_rotation3D_m0F402760524A81307FA4940751751C44DF2F77D0((Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)(&V_0), L_5, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		Particle_set_angularVelocity3D_mE9A9544DF33CD0CCF4F1CA14994A2C436E4DF1F8((Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)(&V_0), L_6, /*hidden argument*/NULL);
		Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  L_7 = ___color4;
		Particle_set_startColor_mA7B0363E82B7A7CBB1C83F8C5D49FB7B7AF75554((Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)(&V_0), L_7, /*hidden argument*/NULL);
		Particle_set_randomSeed_m8FD7A4DB7F8E7EBDEF2C51A28197F8D9D7CB6E04((Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)(&V_0), 5, /*hidden argument*/NULL);
		ParticleSystem_EmitOld_Internal_m4F094DC523986298D5626F0F3F2335DFF596C993(__this, (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)(&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/Particle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Emit_mF1E108B9BF7E0094C35CF71870B5B0EA72ABB485 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  ___particle0, const RuntimeMethod* method)
{
	{
		ParticleSystem_EmitOld_Internal_m4F094DC523986298D5626F0F3F2335DFF596C993(__this, (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)(&___particle0), /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.ParticleSystem::get_startDelay()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystem_get_startDelay_m6661DA2328104EAAF1B3EED1A9AD5BA506B3DC57 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1;
		L_1 = MainModule_get_startDelayMultiplier_mC38890C8853463BB25F031BB8884F3D8B0AF581E((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_startDelay(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_startDelay_m260AAD1034FB43187FA18D7D82858F7B12827662 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___value0, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		MainModule_set_startDelayMultiplier_m23FC7BAFE2DCF940C398D6BE53606E23E1715464((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem::get_loop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_get_loop_m7D0ED1B08C7503790FC5FD2863728AD281098AB2 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1;
		L_1 = MainModule_get_loop_mE45A1A4A1DF6E2590B7B13A73A9A95197DA346F8((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_loop(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_loop_mE7536AE1F42CCD5F8C410D719D3D80571C0054C6 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___value0, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = ___value0;
		MainModule_set_loop_m9F43A092EFABBE92662DF9EDA73AF8D9C72DABB2((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem::get_playOnAwake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_get_playOnAwake_mCC6EB49C6C0400E3CC58024E4AEFD6EB81EBBCB6 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1;
		L_1 = MainModule_get_playOnAwake_m6E07F3ACCC2797500E70AF4A4A33C5C8425A52D5((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_playOnAwake(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_playOnAwake_m9AB94C2B3B742F4E3A1C7EFEBF30CDD33195D21D (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___value0, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = ___value0;
		MainModule_set_playOnAwake_m85E164C05D5C39B0B3C6139BE360BAB41B0D7FCF((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.ParticleSystem::get_duration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystem_get_duration_m25C0E6C6B0EC084ED59977966AF2C2EDE529FA44 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1;
		L_1 = MainModule_get_duration_mD516595EFDD66C83A727BDD7EF495069B4EB6132((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_1;
		return L_2;
	}
}
// System.Single UnityEngine.ParticleSystem::get_playbackSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystem_get_playbackSpeed_mA03269D76AA3AE6F149A19CCF12528D7EA384D52 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1;
		L_1 = MainModule_get_simulationSpeed_m7BCFDA4BD65598CE9CEF34A029E4C03DAB4A3C53((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_playbackSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_playbackSpeed_mCAAD47BDB2A8F83EDE0FA791331FDDA5E29E488C (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___value0, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		MainModule_set_simulationSpeed_m9EA9E4BE76B372EF9674568810BF4F5B403B2833((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem::get_enableEmission()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_get_enableEmission_m4A8F7E3AD120DC00910D5A20B25B90417BF3EB7B (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  L_0;
		L_0 = ParticleSystem_get_emission_m0BAA1EDF775A8712DA7D62BF3C42A2B1A6C96CA1(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1;
		L_1 = EmissionModule_get_enabled_m9DB0C4A45B5D04CD88355536DD3E1DD1180DF107((EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_enableEmission(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_enableEmission_m29DCB9B1C7F2E54DCA3D1A046754ACBEFE162FA9 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___value0, const RuntimeMethod* method)
{
	EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  L_0;
		L_0 = ParticleSystem_get_emission_m0BAA1EDF775A8712DA7D62BF3C42A2B1A6C96CA1(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = ___value0;
		EmissionModule_set_enabled_mD9FD50C9F5EECD04B22D03E5C00C7DD64D2AC1F1((EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *)(&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.ParticleSystem::get_emissionRate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystem_get_emissionRate_m71A71CD1426FC5DC8A0717DABCE413693C412967 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  L_0;
		L_0 = ParticleSystem_get_emission_m0BAA1EDF775A8712DA7D62BF3C42A2B1A6C96CA1(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1;
		L_1 = EmissionModule_get_rateOverTimeMultiplier_mF8F50D01C0F4ADB44423852CD75A334179456FBA((EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_emissionRate(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_emissionRate_m62D12CC0B786ACD83632326F50A1082BE21EBA90 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___value0, const RuntimeMethod* method)
{
	EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  L_0;
		L_0 = ParticleSystem_get_emission_m0BAA1EDF775A8712DA7D62BF3C42A2B1A6C96CA1(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD  L_2;
		L_2 = MinMaxCurve_op_Implicit_m8D746D40E6CCBF5E8C4CE39F23A45712BFC372F5(L_1, /*hidden argument*/NULL);
		EmissionModule_set_rateOverTime_m4EE4643D8E8B8BEC37BCB16DB76C9B8E4E4C8F48((EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *)(&V_0), L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.ParticleSystem::get_startSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystem_get_startSpeed_mC6CEF0642452626DEE0687FE57CE96EE9DC65F41 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1;
		L_1 = MainModule_get_startSpeedMultiplier_mE165D2B967AD8A8827D07DBD694F1C7167C80336((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_startSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_startSpeed_m969B0FF3E7392F9E027D40C8BAAFF932171D9280 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___value0, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		MainModule_set_startSpeedMultiplier_mA3C6B512351C3BFA8E64B40936B3353845A637F2((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.ParticleSystem::get_startSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystem_get_startSize_m7327DF9701486C5476DB9E3296BD5692E2A7CB72 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1;
		L_1 = MainModule_get_startSizeMultiplier_m820351EE78DF731C1AFA8309ECB5ADF3E1166279((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_startSize(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_startSize_m2175FEE1F0ABA3F69C2DC4EC4D940E19A7FBEC6D (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___value0, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		MainModule_set_startSizeMultiplier_m231F400517E7F96EFF7C3C31E59B7B8DD0A1DB64((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color UnityEngine.ParticleSystem::get_startColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ParticleSystem_get_startColor_mEA5A4B07850804959DE58004AB65FEBFE5067515 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  L_1;
		L_1 = MainModule_get_startColor_mC8BAD25B55ACF9586BAFEECF047CCE5268123DD4((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_2;
		L_2 = MinMaxGradient_get_color_m8FA2F2F1E0B9C9E1DFE40CEA325BC5E863C59C90((MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 *)(&V_1), /*hidden argument*/NULL);
		V_2 = L_2;
		goto IL_001a;
	}

IL_001a:
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3 = V_2;
		return L_3;
	}
}
// System.Void UnityEngine.ParticleSystem::set_startColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_startColor_m58AE61985D5B25B108FB4A5E05D7DF4A0FFECAEC (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___value0;
		MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  L_2;
		L_2 = MinMaxGradient_op_Implicit_m5F938B5644AF6BE5DF525839E922B88456B614FA(L_1, /*hidden argument*/NULL);
		MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.ParticleSystem::get_startRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystem_get_startRotation_m19EA2F6770518A7E2F9CB529943E23E083DEC5A0 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1;
		L_1 = MainModule_get_startRotationMultiplier_m72C5648E64889B162553000EF3B7347F47B1F84A((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_startRotation(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_startRotation_mFF30EA836130E5105E8886B27F6051E87EE7DF49 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___value0, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		MainModule_set_startRotationMultiplier_mB7085FB7FF45BDD4C71E08226C6D5F5635CB47EE((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.ParticleSystem::get_startRotation3D()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ParticleSystem_get_startRotation3D_mA9CDEFD28212D4A67F6D3E840BE3A059C361B82F (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1;
		L_1 = MainModule_get_startRotationXMultiplier_m15710A32A7861DAF1A0CD55DB1835EC22617F987((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_2;
		L_2 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3;
		L_3 = MainModule_get_startRotationYMultiplier_mC9C0B9123DDF9D37EF3E5291164E16AB95308011((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_4;
		L_4 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5;
		L_5 = MainModule_get_startRotationZMultiplier_m736451211917A59B78A3B444963D305BD377898D((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), L_1, L_3, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0033;
	}

IL_0033:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.ParticleSystem::set_startRotation3D(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_startRotation3D_m801440D1F8A5B0C8268607D7C6CBB1DBE53602AE (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = ___value0;
		float L_2 = L_1.get_x_2();
		MainModule_set_startRotationXMultiplier_mA489325EDA3D644481BFF44376627B0B6EFB81CD((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___value0;
		float L_4 = L_3.get_y_3();
		MainModule_set_startRotationYMultiplier_m45DC00CC3E94A7CFCEA522F105F93BCE1B727639((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = ___value0;
		float L_6 = L_5.get_z_4();
		MainModule_set_startRotationZMultiplier_m9D604421CED73B2B110158F1FAF955832FB9A6D5((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.ParticleSystem::get_startLifetime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystem_get_startLifetime_m48D95DC48DB180862C6D921B08D3BB0969AEBC02 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1;
		L_1 = MainModule_get_startLifetimeMultiplier_m8D2F788F9A1F5768E64F4511F2158E3BFAEB474D((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_startLifetime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_startLifetime_m3574A8F28C124698F4BBC3CBEE7F638CE6AB2552 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___value0, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		MainModule_set_startLifetimeMultiplier_m219938FDA9324C185D998815836B50FEE3AC0D8D((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.ParticleSystem::get_gravityModifier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystem_get_gravityModifier_m8E0B14851B17FED84102ECC0A4871151C9DAF553 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1;
		L_1 = MainModule_get_gravityModifierMultiplier_m875B65CAEE457EB12A5CD763A5AA311DE014629A((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_gravityModifier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_gravityModifier_m0E1C7D97DD78C58931FB3E502B43920E4A810F1F (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___value0, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		MainModule_set_gravityModifierMultiplier_mBC64B6D0DEF548A94D0C0303855F43931A1F5684((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.ParticleSystem::get_maxParticles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_get_maxParticles_mAF21E5F4E07CC95B46F34FA22E68DE7E1714D596 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1;
		L_1 = MainModule_get_maxParticles_m4A95D2105C1B414331F15EF86ADBC23CC6C8DD35((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_maxParticles(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_maxParticles_m3B2C177B338CAC418232A25E5F3C0B588B5E4ABA (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___value0, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = ___value0;
		MainModule_set_maxParticles_m6FD14B44787B44D38DEDAE4FCBE9596A86A0460D((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem::get_simulationSpace()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_get_simulationSpace_mF6EED4B63164572C99E8EC7F315108BEC7CBB23D (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1;
		L_1 = MainModule_get_simulationSpace_mD08447602DF2E2AC9790D900A1BD04AB8D4FD0A2((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_simulationSpace(UnityEngine.ParticleSystemSimulationSpace)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_simulationSpace_mF4A5FBCDCC9F63BB28545FF4FF68BB77C1F6D04E (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___value0, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = ___value0;
		MainModule_set_simulationSpace_m59583EC5A1D9D64EC8C0CCE275CE6669FC8E268F((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem::get_scalingMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_get_scalingMode_mF2EA9887C71A595E98187564E41D94A710504E63 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1;
		L_1 = MainModule_get_scalingMode_m2FEDBC7969A4BD0071B9467AFFD1B98EE64A9C51((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_scalingMode(UnityEngine.ParticleSystemScalingMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_scalingMode_m4B399177269F81152FAF667684FE84EB44E61E0F (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___value0, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		L_0 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = ___value0;
		MainModule_set_scalingMode_m0992C03429ABFBD1021009BF669BF362705D4E23((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem::get_automaticCullingEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_get_automaticCullingEnabled_m3E0F822B082E66D6AB5813BDF96F48E0B54971C4 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0;
		L_0 = ParticleSystem_get_proceduralSimulationSupported_mEF4AEBA182D7DD12ABB6414971A2365236A80A50(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.ParticleSystem::get_isPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_get_isPlaying_m36FD03CBF99EE4C243B01F37D77CB6B1CFA526BA (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystem_get_isPlaying_m36FD03CBF99EE4C243B01F37D77CB6B1CFA526BA_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *);
	static ParticleSystem_get_isPlaying_m36FD03CBF99EE4C243B01F37D77CB6B1CFA526BA_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isPlaying_m36FD03CBF99EE4C243B01F37D77CB6B1CFA526BA_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isPlaying()");
	bool icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Boolean UnityEngine.ParticleSystem::get_isEmitting()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_get_isEmitting_m1641C8124AE31D556E65164BC5A085C523FE4033 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystem_get_isEmitting_m1641C8124AE31D556E65164BC5A085C523FE4033_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *);
	static ParticleSystem_get_isEmitting_m1641C8124AE31D556E65164BC5A085C523FE4033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isEmitting_m1641C8124AE31D556E65164BC5A085C523FE4033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isEmitting()");
	bool icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Boolean UnityEngine.ParticleSystem::get_isStopped()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_get_isStopped_mE50B27655AFE6F178C9B3B99F18968A8964D93F8 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystem_get_isStopped_mE50B27655AFE6F178C9B3B99F18968A8964D93F8_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *);
	static ParticleSystem_get_isStopped_mE50B27655AFE6F178C9B3B99F18968A8964D93F8_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isStopped_mE50B27655AFE6F178C9B3B99F18968A8964D93F8_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isStopped()");
	bool icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Boolean UnityEngine.ParticleSystem::get_isPaused()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_get_isPaused_mB8AEA85594606CC49078B39F05ED8BA5C8199C0E (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystem_get_isPaused_mB8AEA85594606CC49078B39F05ED8BA5C8199C0E_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *);
	static ParticleSystem_get_isPaused_mB8AEA85594606CC49078B39F05ED8BA5C8199C0E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isPaused_mB8AEA85594606CC49078B39F05ED8BA5C8199C0E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isPaused()");
	bool icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Int32 UnityEngine.ParticleSystem::get_particleCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_get_particleCount_mAD1793317BD6BBCB0C7A7853A9E82D19703B0A52 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystem_get_particleCount_mAD1793317BD6BBCB0C7A7853A9E82D19703B0A52_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *);
	static ParticleSystem_get_particleCount_mAD1793317BD6BBCB0C7A7853A9E82D19703B0A52_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_particleCount_mAD1793317BD6BBCB0C7A7853A9E82D19703B0A52_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_particleCount()");
	int32_t icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Single UnityEngine.ParticleSystem::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystem_get_time_m85B483199DA7D9675ABCB194F2CD431C53D4B3B1 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	typedef float (*ParticleSystem_get_time_m85B483199DA7D9675ABCB194F2CD431C53D4B3B1_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *);
	static ParticleSystem_get_time_m85B483199DA7D9675ABCB194F2CD431C53D4B3B1_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_time_m85B483199DA7D9675ABCB194F2CD431C53D4B3B1_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_time()");
	float icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem::set_time(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_time_m014BD2C09A14861186C79BA3FD12E2AC8DE90544 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_set_time_m014BD2C09A14861186C79BA3FD12E2AC8DE90544_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, float);
	static ParticleSystem_set_time_m014BD2C09A14861186C79BA3FD12E2AC8DE90544_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_time_m014BD2C09A14861186C79BA3FD12E2AC8DE90544_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_time(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.UInt32 UnityEngine.ParticleSystem::get_randomSeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t ParticleSystem_get_randomSeed_m70650D178DEADC42E19B6B22ECE252AE5434F030 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	typedef uint32_t (*ParticleSystem_get_randomSeed_m70650D178DEADC42E19B6B22ECE252AE5434F030_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *);
	static ParticleSystem_get_randomSeed_m70650D178DEADC42E19B6B22ECE252AE5434F030_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_randomSeed_m70650D178DEADC42E19B6B22ECE252AE5434F030_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_randomSeed()");
	uint32_t icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem::set_randomSeed(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_randomSeed_m327828D83321B6F734D52EB91E64A45228040EC1 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, uint32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_set_randomSeed_m327828D83321B6F734D52EB91E64A45228040EC1_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, uint32_t);
	static ParticleSystem_set_randomSeed_m327828D83321B6F734D52EB91E64A45228040EC1_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_randomSeed_m327828D83321B6F734D52EB91E64A45228040EC1_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_randomSeed(System.UInt32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.ParticleSystem::get_useAutoRandomSeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_get_useAutoRandomSeed_m1129F9BC54087CCF6EFE372E9182F7AD298877ED (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystem_get_useAutoRandomSeed_m1129F9BC54087CCF6EFE372E9182F7AD298877ED_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *);
	static ParticleSystem_get_useAutoRandomSeed_m1129F9BC54087CCF6EFE372E9182F7AD298877ED_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_useAutoRandomSeed_m1129F9BC54087CCF6EFE372E9182F7AD298877ED_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_useAutoRandomSeed()");
	bool icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem::set_useAutoRandomSeed(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_set_useAutoRandomSeed_m1A1B3221AD7BAFF3AA0EB8820D0D6C58BBC2831B (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_set_useAutoRandomSeed_m1A1B3221AD7BAFF3AA0EB8820D0D6C58BBC2831B_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, bool);
	static ParticleSystem_set_useAutoRandomSeed_m1A1B3221AD7BAFF3AA0EB8820D0D6C58BBC2831B_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_useAutoRandomSeed_m1A1B3221AD7BAFF3AA0EB8820D0D6C58BBC2831B_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_useAutoRandomSeed(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.ParticleSystem::get_proceduralSimulationSupported()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_get_proceduralSimulationSupported_mEF4AEBA182D7DD12ABB6414971A2365236A80A50 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystem_get_proceduralSimulationSupported_mEF4AEBA182D7DD12ABB6414971A2365236A80A50_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *);
	static ParticleSystem_get_proceduralSimulationSupported_mEF4AEBA182D7DD12ABB6414971A2365236A80A50_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_proceduralSimulationSupported_mEF4AEBA182D7DD12ABB6414971A2365236A80A50_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_proceduralSimulationSupported()");
	bool icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Single UnityEngine.ParticleSystem::GetParticleCurrentSize(UnityEngine.ParticleSystem/Particle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystem_GetParticleCurrentSize_mE561E5E2E69A6C357C642444C25AA794833A06BE (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * ___particle0, const RuntimeMethod* method)
{
	typedef float (*ParticleSystem_GetParticleCurrentSize_mE561E5E2E69A6C357C642444C25AA794833A06BE_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *);
	static ParticleSystem_GetParticleCurrentSize_mE561E5E2E69A6C357C642444C25AA794833A06BE_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetParticleCurrentSize_mE561E5E2E69A6C357C642444C25AA794833A06BE_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetParticleCurrentSize(UnityEngine.ParticleSystem/Particle&)");
	float icallRetVal = _il2cpp_icall_func(__this, ___particle0);
	return icallRetVal;
}
// UnityEngine.Vector3 UnityEngine.ParticleSystem::GetParticleCurrentSize3D(UnityEngine.ParticleSystem/Particle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ParticleSystem_GetParticleCurrentSize3D_m989175446F4C6E99635AD8E1FA33BB1146978A9F (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * ___particle0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * L_0 = ___particle0;
		ParticleSystem_GetParticleCurrentSize3D_Injected_mE1D7D2457CDC32E35F9D3E06721D773667F2A966(__this, (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)L_0, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color32 UnityEngine.ParticleSystem::GetParticleCurrentColor(UnityEngine.ParticleSystem/Particle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ParticleSystem_GetParticleCurrentColor_mBFD3E4B3A3639F1F59D60C8079F36A9A51E03053 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * ___particle0, const RuntimeMethod* method)
{
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * L_0 = ___particle0;
		ParticleSystem_GetParticleCurrentColor_Injected_m98C468104D268559CF2F06F78F970F1814F7A587(__this, (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)L_0, (Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D *)(&V_0), /*hidden argument*/NULL);
		Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.ParticleSystem::GetParticleMeshIndex(UnityEngine.ParticleSystem/Particle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetParticleMeshIndex_m9CBB1891C113D91AA1F0C390EFCA502E1D22C648 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * ___particle0, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystem_GetParticleMeshIndex_m9CBB1891C113D91AA1F0C390EFCA502E1D22C648_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *);
	static ParticleSystem_GetParticleMeshIndex_m9CBB1891C113D91AA1F0C390EFCA502E1D22C648_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetParticleMeshIndex_m9CBB1891C113D91AA1F0C390EFCA502E1D22C648_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetParticleMeshIndex(UnityEngine.ParticleSystem/Particle&)");
	int32_t icallRetVal = _il2cpp_icall_func(__this, ___particle0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetParticles_m79867147E65742BE1E1ADF0222C090BD6AF1E20A (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* ___particles0, int32_t ___size1, int32_t ___offset2, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_SetParticles_m79867147E65742BE1E1ADF0222C090BD6AF1E20A_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C*, int32_t, int32_t);
	static ParticleSystem_SetParticles_m79867147E65742BE1E1ADF0222C090BD6AF1E20A_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_SetParticles_m79867147E65742BE1E1ADF0222C090BD6AF1E20A_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___particles0, ___size1, ___offset2);
}
// System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetParticles_m0658B777D1C6DDA7D244607AC55D5225774CEBFA (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* ___particles0, int32_t ___size1, const RuntimeMethod* method)
{
	{
		ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* L_0 = ___particles0;
		int32_t L_1 = ___size1;
		ParticleSystem_SetParticles_m79867147E65742BE1E1ADF0222C090BD6AF1E20A(__this, L_0, L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetParticles_m1693B5469E364598DDEAEB1B48724C203E743668 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* ___particles0, const RuntimeMethod* method)
{
	{
		ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* L_0 = ___particles0;
		ParticleSystem_SetParticles_m0658B777D1C6DDA7D244607AC55D5225774CEBFA(__this, L_0, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::SetParticlesWithNativeArray(System.IntPtr,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetParticlesWithNativeArray_m163F32606AC234C018E081A3522CCF57C8A2A4FC (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, intptr_t ___particles0, int32_t ___particlesLength1, int32_t ___size2, int32_t ___offset3, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_SetParticlesWithNativeArray_m163F32606AC234C018E081A3522CCF57C8A2A4FC_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, intptr_t, int32_t, int32_t, int32_t);
	static ParticleSystem_SetParticlesWithNativeArray_m163F32606AC234C018E081A3522CCF57C8A2A4FC_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_SetParticlesWithNativeArray_m163F32606AC234C018E081A3522CCF57C8A2A4FC_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::SetParticlesWithNativeArray(System.IntPtr,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___particles0, ___particlesLength1, ___size2, ___offset3);
}
// System.Void UnityEngine.ParticleSystem::SetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetParticles_m380593909B2E9DBB77D8DE09E47C1D0DCCD6ACC2 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  ___particles0, int32_t ___size1, int32_t ___offset2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeArrayUnsafeUtility_GetUnsafeReadOnlyPtr_TisParticle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1_m6DB36AF31E5460B987A006F70D2DD82D2FB350C1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  L_0 = ___particles0;
		void* L_1;
		L_1 = NativeArrayUnsafeUtility_GetUnsafeReadOnlyPtr_TisParticle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1_m6DB36AF31E5460B987A006F70D2DD82D2FB350C1(L_0, /*hidden argument*/NativeArrayUnsafeUtility_GetUnsafeReadOnlyPtr_TisParticle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1_m6DB36AF31E5460B987A006F70D2DD82D2FB350C1_RuntimeMethod_var);
		intptr_t L_2;
		L_2 = IntPtr_op_Explicit_mBD40223EE90BDDF40A24C0F321D3398DEA300495((void*)(void*)L_1, /*hidden argument*/NULL);
		int32_t L_3;
		L_3 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0 *)(&___particles0))->___m_Length_1);
		int32_t L_4 = ___size1;
		ParticleSystem_SetParticlesWithNativeArray_m163F32606AC234C018E081A3522CCF57C8A2A4FC(__this, (intptr_t)L_2, L_3, L_4, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::SetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetParticles_m648CE8AF7118C73FF5E1D0419E7F6BA52478C13E (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  ___particles0, int32_t ___size1, const RuntimeMethod* method)
{
	{
		NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  L_0 = ___particles0;
		int32_t L_1 = ___size1;
		ParticleSystem_SetParticles_m380593909B2E9DBB77D8DE09E47C1D0DCCD6ACC2(__this, L_0, L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::SetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetParticles_m75061A9FC1FBA11FBFF903B64762D0B60118F2B9 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  ___particles0, const RuntimeMethod* method)
{
	{
		NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  L_0 = ___particles0;
		ParticleSystem_SetParticles_m648CE8AF7118C73FF5E1D0419E7F6BA52478C13E(__this, L_0, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetParticles_mC310C32157EA281ECDC320B63B1E43ED5F6292B3 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* ___particles0, int32_t ___size1, int32_t ___offset2, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystem_GetParticles_mC310C32157EA281ECDC320B63B1E43ED5F6292B3_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C*, int32_t, int32_t);
	static ParticleSystem_GetParticles_mC310C32157EA281ECDC320B63B1E43ED5F6292B3_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetParticles_mC310C32157EA281ECDC320B63B1E43ED5F6292B3_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32,System.Int32)");
	int32_t icallRetVal = _il2cpp_icall_func(__this, ___particles0, ___size1, ___offset2);
	return icallRetVal;
}
// System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetParticles_mF6FC6048609DD24AF7F1B8890C44AEC480BDFDEA (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* ___particles0, int32_t ___size1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* L_0 = ___particles0;
		int32_t L_1 = ___size1;
		int32_t L_2;
		L_2 = ParticleSystem_GetParticles_mC310C32157EA281ECDC320B63B1E43ED5F6292B3(__this, L_0, L_1, 0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetParticles_mAE8894E2B022EE009C6DDB1390AB331E7D40A344 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* ___particles0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* L_0 = ___particles0;
		int32_t L_1;
		L_1 = ParticleSystem_GetParticles_mF6FC6048609DD24AF7F1B8890C44AEC480BDFDEA(__this, L_0, (-1), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.ParticleSystem::GetParticlesWithNativeArray(System.IntPtr,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetParticlesWithNativeArray_m4303397CB3084BE1158D854929FE40CA0A6E5253 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, intptr_t ___particles0, int32_t ___particlesLength1, int32_t ___size2, int32_t ___offset3, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystem_GetParticlesWithNativeArray_m4303397CB3084BE1158D854929FE40CA0A6E5253_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, intptr_t, int32_t, int32_t, int32_t);
	static ParticleSystem_GetParticlesWithNativeArray_m4303397CB3084BE1158D854929FE40CA0A6E5253_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetParticlesWithNativeArray_m4303397CB3084BE1158D854929FE40CA0A6E5253_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetParticlesWithNativeArray(System.IntPtr,System.Int32,System.Int32,System.Int32)");
	int32_t icallRetVal = _il2cpp_icall_func(__this, ___particles0, ___particlesLength1, ___size2, ___offset3);
	return icallRetVal;
}
// System.Int32 UnityEngine.ParticleSystem::GetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetParticles_mCD391A836273B8B41035A9F06D5D7ABA83968A95 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  ___particles0, int32_t ___size1, int32_t ___offset2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeArrayUnsafeUtility_GetUnsafePtr_TisParticle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1_mB8C1C175F09CE57B87C863A2827A9F50D1DCEEB7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  L_0 = ___particles0;
		void* L_1;
		L_1 = NativeArrayUnsafeUtility_GetUnsafePtr_TisParticle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1_mB8C1C175F09CE57B87C863A2827A9F50D1DCEEB7(L_0, /*hidden argument*/NativeArrayUnsafeUtility_GetUnsafePtr_TisParticle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1_mB8C1C175F09CE57B87C863A2827A9F50D1DCEEB7_RuntimeMethod_var);
		intptr_t L_2;
		L_2 = IntPtr_op_Explicit_mBD40223EE90BDDF40A24C0F321D3398DEA300495((void*)(void*)L_1, /*hidden argument*/NULL);
		int32_t L_3;
		L_3 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0 *)(&___particles0))->___m_Length_1);
		int32_t L_4 = ___size1;
		int32_t L_5;
		L_5 = ParticleSystem_GetParticlesWithNativeArray_m4303397CB3084BE1158D854929FE40CA0A6E5253(__this, (intptr_t)L_2, L_3, L_4, 0, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001f;
	}

IL_001f:
	{
		int32_t L_6 = V_0;
		return L_6;
	}
}
// System.Int32 UnityEngine.ParticleSystem::GetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetParticles_m870578C1C1E37DFF7BC112662BAE81F3CA7AA411 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  ___particles0, int32_t ___size1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  L_0 = ___particles0;
		int32_t L_1 = ___size1;
		int32_t L_2;
		L_2 = ParticleSystem_GetParticles_mCD391A836273B8B41035A9F06D5D7ABA83968A95(__this, L_0, L_1, 0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Int32 UnityEngine.ParticleSystem::GetParticles(Unity.Collections.NativeArray`1<UnityEngine.ParticleSystem/Particle>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetParticles_mE37AF218E5BF5980DC625491EF3D52756F126BB8 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  ___particles0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		NativeArray_1_t653DD8A75D5D39F2C41C9C1734AC5351DE8252E0  L_0 = ___particles0;
		int32_t L_1;
		L_1 = ParticleSystem_GetParticles_m870578C1C1E37DFF7BC112662BAE81F3CA7AA411(__this, L_0, (-1), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::SetCustomParticleData(System.Collections.Generic.List`1<UnityEngine.Vector4>,UnityEngine.ParticleSystemCustomData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetCustomParticleData_m0304D334E835ECDA56DBFFA873AA75812143D543 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___customData0, int32_t ___streamIndex1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_SetCustomParticleData_m0304D334E835ECDA56DBFFA873AA75812143D543_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *, int32_t);
	static ParticleSystem_SetCustomParticleData_m0304D334E835ECDA56DBFFA873AA75812143D543_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_SetCustomParticleData_m0304D334E835ECDA56DBFFA873AA75812143D543_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::SetCustomParticleData(System.Collections.Generic.List`1<UnityEngine.Vector4>,UnityEngine.ParticleSystemCustomData)");
	_il2cpp_icall_func(__this, ___customData0, ___streamIndex1);
}
// System.Int32 UnityEngine.ParticleSystem::GetCustomParticleData(System.Collections.Generic.List`1<UnityEngine.Vector4>,UnityEngine.ParticleSystemCustomData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystem_GetCustomParticleData_m7EC7DF1370D12E9A5825D8BE1C249672C4794B6B (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___customData0, int32_t ___streamIndex1, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystem_GetCustomParticleData_m7EC7DF1370D12E9A5825D8BE1C249672C4794B6B_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *, int32_t);
	static ParticleSystem_GetCustomParticleData_m7EC7DF1370D12E9A5825D8BE1C249672C4794B6B_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetCustomParticleData_m7EC7DF1370D12E9A5825D8BE1C249672C4794B6B_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetCustomParticleData(System.Collections.Generic.List`1<UnityEngine.Vector4>,UnityEngine.ParticleSystemCustomData)");
	int32_t icallRetVal = _il2cpp_icall_func(__this, ___customData0, ___streamIndex1);
	return icallRetVal;
}
// UnityEngine.ParticleSystem/PlaybackState UnityEngine.ParticleSystem::GetPlaybackState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F  ParticleSystem_GetPlaybackState_m94CFA6AAEF043DBDEE0C6DC3833B32639AA3BA63 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		ParticleSystem_GetPlaybackState_Injected_m6D3692AF2279B0B9C9FDFF01A9B0D4B21AD19C4D(__this, (PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F *)(&V_0), /*hidden argument*/NULL);
		PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.ParticleSystem::SetPlaybackState(UnityEngine.ParticleSystem/PlaybackState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetPlaybackState_m9BE2B963312B05416B540B897C21BAADFCE76E59 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F  ___playbackState0, const RuntimeMethod* method)
{
	{
		ParticleSystem_SetPlaybackState_Injected_m8555985301715DFAAE85761592EEFE881B32EBFC(__this, (PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F *)(&___playbackState0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::GetTrailDataInternal(UnityEngine.ParticleSystem/Trails&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_GetTrailDataInternal_m03273FD85CB69A3D816E91E7D39AAAF58F5BCF78 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Trails_tE9352AAF21D22D5007179B507019C44C823013F2 * ___trailData0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_GetTrailDataInternal_m03273FD85CB69A3D816E91E7D39AAAF58F5BCF78_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, Trails_tE9352AAF21D22D5007179B507019C44C823013F2 *);
	static ParticleSystem_GetTrailDataInternal_m03273FD85CB69A3D816E91E7D39AAAF58F5BCF78_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetTrailDataInternal_m03273FD85CB69A3D816E91E7D39AAAF58F5BCF78_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetTrailDataInternal(UnityEngine.ParticleSystem/Trails&)");
	_il2cpp_icall_func(__this, ___trailData0);
}
// UnityEngine.ParticleSystem/Trails UnityEngine.ParticleSystem::GetTrails()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Trails_tE9352AAF21D22D5007179B507019C44C823013F2  ParticleSystem_GetTrails_m99FE1513C3114EA0D1FA05F10B456A8CE6F2610B (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m8C48C3F6EF447031F54430F3EF5AEB57666345E6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Trails_tE9352AAF21D22D5007179B507019C44C823013F2  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Trails_tE9352AAF21D22D5007179B507019C44C823013F2  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Trails_tE9352AAF21D22D5007179B507019C44C823013F2  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		il2cpp_codegen_initobj((&V_1), sizeof(Trails_tE9352AAF21D22D5007179B507019C44C823013F2 ));
		List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * L_0 = (List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A *)il2cpp_codegen_object_new(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A_il2cpp_TypeInfo_var);
		List_1__ctor_m8C48C3F6EF447031F54430F3EF5AEB57666345E6(L_0, /*hidden argument*/List_1__ctor_m8C48C3F6EF447031F54430F3EF5AEB57666345E6_RuntimeMethod_var);
		(&V_1)->set_positions_0(L_0);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_1 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)il2cpp_codegen_object_new(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD(L_1, /*hidden argument*/List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		(&V_1)->set_frontPositions_1(L_1);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_2 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)il2cpp_codegen_object_new(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD(L_2, /*hidden argument*/List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		(&V_1)->set_backPositions_2(L_2);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_3 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)il2cpp_codegen_object_new(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD(L_3, /*hidden argument*/List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		(&V_1)->set_positionCounts_3(L_3);
		Trails_tE9352AAF21D22D5007179B507019C44C823013F2  L_4 = V_1;
		V_0 = L_4;
		ParticleSystem_GetTrailDataInternal_m03273FD85CB69A3D816E91E7D39AAAF58F5BCF78(__this, (Trails_tE9352AAF21D22D5007179B507019C44C823013F2 *)(&V_0), /*hidden argument*/NULL);
		Trails_tE9352AAF21D22D5007179B507019C44C823013F2  L_5 = V_0;
		V_2 = L_5;
		goto IL_0048;
	}

IL_0048:
	{
		Trails_tE9352AAF21D22D5007179B507019C44C823013F2  L_6 = V_2;
		return L_6;
	}
}
// System.Void UnityEngine.ParticleSystem::SetTrails(UnityEngine.ParticleSystem/Trails)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetTrails_mD865F6A447580EDDCC2568FD900C6027E9F89E66 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Trails_tE9352AAF21D22D5007179B507019C44C823013F2  ___trailData0, const RuntimeMethod* method)
{
	{
		ParticleSystem_SetTrails_Injected_m2B3F93DBAD65DB5A22DC136395BA9D329106BC92(__this, (Trails_tE9352AAF21D22D5007179B507019C44C823013F2 *)(&___trailData0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Simulate_m68A0105281A8BC3F4E9242181586483BA7B796A3 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___t0, bool ___withChildren1, bool ___restart2, bool ___fixedTimeStep3, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Simulate_m68A0105281A8BC3F4E9242181586483BA7B796A3_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, float, bool, bool, bool);
	static ParticleSystem_Simulate_m68A0105281A8BC3F4E9242181586483BA7B796A3_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Simulate_m68A0105281A8BC3F4E9242181586483BA7B796A3_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___t0, ___withChildren1, ___restart2, ___fixedTimeStep3);
}
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Simulate_mC2F2E060D7CE94D4936BA995C49827231DF5F1F8 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___t0, bool ___withChildren1, bool ___restart2, const RuntimeMethod* method)
{
	{
		float L_0 = ___t0;
		bool L_1 = ___withChildren1;
		bool L_2 = ___restart2;
		ParticleSystem_Simulate_m68A0105281A8BC3F4E9242181586483BA7B796A3(__this, L_0, L_1, L_2, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Simulate_m36BAFFACB58953B2A46AA0B86E1F59C0FD6CD9C7 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___t0, bool ___withChildren1, const RuntimeMethod* method)
{
	{
		float L_0 = ___t0;
		bool L_1 = ___withChildren1;
		ParticleSystem_Simulate_mC2F2E060D7CE94D4936BA995C49827231DF5F1F8(__this, L_0, L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Simulate_m2628543E52E4D8D90A7B592F9C6222626F340F60 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, float ___t0, const RuntimeMethod* method)
{
	{
		float L_0 = ___t0;
		ParticleSystem_Simulate_m36BAFFACB58953B2A46AA0B86E1F59C0FD6CD9C7(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Play_m97D70BAF373265E633ACD91833E56D981B19958A (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Play_m97D70BAF373265E633ACD91833E56D981B19958A_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, bool);
	static ParticleSystem_Play_m97D70BAF373265E633ACD91833E56D981B19958A_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Play_m97D70BAF373265E633ACD91833E56D981B19958A_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Play(System.Boolean)");
	_il2cpp_icall_func(__this, ___withChildren0);
}
// System.Void UnityEngine.ParticleSystem::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Play_m28D27CC4CDC1D93195C75647E6F6DAECF8B6BC50 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	{
		ParticleSystem_Play_m97D70BAF373265E633ACD91833E56D981B19958A(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Pause(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Pause_m116581497C92159D3210C733917CD2515399A0CD (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Pause_m116581497C92159D3210C733917CD2515399A0CD_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, bool);
	static ParticleSystem_Pause_m116581497C92159D3210C733917CD2515399A0CD_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Pause_m116581497C92159D3210C733917CD2515399A0CD_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Pause(System.Boolean)");
	_il2cpp_icall_func(__this, ___withChildren0);
}
// System.Void UnityEngine.ParticleSystem::Pause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Pause_mA5AE4D5A290E9DD75A0572738CB0910D6A7E2121 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	{
		ParticleSystem_Pause_m116581497C92159D3210C733917CD2515399A0CD(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Stop_m275B200BC21580C60987EC8FC8B2DD0FEB365C1A (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___withChildren0, int32_t ___stopBehavior1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Stop_m275B200BC21580C60987EC8FC8B2DD0FEB365C1A_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, bool, int32_t);
	static ParticleSystem_Stop_m275B200BC21580C60987EC8FC8B2DD0FEB365C1A_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Stop_m275B200BC21580C60987EC8FC8B2DD0FEB365C1A_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)");
	_il2cpp_icall_func(__this, ___withChildren0, ___stopBehavior1);
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Stop_m5EC8B81A8BD1F8C90729E766789EE5D8D4EC64FB (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___withChildren0;
		ParticleSystem_Stop_m275B200BC21580C60987EC8FC8B2DD0FEB365C1A(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Stop_m8CBF9268DE7B5A40952B4977462B857B5F5AFB9D (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	{
		ParticleSystem_Stop_m5EC8B81A8BD1F8C90729E766789EE5D8D4EC64FB(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Clear(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Clear_m9E873134B8055ACFEE9DC66F7E4FABE178E27057 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Clear_m9E873134B8055ACFEE9DC66F7E4FABE178E27057_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, bool);
	static ParticleSystem_Clear_m9E873134B8055ACFEE9DC66F7E4FABE178E27057_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Clear_m9E873134B8055ACFEE9DC66F7E4FABE178E27057_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Clear(System.Boolean)");
	_il2cpp_icall_func(__this, ___withChildren0);
}
// System.Void UnityEngine.ParticleSystem::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Clear_mD8C9DCD1267F221B0546E4B9B55DBD9354893797 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	{
		ParticleSystem_Clear_m9E873134B8055ACFEE9DC66F7E4FABE178E27057(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_IsAlive_m43E2E84732EBDF5C4C9FD6575051A2A2B135BCF9 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystem_IsAlive_m43E2E84732EBDF5C4C9FD6575051A2A2B135BCF9_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, bool);
	static ParticleSystem_IsAlive_m43E2E84732EBDF5C4C9FD6575051A2A2B135BCF9_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_IsAlive_m43E2E84732EBDF5C4C9FD6575051A2A2B135BCF9_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::IsAlive(System.Boolean)");
	bool icallRetVal = _il2cpp_icall_func(__this, ___withChildren0);
	return icallRetVal;
}
// System.Boolean UnityEngine.ParticleSystem::IsAlive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystem_IsAlive_m59D28AC79A1A5FD1D97523D83D687ACFA2510198 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0;
		L_0 = ParticleSystem_IsAlive_m43E2E84732EBDF5C4C9FD6575051A2A2B135BCF9(__this, (bool)1, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000b;
	}

IL_000b:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.ParticleSystem::Emit(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Emit_m07EF0D2DA84EB04814DA7EE6B8618B008DE75F28 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___count0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___count0;
		ParticleSystem_Emit_Internal_m7C72C31F7F4875B54B00E255A450B045A4449646(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Emit_Internal(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Emit_Internal_m7C72C31F7F4875B54B00E255A450B045A4449646 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___count0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Emit_Internal_m7C72C31F7F4875B54B00E255A450B045A4449646_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, int32_t);
	static ParticleSystem_Emit_Internal_m7C72C31F7F4875B54B00E255A450B045A4449646_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Emit_Internal_m7C72C31F7F4875B54B00E255A450B045A4449646_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Emit_Internal(System.Int32)");
	_il2cpp_icall_func(__this, ___count0);
}
// System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/EmitParams,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Emit_m1598252E2EF701A5010EFA395A87368495E9F9F7 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, EmitParams_t4F6429654653488A5D430701CD0743D011807CCC  ___emitParams0, int32_t ___count1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___count1;
		ParticleSystem_Emit_Injected_mC31E36D577A2D3135436438BFC27B6C76D9ADC72(__this, (EmitParams_t4F6429654653488A5D430701CD0743D011807CCC *)(&___emitParams0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::EmitOld_Internal(UnityEngine.ParticleSystem/Particle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_EmitOld_Internal_m4F094DC523986298D5626F0F3F2335DFF596C993 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * ___particle0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_EmitOld_Internal_m4F094DC523986298D5626F0F3F2335DFF596C993_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *);
	static ParticleSystem_EmitOld_Internal_m4F094DC523986298D5626F0F3F2335DFF596C993_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_EmitOld_Internal_m4F094DC523986298D5626F0F3F2335DFF596C993_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::EmitOld_Internal(UnityEngine.ParticleSystem/Particle&)");
	_il2cpp_icall_func(__this, ___particle0);
}
// System.Void UnityEngine.ParticleSystem::TriggerSubEmitter(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_TriggerSubEmitter_m9D4541DB830D5C80CD4BF88C3C8B8A8A0C4DFBFA (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___subEmitterIndex0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___subEmitterIndex0;
		ParticleSystem_TriggerSubEmitter_mBEB9C15312BB6AD76AF9B42B2CE184B7149F4C98(__this, L_0, (List_1_tA1D5161C2AD941530756A2128C933F0D76F48E95 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::TriggerSubEmitter(System.Int32,UnityEngine.ParticleSystem/Particle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_TriggerSubEmitter_m54FED6396579982E9C0AF774853E9A2290379A37 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___subEmitterIndex0, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * ___particle1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___subEmitterIndex0;
		Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * L_1 = ___particle1;
		Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  L_2 = (*(Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)L_1);
		ParticleSystem_TriggerSubEmitterForParticle_mCC9F7D09CA9D8DB5576389ABCFC287A21952003A(__this, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::TriggerSubEmitterForParticle(System.Int32,UnityEngine.ParticleSystem/Particle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_TriggerSubEmitterForParticle_mCC9F7D09CA9D8DB5576389ABCFC287A21952003A (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___subEmitterIndex0, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  ___particle1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___subEmitterIndex0;
		ParticleSystem_TriggerSubEmitterForParticle_Injected_mB9BC0EBDC5CC0D5C392B7287351A8D85337FBBE0(__this, L_0, (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)(&___particle1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::TriggerSubEmitter(System.Int32,System.Collections.Generic.List`1<UnityEngine.ParticleSystem/Particle>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_TriggerSubEmitter_mBEB9C15312BB6AD76AF9B42B2CE184B7149F4C98 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___subEmitterIndex0, List_1_tA1D5161C2AD941530756A2128C933F0D76F48E95 * ___particles1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_TriggerSubEmitter_mBEB9C15312BB6AD76AF9B42B2CE184B7149F4C98_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, int32_t, List_1_tA1D5161C2AD941530756A2128C933F0D76F48E95 *);
	static ParticleSystem_TriggerSubEmitter_mBEB9C15312BB6AD76AF9B42B2CE184B7149F4C98_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_TriggerSubEmitter_mBEB9C15312BB6AD76AF9B42B2CE184B7149F4C98_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::TriggerSubEmitter(System.Int32,System.Collections.Generic.List`1<UnityEngine.ParticleSystem/Particle>)");
	_il2cpp_icall_func(__this, ___subEmitterIndex0, ___particles1);
}
// System.Void UnityEngine.ParticleSystem::ResetPreMappedBufferMemory()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_ResetPreMappedBufferMemory_m1B0AEE23A61FA29580513BD4258B263C4F7FAAC3 (const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_ResetPreMappedBufferMemory_m1B0AEE23A61FA29580513BD4258B263C4F7FAAC3_ftn) ();
	static ParticleSystem_ResetPreMappedBufferMemory_m1B0AEE23A61FA29580513BD4258B263C4F7FAAC3_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_ResetPreMappedBufferMemory_m1B0AEE23A61FA29580513BD4258B263C4F7FAAC3_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::ResetPreMappedBufferMemory()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.ParticleSystem::SetMaximumPreMappedBufferCounts(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetMaximumPreMappedBufferCounts_m483102DE3EFE86F3E6721C16942E04130B3CAE3F (int32_t ___vertexBuffersCount0, int32_t ___indexBuffersCount1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_SetMaximumPreMappedBufferCounts_m483102DE3EFE86F3E6721C16942E04130B3CAE3F_ftn) (int32_t, int32_t);
	static ParticleSystem_SetMaximumPreMappedBufferCounts_m483102DE3EFE86F3E6721C16942E04130B3CAE3F_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_SetMaximumPreMappedBufferCounts_m483102DE3EFE86F3E6721C16942E04130B3CAE3F_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::SetMaximumPreMappedBufferCounts(System.Int32,System.Int32)");
	_il2cpp_icall_func(___vertexBuffersCount0, ___indexBuffersCount1);
}
// System.Void UnityEngine.ParticleSystem::AllocateAxisOfRotationAttribute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_AllocateAxisOfRotationAttribute_mDE6232FD099AD8F1F6F0DF3726C1C32E4729E1D5 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_AllocateAxisOfRotationAttribute_mDE6232FD099AD8F1F6F0DF3726C1C32E4729E1D5_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *);
	static ParticleSystem_AllocateAxisOfRotationAttribute_mDE6232FD099AD8F1F6F0DF3726C1C32E4729E1D5_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_AllocateAxisOfRotationAttribute_mDE6232FD099AD8F1F6F0DF3726C1C32E4729E1D5_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::AllocateAxisOfRotationAttribute()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::AllocateMeshIndexAttribute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_AllocateMeshIndexAttribute_m25F7096688B776F1CAF7F8F6D6F6222B41AF9E18 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_AllocateMeshIndexAttribute_m25F7096688B776F1CAF7F8F6D6F6222B41AF9E18_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *);
	static ParticleSystem_AllocateMeshIndexAttribute_m25F7096688B776F1CAF7F8F6D6F6222B41AF9E18_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_AllocateMeshIndexAttribute_m25F7096688B776F1CAF7F8F6D6F6222B41AF9E18_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::AllocateMeshIndexAttribute()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::AllocateCustomDataAttribute(UnityEngine.ParticleSystemCustomData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_AllocateCustomDataAttribute_mAB843E372C9D175730E0730DB54FB34AF7A1DC7F (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___stream0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_AllocateCustomDataAttribute_mAB843E372C9D175730E0730DB54FB34AF7A1DC7F_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, int32_t);
	static ParticleSystem_AllocateCustomDataAttribute_mAB843E372C9D175730E0730DB54FB34AF7A1DC7F_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_AllocateCustomDataAttribute_mAB843E372C9D175730E0730DB54FB34AF7A1DC7F_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::AllocateCustomDataAttribute(UnityEngine.ParticleSystemCustomData)");
	_il2cpp_icall_func(__this, ___stream0);
}
// System.Void* UnityEngine.ParticleSystem::GetManagedJobData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void* ParticleSystem_GetManagedJobData_mAE6749636F4F3A7EBD7775AE4B8E9BFD24C2CF95 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	typedef void* (*ParticleSystem_GetManagedJobData_mAE6749636F4F3A7EBD7775AE4B8E9BFD24C2CF95_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *);
	static ParticleSystem_GetManagedJobData_mAE6749636F4F3A7EBD7775AE4B8E9BFD24C2CF95_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetManagedJobData_mAE6749636F4F3A7EBD7775AE4B8E9BFD24C2CF95_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetManagedJobData()");
	void* icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// Unity.Jobs.JobHandle UnityEngine.ParticleSystem::GetManagedJobHandle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847  ParticleSystem_GetManagedJobHandle_m63494F9028CE37DDF08822AFF19828A2E0A06C6E (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		ParticleSystem_GetManagedJobHandle_Injected_m294A930F43B3561DDE1ABD8512F7773C3D7B4318(__this, (JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847 *)(&V_0), /*hidden argument*/NULL);
		JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.ParticleSystem::SetManagedJobHandle(Unity.Jobs.JobHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetManagedJobHandle_mFD5D834CE36338E182A246668AD58E3CD09A6608 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847  ___handle0, const RuntimeMethod* method)
{
	{
		ParticleSystem_SetManagedJobHandle_Injected_m58056683B09174D75E69BFF45F191CB91542617C(__this, (JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847 *)(&___handle0), /*hidden argument*/NULL);
		return;
	}
}
// Unity.Jobs.JobHandle UnityEngine.ParticleSystem::ScheduleManagedJob(Unity.Jobs.LowLevel.Unsafe.JobsUtility/JobScheduleParameters&,System.Void*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847  ParticleSystem_ScheduleManagedJob_m7B340DC400DAAD69AFC906EA0BAD0D7C3459CB89 (JobScheduleParameters_tA29064729507A938ACB05D9F98B2C4A6977E3EF8 * ___parameters0, void* ___additionalData1, const RuntimeMethod* method)
{
	JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		JobScheduleParameters_tA29064729507A938ACB05D9F98B2C4A6977E3EF8 * L_0 = ___parameters0;
		void* L_1 = ___additionalData1;
		ParticleSystem_ScheduleManagedJob_Injected_m2C68F3306AA63CBE2D5F1D2FB6A6571B031255A9((JobScheduleParameters_tA29064729507A938ACB05D9F98B2C4A6977E3EF8 *)L_0, (void*)(void*)L_1, (JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847 *)(&V_0), /*hidden argument*/NULL);
		JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::CopyManagedJobData(System.Void*,UnityEngine.ParticleSystemJobs.NativeParticleData&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_CopyManagedJobData_mF72E9C4B5DE2228B29D4AB03ADFC3499BA009D95 (void* ___systemPtr0, NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD * ___particleData1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_CopyManagedJobData_mF72E9C4B5DE2228B29D4AB03ADFC3499BA009D95_ftn) (void*, NativeParticleData_tD631B454D2194AF5A4B56E9A7648BC16EA0F15FD *);
	static ParticleSystem_CopyManagedJobData_mF72E9C4B5DE2228B29D4AB03ADFC3499BA009D95_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_CopyManagedJobData_mF72E9C4B5DE2228B29D4AB03ADFC3499BA009D95_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::CopyManagedJobData(System.Void*,UnityEngine.ParticleSystemJobs.NativeParticleData&)");
	_il2cpp_icall_func(___systemPtr0, ___particleData1);
}
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_0;
		memset((&L_0), 0, sizeof(L_0));
		MainModule__ctor_m34F626B568C6D3C80A036409049761C8316E6A76((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  ParticleSystem_get_emission_m0BAA1EDF775A8712DA7D62BF3C42A2B1A6C96CA1 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  L_0;
		memset((&L_0), 0, sizeof(L_0));
		EmissionModule__ctor_m9DFD2A0BB8BFAD2191C54E6627FC3B0E641EDD51((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/ShapeModule UnityEngine.ParticleSystem::get_shape()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA  ParticleSystem_get_shape_m986023201B5140A525EF34F81DAAF1866D889052 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA  L_0;
		memset((&L_0), 0, sizeof(L_0));
		ShapeModule__ctor_mB0CE9D41BBE5FFA5B70F465541BA63D9BEEA7264((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/VelocityOverLifetimeModule UnityEngine.ParticleSystem::get_velocityOverLifetime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924  ParticleSystem_get_velocityOverLifetime_mA0FEC4CD424A882B27AEBDDB06CBFA61951C2868 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924  L_0;
		memset((&L_0), 0, sizeof(L_0));
		VelocityOverLifetimeModule__ctor_m03EB5FA8BB69309D7DA91CE70EE3A9E65C60AE66((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule UnityEngine.ParticleSystem::get_limitVelocityOverLifetime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C  ParticleSystem_get_limitVelocityOverLifetime_m25ABE96DD9F88188874A538AA4992A72EC4AE394 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C  L_0;
		memset((&L_0), 0, sizeof(L_0));
		LimitVelocityOverLifetimeModule__ctor_mA327985B69E145AAC3FFEE42EE44A81D7800BCCC((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/InheritVelocityModule UnityEngine.ParticleSystem::get_inheritVelocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619  ParticleSystem_get_inheritVelocity_mE8C58E2805CB8A7C39F39D7ECE0F3978EAEDBC0A (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619  L_0;
		memset((&L_0), 0, sizeof(L_0));
		InheritVelocityModule__ctor_m76B46612309334A443F9939A47FBAAF024606892((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/LifetimeByEmitterSpeedModule UnityEngine.ParticleSystem::get_lifetimeByEmitterSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1  ParticleSystem_get_lifetimeByEmitterSpeed_m4901408692AC775C037D2D16F55810D9A574BB7B (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1  L_0;
		memset((&L_0), 0, sizeof(L_0));
		LifetimeByEmitterSpeedModule__ctor_m80A22170A2E3047A3B364FE232CC4EB289F87BED((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/ForceOverLifetimeModule UnityEngine.ParticleSystem::get_forceOverLifetime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7  ParticleSystem_get_forceOverLifetime_m044BAE00399D6EB3369C001237E7D8C11BCA3874 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7  L_0;
		memset((&L_0), 0, sizeof(L_0));
		ForceOverLifetimeModule__ctor_m4FF722421C5B1449CB900FF0506EF33549A450CD((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/ColorOverLifetimeModule UnityEngine.ParticleSystem::get_colorOverLifetime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75  ParticleSystem_get_colorOverLifetime_m79598BD17DFAC4CA4C0CAFEA363BA9212000FC31 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75  L_0;
		memset((&L_0), 0, sizeof(L_0));
		ColorOverLifetimeModule__ctor_m3AFAD44AD8E69F213881272693C23536EEBAB71D((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/ColorBySpeedModule UnityEngine.ParticleSystem::get_colorBySpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F  ParticleSystem_get_colorBySpeed_mF7C88FB024568B5DB6D06DE5C45965FC28633520 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F  L_0;
		memset((&L_0), 0, sizeof(L_0));
		ColorBySpeedModule__ctor_m71C9D80CE70BB41F782B4C262D0FE40477A7D0AF((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/SizeOverLifetimeModule UnityEngine.ParticleSystem::get_sizeOverLifetime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD  ParticleSystem_get_sizeOverLifetime_m4A532D865E6EA79D745D8C4792CCA4072EC7AA05 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD  L_0;
		memset((&L_0), 0, sizeof(L_0));
		SizeOverLifetimeModule__ctor_mCA80885490FC3B1010887B10BDE21A0DACEFB557((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/SizeBySpeedModule UnityEngine.ParticleSystem::get_sizeBySpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752  ParticleSystem_get_sizeBySpeed_m0D7FF6F71988E4A1ACF8DD2257E06F5CB6580439 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752  L_0;
		memset((&L_0), 0, sizeof(L_0));
		SizeBySpeedModule__ctor_m479D4802A862D4E07AAF2B47833852A62A17F6DE((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/RotationOverLifetimeModule UnityEngine.ParticleSystem::get_rotationOverLifetime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B  ParticleSystem_get_rotationOverLifetime_mF5F527E0112C591D297C13CD60A7B888B98E9DEF (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B  L_0;
		memset((&L_0), 0, sizeof(L_0));
		RotationOverLifetimeModule__ctor_mA0E0BDF5A88582DFEC3FAB42F8B237611C8E0182((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/RotationBySpeedModule UnityEngine.ParticleSystem::get_rotationBySpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD  ParticleSystem_get_rotationBySpeed_m2ED46A54093DDB496DC8148D54FCB01D0BC6FE2B (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD  L_0;
		memset((&L_0), 0, sizeof(L_0));
		RotationBySpeedModule__ctor_mCEAFF0BBEC6AB5FB3E59FD1584B22079A2142248((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/ExternalForcesModule UnityEngine.ParticleSystem::get_externalForces()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5  ParticleSystem_get_externalForces_mA09E3DE23F158D76C503A32C3DECBF81FE5FE7E9 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5  L_0;
		memset((&L_0), 0, sizeof(L_0));
		ExternalForcesModule__ctor_m2FB0D6776AAAB6731E3E80BA6ADD858458976DF5((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/NoiseModule UnityEngine.ParticleSystem::get_noise()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591  ParticleSystem_get_noise_m4059B8305FDEEE3FDE2DA67DBA2B56793D280EF8 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591  L_0;
		memset((&L_0), 0, sizeof(L_0));
		NoiseModule__ctor_mE450BC95A9B436FA1627E4D24B7B4E7E723DDB5B((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/CollisionModule UnityEngine.ParticleSystem::get_collision()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09  ParticleSystem_get_collision_m1914663D5844842893E1F33CF4BFA1D5B3DF4AC1 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09  L_0;
		memset((&L_0), 0, sizeof(L_0));
		CollisionModule__ctor_m08D1EEAC52F7833F28D4FC2563120FF46A9E4220((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/TriggerModule UnityEngine.ParticleSystem::get_trigger()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8  ParticleSystem_get_trigger_m0CB1A47823806DA2671918708FEC768C754B5120 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8  L_0;
		memset((&L_0), 0, sizeof(L_0));
		TriggerModule__ctor_mEB88CDC59DC3AFBC343D3042488445F6E8A5AE9D((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/SubEmittersModule UnityEngine.ParticleSystem::get_subEmitters()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C  ParticleSystem_get_subEmitters_mCFE3CAEF76ADEAF3754210EC731A54A9DA836428 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C  L_0;
		memset((&L_0), 0, sizeof(L_0));
		SubEmittersModule__ctor_m39DB9FE3DF4B1E15890712EF4E4024818D568A3C((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/TextureSheetAnimationModule UnityEngine.ParticleSystem::get_textureSheetAnimation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2  ParticleSystem_get_textureSheetAnimation_mE7D4FF28B018DD7BB94904CE014F5FD56E53AA90 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2  L_0;
		memset((&L_0), 0, sizeof(L_0));
		TextureSheetAnimationModule__ctor_mF02B5303373B93D856A5BDF92B4E4AABC7CB2F65((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/LightsModule UnityEngine.ParticleSystem::get_lights()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705  ParticleSystem_get_lights_mBAA747B7858C2C39BF4D5502D3026FDA6A9DCE52 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705  L_0;
		memset((&L_0), 0, sizeof(L_0));
		LightsModule__ctor_mD59B002A60EB418DACA4986EC1C4D34B8DAC555F((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/TrailModule UnityEngine.ParticleSystem::get_trails()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8  ParticleSystem_get_trails_m9514134A5A05CCDEF2C901BC0D60FC287959414C (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8  L_0;
		memset((&L_0), 0, sizeof(L_0));
		TrailModule__ctor_m15960ADC2381E58BE70FF2653288599694915412((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/CustomDataModule UnityEngine.ParticleSystem::get_customData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF  ParticleSystem_get_customData_m7112FE08433F44A3425142B2D4862F8CD889A913 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF  L_0;
		memset((&L_0), 0, sizeof(L_0));
		CustomDataModule__ctor_m8F46AC54F3CC41A63AFE9856B18CC41A9A237277((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.ParticleSystem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem__ctor_mEC46AD331B88935F1CFBAA282FD480C53E55ACDC (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method)
{
	{
		Component__ctor_m0B00FA207EB3E560B78938D8AD877DB2BC1E3722(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::GetParticleCurrentSize3D_Injected(UnityEngine.ParticleSystem/Particle&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_GetParticleCurrentSize3D_Injected_mE1D7D2457CDC32E35F9D3E06721D773667F2A966 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * ___particle0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___ret1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_GetParticleCurrentSize3D_Injected_mE1D7D2457CDC32E35F9D3E06721D773667F2A966_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *);
	static ParticleSystem_GetParticleCurrentSize3D_Injected_mE1D7D2457CDC32E35F9D3E06721D773667F2A966_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetParticleCurrentSize3D_Injected_mE1D7D2457CDC32E35F9D3E06721D773667F2A966_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetParticleCurrentSize3D_Injected(UnityEngine.ParticleSystem/Particle&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___particle0, ___ret1);
}
// System.Void UnityEngine.ParticleSystem::GetParticleCurrentColor_Injected(UnityEngine.ParticleSystem/Particle&,UnityEngine.Color32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_GetParticleCurrentColor_Injected_m98C468104D268559CF2F06F78F970F1814F7A587 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * ___particle0, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * ___ret1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_GetParticleCurrentColor_Injected_m98C468104D268559CF2F06F78F970F1814F7A587_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D *);
	static ParticleSystem_GetParticleCurrentColor_Injected_m98C468104D268559CF2F06F78F970F1814F7A587_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetParticleCurrentColor_Injected_m98C468104D268559CF2F06F78F970F1814F7A587_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetParticleCurrentColor_Injected(UnityEngine.ParticleSystem/Particle&,UnityEngine.Color32&)");
	_il2cpp_icall_func(__this, ___particle0, ___ret1);
}
// System.Void UnityEngine.ParticleSystem::GetPlaybackState_Injected(UnityEngine.ParticleSystem/PlaybackState&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_GetPlaybackState_Injected_m6D3692AF2279B0B9C9FDFF01A9B0D4B21AD19C4D (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F * ___ret0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_GetPlaybackState_Injected_m6D3692AF2279B0B9C9FDFF01A9B0D4B21AD19C4D_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F *);
	static ParticleSystem_GetPlaybackState_Injected_m6D3692AF2279B0B9C9FDFF01A9B0D4B21AD19C4D_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetPlaybackState_Injected_m6D3692AF2279B0B9C9FDFF01A9B0D4B21AD19C4D_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetPlaybackState_Injected(UnityEngine.ParticleSystem/PlaybackState&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.ParticleSystem::SetPlaybackState_Injected(UnityEngine.ParticleSystem/PlaybackState&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetPlaybackState_Injected_m8555985301715DFAAE85761592EEFE881B32EBFC (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F * ___playbackState0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_SetPlaybackState_Injected_m8555985301715DFAAE85761592EEFE881B32EBFC_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, PlaybackState_tC0082FD121458A470D89E5F633036E73A36DFB8F *);
	static ParticleSystem_SetPlaybackState_Injected_m8555985301715DFAAE85761592EEFE881B32EBFC_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_SetPlaybackState_Injected_m8555985301715DFAAE85761592EEFE881B32EBFC_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::SetPlaybackState_Injected(UnityEngine.ParticleSystem/PlaybackState&)");
	_il2cpp_icall_func(__this, ___playbackState0);
}
// System.Void UnityEngine.ParticleSystem::SetTrails_Injected(UnityEngine.ParticleSystem/Trails&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetTrails_Injected_m2B3F93DBAD65DB5A22DC136395BA9D329106BC92 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, Trails_tE9352AAF21D22D5007179B507019C44C823013F2 * ___trailData0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_SetTrails_Injected_m2B3F93DBAD65DB5A22DC136395BA9D329106BC92_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, Trails_tE9352AAF21D22D5007179B507019C44C823013F2 *);
	static ParticleSystem_SetTrails_Injected_m2B3F93DBAD65DB5A22DC136395BA9D329106BC92_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_SetTrails_Injected_m2B3F93DBAD65DB5A22DC136395BA9D329106BC92_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::SetTrails_Injected(UnityEngine.ParticleSystem/Trails&)");
	_il2cpp_icall_func(__this, ___trailData0);
}
// System.Void UnityEngine.ParticleSystem::Emit_Injected(UnityEngine.ParticleSystem/EmitParams&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Emit_Injected_mC31E36D577A2D3135436438BFC27B6C76D9ADC72 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, EmitParams_t4F6429654653488A5D430701CD0743D011807CCC * ___emitParams0, int32_t ___count1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Emit_Injected_mC31E36D577A2D3135436438BFC27B6C76D9ADC72_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, EmitParams_t4F6429654653488A5D430701CD0743D011807CCC *, int32_t);
	static ParticleSystem_Emit_Injected_mC31E36D577A2D3135436438BFC27B6C76D9ADC72_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Emit_Injected_mC31E36D577A2D3135436438BFC27B6C76D9ADC72_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Emit_Injected(UnityEngine.ParticleSystem/EmitParams&,System.Int32)");
	_il2cpp_icall_func(__this, ___emitParams0, ___count1);
}
// System.Void UnityEngine.ParticleSystem::TriggerSubEmitterForParticle_Injected(System.Int32,UnityEngine.ParticleSystem/Particle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_TriggerSubEmitterForParticle_Injected_mB9BC0EBDC5CC0D5C392B7287351A8D85337FBBE0 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, int32_t ___subEmitterIndex0, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * ___particle1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_TriggerSubEmitterForParticle_Injected_mB9BC0EBDC5CC0D5C392B7287351A8D85337FBBE0_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, int32_t, Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *);
	static ParticleSystem_TriggerSubEmitterForParticle_Injected_mB9BC0EBDC5CC0D5C392B7287351A8D85337FBBE0_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_TriggerSubEmitterForParticle_Injected_mB9BC0EBDC5CC0D5C392B7287351A8D85337FBBE0_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::TriggerSubEmitterForParticle_Injected(System.Int32,UnityEngine.ParticleSystem/Particle&)");
	_il2cpp_icall_func(__this, ___subEmitterIndex0, ___particle1);
}
// System.Void UnityEngine.ParticleSystem::GetManagedJobHandle_Injected(Unity.Jobs.JobHandle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_GetManagedJobHandle_Injected_m294A930F43B3561DDE1ABD8512F7773C3D7B4318 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_GetManagedJobHandle_Injected_m294A930F43B3561DDE1ABD8512F7773C3D7B4318_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847 *);
	static ParticleSystem_GetManagedJobHandle_Injected_m294A930F43B3561DDE1ABD8512F7773C3D7B4318_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetManagedJobHandle_Injected_m294A930F43B3561DDE1ABD8512F7773C3D7B4318_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetManagedJobHandle_Injected(Unity.Jobs.JobHandle&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.ParticleSystem::SetManagedJobHandle_Injected(Unity.Jobs.JobHandle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_SetManagedJobHandle_Injected_m58056683B09174D75E69BFF45F191CB91542617C (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847 * ___handle0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_SetManagedJobHandle_Injected_m58056683B09174D75E69BFF45F191CB91542617C_ftn) (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E *, JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847 *);
	static ParticleSystem_SetManagedJobHandle_Injected_m58056683B09174D75E69BFF45F191CB91542617C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_SetManagedJobHandle_Injected_m58056683B09174D75E69BFF45F191CB91542617C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::SetManagedJobHandle_Injected(Unity.Jobs.JobHandle&)");
	_il2cpp_icall_func(__this, ___handle0);
}
// System.Void UnityEngine.ParticleSystem::ScheduleManagedJob_Injected(Unity.Jobs.LowLevel.Unsafe.JobsUtility/JobScheduleParameters&,System.Void*,Unity.Jobs.JobHandle&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_ScheduleManagedJob_Injected_m2C68F3306AA63CBE2D5F1D2FB6A6571B031255A9 (JobScheduleParameters_tA29064729507A938ACB05D9F98B2C4A6977E3EF8 * ___parameters0, void* ___additionalData1, JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847 * ___ret2, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_ScheduleManagedJob_Injected_m2C68F3306AA63CBE2D5F1D2FB6A6571B031255A9_ftn) (JobScheduleParameters_tA29064729507A938ACB05D9F98B2C4A6977E3EF8 *, void*, JobHandle_t8AEB8D31C25D7774C71D62B0C662525E6E36D847 *);
	static ParticleSystem_ScheduleManagedJob_Injected_m2C68F3306AA63CBE2D5F1D2FB6A6571B031255A9_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_ScheduleManagedJob_Injected_m2C68F3306AA63CBE2D5F1D2FB6A6571B031255A9_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::ScheduleManagedJob_Injected(Unity.Jobs.LowLevel.Unsafe.JobsUtility/JobScheduleParameters&,System.Void*,Unity.Jobs.JobHandle&)");
	_il2cpp_icall_func(___parameters0, ___additionalData1, ___ret2);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.ParticleSystemRenderer::EnableVertexStreams(UnityEngine.ParticleSystemVertexStreams)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_EnableVertexStreams_mA2631E5FF8C6CA157E26581BDC47506837AB4696 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, int32_t ___streams0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___streams0;
		ParticleSystemRenderer_Internal_SetVertexStreams_mA00D4C6C868E8309E715015704CCFC12A4CBD8FD(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystemRenderer::DisableVertexStreams(UnityEngine.ParticleSystemVertexStreams)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_DisableVertexStreams_m1317B70228142564B45B3E354615237C312FA9F4 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, int32_t ___streams0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___streams0;
		ParticleSystemRenderer_Internal_SetVertexStreams_mA00D4C6C868E8309E715015704CCFC12A4CBD8FD(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystemRenderer::AreVertexStreamsEnabled(UnityEngine.ParticleSystemVertexStreams)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystemRenderer_AreVertexStreamsEnabled_m70D3591D4793FEE8BC879439968884E7889CBD04 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, int32_t ___streams0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = ___streams0;
		int32_t L_1;
		L_1 = ParticleSystemRenderer_Internal_GetEnabledVertexStreams_m1AA436781571C985E87EE8706AF264C977D7A979(__this, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___streams0;
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.ParticleSystemVertexStreams UnityEngine.ParticleSystemRenderer::GetEnabledVertexStreams(UnityEngine.ParticleSystemVertexStreams)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystemRenderer_GetEnabledVertexStreams_mA38CE0B9D50452BE6405ADF18050306C22A29CDC (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, int32_t ___streams0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___streams0;
		int32_t L_1;
		L_1 = ParticleSystemRenderer_Internal_GetEnabledVertexStreams_m1AA436781571C985E87EE8706AF264C977D7A979(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000b;
	}

IL_000b:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystemRenderer::Internal_SetVertexStreams(UnityEngine.ParticleSystemVertexStreams,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_Internal_SetVertexStreams_mA00D4C6C868E8309E715015704CCFC12A4CBD8FD (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, int32_t ___streams0, bool ___enabled1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m50FEC7E4E5C76F2F1388964CF2AF151F6BC4216C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	bool V_10 = false;
	bool V_11 = false;
	bool V_12 = false;
	bool V_13 = false;
	bool V_14 = false;
	bool V_15 = false;
	bool V_16 = false;
	bool V_17 = false;
	bool V_18 = false;
	bool V_19 = false;
	bool V_20 = false;
	bool V_21 = false;
	bool V_22 = false;
	bool V_23 = false;
	bool V_24 = false;
	bool V_25 = false;
	bool V_26 = false;
	bool V_27 = false;
	bool V_28 = false;
	bool V_29 = false;
	bool V_30 = false;
	bool V_31 = false;
	bool V_32 = false;
	bool V_33 = false;
	bool V_34 = false;
	bool V_35 = false;
	bool V_36 = false;
	bool V_37 = false;
	bool V_38 = false;
	bool V_39 = false;
	bool V_40 = false;
	bool V_41 = false;
	bool V_42 = false;
	bool V_43 = false;
	{
		int32_t L_0;
		L_0 = ParticleSystemRenderer_get_activeVertexStreamsCount_m948DD12ED56E758FF7F55DF8D1D9B6D644A3D3FE(__this, /*hidden argument*/NULL);
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_1 = (List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 *)il2cpp_codegen_object_new(List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0_il2cpp_TypeInfo_var);
		List_1__ctor_m50FEC7E4E5C76F2F1388964CF2AF151F6BC4216C(L_1, L_0, /*hidden argument*/List_1__ctor_m50FEC7E4E5C76F2F1388964CF2AF151F6BC4216C_RuntimeMethod_var);
		V_0 = L_1;
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_2 = V_0;
		ParticleSystemRenderer_GetActiveVertexStreams_m32C738BAD8EAB54463B9BBCA2CAA0324BC702014(__this, L_2, /*hidden argument*/NULL);
		bool L_3 = ___enabled1;
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_02ab;
		}
	}
	{
		int32_t L_5 = ___streams0;
		V_2 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_5&(int32_t)1))) <= ((uint32_t)0)))? 1 : 0);
		bool L_6 = V_2;
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_7 = V_0;
		bool L_8;
		L_8 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_7, 0, /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_3 = (bool)((((int32_t)L_8) == ((int32_t)0))? 1 : 0);
		bool L_9 = V_3;
		if (!L_9)
		{
			goto IL_0041;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_10 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_10, 0, /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
	}

IL_0041:
	{
	}

IL_0042:
	{
		int32_t L_11 = ___streams0;
		V_4 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_11&(int32_t)2))) <= ((uint32_t)0)))? 1 : 0);
		bool L_12 = V_4;
		if (!L_12)
		{
			goto IL_006a;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_13 = V_0;
		bool L_14;
		L_14 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_13, 1, /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_5 = (bool)((((int32_t)L_14) == ((int32_t)0))? 1 : 0);
		bool L_15 = V_5;
		if (!L_15)
		{
			goto IL_0069;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_16 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_16, 1, /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
	}

IL_0069:
	{
	}

IL_006a:
	{
		int32_t L_17 = ___streams0;
		V_6 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_17&(int32_t)4))) <= ((uint32_t)0)))? 1 : 0);
		bool L_18 = V_6;
		if (!L_18)
		{
			goto IL_0092;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_19 = V_0;
		bool L_20;
		L_20 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_19, 2, /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_7 = (bool)((((int32_t)L_20) == ((int32_t)0))? 1 : 0);
		bool L_21 = V_7;
		if (!L_21)
		{
			goto IL_0091;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_22 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_22, 2, /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
	}

IL_0091:
	{
	}

IL_0092:
	{
		int32_t L_23 = ___streams0;
		V_8 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_23&(int32_t)8))) <= ((uint32_t)0)))? 1 : 0);
		bool L_24 = V_8;
		if (!L_24)
		{
			goto IL_00ba;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_25 = V_0;
		bool L_26;
		L_26 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_25, 3, /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_9 = (bool)((((int32_t)L_26) == ((int32_t)0))? 1 : 0);
		bool L_27 = V_9;
		if (!L_27)
		{
			goto IL_00b9;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_28 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_28, 3, /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
	}

IL_00b9:
	{
	}

IL_00ba:
	{
		int32_t L_29 = ___streams0;
		V_10 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_29&(int32_t)((int32_t)16)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_30 = V_10;
		if (!L_30)
		{
			goto IL_00e3;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_31 = V_0;
		bool L_32;
		L_32 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_31, 4, /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_11 = (bool)((((int32_t)L_32) == ((int32_t)0))? 1 : 0);
		bool L_33 = V_11;
		if (!L_33)
		{
			goto IL_00e2;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_34 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_34, 4, /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
	}

IL_00e2:
	{
	}

IL_00e3:
	{
		int32_t L_35 = ___streams0;
		V_12 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_35&(int32_t)((int32_t)32)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_36 = V_12;
		if (!L_36)
		{
			goto IL_011d;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_37 = V_0;
		bool L_38;
		L_38 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_37, 5, /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_13 = (bool)((((int32_t)L_38) == ((int32_t)0))? 1 : 0);
		bool L_39 = V_13;
		if (!L_39)
		{
			goto IL_011c;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_40 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_40, 5, /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_41 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_41, 8, /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_42 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_42, ((int32_t)9), /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
	}

IL_011c:
	{
	}

IL_011d:
	{
		int32_t L_43 = ___streams0;
		V_14 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_43&(int32_t)((int32_t)64)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_44 = V_14;
		if (!L_44)
		{
			goto IL_0151;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_45 = V_0;
		bool L_46;
		L_46 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_45, ((int32_t)10), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_15 = (bool)((((int32_t)L_46) == ((int32_t)0))? 1 : 0);
		bool L_47 = V_15;
		if (!L_47)
		{
			goto IL_0150;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_48 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_48, ((int32_t)10), /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_49 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_49, ((int32_t)11), /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
	}

IL_0150:
	{
	}

IL_0151:
	{
		int32_t L_50 = ___streams0;
		V_16 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_50&(int32_t)((int32_t)128)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_51 = V_16;
		if (!L_51)
		{
			goto IL_017f;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_52 = V_0;
		bool L_53;
		L_53 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_52, ((int32_t)14), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_17 = (bool)((((int32_t)L_53) == ((int32_t)0))? 1 : 0);
		bool L_54 = V_17;
		if (!L_54)
		{
			goto IL_017e;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_55 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_55, ((int32_t)14), /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
	}

IL_017e:
	{
	}

IL_017f:
	{
		int32_t L_56 = ___streams0;
		V_18 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_56&(int32_t)((int32_t)256)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_57 = V_18;
		if (!L_57)
		{
			goto IL_01ad;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_58 = V_0;
		bool L_59;
		L_59 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_58, ((int32_t)16), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_19 = (bool)((((int32_t)L_59) == ((int32_t)0))? 1 : 0);
		bool L_60 = V_19;
		if (!L_60)
		{
			goto IL_01ac;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_61 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_61, ((int32_t)16), /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
	}

IL_01ac:
	{
	}

IL_01ad:
	{
		int32_t L_62 = ___streams0;
		V_20 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_62&(int32_t)((int32_t)512)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_63 = V_20;
		if (!L_63)
		{
			goto IL_01db;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_64 = V_0;
		bool L_65;
		L_65 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_64, ((int32_t)19), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_21 = (bool)((((int32_t)L_65) == ((int32_t)0))? 1 : 0);
		bool L_66 = V_21;
		if (!L_66)
		{
			goto IL_01da;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_67 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_67, ((int32_t)19), /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
	}

IL_01da:
	{
	}

IL_01db:
	{
		int32_t L_68 = ___streams0;
		V_22 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_68&(int32_t)((int32_t)1024)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_69 = V_22;
		if (!L_69)
		{
			goto IL_0212;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_70 = V_0;
		bool L_71;
		L_71 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_70, ((int32_t)21), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_23 = (bool)((((int32_t)L_71) == ((int32_t)0))? 1 : 0);
		bool L_72 = V_23;
		if (!L_72)
		{
			goto IL_0211;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_73 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_73, ((int32_t)21), /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_74 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_74, ((int32_t)22), /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
	}

IL_0211:
	{
	}

IL_0212:
	{
		int32_t L_75 = ___streams0;
		V_24 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_75&(int32_t)((int32_t)2048)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_76 = V_24;
		if (!L_76)
		{
			goto IL_0240;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_77 = V_0;
		bool L_78;
		L_78 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_77, ((int32_t)34), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_25 = (bool)((((int32_t)L_78) == ((int32_t)0))? 1 : 0);
		bool L_79 = V_25;
		if (!L_79)
		{
			goto IL_023f;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_80 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_80, ((int32_t)34), /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
	}

IL_023f:
	{
	}

IL_0240:
	{
		int32_t L_81 = ___streams0;
		V_26 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_81&(int32_t)((int32_t)4096)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_82 = V_26;
		if (!L_82)
		{
			goto IL_026e;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_83 = V_0;
		bool L_84;
		L_84 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_83, ((int32_t)38), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_27 = (bool)((((int32_t)L_84) == ((int32_t)0))? 1 : 0);
		bool L_85 = V_27;
		if (!L_85)
		{
			goto IL_026d;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_86 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_86, ((int32_t)38), /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
	}

IL_026d:
	{
	}

IL_026e:
	{
		int32_t L_87 = ___streams0;
		V_28 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_87&(int32_t)((int32_t)8192)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_88 = V_28;
		if (!L_88)
		{
			goto IL_02a5;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_89 = V_0;
		bool L_90;
		L_90 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_89, ((int32_t)25), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_29 = (bool)((((int32_t)L_90) == ((int32_t)0))? 1 : 0);
		bool L_91 = V_29;
		if (!L_91)
		{
			goto IL_02a4;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_92 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_92, ((int32_t)25), /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_93 = V_0;
		List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93(L_93, ((int32_t)27), /*hidden argument*/List_1_Add_m4C23013D76D691FC6AC327F37DF978BBB601DD93_RuntimeMethod_var);
	}

IL_02a4:
	{
	}

IL_02a5:
	{
		goto IL_0434;
	}

IL_02ab:
	{
		int32_t L_94 = ___streams0;
		V_30 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_94&(int32_t)1))) <= ((uint32_t)0)))? 1 : 0);
		bool L_95 = V_30;
		if (!L_95)
		{
			goto IL_02c2;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_96 = V_0;
		bool L_97;
		L_97 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_96, 0, /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
	}

IL_02c2:
	{
		int32_t L_98 = ___streams0;
		V_31 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_98&(int32_t)2))) <= ((uint32_t)0)))? 1 : 0);
		bool L_99 = V_31;
		if (!L_99)
		{
			goto IL_02d8;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_100 = V_0;
		bool L_101;
		L_101 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_100, 1, /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
	}

IL_02d8:
	{
		int32_t L_102 = ___streams0;
		V_32 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_102&(int32_t)4))) <= ((uint32_t)0)))? 1 : 0);
		bool L_103 = V_32;
		if (!L_103)
		{
			goto IL_02ee;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_104 = V_0;
		bool L_105;
		L_105 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_104, 2, /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
	}

IL_02ee:
	{
		int32_t L_106 = ___streams0;
		V_33 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_106&(int32_t)8))) <= ((uint32_t)0)))? 1 : 0);
		bool L_107 = V_33;
		if (!L_107)
		{
			goto IL_0304;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_108 = V_0;
		bool L_109;
		L_109 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_108, 3, /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
	}

IL_0304:
	{
		int32_t L_110 = ___streams0;
		V_34 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_110&(int32_t)((int32_t)16)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_111 = V_34;
		if (!L_111)
		{
			goto IL_031b;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_112 = V_0;
		bool L_113;
		L_113 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_112, 4, /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
	}

IL_031b:
	{
		int32_t L_114 = ___streams0;
		V_35 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_114&(int32_t)((int32_t)32)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_115 = V_35;
		if (!L_115)
		{
			goto IL_0343;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_116 = V_0;
		bool L_117;
		L_117 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_116, 5, /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_118 = V_0;
		bool L_119;
		L_119 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_118, 8, /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_120 = V_0;
		bool L_121;
		L_121 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_120, ((int32_t)9), /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
	}

IL_0343:
	{
		int32_t L_122 = ___streams0;
		V_36 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_122&(int32_t)((int32_t)64)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_123 = V_36;
		if (!L_123)
		{
			goto IL_0364;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_124 = V_0;
		bool L_125;
		L_125 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_124, ((int32_t)10), /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_126 = V_0;
		bool L_127;
		L_127 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_126, ((int32_t)11), /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
	}

IL_0364:
	{
		int32_t L_128 = ___streams0;
		V_37 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_128&(int32_t)((int32_t)128)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_129 = V_37;
		if (!L_129)
		{
			goto IL_037f;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_130 = V_0;
		bool L_131;
		L_131 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_130, ((int32_t)14), /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
	}

IL_037f:
	{
		int32_t L_132 = ___streams0;
		V_38 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_132&(int32_t)((int32_t)256)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_133 = V_38;
		if (!L_133)
		{
			goto IL_039a;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_134 = V_0;
		bool L_135;
		L_135 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_134, ((int32_t)16), /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
	}

IL_039a:
	{
		int32_t L_136 = ___streams0;
		V_39 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_136&(int32_t)((int32_t)512)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_137 = V_39;
		if (!L_137)
		{
			goto IL_03b5;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_138 = V_0;
		bool L_139;
		L_139 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_138, ((int32_t)19), /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
	}

IL_03b5:
	{
		int32_t L_140 = ___streams0;
		V_40 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_140&(int32_t)((int32_t)1024)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_141 = V_40;
		if (!L_141)
		{
			goto IL_03d9;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_142 = V_0;
		bool L_143;
		L_143 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_142, ((int32_t)21), /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_144 = V_0;
		bool L_145;
		L_145 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_144, ((int32_t)22), /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
	}

IL_03d9:
	{
		int32_t L_146 = ___streams0;
		V_41 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_146&(int32_t)((int32_t)2048)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_147 = V_41;
		if (!L_147)
		{
			goto IL_03f4;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_148 = V_0;
		bool L_149;
		L_149 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_148, ((int32_t)34), /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
	}

IL_03f4:
	{
		int32_t L_150 = ___streams0;
		V_42 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_150&(int32_t)((int32_t)4096)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_151 = V_42;
		if (!L_151)
		{
			goto IL_040f;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_152 = V_0;
		bool L_153;
		L_153 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_152, ((int32_t)38), /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
	}

IL_040f:
	{
		int32_t L_154 = ___streams0;
		V_43 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_154&(int32_t)((int32_t)8192)))) <= ((uint32_t)0)))? 1 : 0);
		bool L_155 = V_43;
		if (!L_155)
		{
			goto IL_0433;
		}
	}
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_156 = V_0;
		bool L_157;
		L_157 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_156, ((int32_t)26), /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_158 = V_0;
		bool L_159;
		L_159 = List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F(L_158, ((int32_t)27), /*hidden argument*/List_1_Remove_m098FBCF253B8AC42760C9E5DFFD943A8912B8A0F_RuntimeMethod_var);
	}

IL_0433:
	{
	}

IL_0434:
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_160 = V_0;
		ParticleSystemRenderer_SetActiveVertexStreams_m6915A0D7D1E0DCEBEA77A31DC78BE0CE8DBEAD22(__this, L_160, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ParticleSystemVertexStreams UnityEngine.ParticleSystemRenderer::Internal_GetEnabledVertexStreams(UnityEngine.ParticleSystemVertexStreams)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystemRenderer_Internal_GetEnabledVertexStreams_m1AA436781571C985E87EE8706AF264C977D7A979 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, int32_t ___streams0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m50FEC7E4E5C76F2F1388964CF2AF151F6BC4216C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	bool V_10 = false;
	bool V_11 = false;
	bool V_12 = false;
	bool V_13 = false;
	bool V_14 = false;
	bool V_15 = false;
	int32_t V_16 = 0;
	{
		int32_t L_0;
		L_0 = ParticleSystemRenderer_get_activeVertexStreamsCount_m948DD12ED56E758FF7F55DF8D1D9B6D644A3D3FE(__this, /*hidden argument*/NULL);
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_1 = (List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 *)il2cpp_codegen_object_new(List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0_il2cpp_TypeInfo_var);
		List_1__ctor_m50FEC7E4E5C76F2F1388964CF2AF151F6BC4216C(L_1, L_0, /*hidden argument*/List_1__ctor_m50FEC7E4E5C76F2F1388964CF2AF151F6BC4216C_RuntimeMethod_var);
		V_0 = L_1;
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_2 = V_0;
		ParticleSystemRenderer_GetActiveVertexStreams_m32C738BAD8EAB54463B9BBCA2CAA0324BC702014(__this, L_2, /*hidden argument*/NULL);
		V_1 = 0;
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_3 = V_0;
		bool L_4;
		L_4 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_3, 0, /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_2 = L_4;
		bool L_5 = V_2;
		if (!L_5)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6|(int32_t)1));
	}

IL_0026:
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_7 = V_0;
		bool L_8;
		L_8 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_7, 1, /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_3 = L_8;
		bool L_9 = V_3;
		if (!L_9)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10|(int32_t)2));
	}

IL_0035:
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_11 = V_0;
		bool L_12;
		L_12 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_11, 2, /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_4 = L_12;
		bool L_13 = V_4;
		if (!L_13)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_14 = V_1;
		V_1 = ((int32_t)((int32_t)L_14|(int32_t)4));
	}

IL_0046:
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_15 = V_0;
		bool L_16;
		L_16 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_15, 3, /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_5 = L_16;
		bool L_17 = V_5;
		if (!L_17)
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_18 = V_1;
		V_1 = ((int32_t)((int32_t)L_18|(int32_t)8));
	}

IL_0057:
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_19 = V_0;
		bool L_20;
		L_20 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_19, 4, /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_6 = L_20;
		bool L_21 = V_6;
		if (!L_21)
		{
			goto IL_0069;
		}
	}
	{
		int32_t L_22 = V_1;
		V_1 = ((int32_t)((int32_t)L_22|(int32_t)((int32_t)16)));
	}

IL_0069:
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_23 = V_0;
		bool L_24;
		L_24 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_23, 5, /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_7 = L_24;
		bool L_25 = V_7;
		if (!L_25)
		{
			goto IL_007b;
		}
	}
	{
		int32_t L_26 = V_1;
		V_1 = ((int32_t)((int32_t)L_26|(int32_t)((int32_t)32)));
	}

IL_007b:
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_27 = V_0;
		bool L_28;
		L_28 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_27, ((int32_t)10), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_8 = L_28;
		bool L_29 = V_8;
		if (!L_29)
		{
			goto IL_008e;
		}
	}
	{
		int32_t L_30 = V_1;
		V_1 = ((int32_t)((int32_t)L_30|(int32_t)((int32_t)64)));
	}

IL_008e:
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_31 = V_0;
		bool L_32;
		L_32 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_31, ((int32_t)14), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_9 = L_32;
		bool L_33 = V_9;
		if (!L_33)
		{
			goto IL_00a4;
		}
	}
	{
		int32_t L_34 = V_1;
		V_1 = ((int32_t)((int32_t)L_34|(int32_t)((int32_t)128)));
	}

IL_00a4:
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_35 = V_0;
		bool L_36;
		L_36 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_35, ((int32_t)16), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_10 = L_36;
		bool L_37 = V_10;
		if (!L_37)
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_38 = V_1;
		V_1 = ((int32_t)((int32_t)L_38|(int32_t)((int32_t)256)));
	}

IL_00ba:
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_39 = V_0;
		bool L_40;
		L_40 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_39, ((int32_t)19), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_11 = L_40;
		bool L_41 = V_11;
		if (!L_41)
		{
			goto IL_00d0;
		}
	}
	{
		int32_t L_42 = V_1;
		V_1 = ((int32_t)((int32_t)L_42|(int32_t)((int32_t)512)));
	}

IL_00d0:
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_43 = V_0;
		bool L_44;
		L_44 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_43, ((int32_t)21), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_12 = L_44;
		bool L_45 = V_12;
		if (!L_45)
		{
			goto IL_00e6;
		}
	}
	{
		int32_t L_46 = V_1;
		V_1 = ((int32_t)((int32_t)L_46|(int32_t)((int32_t)1024)));
	}

IL_00e6:
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_47 = V_0;
		bool L_48;
		L_48 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_47, ((int32_t)34), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_13 = L_48;
		bool L_49 = V_13;
		if (!L_49)
		{
			goto IL_00fc;
		}
	}
	{
		int32_t L_50 = V_1;
		V_1 = ((int32_t)((int32_t)L_50|(int32_t)((int32_t)2048)));
	}

IL_00fc:
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_51 = V_0;
		bool L_52;
		L_52 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_51, ((int32_t)38), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_14 = L_52;
		bool L_53 = V_14;
		if (!L_53)
		{
			goto IL_0112;
		}
	}
	{
		int32_t L_54 = V_1;
		V_1 = ((int32_t)((int32_t)L_54|(int32_t)((int32_t)4096)));
	}

IL_0112:
	{
		List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * L_55 = V_0;
		bool L_56;
		L_56 = List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86(L_55, ((int32_t)25), /*hidden argument*/List_1_Contains_mDE02CDCA04C077B2B0CED6F3DC9A718810DBEF86_RuntimeMethod_var);
		V_15 = L_56;
		bool L_57 = V_15;
		if (!L_57)
		{
			goto IL_0128;
		}
	}
	{
		int32_t L_58 = V_1;
		V_1 = ((int32_t)((int32_t)L_58|(int32_t)((int32_t)8192)));
	}

IL_0128:
	{
		int32_t L_59 = V_1;
		int32_t L_60 = ___streams0;
		V_16 = ((int32_t)((int32_t)L_59&(int32_t)L_60));
		goto IL_012f;
	}

IL_012f:
	{
		int32_t L_61 = V_16;
		return L_61;
	}
}
// UnityEngine.ParticleSystemRenderSpace UnityEngine.ParticleSystemRenderer::get_alignment()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystemRenderer_get_alignment_m5D4063743194C66B9671E5CBC3DC5803AAB8D023 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystemRenderer_get_alignment_m5D4063743194C66B9671E5CBC3DC5803AAB8D023_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_alignment_m5D4063743194C66B9671E5CBC3DC5803AAB8D023_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_alignment_m5D4063743194C66B9671E5CBC3DC5803AAB8D023_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_alignment()");
	int32_t icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_alignment(UnityEngine.ParticleSystemRenderSpace)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_alignment_m0A6049A94D26A72853875B9A46CA4BE829B439A7 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_alignment_m0A6049A94D26A72853875B9A46CA4BE829B439A7_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, int32_t);
	static ParticleSystemRenderer_set_alignment_m0A6049A94D26A72853875B9A46CA4BE829B439A7_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_alignment_m0A6049A94D26A72853875B9A46CA4BE829B439A7_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_alignment(UnityEngine.ParticleSystemRenderSpace)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.ParticleSystemRenderMode UnityEngine.ParticleSystemRenderer::get_renderMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystemRenderer_get_renderMode_mE203DEDF25DAA9E549CECD34F7552E8B39A81576 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystemRenderer_get_renderMode_mE203DEDF25DAA9E549CECD34F7552E8B39A81576_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_renderMode_mE203DEDF25DAA9E549CECD34F7552E8B39A81576_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_renderMode_mE203DEDF25DAA9E549CECD34F7552E8B39A81576_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_renderMode()");
	int32_t icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_renderMode(UnityEngine.ParticleSystemRenderMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_renderMode_mEA05819300408CA36E2D7AA83E15451A22B83D69 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_renderMode_mEA05819300408CA36E2D7AA83E15451A22B83D69_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, int32_t);
	static ParticleSystemRenderer_set_renderMode_mEA05819300408CA36E2D7AA83E15451A22B83D69_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_renderMode_mEA05819300408CA36E2D7AA83E15451A22B83D69_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_renderMode(UnityEngine.ParticleSystemRenderMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.ParticleSystemSortMode UnityEngine.ParticleSystemRenderer::get_sortMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystemRenderer_get_sortMode_m9BDC333B6BFDBA07EB7254E614B819A35BE54275 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystemRenderer_get_sortMode_m9BDC333B6BFDBA07EB7254E614B819A35BE54275_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_sortMode_m9BDC333B6BFDBA07EB7254E614B819A35BE54275_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_sortMode_m9BDC333B6BFDBA07EB7254E614B819A35BE54275_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_sortMode()");
	int32_t icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_sortMode(UnityEngine.ParticleSystemSortMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_sortMode_mC3736E7991D0D8C55DAF0D40E872BACADF18A560 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_sortMode_mC3736E7991D0D8C55DAF0D40E872BACADF18A560_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, int32_t);
	static ParticleSystemRenderer_set_sortMode_mC3736E7991D0D8C55DAF0D40E872BACADF18A560_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_sortMode_mC3736E7991D0D8C55DAF0D40E872BACADF18A560_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_sortMode(UnityEngine.ParticleSystemSortMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.ParticleSystemRenderer::get_lengthScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystemRenderer_get_lengthScale_m2967DAF0E340DE04D08C03B0895496D90E1F8C20 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef float (*ParticleSystemRenderer_get_lengthScale_m2967DAF0E340DE04D08C03B0895496D90E1F8C20_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_lengthScale_m2967DAF0E340DE04D08C03B0895496D90E1F8C20_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_lengthScale_m2967DAF0E340DE04D08C03B0895496D90E1F8C20_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_lengthScale()");
	float icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_lengthScale(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_lengthScale_m082D8A542FC1A7FA8772CE01C98DC44C359FFB8A (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_lengthScale_m082D8A542FC1A7FA8772CE01C98DC44C359FFB8A_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, float);
	static ParticleSystemRenderer_set_lengthScale_m082D8A542FC1A7FA8772CE01C98DC44C359FFB8A_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_lengthScale_m082D8A542FC1A7FA8772CE01C98DC44C359FFB8A_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_lengthScale(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.ParticleSystemRenderer::get_velocityScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystemRenderer_get_velocityScale_mF549344ED73B7231CE97A0C6E9D76DFA74E0CBBB (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef float (*ParticleSystemRenderer_get_velocityScale_mF549344ED73B7231CE97A0C6E9D76DFA74E0CBBB_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_velocityScale_mF549344ED73B7231CE97A0C6E9D76DFA74E0CBBB_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_velocityScale_mF549344ED73B7231CE97A0C6E9D76DFA74E0CBBB_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_velocityScale()");
	float icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_velocityScale(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_velocityScale_mB3BADC3EB04FE4BDD4BDE4E2D4725371BD0AA034 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_velocityScale_mB3BADC3EB04FE4BDD4BDE4E2D4725371BD0AA034_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, float);
	static ParticleSystemRenderer_set_velocityScale_mB3BADC3EB04FE4BDD4BDE4E2D4725371BD0AA034_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_velocityScale_mB3BADC3EB04FE4BDD4BDE4E2D4725371BD0AA034_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_velocityScale(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.ParticleSystemRenderer::get_cameraVelocityScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystemRenderer_get_cameraVelocityScale_m4EFFB41B99BAD7CC6CBBC1E8E56BE91B459B386C (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef float (*ParticleSystemRenderer_get_cameraVelocityScale_m4EFFB41B99BAD7CC6CBBC1E8E56BE91B459B386C_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_cameraVelocityScale_m4EFFB41B99BAD7CC6CBBC1E8E56BE91B459B386C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_cameraVelocityScale_m4EFFB41B99BAD7CC6CBBC1E8E56BE91B459B386C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_cameraVelocityScale()");
	float icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_cameraVelocityScale(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_cameraVelocityScale_m05772225B1824D2AE906FF68CA45C5901C4F9E95 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_cameraVelocityScale_m05772225B1824D2AE906FF68CA45C5901C4F9E95_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, float);
	static ParticleSystemRenderer_set_cameraVelocityScale_m05772225B1824D2AE906FF68CA45C5901C4F9E95_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_cameraVelocityScale_m05772225B1824D2AE906FF68CA45C5901C4F9E95_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_cameraVelocityScale(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.ParticleSystemRenderer::get_normalDirection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystemRenderer_get_normalDirection_m4F7D8E0376239DD36E1526BBA511B76133B57C9D (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef float (*ParticleSystemRenderer_get_normalDirection_m4F7D8E0376239DD36E1526BBA511B76133B57C9D_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_normalDirection_m4F7D8E0376239DD36E1526BBA511B76133B57C9D_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_normalDirection_m4F7D8E0376239DD36E1526BBA511B76133B57C9D_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_normalDirection()");
	float icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_normalDirection(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_normalDirection_mFC60F14E941A1C4939F977F22B0D081A740AB5F2 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_normalDirection_mFC60F14E941A1C4939F977F22B0D081A740AB5F2_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, float);
	static ParticleSystemRenderer_set_normalDirection_mFC60F14E941A1C4939F977F22B0D081A740AB5F2_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_normalDirection_mFC60F14E941A1C4939F977F22B0D081A740AB5F2_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_normalDirection(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.ParticleSystemRenderer::get_shadowBias()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystemRenderer_get_shadowBias_m8324464177725CD2C3A4FEB227F7FD00394FF53D (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef float (*ParticleSystemRenderer_get_shadowBias_m8324464177725CD2C3A4FEB227F7FD00394FF53D_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_shadowBias_m8324464177725CD2C3A4FEB227F7FD00394FF53D_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_shadowBias_m8324464177725CD2C3A4FEB227F7FD00394FF53D_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_shadowBias()");
	float icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_shadowBias(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_shadowBias_mCB3BD9449A66F4F20077BFB970D1A6E99D6EDA2A (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_shadowBias_mCB3BD9449A66F4F20077BFB970D1A6E99D6EDA2A_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, float);
	static ParticleSystemRenderer_set_shadowBias_mCB3BD9449A66F4F20077BFB970D1A6E99D6EDA2A_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_shadowBias_mCB3BD9449A66F4F20077BFB970D1A6E99D6EDA2A_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_shadowBias(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.ParticleSystemRenderer::get_sortingFudge()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystemRenderer_get_sortingFudge_m2F9B4EFF4F196E685AECB9462D69B13E340C43EA (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef float (*ParticleSystemRenderer_get_sortingFudge_m2F9B4EFF4F196E685AECB9462D69B13E340C43EA_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_sortingFudge_m2F9B4EFF4F196E685AECB9462D69B13E340C43EA_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_sortingFudge_m2F9B4EFF4F196E685AECB9462D69B13E340C43EA_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_sortingFudge()");
	float icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_sortingFudge(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_sortingFudge_m619B3A624EF266C7DE36BE083085E327033ADBAF (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_sortingFudge_m619B3A624EF266C7DE36BE083085E327033ADBAF_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, float);
	static ParticleSystemRenderer_set_sortingFudge_m619B3A624EF266C7DE36BE083085E327033ADBAF_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_sortingFudge_m619B3A624EF266C7DE36BE083085E327033ADBAF_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_sortingFudge(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.ParticleSystemRenderer::get_minParticleSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystemRenderer_get_minParticleSize_m1BDB16135B182231F359499E811BF5274664E140 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef float (*ParticleSystemRenderer_get_minParticleSize_m1BDB16135B182231F359499E811BF5274664E140_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_minParticleSize_m1BDB16135B182231F359499E811BF5274664E140_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_minParticleSize_m1BDB16135B182231F359499E811BF5274664E140_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_minParticleSize()");
	float icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_minParticleSize(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_minParticleSize_m1FDD2EEFBF60703A7AC154FC91DD5015F65A34F4 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_minParticleSize_m1FDD2EEFBF60703A7AC154FC91DD5015F65A34F4_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, float);
	static ParticleSystemRenderer_set_minParticleSize_m1FDD2EEFBF60703A7AC154FC91DD5015F65A34F4_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_minParticleSize_m1FDD2EEFBF60703A7AC154FC91DD5015F65A34F4_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_minParticleSize(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.ParticleSystemRenderer::get_maxParticleSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParticleSystemRenderer_get_maxParticleSize_mA9131C2FE7C8784D02EC94E20E7F5734C06C8EB4 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef float (*ParticleSystemRenderer_get_maxParticleSize_mA9131C2FE7C8784D02EC94E20E7F5734C06C8EB4_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_maxParticleSize_mA9131C2FE7C8784D02EC94E20E7F5734C06C8EB4_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_maxParticleSize_mA9131C2FE7C8784D02EC94E20E7F5734C06C8EB4_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_maxParticleSize()");
	float icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_maxParticleSize(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_maxParticleSize_m7D19DF8E661235A19910F216DFDBBD447940CD06 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_maxParticleSize_m7D19DF8E661235A19910F216DFDBBD447940CD06_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, float);
	static ParticleSystemRenderer_set_maxParticleSize_m7D19DF8E661235A19910F216DFDBBD447940CD06_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_maxParticleSize_m7D19DF8E661235A19910F216DFDBBD447940CD06_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_maxParticleSize(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.ParticleSystemRenderer::get_pivot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ParticleSystemRenderer_get_pivot_m19CA73F7EE2F456F16E9E4753FC197A10649A6CE (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		ParticleSystemRenderer_get_pivot_Injected_mB393D1D0F0C69BD2F52A282FD0868E3E2585488C(__this, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.ParticleSystemRenderer::set_pivot(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_pivot_mF7A808C4BD4258937742A26C4E993902A4AA5C80 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystemRenderer_set_pivot_Injected_m8CE9A3B8B0EB136F3E658CFB2B00616D7D8A8AB3(__this, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.ParticleSystemRenderer::get_flip()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ParticleSystemRenderer_get_flip_m5499AACC6A0BF4AD6ECB0919121D219BDEE2D0BC (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		ParticleSystemRenderer_get_flip_Injected_m08B63FCDBAB2C235981432A80901FE97A29DA985(__this, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.ParticleSystemRenderer::set_flip(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_flip_m6148ED3103BBC6999C1121DFEEF355B139CEA051 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystemRenderer_set_flip_Injected_mE957C7B1BCD98C9D2D19900F1CC8EE50ED1DD688(__this, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SpriteMaskInteraction UnityEngine.ParticleSystemRenderer::get_maskInteraction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystemRenderer_get_maskInteraction_mAE4B3D48137AA10DEA775FD30A2D161D54747AE8 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystemRenderer_get_maskInteraction_mAE4B3D48137AA10DEA775FD30A2D161D54747AE8_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_maskInteraction_mAE4B3D48137AA10DEA775FD30A2D161D54747AE8_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_maskInteraction_mAE4B3D48137AA10DEA775FD30A2D161D54747AE8_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_maskInteraction()");
	int32_t icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_maskInteraction(UnityEngine.SpriteMaskInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_maskInteraction_mFC9DD6C5CDADD003946F4CDA508D5A814992F2BB (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_maskInteraction_mFC9DD6C5CDADD003946F4CDA508D5A814992F2BB_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, int32_t);
	static ParticleSystemRenderer_set_maskInteraction_mFC9DD6C5CDADD003946F4CDA508D5A814992F2BB_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_maskInteraction_mFC9DD6C5CDADD003946F4CDA508D5A814992F2BB_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_maskInteraction(UnityEngine.SpriteMaskInteraction)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.ParticleSystemRenderer::get_trailMaterial()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * ParticleSystemRenderer_get_trailMaterial_mB7C8AE9C0D9B9A2092E2FCF55AA6CFE3B0B3A400 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef Material_t8927C00353A72755313F046D0CE85178AE8218EE * (*ParticleSystemRenderer_get_trailMaterial_mB7C8AE9C0D9B9A2092E2FCF55AA6CFE3B0B3A400_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_trailMaterial_mB7C8AE9C0D9B9A2092E2FCF55AA6CFE3B0B3A400_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_trailMaterial_mB7C8AE9C0D9B9A2092E2FCF55AA6CFE3B0B3A400_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_trailMaterial()");
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_trailMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_trailMaterial_mB28936A342D1805B3F8246DC5DD531970452D016 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_trailMaterial_mB28936A342D1805B3F8246DC5DD531970452D016_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, Material_t8927C00353A72755313F046D0CE85178AE8218EE *);
	static ParticleSystemRenderer_set_trailMaterial_mB28936A342D1805B3F8246DC5DD531970452D016_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_trailMaterial_mB28936A342D1805B3F8246DC5DD531970452D016_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_trailMaterial(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.ParticleSystemRenderer::get_enableGPUInstancing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystemRenderer_get_enableGPUInstancing_mAC82F715FF3D827AB2EE7AF70FF0CC828DD71B34 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystemRenderer_get_enableGPUInstancing_mAC82F715FF3D827AB2EE7AF70FF0CC828DD71B34_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_enableGPUInstancing_mAC82F715FF3D827AB2EE7AF70FF0CC828DD71B34_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_enableGPUInstancing_mAC82F715FF3D827AB2EE7AF70FF0CC828DD71B34_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_enableGPUInstancing()");
	bool icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_enableGPUInstancing(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_enableGPUInstancing_m643A1F548832BECBB84384F2A85E724E5FBE3D0E (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_enableGPUInstancing_m643A1F548832BECBB84384F2A85E724E5FBE3D0E_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, bool);
	static ParticleSystemRenderer_set_enableGPUInstancing_m643A1F548832BECBB84384F2A85E724E5FBE3D0E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_enableGPUInstancing_m643A1F548832BECBB84384F2A85E724E5FBE3D0E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_enableGPUInstancing(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.ParticleSystemRenderer::get_allowRoll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystemRenderer_get_allowRoll_m8D1E22AA8E4426C79CB995899196CF22FBF17023 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystemRenderer_get_allowRoll_m8D1E22AA8E4426C79CB995899196CF22FBF17023_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_allowRoll_m8D1E22AA8E4426C79CB995899196CF22FBF17023_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_allowRoll_m8D1E22AA8E4426C79CB995899196CF22FBF17023_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_allowRoll()");
	bool icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_allowRoll(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_allowRoll_mB321729C721EBFCA6688F9AC4DBAF38A166D8CE7 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_allowRoll_mB321729C721EBFCA6688F9AC4DBAF38A166D8CE7_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, bool);
	static ParticleSystemRenderer_set_allowRoll_mB321729C721EBFCA6688F9AC4DBAF38A166D8CE7_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_allowRoll_mB321729C721EBFCA6688F9AC4DBAF38A166D8CE7_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_allowRoll(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.ParticleSystemRenderer::get_freeformStretching()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystemRenderer_get_freeformStretching_m93B2D5A92718928D43E185DF0B0F2B13CFF4889E (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystemRenderer_get_freeformStretching_m93B2D5A92718928D43E185DF0B0F2B13CFF4889E_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_freeformStretching_m93B2D5A92718928D43E185DF0B0F2B13CFF4889E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_freeformStretching_m93B2D5A92718928D43E185DF0B0F2B13CFF4889E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_freeformStretching()");
	bool icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_freeformStretching(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_freeformStretching_m4388C3A17F73D42B6A3951BA30D4805E9869F14B (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_freeformStretching_m4388C3A17F73D42B6A3951BA30D4805E9869F14B_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, bool);
	static ParticleSystemRenderer_set_freeformStretching_m4388C3A17F73D42B6A3951BA30D4805E9869F14B_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_freeformStretching_m4388C3A17F73D42B6A3951BA30D4805E9869F14B_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_freeformStretching(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.ParticleSystemRenderer::get_rotateWithStretchDirection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ParticleSystemRenderer_get_rotateWithStretchDirection_m1E966F0539AA2A9C806652E59158EC939DBF5000 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef bool (*ParticleSystemRenderer_get_rotateWithStretchDirection_m1E966F0539AA2A9C806652E59158EC939DBF5000_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_rotateWithStretchDirection_m1E966F0539AA2A9C806652E59158EC939DBF5000_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_rotateWithStretchDirection_m1E966F0539AA2A9C806652E59158EC939DBF5000_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_rotateWithStretchDirection()");
	bool icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_rotateWithStretchDirection(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_rotateWithStretchDirection_m027CFD4D235694227FA2FA0F01BA663E8392E2BC (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_rotateWithStretchDirection_m027CFD4D235694227FA2FA0F01BA663E8392E2BC_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, bool);
	static ParticleSystemRenderer_set_rotateWithStretchDirection_m027CFD4D235694227FA2FA0F01BA663E8392E2BC_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_rotateWithStretchDirection_m027CFD4D235694227FA2FA0F01BA663E8392E2BC_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_rotateWithStretchDirection(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Mesh UnityEngine.ParticleSystemRenderer::get_mesh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ParticleSystemRenderer_get_mesh_m32A036955DAEA0FDDA2D4B8EE3D5AEE7FD67A92F (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * (*ParticleSystemRenderer_get_mesh_m32A036955DAEA0FDDA2D4B8EE3D5AEE7FD67A92F_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_mesh_m32A036955DAEA0FDDA2D4B8EE3D5AEE7FD67A92F_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_mesh_m32A036955DAEA0FDDA2D4B8EE3D5AEE7FD67A92F_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_mesh()");
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::set_mesh(UnityEngine.Mesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_mesh_m5AEE1784648604DA92FD5B99759D366EA7C76C65 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_mesh_m5AEE1784648604DA92FD5B99759D366EA7C76C65_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 *);
	static ParticleSystemRenderer_set_mesh_m5AEE1784648604DA92FD5B99759D366EA7C76C65_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_mesh_m5AEE1784648604DA92FD5B99759D366EA7C76C65_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_mesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.ParticleSystemRenderer::GetMeshes(UnityEngine.Mesh[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystemRenderer_GetMeshes_m1B36A6BFF152AAE5520D727976E3DA26722C3A75 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8* ___meshes0, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystemRenderer_GetMeshes_m1B36A6BFF152AAE5520D727976E3DA26722C3A75_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8*);
	static ParticleSystemRenderer_GetMeshes_m1B36A6BFF152AAE5520D727976E3DA26722C3A75_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_GetMeshes_m1B36A6BFF152AAE5520D727976E3DA26722C3A75_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::GetMeshes(UnityEngine.Mesh[])");
	int32_t icallRetVal = _il2cpp_icall_func(__this, ___meshes0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::SetMeshes(UnityEngine.Mesh[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_SetMeshes_mAD7FCBD6E24A447D5304058EA04F751F85AF5A99 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8* ___meshes0, int32_t ___size1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_SetMeshes_mAD7FCBD6E24A447D5304058EA04F751F85AF5A99_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8*, int32_t);
	static ParticleSystemRenderer_SetMeshes_mAD7FCBD6E24A447D5304058EA04F751F85AF5A99_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_SetMeshes_mAD7FCBD6E24A447D5304058EA04F751F85AF5A99_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::SetMeshes(UnityEngine.Mesh[],System.Int32)");
	_il2cpp_icall_func(__this, ___meshes0, ___size1);
}
// System.Void UnityEngine.ParticleSystemRenderer::SetMeshes(UnityEngine.Mesh[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_SetMeshes_m1528C9A3211F04578ECBC89E84125ABFE82796DC (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8* ___meshes0, const RuntimeMethod* method)
{
	{
		MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8* L_0 = ___meshes0;
		MeshU5BU5D_t9083AB7B9B72E4E05B55B7CA24A5D6020DC1C6D8* L_1 = ___meshes0;
		ParticleSystemRenderer_SetMeshes_mAD7FCBD6E24A447D5304058EA04F751F85AF5A99(__this, L_0, ((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.ParticleSystemRenderer::get_meshCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystemRenderer_get_meshCount_mCB20BE0EC5F2671EF6F6119227130F3473C3302D (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystemRenderer_get_meshCount_mCB20BE0EC5F2671EF6F6119227130F3473C3302D_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_meshCount_mCB20BE0EC5F2671EF6F6119227130F3473C3302D_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_meshCount_mCB20BE0EC5F2671EF6F6119227130F3473C3302D_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_meshCount()");
	int32_t icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::BakeMesh(UnityEngine.Mesh,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_BakeMesh_m0833E4AFD66E76A995B877E9759F3497EECFC3A6 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___mesh0, bool ___useTransform1, const RuntimeMethod* method)
{
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = ___mesh0;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_1;
		L_1 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		bool L_2 = ___useTransform1;
		ParticleSystemRenderer_BakeMesh_m1A53F569D7DFCBE964E66D5E3ACA11D85ED56C9E(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystemRenderer::BakeMesh(UnityEngine.Mesh,UnityEngine.Camera,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_BakeMesh_m1A53F569D7DFCBE964E66D5E3ACA11D85ED56C9E (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___mesh0, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___camera1, bool ___useTransform2, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_BakeMesh_m1A53F569D7DFCBE964E66D5E3ACA11D85ED56C9E_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 *, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C *, bool);
	static ParticleSystemRenderer_BakeMesh_m1A53F569D7DFCBE964E66D5E3ACA11D85ED56C9E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_BakeMesh_m1A53F569D7DFCBE964E66D5E3ACA11D85ED56C9E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::BakeMesh(UnityEngine.Mesh,UnityEngine.Camera,System.Boolean)");
	_il2cpp_icall_func(__this, ___mesh0, ___camera1, ___useTransform2);
}
// System.Void UnityEngine.ParticleSystemRenderer::BakeTrailsMesh(UnityEngine.Mesh,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_BakeTrailsMesh_m07C7C29BB8DBA0621E414DC443838F287AF58DFE (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___mesh0, bool ___useTransform1, const RuntimeMethod* method)
{
	{
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = ___mesh0;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_1;
		L_1 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		bool L_2 = ___useTransform1;
		ParticleSystemRenderer_BakeTrailsMesh_m7BE3F730731BBFA0C6A9399BF91813236AC14E3F(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystemRenderer::BakeTrailsMesh(UnityEngine.Mesh,UnityEngine.Camera,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_BakeTrailsMesh_m7BE3F730731BBFA0C6A9399BF91813236AC14E3F (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___mesh0, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___camera1, bool ___useTransform2, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_BakeTrailsMesh_m7BE3F730731BBFA0C6A9399BF91813236AC14E3F_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 *, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C *, bool);
	static ParticleSystemRenderer_BakeTrailsMesh_m7BE3F730731BBFA0C6A9399BF91813236AC14E3F_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_BakeTrailsMesh_m7BE3F730731BBFA0C6A9399BF91813236AC14E3F_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::BakeTrailsMesh(UnityEngine.Mesh,UnityEngine.Camera,System.Boolean)");
	_il2cpp_icall_func(__this, ___mesh0, ___camera1, ___useTransform2);
}
// System.Int32 UnityEngine.ParticleSystemRenderer::get_activeVertexStreamsCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ParticleSystemRenderer_get_activeVertexStreamsCount_m948DD12ED56E758FF7F55DF8D1D9B6D644A3D3FE (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ParticleSystemRenderer_get_activeVertexStreamsCount_m948DD12ED56E758FF7F55DF8D1D9B6D644A3D3FE_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *);
	static ParticleSystemRenderer_get_activeVertexStreamsCount_m948DD12ED56E758FF7F55DF8D1D9B6D644A3D3FE_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_activeVertexStreamsCount_m948DD12ED56E758FF7F55DF8D1D9B6D644A3D3FE_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_activeVertexStreamsCount()");
	int32_t icallRetVal = _il2cpp_icall_func(__this);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystemRenderer::SetActiveVertexStreams(System.Collections.Generic.List`1<UnityEngine.ParticleSystemVertexStream>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_SetActiveVertexStreams_m6915A0D7D1E0DCEBEA77A31DC78BE0CE8DBEAD22 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * ___streams0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_SetActiveVertexStreams_m6915A0D7D1E0DCEBEA77A31DC78BE0CE8DBEAD22_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 *);
	static ParticleSystemRenderer_SetActiveVertexStreams_m6915A0D7D1E0DCEBEA77A31DC78BE0CE8DBEAD22_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_SetActiveVertexStreams_m6915A0D7D1E0DCEBEA77A31DC78BE0CE8DBEAD22_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::SetActiveVertexStreams(System.Collections.Generic.List`1<UnityEngine.ParticleSystemVertexStream>)");
	_il2cpp_icall_func(__this, ___streams0);
}
// System.Void UnityEngine.ParticleSystemRenderer::GetActiveVertexStreams(System.Collections.Generic.List`1<UnityEngine.ParticleSystemVertexStream>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_GetActiveVertexStreams_m32C738BAD8EAB54463B9BBCA2CAA0324BC702014 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 * ___streams0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_GetActiveVertexStreams_m32C738BAD8EAB54463B9BBCA2CAA0324BC702014_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, List_1_t766D8AF0DF5A214F4F57B93B561EE445363306E0 *);
	static ParticleSystemRenderer_GetActiveVertexStreams_m32C738BAD8EAB54463B9BBCA2CAA0324BC702014_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_GetActiveVertexStreams_m32C738BAD8EAB54463B9BBCA2CAA0324BC702014_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::GetActiveVertexStreams(System.Collections.Generic.List`1<UnityEngine.ParticleSystemVertexStream>)");
	_il2cpp_icall_func(__this, ___streams0);
}
// System.Void UnityEngine.ParticleSystemRenderer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer__ctor_mD6D0E9A92CF103E60163821DCC7C3D6DF3AA838B (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, const RuntimeMethod* method)
{
	{
		Renderer__ctor_m998612F51A0E95E387FC2032AB5FEE1304E346EE(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystemRenderer::get_pivot_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_get_pivot_Injected_mB393D1D0F0C69BD2F52A282FD0868E3E2585488C (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___ret0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_get_pivot_Injected_mB393D1D0F0C69BD2F52A282FD0868E3E2585488C_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *);
	static ParticleSystemRenderer_get_pivot_Injected_mB393D1D0F0C69BD2F52A282FD0868E3E2585488C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_pivot_Injected_mB393D1D0F0C69BD2F52A282FD0868E3E2585488C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_pivot_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.ParticleSystemRenderer::set_pivot_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_pivot_Injected_m8CE9A3B8B0EB136F3E658CFB2B00616D7D8A8AB3 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_pivot_Injected_m8CE9A3B8B0EB136F3E658CFB2B00616D7D8A8AB3_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *);
	static ParticleSystemRenderer_set_pivot_Injected_m8CE9A3B8B0EB136F3E658CFB2B00616D7D8A8AB3_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_pivot_Injected_m8CE9A3B8B0EB136F3E658CFB2B00616D7D8A8AB3_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_pivot_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ParticleSystemRenderer::get_flip_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_get_flip_Injected_m08B63FCDBAB2C235981432A80901FE97A29DA985 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___ret0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_get_flip_Injected_m08B63FCDBAB2C235981432A80901FE97A29DA985_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *);
	static ParticleSystemRenderer_get_flip_Injected_m08B63FCDBAB2C235981432A80901FE97A29DA985_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_get_flip_Injected_m08B63FCDBAB2C235981432A80901FE97A29DA985_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::get_flip_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.ParticleSystemRenderer::set_flip_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystemRenderer_set_flip_Injected_mE957C7B1BCD98C9D2D19900F1CC8EE50ED1DD688 (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___value0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystemRenderer_set_flip_Injected_mE957C7B1BCD98C9D2D19900F1CC8EE50ED1DD688_ftn) (ParticleSystemRenderer_tD559F69F1B7EB14FD58CEB08E46657B6A54A6269 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *);
	static ParticleSystemRenderer_set_flip_Injected_mE957C7B1BCD98C9D2D19900F1CC8EE50ED1DD688_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystemRenderer_set_flip_Injected_mE957C7B1BCD98C9D2D19900F1CC8EE50ED1DD688_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystemRenderer::set_flip_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/CollisionModule
IL2CPP_EXTERN_C void CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09_marshal_pinvoke(const CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09& unmarshaled, CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CollisionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09_marshal_pinvoke_back(const CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09_marshaled_pinvoke& marshaled, CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CollisionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/CollisionModule
IL2CPP_EXTERN_C void CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09_marshal_pinvoke_cleanup(CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/CollisionModule
IL2CPP_EXTERN_C void CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09_marshal_com(const CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09& unmarshaled, CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CollisionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09_marshal_com_back(const CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09_marshaled_com& marshaled, CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CollisionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/CollisionModule
IL2CPP_EXTERN_C void CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09_marshal_com_cleanup(CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/CollisionModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollisionModule__ctor_m08D1EEAC52F7833F28D4FC2563120FF46A9E4220 (CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void CollisionModule__ctor_m08D1EEAC52F7833F28D4FC2563120FF46A9E4220_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09 * _thisAdjusted = reinterpret_cast<CollisionModule_t2018942ABE80D7FE280ADCE09399C56FF1BA8E09 *>(__this + _offset);
	CollisionModule__ctor_m08D1EEAC52F7833F28D4FC2563120FF46A9E4220(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ColorBySpeedModule
IL2CPP_EXTERN_C void ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F_marshal_pinvoke(const ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F& unmarshaled, ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F_marshal_pinvoke_back(const ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F_marshaled_pinvoke& marshaled, ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ColorBySpeedModule
IL2CPP_EXTERN_C void ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F_marshal_pinvoke_cleanup(ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ColorBySpeedModule
IL2CPP_EXTERN_C void ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F_marshal_com(const ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F& unmarshaled, ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F_marshal_com_back(const ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F_marshaled_com& marshaled, ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ColorBySpeedModule
IL2CPP_EXTERN_C void ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F_marshal_com_cleanup(ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/ColorBySpeedModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorBySpeedModule__ctor_m71C9D80CE70BB41F782B4C262D0FE40477A7D0AF (ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void ColorBySpeedModule__ctor_m71C9D80CE70BB41F782B4C262D0FE40477A7D0AF_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F * _thisAdjusted = reinterpret_cast<ColorBySpeedModule_tDB7CC6D16647BBB53D710394794B7C6C7799F58F *>(__this + _offset);
	ColorBySpeedModule__ctor_m71C9D80CE70BB41F782B4C262D0FE40477A7D0AF(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ColorOverLifetimeModule
IL2CPP_EXTERN_C void ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75_marshal_pinvoke(const ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75& unmarshaled, ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75_marshal_pinvoke_back(const ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75_marshaled_pinvoke& marshaled, ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ColorOverLifetimeModule
IL2CPP_EXTERN_C void ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75_marshal_pinvoke_cleanup(ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ColorOverLifetimeModule
IL2CPP_EXTERN_C void ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75_marshal_com(const ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75& unmarshaled, ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75_marshal_com_back(const ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75_marshaled_com& marshaled, ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ColorOverLifetimeModule
IL2CPP_EXTERN_C void ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75_marshal_com_cleanup(ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/ColorOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorOverLifetimeModule__ctor_m3AFAD44AD8E69F213881272693C23536EEBAB71D (ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void ColorOverLifetimeModule__ctor_m3AFAD44AD8E69F213881272693C23536EEBAB71D_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75 * _thisAdjusted = reinterpret_cast<ColorOverLifetimeModule_t84B571334582EB5A79D1C199366F06416FF7BC75 *>(__this + _offset);
	ColorOverLifetimeModule__ctor_m3AFAD44AD8E69F213881272693C23536EEBAB71D(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/CustomDataModule
IL2CPP_EXTERN_C void CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF_marshal_pinvoke(const CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF& unmarshaled, CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CustomDataModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF_marshal_pinvoke_back(const CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF_marshaled_pinvoke& marshaled, CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CustomDataModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/CustomDataModule
IL2CPP_EXTERN_C void CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF_marshal_pinvoke_cleanup(CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/CustomDataModule
IL2CPP_EXTERN_C void CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF_marshal_com(const CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF& unmarshaled, CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CustomDataModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF_marshal_com_back(const CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF_marshaled_com& marshaled, CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CustomDataModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/CustomDataModule
IL2CPP_EXTERN_C void CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF_marshal_com_cleanup(CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/CustomDataModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomDataModule__ctor_m8F46AC54F3CC41A63AFE9856B18CC41A9A237277 (CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void CustomDataModule__ctor_m8F46AC54F3CC41A63AFE9856B18CC41A9A237277_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF * _thisAdjusted = reinterpret_cast<CustomDataModule_t07CC64A5671610A5DDB063E0FEABA022C0F57EEF *>(__this + _offset);
	CustomDataModule__ctor_m8F46AC54F3CC41A63AFE9856B18CC41A9A237277(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
IL2CPP_EXTERN_C void EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshal_pinvoke(const EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D& unmarshaled, EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshal_pinvoke_back(const EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshaled_pinvoke& marshaled, EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
IL2CPP_EXTERN_C void EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshal_pinvoke_cleanup(EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
IL2CPP_EXTERN_C void EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshal_com(const EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D& unmarshaled, EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshal_com_back(const EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshaled_com& marshaled, EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
IL2CPP_EXTERN_C void EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshal_com_cleanup(EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmissionModule__ctor_m9DFD2A0BB8BFAD2191C54E6627FC3B0E641EDD51 (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void EmissionModule__ctor_m9DFD2A0BB8BFAD2191C54E6627FC3B0E641EDD51_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * _thisAdjusted = reinterpret_cast<EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *>(__this + _offset);
	EmissionModule__ctor_m9DFD2A0BB8BFAD2191C54E6627FC3B0E641EDD51(_thisAdjusted, ___particleSystem0, method);
}
// System.Boolean UnityEngine.ParticleSystem/EmissionModule::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EmissionModule_get_enabled_m9DB0C4A45B5D04CD88355536DD3E1DD1180DF107 (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * __this, const RuntimeMethod* method)
{
	{
		bool L_0;
		L_0 = EmissionModule_get_enabled_Injected_m69F97462BF229B56970ABFBA4BCE59AFAF17386C((EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  bool EmissionModule_get_enabled_m9DB0C4A45B5D04CD88355536DD3E1DD1180DF107_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * _thisAdjusted = reinterpret_cast<EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *>(__this + _offset);
	bool _returnValue;
	_returnValue = EmissionModule_get_enabled_m9DB0C4A45B5D04CD88355536DD3E1DD1180DF107(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmissionModule_set_enabled_mD9FD50C9F5EECD04B22D03E5C00C7DD64D2AC1F1 (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		EmissionModule_set_enabled_Injected_m35208A8279D12A78D5DBCF751BF1760869051C82((EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void EmissionModule_set_enabled_mD9FD50C9F5EECD04B22D03E5C00C7DD64D2AC1F1_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * _thisAdjusted = reinterpret_cast<EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *>(__this + _offset);
	EmissionModule_set_enabled_mD9FD50C9F5EECD04B22D03E5C00C7DD64D2AC1F1(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime(UnityEngine.ParticleSystem/MinMaxCurve)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmissionModule_set_rateOverTime_m4EE4643D8E8B8BEC37BCB16DB76C9B8E4E4C8F48 (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * __this, MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD  ___value0, const RuntimeMethod* method)
{
	{
		EmissionModule_set_rateOverTime_Injected_m19782FA56F82A6F0A6AFE6454E662F41098B6E3B((EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *)__this, (MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void EmissionModule_set_rateOverTime_m4EE4643D8E8B8BEC37BCB16DB76C9B8E4E4C8F48_AdjustorThunk (RuntimeObject * __this, MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD  ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * _thisAdjusted = reinterpret_cast<EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *>(__this + _offset);
	EmissionModule_set_rateOverTime_m4EE4643D8E8B8BEC37BCB16DB76C9B8E4E4C8F48(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/EmissionModule::get_rateOverTimeMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float EmissionModule_get_rateOverTimeMultiplier_mF8F50D01C0F4ADB44423852CD75A334179456FBA (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * __this, const RuntimeMethod* method)
{
	{
		float L_0;
		L_0 = EmissionModule_get_rateOverTimeMultiplier_Injected_m1893AB6D1B181596FB7CC1D080313785DB5E5791((EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  float EmissionModule_get_rateOverTimeMultiplier_mF8F50D01C0F4ADB44423852CD75A334179456FBA_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * _thisAdjusted = reinterpret_cast<EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *>(__this + _offset);
	float _returnValue;
	_returnValue = EmissionModule_get_rateOverTimeMultiplier_mF8F50D01C0F4ADB44423852CD75A334179456FBA(_thisAdjusted, method);
	return _returnValue;
}
// System.Boolean UnityEngine.ParticleSystem/EmissionModule::get_enabled_Injected(UnityEngine.ParticleSystem/EmissionModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EmissionModule_get_enabled_Injected_m69F97462BF229B56970ABFBA4BCE59AFAF17386C (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * ____unity_self0, const RuntimeMethod* method)
{
	typedef bool (*EmissionModule_get_enabled_Injected_m69F97462BF229B56970ABFBA4BCE59AFAF17386C_ftn) (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *);
	static EmissionModule_get_enabled_Injected_m69F97462BF229B56970ABFBA4BCE59AFAF17386C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_get_enabled_Injected_m69F97462BF229B56970ABFBA4BCE59AFAF17386C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::get_enabled_Injected(UnityEngine.ParticleSystem/EmissionModule&)");
	bool icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled_Injected(UnityEngine.ParticleSystem/EmissionModule&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmissionModule_set_enabled_Injected_m35208A8279D12A78D5DBCF751BF1760869051C82 (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * ____unity_self0, bool ___value1, const RuntimeMethod* method)
{
	typedef void (*EmissionModule_set_enabled_Injected_m35208A8279D12A78D5DBCF751BF1760869051C82_ftn) (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *, bool);
	static EmissionModule_set_enabled_Injected_m35208A8279D12A78D5DBCF751BF1760869051C82_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_set_enabled_Injected_m35208A8279D12A78D5DBCF751BF1760869051C82_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::set_enabled_Injected(UnityEngine.ParticleSystem/EmissionModule&,System.Boolean)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime_Injected(UnityEngine.ParticleSystem/EmissionModule&,UnityEngine.ParticleSystem/MinMaxCurve&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmissionModule_set_rateOverTime_Injected_m19782FA56F82A6F0A6AFE6454E662F41098B6E3B (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * ____unity_self0, MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD * ___value1, const RuntimeMethod* method)
{
	typedef void (*EmissionModule_set_rateOverTime_Injected_m19782FA56F82A6F0A6AFE6454E662F41098B6E3B_ftn) (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *, MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD *);
	static EmissionModule_set_rateOverTime_Injected_m19782FA56F82A6F0A6AFE6454E662F41098B6E3B_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_set_rateOverTime_Injected_m19782FA56F82A6F0A6AFE6454E662F41098B6E3B_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime_Injected(UnityEngine.ParticleSystem/EmissionModule&,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Single UnityEngine.ParticleSystem/EmissionModule::get_rateOverTimeMultiplier_Injected(UnityEngine.ParticleSystem/EmissionModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float EmissionModule_get_rateOverTimeMultiplier_Injected_m1893AB6D1B181596FB7CC1D080313785DB5E5791 (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * ____unity_self0, const RuntimeMethod* method)
{
	typedef float (*EmissionModule_get_rateOverTimeMultiplier_Injected_m1893AB6D1B181596FB7CC1D080313785DB5E5791_ftn) (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *);
	static EmissionModule_get_rateOverTimeMultiplier_Injected_m1893AB6D1B181596FB7CC1D080313785DB5E5791_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_get_rateOverTimeMultiplier_Injected_m1893AB6D1B181596FB7CC1D080313785DB5E5791_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::get_rateOverTimeMultiplier_Injected(UnityEngine.ParticleSystem/EmissionModule&)");
	float icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmitParams
IL2CPP_EXTERN_C void EmitParams_t4F6429654653488A5D430701CD0743D011807CCC_marshal_pinvoke(const EmitParams_t4F6429654653488A5D430701CD0743D011807CCC& unmarshaled, EmitParams_t4F6429654653488A5D430701CD0743D011807CCC_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Particle_0 = unmarshaled.get_m_Particle_0();
	marshaled.___m_PositionSet_1 = static_cast<int32_t>(unmarshaled.get_m_PositionSet_1());
	marshaled.___m_VelocitySet_2 = static_cast<int32_t>(unmarshaled.get_m_VelocitySet_2());
	marshaled.___m_AxisOfRotationSet_3 = static_cast<int32_t>(unmarshaled.get_m_AxisOfRotationSet_3());
	marshaled.___m_RotationSet_4 = static_cast<int32_t>(unmarshaled.get_m_RotationSet_4());
	marshaled.___m_AngularVelocitySet_5 = static_cast<int32_t>(unmarshaled.get_m_AngularVelocitySet_5());
	marshaled.___m_StartSizeSet_6 = static_cast<int32_t>(unmarshaled.get_m_StartSizeSet_6());
	marshaled.___m_StartColorSet_7 = static_cast<int32_t>(unmarshaled.get_m_StartColorSet_7());
	marshaled.___m_RandomSeedSet_8 = static_cast<int32_t>(unmarshaled.get_m_RandomSeedSet_8());
	marshaled.___m_StartLifetimeSet_9 = static_cast<int32_t>(unmarshaled.get_m_StartLifetimeSet_9());
	marshaled.___m_MeshIndexSet_10 = static_cast<int32_t>(unmarshaled.get_m_MeshIndexSet_10());
	marshaled.___m_ApplyShapeToPosition_11 = static_cast<int32_t>(unmarshaled.get_m_ApplyShapeToPosition_11());
}
IL2CPP_EXTERN_C void EmitParams_t4F6429654653488A5D430701CD0743D011807CCC_marshal_pinvoke_back(const EmitParams_t4F6429654653488A5D430701CD0743D011807CCC_marshaled_pinvoke& marshaled, EmitParams_t4F6429654653488A5D430701CD0743D011807CCC& unmarshaled)
{
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  unmarshaled_m_Particle_temp_0;
	memset((&unmarshaled_m_Particle_temp_0), 0, sizeof(unmarshaled_m_Particle_temp_0));
	unmarshaled_m_Particle_temp_0 = marshaled.___m_Particle_0;
	unmarshaled.set_m_Particle_0(unmarshaled_m_Particle_temp_0);
	bool unmarshaled_m_PositionSet_temp_1 = false;
	unmarshaled_m_PositionSet_temp_1 = static_cast<bool>(marshaled.___m_PositionSet_1);
	unmarshaled.set_m_PositionSet_1(unmarshaled_m_PositionSet_temp_1);
	bool unmarshaled_m_VelocitySet_temp_2 = false;
	unmarshaled_m_VelocitySet_temp_2 = static_cast<bool>(marshaled.___m_VelocitySet_2);
	unmarshaled.set_m_VelocitySet_2(unmarshaled_m_VelocitySet_temp_2);
	bool unmarshaled_m_AxisOfRotationSet_temp_3 = false;
	unmarshaled_m_AxisOfRotationSet_temp_3 = static_cast<bool>(marshaled.___m_AxisOfRotationSet_3);
	unmarshaled.set_m_AxisOfRotationSet_3(unmarshaled_m_AxisOfRotationSet_temp_3);
	bool unmarshaled_m_RotationSet_temp_4 = false;
	unmarshaled_m_RotationSet_temp_4 = static_cast<bool>(marshaled.___m_RotationSet_4);
	unmarshaled.set_m_RotationSet_4(unmarshaled_m_RotationSet_temp_4);
	bool unmarshaled_m_AngularVelocitySet_temp_5 = false;
	unmarshaled_m_AngularVelocitySet_temp_5 = static_cast<bool>(marshaled.___m_AngularVelocitySet_5);
	unmarshaled.set_m_AngularVelocitySet_5(unmarshaled_m_AngularVelocitySet_temp_5);
	bool unmarshaled_m_StartSizeSet_temp_6 = false;
	unmarshaled_m_StartSizeSet_temp_6 = static_cast<bool>(marshaled.___m_StartSizeSet_6);
	unmarshaled.set_m_StartSizeSet_6(unmarshaled_m_StartSizeSet_temp_6);
	bool unmarshaled_m_StartColorSet_temp_7 = false;
	unmarshaled_m_StartColorSet_temp_7 = static_cast<bool>(marshaled.___m_StartColorSet_7);
	unmarshaled.set_m_StartColorSet_7(unmarshaled_m_StartColorSet_temp_7);
	bool unmarshaled_m_RandomSeedSet_temp_8 = false;
	unmarshaled_m_RandomSeedSet_temp_8 = static_cast<bool>(marshaled.___m_RandomSeedSet_8);
	unmarshaled.set_m_RandomSeedSet_8(unmarshaled_m_RandomSeedSet_temp_8);
	bool unmarshaled_m_StartLifetimeSet_temp_9 = false;
	unmarshaled_m_StartLifetimeSet_temp_9 = static_cast<bool>(marshaled.___m_StartLifetimeSet_9);
	unmarshaled.set_m_StartLifetimeSet_9(unmarshaled_m_StartLifetimeSet_temp_9);
	bool unmarshaled_m_MeshIndexSet_temp_10 = false;
	unmarshaled_m_MeshIndexSet_temp_10 = static_cast<bool>(marshaled.___m_MeshIndexSet_10);
	unmarshaled.set_m_MeshIndexSet_10(unmarshaled_m_MeshIndexSet_temp_10);
	bool unmarshaled_m_ApplyShapeToPosition_temp_11 = false;
	unmarshaled_m_ApplyShapeToPosition_temp_11 = static_cast<bool>(marshaled.___m_ApplyShapeToPosition_11);
	unmarshaled.set_m_ApplyShapeToPosition_11(unmarshaled_m_ApplyShapeToPosition_temp_11);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmitParams
IL2CPP_EXTERN_C void EmitParams_t4F6429654653488A5D430701CD0743D011807CCC_marshal_pinvoke_cleanup(EmitParams_t4F6429654653488A5D430701CD0743D011807CCC_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmitParams
IL2CPP_EXTERN_C void EmitParams_t4F6429654653488A5D430701CD0743D011807CCC_marshal_com(const EmitParams_t4F6429654653488A5D430701CD0743D011807CCC& unmarshaled, EmitParams_t4F6429654653488A5D430701CD0743D011807CCC_marshaled_com& marshaled)
{
	marshaled.___m_Particle_0 = unmarshaled.get_m_Particle_0();
	marshaled.___m_PositionSet_1 = static_cast<int32_t>(unmarshaled.get_m_PositionSet_1());
	marshaled.___m_VelocitySet_2 = static_cast<int32_t>(unmarshaled.get_m_VelocitySet_2());
	marshaled.___m_AxisOfRotationSet_3 = static_cast<int32_t>(unmarshaled.get_m_AxisOfRotationSet_3());
	marshaled.___m_RotationSet_4 = static_cast<int32_t>(unmarshaled.get_m_RotationSet_4());
	marshaled.___m_AngularVelocitySet_5 = static_cast<int32_t>(unmarshaled.get_m_AngularVelocitySet_5());
	marshaled.___m_StartSizeSet_6 = static_cast<int32_t>(unmarshaled.get_m_StartSizeSet_6());
	marshaled.___m_StartColorSet_7 = static_cast<int32_t>(unmarshaled.get_m_StartColorSet_7());
	marshaled.___m_RandomSeedSet_8 = static_cast<int32_t>(unmarshaled.get_m_RandomSeedSet_8());
	marshaled.___m_StartLifetimeSet_9 = static_cast<int32_t>(unmarshaled.get_m_StartLifetimeSet_9());
	marshaled.___m_MeshIndexSet_10 = static_cast<int32_t>(unmarshaled.get_m_MeshIndexSet_10());
	marshaled.___m_ApplyShapeToPosition_11 = static_cast<int32_t>(unmarshaled.get_m_ApplyShapeToPosition_11());
}
IL2CPP_EXTERN_C void EmitParams_t4F6429654653488A5D430701CD0743D011807CCC_marshal_com_back(const EmitParams_t4F6429654653488A5D430701CD0743D011807CCC_marshaled_com& marshaled, EmitParams_t4F6429654653488A5D430701CD0743D011807CCC& unmarshaled)
{
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1  unmarshaled_m_Particle_temp_0;
	memset((&unmarshaled_m_Particle_temp_0), 0, sizeof(unmarshaled_m_Particle_temp_0));
	unmarshaled_m_Particle_temp_0 = marshaled.___m_Particle_0;
	unmarshaled.set_m_Particle_0(unmarshaled_m_Particle_temp_0);
	bool unmarshaled_m_PositionSet_temp_1 = false;
	unmarshaled_m_PositionSet_temp_1 = static_cast<bool>(marshaled.___m_PositionSet_1);
	unmarshaled.set_m_PositionSet_1(unmarshaled_m_PositionSet_temp_1);
	bool unmarshaled_m_VelocitySet_temp_2 = false;
	unmarshaled_m_VelocitySet_temp_2 = static_cast<bool>(marshaled.___m_VelocitySet_2);
	unmarshaled.set_m_VelocitySet_2(unmarshaled_m_VelocitySet_temp_2);
	bool unmarshaled_m_AxisOfRotationSet_temp_3 = false;
	unmarshaled_m_AxisOfRotationSet_temp_3 = static_cast<bool>(marshaled.___m_AxisOfRotationSet_3);
	unmarshaled.set_m_AxisOfRotationSet_3(unmarshaled_m_AxisOfRotationSet_temp_3);
	bool unmarshaled_m_RotationSet_temp_4 = false;
	unmarshaled_m_RotationSet_temp_4 = static_cast<bool>(marshaled.___m_RotationSet_4);
	unmarshaled.set_m_RotationSet_4(unmarshaled_m_RotationSet_temp_4);
	bool unmarshaled_m_AngularVelocitySet_temp_5 = false;
	unmarshaled_m_AngularVelocitySet_temp_5 = static_cast<bool>(marshaled.___m_AngularVelocitySet_5);
	unmarshaled.set_m_AngularVelocitySet_5(unmarshaled_m_AngularVelocitySet_temp_5);
	bool unmarshaled_m_StartSizeSet_temp_6 = false;
	unmarshaled_m_StartSizeSet_temp_6 = static_cast<bool>(marshaled.___m_StartSizeSet_6);
	unmarshaled.set_m_StartSizeSet_6(unmarshaled_m_StartSizeSet_temp_6);
	bool unmarshaled_m_StartColorSet_temp_7 = false;
	unmarshaled_m_StartColorSet_temp_7 = static_cast<bool>(marshaled.___m_StartColorSet_7);
	unmarshaled.set_m_StartColorSet_7(unmarshaled_m_StartColorSet_temp_7);
	bool unmarshaled_m_RandomSeedSet_temp_8 = false;
	unmarshaled_m_RandomSeedSet_temp_8 = static_cast<bool>(marshaled.___m_RandomSeedSet_8);
	unmarshaled.set_m_RandomSeedSet_8(unmarshaled_m_RandomSeedSet_temp_8);
	bool unmarshaled_m_StartLifetimeSet_temp_9 = false;
	unmarshaled_m_StartLifetimeSet_temp_9 = static_cast<bool>(marshaled.___m_StartLifetimeSet_9);
	unmarshaled.set_m_StartLifetimeSet_9(unmarshaled_m_StartLifetimeSet_temp_9);
	bool unmarshaled_m_MeshIndexSet_temp_10 = false;
	unmarshaled_m_MeshIndexSet_temp_10 = static_cast<bool>(marshaled.___m_MeshIndexSet_10);
	unmarshaled.set_m_MeshIndexSet_10(unmarshaled_m_MeshIndexSet_temp_10);
	bool unmarshaled_m_ApplyShapeToPosition_temp_11 = false;
	unmarshaled_m_ApplyShapeToPosition_temp_11 = static_cast<bool>(marshaled.___m_ApplyShapeToPosition_11);
	unmarshaled.set_m_ApplyShapeToPosition_11(unmarshaled_m_ApplyShapeToPosition_temp_11);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmitParams
IL2CPP_EXTERN_C void EmitParams_t4F6429654653488A5D430701CD0743D011807CCC_marshal_com_cleanup(EmitParams_t4F6429654653488A5D430701CD0743D011807CCC_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ExternalForcesModule
IL2CPP_EXTERN_C void ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5_marshal_pinvoke(const ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5& unmarshaled, ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ExternalForcesModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5_marshal_pinvoke_back(const ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5_marshaled_pinvoke& marshaled, ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ExternalForcesModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ExternalForcesModule
IL2CPP_EXTERN_C void ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5_marshal_pinvoke_cleanup(ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ExternalForcesModule
IL2CPP_EXTERN_C void ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5_marshal_com(const ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5& unmarshaled, ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ExternalForcesModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5_marshal_com_back(const ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5_marshaled_com& marshaled, ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ExternalForcesModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ExternalForcesModule
IL2CPP_EXTERN_C void ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5_marshal_com_cleanup(ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/ExternalForcesModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExternalForcesModule__ctor_m2FB0D6776AAAB6731E3E80BA6ADD858458976DF5 (ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void ExternalForcesModule__ctor_m2FB0D6776AAAB6731E3E80BA6ADD858458976DF5_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5 * _thisAdjusted = reinterpret_cast<ExternalForcesModule_tBF4BE6C6AA63EB8C57DE1B0EDF3726B2F59FA7B5 *>(__this + _offset);
	ExternalForcesModule__ctor_m2FB0D6776AAAB6731E3E80BA6ADD858458976DF5(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ForceOverLifetimeModule
IL2CPP_EXTERN_C void ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7_marshal_pinvoke(const ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7& unmarshaled, ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ForceOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7_marshal_pinvoke_back(const ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7_marshaled_pinvoke& marshaled, ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ForceOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ForceOverLifetimeModule
IL2CPP_EXTERN_C void ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7_marshal_pinvoke_cleanup(ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ForceOverLifetimeModule
IL2CPP_EXTERN_C void ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7_marshal_com(const ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7& unmarshaled, ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ForceOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7_marshal_com_back(const ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7_marshaled_com& marshaled, ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ForceOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ForceOverLifetimeModule
IL2CPP_EXTERN_C void ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7_marshal_com_cleanup(ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/ForceOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ForceOverLifetimeModule__ctor_m4FF722421C5B1449CB900FF0506EF33549A450CD (ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void ForceOverLifetimeModule__ctor_m4FF722421C5B1449CB900FF0506EF33549A450CD_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7 * _thisAdjusted = reinterpret_cast<ForceOverLifetimeModule_t55EF1F67023C6920BB93B40FAC17E5665E9BBCF7 *>(__this + _offset);
	ForceOverLifetimeModule__ctor_m4FF722421C5B1449CB900FF0506EF33549A450CD(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/InheritVelocityModule
IL2CPP_EXTERN_C void InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619_marshal_pinvoke(const InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619& unmarshaled, InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'InheritVelocityModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619_marshal_pinvoke_back(const InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619_marshaled_pinvoke& marshaled, InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'InheritVelocityModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/InheritVelocityModule
IL2CPP_EXTERN_C void InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619_marshal_pinvoke_cleanup(InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/InheritVelocityModule
IL2CPP_EXTERN_C void InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619_marshal_com(const InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619& unmarshaled, InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'InheritVelocityModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619_marshal_com_back(const InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619_marshaled_com& marshaled, InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'InheritVelocityModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/InheritVelocityModule
IL2CPP_EXTERN_C void InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619_marshal_com_cleanup(InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/InheritVelocityModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InheritVelocityModule__ctor_m76B46612309334A443F9939A47FBAAF024606892 (InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void InheritVelocityModule__ctor_m76B46612309334A443F9939A47FBAAF024606892_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619 * _thisAdjusted = reinterpret_cast<InheritVelocityModule_t25F2271E4D9ED5D27234CDD00271CB7D435D9619 *>(__this + _offset);
	InheritVelocityModule__ctor_m76B46612309334A443F9939A47FBAAF024606892(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/LifetimeByEmitterSpeedModule
IL2CPP_EXTERN_C void LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1_marshal_pinvoke(const LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1& unmarshaled, LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LifetimeByEmitterSpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1_marshal_pinvoke_back(const LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1_marshaled_pinvoke& marshaled, LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LifetimeByEmitterSpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/LifetimeByEmitterSpeedModule
IL2CPP_EXTERN_C void LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1_marshal_pinvoke_cleanup(LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/LifetimeByEmitterSpeedModule
IL2CPP_EXTERN_C void LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1_marshal_com(const LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1& unmarshaled, LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LifetimeByEmitterSpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1_marshal_com_back(const LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1_marshaled_com& marshaled, LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LifetimeByEmitterSpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/LifetimeByEmitterSpeedModule
IL2CPP_EXTERN_C void LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1_marshal_com_cleanup(LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/LifetimeByEmitterSpeedModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LifetimeByEmitterSpeedModule__ctor_m80A22170A2E3047A3B364FE232CC4EB289F87BED (LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void LifetimeByEmitterSpeedModule__ctor_m80A22170A2E3047A3B364FE232CC4EB289F87BED_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1 * _thisAdjusted = reinterpret_cast<LifetimeByEmitterSpeedModule_t9230273C6231EFAA74F1E7A9B1BB0CCC01208CD1 *>(__this + _offset);
	LifetimeByEmitterSpeedModule__ctor_m80A22170A2E3047A3B364FE232CC4EB289F87BED(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/LightsModule
IL2CPP_EXTERN_C void LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705_marshal_pinvoke(const LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705& unmarshaled, LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LightsModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705_marshal_pinvoke_back(const LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705_marshaled_pinvoke& marshaled, LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LightsModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/LightsModule
IL2CPP_EXTERN_C void LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705_marshal_pinvoke_cleanup(LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/LightsModule
IL2CPP_EXTERN_C void LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705_marshal_com(const LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705& unmarshaled, LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LightsModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705_marshal_com_back(const LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705_marshaled_com& marshaled, LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LightsModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/LightsModule
IL2CPP_EXTERN_C void LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705_marshal_com_cleanup(LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/LightsModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LightsModule__ctor_mD59B002A60EB418DACA4986EC1C4D34B8DAC555F (LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void LightsModule__ctor_mD59B002A60EB418DACA4986EC1C4D34B8DAC555F_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705 * _thisAdjusted = reinterpret_cast<LightsModule_t74903A9B68956FEDBDCC81420474A4B9AE399705 *>(__this + _offset);
	LightsModule__ctor_mD59B002A60EB418DACA4986EC1C4D34B8DAC555F(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule
IL2CPP_EXTERN_C void LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C_marshal_pinvoke(const LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C& unmarshaled, LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LimitVelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C_marshal_pinvoke_back(const LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C_marshaled_pinvoke& marshaled, LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LimitVelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule
IL2CPP_EXTERN_C void LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C_marshal_pinvoke_cleanup(LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule
IL2CPP_EXTERN_C void LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C_marshal_com(const LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C& unmarshaled, LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LimitVelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C_marshal_com_back(const LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C_marshaled_com& marshaled, LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LimitVelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule
IL2CPP_EXTERN_C void LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C_marshal_com_cleanup(LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LimitVelocityOverLifetimeModule__ctor_mA327985B69E145AAC3FFEE42EE44A81D7800BCCC (LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void LimitVelocityOverLifetimeModule__ctor_mA327985B69E145AAC3FFEE42EE44A81D7800BCCC_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C * _thisAdjusted = reinterpret_cast<LimitVelocityOverLifetimeModule_tA8F1AC2538B089795E58CC7AEC7E8D9E60B5669C *>(__this + _offset);
	LimitVelocityOverLifetimeModule__ctor_mA327985B69E145AAC3FFEE42EE44A81D7800BCCC(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
IL2CPP_EXTERN_C void MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshal_pinvoke(const MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B& unmarshaled, MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshal_pinvoke_back(const MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshaled_pinvoke& marshaled, MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
IL2CPP_EXTERN_C void MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshal_pinvoke_cleanup(MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
IL2CPP_EXTERN_C void MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshal_com(const MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B& unmarshaled, MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshal_com_back(const MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshaled_com& marshaled, MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
IL2CPP_EXTERN_C void MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshal_com_cleanup(MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule__ctor_m34F626B568C6D3C80A036409049761C8316E6A76 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule__ctor_m34F626B568C6D3C80A036409049761C8316E6A76_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule__ctor_m34F626B568C6D3C80A036409049761C8316E6A76(_thisAdjusted, ___particleSystem0, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_duration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_duration_mD516595EFDD66C83A727BDD7EF495069B4EB6132 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		float L_0;
		L_0 = MainModule_get_duration_Injected_mBD7B5FEFFD26DB833052D35A3AC492699B4BEE41((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  float MainModule_get_duration_mD516595EFDD66C83A727BDD7EF495069B4EB6132_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	float _returnValue;
	_returnValue = MainModule_get_duration_mD516595EFDD66C83A727BDD7EF495069B4EB6132(_thisAdjusted, method);
	return _returnValue;
}
// System.Boolean UnityEngine.ParticleSystem/MainModule::get_loop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MainModule_get_loop_mE45A1A4A1DF6E2590B7B13A73A9A95197DA346F8 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		bool L_0;
		L_0 = MainModule_get_loop_Injected_mD8FD195A3CF5008FD319636A3945DE2F4F0EEBD2((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  bool MainModule_get_loop_mE45A1A4A1DF6E2590B7B13A73A9A95197DA346F8_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	bool _returnValue;
	_returnValue = MainModule_get_loop_mE45A1A4A1DF6E2590B7B13A73A9A95197DA346F8(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_loop(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_loop_m9F43A092EFABBE92662DF9EDA73AF8D9C72DABB2 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		MainModule_set_loop_Injected_m064E3166CAA11D0842F105E473A231C0C15DE1B3((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_loop_m9F43A092EFABBE92662DF9EDA73AF8D9C72DABB2_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_loop_m9F43A092EFABBE92662DF9EDA73AF8D9C72DABB2(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startDelayMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startDelayMultiplier_mC38890C8853463BB25F031BB8884F3D8B0AF581E (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		float L_0;
		L_0 = MainModule_get_startDelayMultiplier_Injected_mEC1750CCDB02393B6FB1E766FDF2C875A9DA5247((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  float MainModule_get_startDelayMultiplier_mC38890C8853463BB25F031BB8884F3D8B0AF581E_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	float _returnValue;
	_returnValue = MainModule_get_startDelayMultiplier_mC38890C8853463BB25F031BB8884F3D8B0AF581E(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startDelayMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startDelayMultiplier_m23FC7BAFE2DCF940C398D6BE53606E23E1715464 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		MainModule_set_startDelayMultiplier_Injected_m6C0ADED60F67E9C685F6A4BCAF6E658A498143CC((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_startDelayMultiplier_m23FC7BAFE2DCF940C398D6BE53606E23E1715464_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_startDelayMultiplier_m23FC7BAFE2DCF940C398D6BE53606E23E1715464(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startLifetimeMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startLifetimeMultiplier_m8D2F788F9A1F5768E64F4511F2158E3BFAEB474D (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		float L_0;
		L_0 = MainModule_get_startLifetimeMultiplier_Injected_m5DFF410088ECCDC91DB86AEFB2BB1DCB6411666D((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  float MainModule_get_startLifetimeMultiplier_m8D2F788F9A1F5768E64F4511F2158E3BFAEB474D_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	float _returnValue;
	_returnValue = MainModule_get_startLifetimeMultiplier_m8D2F788F9A1F5768E64F4511F2158E3BFAEB474D(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startLifetimeMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startLifetimeMultiplier_m219938FDA9324C185D998815836B50FEE3AC0D8D (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		MainModule_set_startLifetimeMultiplier_Injected_m2000D61A48B2CB2AAEE1FCBB141D58299A29446E((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_startLifetimeMultiplier_m219938FDA9324C185D998815836B50FEE3AC0D8D_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_startLifetimeMultiplier_m219938FDA9324C185D998815836B50FEE3AC0D8D(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startSpeedMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startSpeedMultiplier_mE165D2B967AD8A8827D07DBD694F1C7167C80336 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		float L_0;
		L_0 = MainModule_get_startSpeedMultiplier_Injected_mC59B6818EF7C52D84832165A74B0F5AFD2CC02CA((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  float MainModule_get_startSpeedMultiplier_mE165D2B967AD8A8827D07DBD694F1C7167C80336_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	float _returnValue;
	_returnValue = MainModule_get_startSpeedMultiplier_mE165D2B967AD8A8827D07DBD694F1C7167C80336(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeedMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startSpeedMultiplier_mA3C6B512351C3BFA8E64B40936B3353845A637F2 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		MainModule_set_startSpeedMultiplier_Injected_mD454E7C2AF126C2F46265F32E00A7ACA8D632B5C((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_startSpeedMultiplier_mA3C6B512351C3BFA8E64B40936B3353845A637F2_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_startSpeedMultiplier_mA3C6B512351C3BFA8E64B40936B3353845A637F2(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startSizeMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startSizeMultiplier_m820351EE78DF731C1AFA8309ECB5ADF3E1166279 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		float L_0;
		L_0 = MainModule_get_startSizeMultiplier_Injected_m04AAFC24A6357D932E1F36C9FA94CA955FCB051F((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  float MainModule_get_startSizeMultiplier_m820351EE78DF731C1AFA8309ECB5ADF3E1166279_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	float _returnValue;
	_returnValue = MainModule_get_startSizeMultiplier_m820351EE78DF731C1AFA8309ECB5ADF3E1166279(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSizeMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startSizeMultiplier_m231F400517E7F96EFF7C3C31E59B7B8DD0A1DB64 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		MainModule_set_startSizeMultiplier_Injected_m09FF5ECF76CB00E810A2EC29926CF5EE12BD94D5((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_startSizeMultiplier_m231F400517E7F96EFF7C3C31E59B7B8DD0A1DB64_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_startSizeMultiplier_m231F400517E7F96EFF7C3C31E59B7B8DD0A1DB64(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationMultiplier_m72C5648E64889B162553000EF3B7347F47B1F84A (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		float L_0;
		L_0 = MainModule_get_startRotationMultiplier_Injected_mFF1B0ED143B0700ACBC5499AE1EED0F9C2334FD8((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  float MainModule_get_startRotationMultiplier_m72C5648E64889B162553000EF3B7347F47B1F84A_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	float _returnValue;
	_returnValue = MainModule_get_startRotationMultiplier_m72C5648E64889B162553000EF3B7347F47B1F84A(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationMultiplier_mB7085FB7FF45BDD4C71E08226C6D5F5635CB47EE (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		MainModule_set_startRotationMultiplier_Injected_m1D25033340DFEC86D7FE7537E0EC7EC9C435A140((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_startRotationMultiplier_mB7085FB7FF45BDD4C71E08226C6D5F5635CB47EE_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_startRotationMultiplier_mB7085FB7FF45BDD4C71E08226C6D5F5635CB47EE(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationXMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationXMultiplier_m15710A32A7861DAF1A0CD55DB1835EC22617F987 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		float L_0;
		L_0 = MainModule_get_startRotationXMultiplier_Injected_mA907E58F38775A2AD084A95BD11470E9312A0132((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  float MainModule_get_startRotationXMultiplier_m15710A32A7861DAF1A0CD55DB1835EC22617F987_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	float _returnValue;
	_returnValue = MainModule_get_startRotationXMultiplier_m15710A32A7861DAF1A0CD55DB1835EC22617F987(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationXMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationXMultiplier_mA489325EDA3D644481BFF44376627B0B6EFB81CD (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		MainModule_set_startRotationXMultiplier_Injected_m68AC8C3218148C2212765904127F87ACB58F6C69((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_startRotationXMultiplier_mA489325EDA3D644481BFF44376627B0B6EFB81CD_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_startRotationXMultiplier_mA489325EDA3D644481BFF44376627B0B6EFB81CD(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationYMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationYMultiplier_mC9C0B9123DDF9D37EF3E5291164E16AB95308011 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		float L_0;
		L_0 = MainModule_get_startRotationYMultiplier_Injected_m4A540D71853039082422C270FC1A14226FDFB43C((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  float MainModule_get_startRotationYMultiplier_mC9C0B9123DDF9D37EF3E5291164E16AB95308011_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	float _returnValue;
	_returnValue = MainModule_get_startRotationYMultiplier_mC9C0B9123DDF9D37EF3E5291164E16AB95308011(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationYMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationYMultiplier_m45DC00CC3E94A7CFCEA522F105F93BCE1B727639 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		MainModule_set_startRotationYMultiplier_Injected_m05F30D5DAFC51E007F1D350A6CB8963EC269901E((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_startRotationYMultiplier_m45DC00CC3E94A7CFCEA522F105F93BCE1B727639_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_startRotationYMultiplier_m45DC00CC3E94A7CFCEA522F105F93BCE1B727639(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationZMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationZMultiplier_m736451211917A59B78A3B444963D305BD377898D (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		float L_0;
		L_0 = MainModule_get_startRotationZMultiplier_Injected_mF08F4933FBA184D447C01ED33E23B0C3B6067735((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  float MainModule_get_startRotationZMultiplier_m736451211917A59B78A3B444963D305BD377898D_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	float _returnValue;
	_returnValue = MainModule_get_startRotationZMultiplier_m736451211917A59B78A3B444963D305BD377898D(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationZMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationZMultiplier_m9D604421CED73B2B110158F1FAF955832FB9A6D5 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		MainModule_set_startRotationZMultiplier_Injected_m3ABFA58E2E34C80CBB9E6BADC32F12CD34B115A0((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_startRotationZMultiplier_m9D604421CED73B2B110158F1FAF955832FB9A6D5_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_startRotationZMultiplier_m9D604421CED73B2B110158F1FAF955832FB9A6D5(_thisAdjusted, ___value0, method);
}
// UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MainModule::get_startColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  MainModule_get_startColor_mC8BAD25B55ACF9586BAFEECF047CCE5268123DD4 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		MainModule_get_startColor_Injected_mB8F4FB3EAB113FEE09389BED8F41E0F047128276((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, (MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 *)(&V_0), /*hidden argument*/NULL);
		MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  L_0 = V_0;
		return L_0;
	}
}
IL2CPP_EXTERN_C  MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  MainModule_get_startColor_mC8BAD25B55ACF9586BAFEECF047CCE5268123DD4_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  _returnValue;
	_returnValue = MainModule_get_startColor_mC8BAD25B55ACF9586BAFEECF047CCE5268123DD4(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startColor(UnityEngine.ParticleSystem/MinMaxGradient)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  ___value0, const RuntimeMethod* method)
{
	{
		MainModule_set_startColor_Injected_mBDC348394A0F1920C4123AF0329DECB2869D6EE6((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, (MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42_AdjustorThunk (RuntimeObject * __this, MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_gravityModifierMultiplier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_gravityModifierMultiplier_m875B65CAEE457EB12A5CD763A5AA311DE014629A (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		float L_0;
		L_0 = MainModule_get_gravityModifierMultiplier_Injected_mB86E9F34B1B61A23C2F5CB9BFD06703BC8B9972D((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  float MainModule_get_gravityModifierMultiplier_m875B65CAEE457EB12A5CD763A5AA311DE014629A_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	float _returnValue;
	_returnValue = MainModule_get_gravityModifierMultiplier_m875B65CAEE457EB12A5CD763A5AA311DE014629A(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_gravityModifierMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_gravityModifierMultiplier_mBC64B6D0DEF548A94D0C0303855F43931A1F5684 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		MainModule_set_gravityModifierMultiplier_Injected_m2AEE5F42B889A4ED22D05AA5E76A87F43680D601((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_gravityModifierMultiplier_mBC64B6D0DEF548A94D0C0303855F43931A1F5684_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_gravityModifierMultiplier_mBC64B6D0DEF548A94D0C0303855F43931A1F5684(_thisAdjusted, ___value0, method);
}
// UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem/MainModule::get_simulationSpace()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MainModule_get_simulationSpace_mD08447602DF2E2AC9790D900A1BD04AB8D4FD0A2 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0;
		L_0 = MainModule_get_simulationSpace_Injected_mD56565C6B7CDDAFDD418BEB202E8759DCA45D69F((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t MainModule_get_simulationSpace_mD08447602DF2E2AC9790D900A1BD04AB8D4FD0A2_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = MainModule_get_simulationSpace_mD08447602DF2E2AC9790D900A1BD04AB8D4FD0A2(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpace(UnityEngine.ParticleSystemSimulationSpace)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_simulationSpace_m59583EC5A1D9D64EC8C0CCE275CE6669FC8E268F (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		MainModule_set_simulationSpace_Injected_mFD6F06EA32114BC5C9D9CF3C10A5A0066E40664C((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_simulationSpace_m59583EC5A1D9D64EC8C0CCE275CE6669FC8E268F_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_simulationSpace_m59583EC5A1D9D64EC8C0CCE275CE6669FC8E268F(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_simulationSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_simulationSpeed_m7BCFDA4BD65598CE9CEF34A029E4C03DAB4A3C53 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		float L_0;
		L_0 = MainModule_get_simulationSpeed_Injected_m12263544CFAE95F37D2A9B4C45AB58452AE963F6((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  float MainModule_get_simulationSpeed_m7BCFDA4BD65598CE9CEF34A029E4C03DAB4A3C53_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	float _returnValue;
	_returnValue = MainModule_get_simulationSpeed_m7BCFDA4BD65598CE9CEF34A029E4C03DAB4A3C53(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpeed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_simulationSpeed_m9EA9E4BE76B372EF9674568810BF4F5B403B2833 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		MainModule_set_simulationSpeed_Injected_m86AEA2C2313AC96FC68800A3871248ED2597E26C((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_simulationSpeed_m9EA9E4BE76B372EF9674568810BF4F5B403B2833_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_simulationSpeed_m9EA9E4BE76B372EF9674568810BF4F5B403B2833(_thisAdjusted, ___value0, method);
}
// UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem/MainModule::get_scalingMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MainModule_get_scalingMode_m2FEDBC7969A4BD0071B9467AFFD1B98EE64A9C51 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0;
		L_0 = MainModule_get_scalingMode_Injected_m0DD794410FA56B46A8651B4FC504CFB2484C0A02((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t MainModule_get_scalingMode_m2FEDBC7969A4BD0071B9467AFFD1B98EE64A9C51_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = MainModule_get_scalingMode_m2FEDBC7969A4BD0071B9467AFFD1B98EE64A9C51(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_scalingMode(UnityEngine.ParticleSystemScalingMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_scalingMode_m0992C03429ABFBD1021009BF669BF362705D4E23 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		MainModule_set_scalingMode_Injected_mEABFC2E8FE084F997A630B8D2A643B2C9249A5A0((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_scalingMode_m0992C03429ABFBD1021009BF669BF362705D4E23_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_scalingMode_m0992C03429ABFBD1021009BF669BF362705D4E23(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.ParticleSystem/MainModule::get_playOnAwake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MainModule_get_playOnAwake_m6E07F3ACCC2797500E70AF4A4A33C5C8425A52D5 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		bool L_0;
		L_0 = MainModule_get_playOnAwake_Injected_m87D4B5B2508D9CF9B3C61F84BC001945B20A06C2((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  bool MainModule_get_playOnAwake_m6E07F3ACCC2797500E70AF4A4A33C5C8425A52D5_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	bool _returnValue;
	_returnValue = MainModule_get_playOnAwake_m6E07F3ACCC2797500E70AF4A4A33C5C8425A52D5(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_playOnAwake(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_playOnAwake_m85E164C05D5C39B0B3C6139BE360BAB41B0D7FCF (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		MainModule_set_playOnAwake_Injected_mAE88821DF50EB8AFD7079C587CC7FB633626B5FB((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_playOnAwake_m85E164C05D5C39B0B3C6139BE360BAB41B0D7FCF_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_playOnAwake_m85E164C05D5C39B0B3C6139BE360BAB41B0D7FCF(_thisAdjusted, ___value0, method);
}
// System.Int32 UnityEngine.ParticleSystem/MainModule::get_maxParticles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MainModule_get_maxParticles_m4A95D2105C1B414331F15EF86ADBC23CC6C8DD35 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0;
		L_0 = MainModule_get_maxParticles_Injected_m260F43233BCA02794176F596FF08A96BFCBE0D91((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, /*hidden argument*/NULL);
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t MainModule_get_maxParticles_m4A95D2105C1B414331F15EF86ADBC23CC6C8DD35_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = MainModule_get_maxParticles_m4A95D2105C1B414331F15EF86ADBC23CC6C8DD35(_thisAdjusted, method);
	return _returnValue;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_maxParticles(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_maxParticles_m6FD14B44787B44D38DEDAE4FCBE9596A86A0460D (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		MainModule_set_maxParticles_Injected_m6D4D611540F53CC4D8F21D9DD7EF3F68FC50F471((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void MainModule_set_maxParticles_m6FD14B44787B44D38DEDAE4FCBE9596A86A0460D_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * _thisAdjusted = reinterpret_cast<MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *>(__this + _offset);
	MainModule_set_maxParticles_m6FD14B44787B44D38DEDAE4FCBE9596A86A0460D(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_duration_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_duration_Injected_mBD7B5FEFFD26DB833052D35A3AC492699B4BEE41 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef float (*MainModule_get_duration_Injected_mBD7B5FEFFD26DB833052D35A3AC492699B4BEE41_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_duration_Injected_mBD7B5FEFFD26DB833052D35A3AC492699B4BEE41_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_duration_Injected_mBD7B5FEFFD26DB833052D35A3AC492699B4BEE41_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_duration_Injected(UnityEngine.ParticleSystem/MainModule&)");
	float icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Boolean UnityEngine.ParticleSystem/MainModule::get_loop_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MainModule_get_loop_Injected_mD8FD195A3CF5008FD319636A3945DE2F4F0EEBD2 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef bool (*MainModule_get_loop_Injected_mD8FD195A3CF5008FD319636A3945DE2F4F0EEBD2_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_loop_Injected_mD8FD195A3CF5008FD319636A3945DE2F4F0EEBD2_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_loop_Injected_mD8FD195A3CF5008FD319636A3945DE2F4F0EEBD2_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_loop_Injected(UnityEngine.ParticleSystem/MainModule&)");
	bool icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_loop_Injected(UnityEngine.ParticleSystem/MainModule&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_loop_Injected_m064E3166CAA11D0842F105E473A231C0C15DE1B3 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, bool ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_loop_Injected_m064E3166CAA11D0842F105E473A231C0C15DE1B3_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, bool);
	static MainModule_set_loop_Injected_m064E3166CAA11D0842F105E473A231C0C15DE1B3_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_loop_Injected_m064E3166CAA11D0842F105E473A231C0C15DE1B3_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_loop_Injected(UnityEngine.ParticleSystem/MainModule&,System.Boolean)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startDelayMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startDelayMultiplier_Injected_mEC1750CCDB02393B6FB1E766FDF2C875A9DA5247 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef float (*MainModule_get_startDelayMultiplier_Injected_mEC1750CCDB02393B6FB1E766FDF2C875A9DA5247_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_startDelayMultiplier_Injected_mEC1750CCDB02393B6FB1E766FDF2C875A9DA5247_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_startDelayMultiplier_Injected_mEC1750CCDB02393B6FB1E766FDF2C875A9DA5247_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_startDelayMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)");
	float icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startDelayMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startDelayMultiplier_Injected_m6C0ADED60F67E9C685F6A4BCAF6E658A498143CC (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_startDelayMultiplier_Injected_m6C0ADED60F67E9C685F6A4BCAF6E658A498143CC_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, float);
	static MainModule_set_startDelayMultiplier_Injected_m6C0ADED60F67E9C685F6A4BCAF6E658A498143CC_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_startDelayMultiplier_Injected_m6C0ADED60F67E9C685F6A4BCAF6E658A498143CC_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_startDelayMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startLifetimeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startLifetimeMultiplier_Injected_m5DFF410088ECCDC91DB86AEFB2BB1DCB6411666D (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef float (*MainModule_get_startLifetimeMultiplier_Injected_m5DFF410088ECCDC91DB86AEFB2BB1DCB6411666D_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_startLifetimeMultiplier_Injected_m5DFF410088ECCDC91DB86AEFB2BB1DCB6411666D_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_startLifetimeMultiplier_Injected_m5DFF410088ECCDC91DB86AEFB2BB1DCB6411666D_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_startLifetimeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)");
	float icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startLifetimeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startLifetimeMultiplier_Injected_m2000D61A48B2CB2AAEE1FCBB141D58299A29446E (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_startLifetimeMultiplier_Injected_m2000D61A48B2CB2AAEE1FCBB141D58299A29446E_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, float);
	static MainModule_set_startLifetimeMultiplier_Injected_m2000D61A48B2CB2AAEE1FCBB141D58299A29446E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_startLifetimeMultiplier_Injected_m2000D61A48B2CB2AAEE1FCBB141D58299A29446E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_startLifetimeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startSpeedMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startSpeedMultiplier_Injected_mC59B6818EF7C52D84832165A74B0F5AFD2CC02CA (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef float (*MainModule_get_startSpeedMultiplier_Injected_mC59B6818EF7C52D84832165A74B0F5AFD2CC02CA_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_startSpeedMultiplier_Injected_mC59B6818EF7C52D84832165A74B0F5AFD2CC02CA_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_startSpeedMultiplier_Injected_mC59B6818EF7C52D84832165A74B0F5AFD2CC02CA_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_startSpeedMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)");
	float icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSpeedMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startSpeedMultiplier_Injected_mD454E7C2AF126C2F46265F32E00A7ACA8D632B5C (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_startSpeedMultiplier_Injected_mD454E7C2AF126C2F46265F32E00A7ACA8D632B5C_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, float);
	static MainModule_set_startSpeedMultiplier_Injected_mD454E7C2AF126C2F46265F32E00A7ACA8D632B5C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_startSpeedMultiplier_Injected_mD454E7C2AF126C2F46265F32E00A7ACA8D632B5C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_startSpeedMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startSizeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startSizeMultiplier_Injected_m04AAFC24A6357D932E1F36C9FA94CA955FCB051F (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef float (*MainModule_get_startSizeMultiplier_Injected_m04AAFC24A6357D932E1F36C9FA94CA955FCB051F_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_startSizeMultiplier_Injected_m04AAFC24A6357D932E1F36C9FA94CA955FCB051F_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_startSizeMultiplier_Injected_m04AAFC24A6357D932E1F36C9FA94CA955FCB051F_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_startSizeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)");
	float icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startSizeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startSizeMultiplier_Injected_m09FF5ECF76CB00E810A2EC29926CF5EE12BD94D5 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_startSizeMultiplier_Injected_m09FF5ECF76CB00E810A2EC29926CF5EE12BD94D5_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, float);
	static MainModule_set_startSizeMultiplier_Injected_m09FF5ECF76CB00E810A2EC29926CF5EE12BD94D5_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_startSizeMultiplier_Injected_m09FF5ECF76CB00E810A2EC29926CF5EE12BD94D5_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_startSizeMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationMultiplier_Injected_mFF1B0ED143B0700ACBC5499AE1EED0F9C2334FD8 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef float (*MainModule_get_startRotationMultiplier_Injected_mFF1B0ED143B0700ACBC5499AE1EED0F9C2334FD8_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_startRotationMultiplier_Injected_mFF1B0ED143B0700ACBC5499AE1EED0F9C2334FD8_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_startRotationMultiplier_Injected_mFF1B0ED143B0700ACBC5499AE1EED0F9C2334FD8_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_startRotationMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)");
	float icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationMultiplier_Injected_m1D25033340DFEC86D7FE7537E0EC7EC9C435A140 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_startRotationMultiplier_Injected_m1D25033340DFEC86D7FE7537E0EC7EC9C435A140_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, float);
	static MainModule_set_startRotationMultiplier_Injected_m1D25033340DFEC86D7FE7537E0EC7EC9C435A140_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_startRotationMultiplier_Injected_m1D25033340DFEC86D7FE7537E0EC7EC9C435A140_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_startRotationMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationXMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationXMultiplier_Injected_mA907E58F38775A2AD084A95BD11470E9312A0132 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef float (*MainModule_get_startRotationXMultiplier_Injected_mA907E58F38775A2AD084A95BD11470E9312A0132_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_startRotationXMultiplier_Injected_mA907E58F38775A2AD084A95BD11470E9312A0132_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_startRotationXMultiplier_Injected_mA907E58F38775A2AD084A95BD11470E9312A0132_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_startRotationXMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)");
	float icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationXMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationXMultiplier_Injected_m68AC8C3218148C2212765904127F87ACB58F6C69 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_startRotationXMultiplier_Injected_m68AC8C3218148C2212765904127F87ACB58F6C69_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, float);
	static MainModule_set_startRotationXMultiplier_Injected_m68AC8C3218148C2212765904127F87ACB58F6C69_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_startRotationXMultiplier_Injected_m68AC8C3218148C2212765904127F87ACB58F6C69_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_startRotationXMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationYMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationYMultiplier_Injected_m4A540D71853039082422C270FC1A14226FDFB43C (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef float (*MainModule_get_startRotationYMultiplier_Injected_m4A540D71853039082422C270FC1A14226FDFB43C_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_startRotationYMultiplier_Injected_m4A540D71853039082422C270FC1A14226FDFB43C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_startRotationYMultiplier_Injected_m4A540D71853039082422C270FC1A14226FDFB43C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_startRotationYMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)");
	float icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationYMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationYMultiplier_Injected_m05F30D5DAFC51E007F1D350A6CB8963EC269901E (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_startRotationYMultiplier_Injected_m05F30D5DAFC51E007F1D350A6CB8963EC269901E_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, float);
	static MainModule_set_startRotationYMultiplier_Injected_m05F30D5DAFC51E007F1D350A6CB8963EC269901E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_startRotationYMultiplier_Injected_m05F30D5DAFC51E007F1D350A6CB8963EC269901E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_startRotationYMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_startRotationZMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_startRotationZMultiplier_Injected_mF08F4933FBA184D447C01ED33E23B0C3B6067735 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef float (*MainModule_get_startRotationZMultiplier_Injected_mF08F4933FBA184D447C01ED33E23B0C3B6067735_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_startRotationZMultiplier_Injected_mF08F4933FBA184D447C01ED33E23B0C3B6067735_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_startRotationZMultiplier_Injected_mF08F4933FBA184D447C01ED33E23B0C3B6067735_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_startRotationZMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)");
	float icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startRotationZMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startRotationZMultiplier_Injected_m3ABFA58E2E34C80CBB9E6BADC32F12CD34B115A0 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_startRotationZMultiplier_Injected_m3ABFA58E2E34C80CBB9E6BADC32F12CD34B115A0_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, float);
	static MainModule_set_startRotationZMultiplier_Injected_m3ABFA58E2E34C80CBB9E6BADC32F12CD34B115A0_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_startRotationZMultiplier_Injected_m3ABFA58E2E34C80CBB9E6BADC32F12CD34B115A0_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_startRotationZMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::get_startColor_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystem/MinMaxGradient&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_get_startColor_Injected_mB8F4FB3EAB113FEE09389BED8F41E0F047128276 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 * ___ret1, const RuntimeMethod* method)
{
	typedef void (*MainModule_get_startColor_Injected_mB8F4FB3EAB113FEE09389BED8F41E0F047128276_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 *);
	static MainModule_get_startColor_Injected_mB8F4FB3EAB113FEE09389BED8F41E0F047128276_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_startColor_Injected_mB8F4FB3EAB113FEE09389BED8F41E0F047128276_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_startColor_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystem/MinMaxGradient&)");
	_il2cpp_icall_func(____unity_self0, ___ret1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startColor_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystem/MinMaxGradient&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startColor_Injected_mBDC348394A0F1920C4123AF0329DECB2869D6EE6 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 * ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_startColor_Injected_mBDC348394A0F1920C4123AF0329DECB2869D6EE6_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 *);
	static MainModule_set_startColor_Injected_mBDC348394A0F1920C4123AF0329DECB2869D6EE6_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_startColor_Injected_mBDC348394A0F1920C4123AF0329DECB2869D6EE6_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_startColor_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystem/MinMaxGradient&)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_gravityModifierMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_gravityModifierMultiplier_Injected_mB86E9F34B1B61A23C2F5CB9BFD06703BC8B9972D (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef float (*MainModule_get_gravityModifierMultiplier_Injected_mB86E9F34B1B61A23C2F5CB9BFD06703BC8B9972D_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_gravityModifierMultiplier_Injected_mB86E9F34B1B61A23C2F5CB9BFD06703BC8B9972D_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_gravityModifierMultiplier_Injected_mB86E9F34B1B61A23C2F5CB9BFD06703BC8B9972D_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_gravityModifierMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&)");
	float icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_gravityModifierMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_gravityModifierMultiplier_Injected_m2AEE5F42B889A4ED22D05AA5E76A87F43680D601 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_gravityModifierMultiplier_Injected_m2AEE5F42B889A4ED22D05AA5E76A87F43680D601_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, float);
	static MainModule_set_gravityModifierMultiplier_Injected_m2AEE5F42B889A4ED22D05AA5E76A87F43680D601_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_gravityModifierMultiplier_Injected_m2AEE5F42B889A4ED22D05AA5E76A87F43680D601_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_gravityModifierMultiplier_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem/MainModule::get_simulationSpace_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MainModule_get_simulationSpace_Injected_mD56565C6B7CDDAFDD418BEB202E8759DCA45D69F (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef int32_t (*MainModule_get_simulationSpace_Injected_mD56565C6B7CDDAFDD418BEB202E8759DCA45D69F_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_simulationSpace_Injected_mD56565C6B7CDDAFDD418BEB202E8759DCA45D69F_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_simulationSpace_Injected_mD56565C6B7CDDAFDD418BEB202E8759DCA45D69F_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_simulationSpace_Injected(UnityEngine.ParticleSystem/MainModule&)");
	int32_t icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpace_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystemSimulationSpace)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_simulationSpace_Injected_mFD6F06EA32114BC5C9D9CF3C10A5A0066E40664C (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, int32_t ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_simulationSpace_Injected_mFD6F06EA32114BC5C9D9CF3C10A5A0066E40664C_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, int32_t);
	static MainModule_set_simulationSpace_Injected_mFD6F06EA32114BC5C9D9CF3C10A5A0066E40664C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_simulationSpace_Injected_mFD6F06EA32114BC5C9D9CF3C10A5A0066E40664C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_simulationSpace_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystemSimulationSpace)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Single UnityEngine.ParticleSystem/MainModule::get_simulationSpeed_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MainModule_get_simulationSpeed_Injected_m12263544CFAE95F37D2A9B4C45AB58452AE963F6 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef float (*MainModule_get_simulationSpeed_Injected_m12263544CFAE95F37D2A9B4C45AB58452AE963F6_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_simulationSpeed_Injected_m12263544CFAE95F37D2A9B4C45AB58452AE963F6_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_simulationSpeed_Injected_m12263544CFAE95F37D2A9B4C45AB58452AE963F6_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_simulationSpeed_Injected(UnityEngine.ParticleSystem/MainModule&)");
	float icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_simulationSpeed_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_simulationSpeed_Injected_m86AEA2C2313AC96FC68800A3871248ED2597E26C (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_simulationSpeed_Injected_m86AEA2C2313AC96FC68800A3871248ED2597E26C_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, float);
	static MainModule_set_simulationSpeed_Injected_m86AEA2C2313AC96FC68800A3871248ED2597E26C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_simulationSpeed_Injected_m86AEA2C2313AC96FC68800A3871248ED2597E26C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_simulationSpeed_Injected(UnityEngine.ParticleSystem/MainModule&,System.Single)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem/MainModule::get_scalingMode_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MainModule_get_scalingMode_Injected_m0DD794410FA56B46A8651B4FC504CFB2484C0A02 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef int32_t (*MainModule_get_scalingMode_Injected_m0DD794410FA56B46A8651B4FC504CFB2484C0A02_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_scalingMode_Injected_m0DD794410FA56B46A8651B4FC504CFB2484C0A02_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_scalingMode_Injected_m0DD794410FA56B46A8651B4FC504CFB2484C0A02_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_scalingMode_Injected(UnityEngine.ParticleSystem/MainModule&)");
	int32_t icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_scalingMode_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystemScalingMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_scalingMode_Injected_mEABFC2E8FE084F997A630B8D2A643B2C9249A5A0 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, int32_t ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_scalingMode_Injected_mEABFC2E8FE084F997A630B8D2A643B2C9249A5A0_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, int32_t);
	static MainModule_set_scalingMode_Injected_mEABFC2E8FE084F997A630B8D2A643B2C9249A5A0_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_scalingMode_Injected_mEABFC2E8FE084F997A630B8D2A643B2C9249A5A0_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_scalingMode_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystemScalingMode)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Boolean UnityEngine.ParticleSystem/MainModule::get_playOnAwake_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MainModule_get_playOnAwake_Injected_m87D4B5B2508D9CF9B3C61F84BC001945B20A06C2 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef bool (*MainModule_get_playOnAwake_Injected_m87D4B5B2508D9CF9B3C61F84BC001945B20A06C2_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_playOnAwake_Injected_m87D4B5B2508D9CF9B3C61F84BC001945B20A06C2_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_playOnAwake_Injected_m87D4B5B2508D9CF9B3C61F84BC001945B20A06C2_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_playOnAwake_Injected(UnityEngine.ParticleSystem/MainModule&)");
	bool icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_playOnAwake_Injected(UnityEngine.ParticleSystem/MainModule&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_playOnAwake_Injected_mAE88821DF50EB8AFD7079C587CC7FB633626B5FB (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, bool ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_playOnAwake_Injected_mAE88821DF50EB8AFD7079C587CC7FB633626B5FB_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, bool);
	static MainModule_set_playOnAwake_Injected_mAE88821DF50EB8AFD7079C587CC7FB633626B5FB_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_playOnAwake_Injected_mAE88821DF50EB8AFD7079C587CC7FB633626B5FB_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_playOnAwake_Injected(UnityEngine.ParticleSystem/MainModule&,System.Boolean)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
// System.Int32 UnityEngine.ParticleSystem/MainModule::get_maxParticles_Injected(UnityEngine.ParticleSystem/MainModule&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MainModule_get_maxParticles_Injected_m260F43233BCA02794176F596FF08A96BFCBE0D91 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, const RuntimeMethod* method)
{
	typedef int32_t (*MainModule_get_maxParticles_Injected_m260F43233BCA02794176F596FF08A96BFCBE0D91_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *);
	static MainModule_get_maxParticles_Injected_m260F43233BCA02794176F596FF08A96BFCBE0D91_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_get_maxParticles_Injected_m260F43233BCA02794176F596FF08A96BFCBE0D91_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::get_maxParticles_Injected(UnityEngine.ParticleSystem/MainModule&)");
	int32_t icallRetVal = _il2cpp_icall_func(____unity_self0);
	return icallRetVal;
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_maxParticles_Injected(UnityEngine.ParticleSystem/MainModule&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_maxParticles_Injected_m6D4D611540F53CC4D8F21D9DD7EF3F68FC50F471 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * ____unity_self0, int32_t ___value1, const RuntimeMethod* method)
{
	typedef void (*MainModule_set_maxParticles_Injected_m6D4D611540F53CC4D8F21D9DD7EF3F68FC50F471_ftn) (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *, int32_t);
	static MainModule_set_maxParticles_Injected_m6D4D611540F53CC4D8F21D9DD7EF3F68FC50F471_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_set_maxParticles_Injected_m6D4D611540F53CC4D8F21D9DD7EF3F68FC50F471_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::set_maxParticles_Injected(UnityEngine.ParticleSystem/MainModule&,System.Int32)");
	_il2cpp_icall_func(____unity_self0, ___value1);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxCurve__ctor_mF14A2DBABFDCF1E855911241A555949CAF212AE3 (MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD * __this, float ___constant0, const RuntimeMethod* method)
{
	{
		__this->set_m_Mode_0(0);
		__this->set_m_CurveMultiplier_1((0.0f));
		__this->set_m_CurveMin_2((AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)NULL);
		__this->set_m_CurveMax_3((AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)NULL);
		__this->set_m_ConstantMin_4((0.0f));
		float L_0 = ___constant0;
		__this->set_m_ConstantMax_5(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void MinMaxCurve__ctor_mF14A2DBABFDCF1E855911241A555949CAF212AE3_AdjustorThunk (RuntimeObject * __this, float ___constant0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD * _thisAdjusted = reinterpret_cast<MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD *>(__this + _offset);
	MinMaxCurve__ctor_mF14A2DBABFDCF1E855911241A555949CAF212AE3(_thisAdjusted, ___constant0, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MinMaxCurve::op_Implicit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD  MinMaxCurve_op_Implicit_m8D746D40E6CCBF5E8C4CE39F23A45712BFC372F5 (float ___constant0, const RuntimeMethod* method)
{
	MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___constant0;
		MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD  L_1;
		memset((&L_1), 0, sizeof(L_1));
		MinMaxCurve__ctor_mF14A2DBABFDCF1E855911241A555949CAF212AE3((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		MinMaxCurve_tF036239442AB2D438B1EDABEBC785426871084CD  L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.ParticleSystem/MinMaxGradient::.ctor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxGradient__ctor_m8D0DE766F67FF065E4C2B6DCD841752C447977AA (MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color0, const RuntimeMethod* method)
{
	{
		__this->set_m_Mode_0(0);
		__this->set_m_GradientMin_1((Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 *)NULL);
		__this->set_m_GradientMax_2((Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 *)NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		L_0 = Color_get_black_m67E91EB7017FC74D9AB5ADEF6B6929B7EFC9A982(/*hidden argument*/NULL);
		__this->set_m_ColorMin_3(L_0);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___color0;
		__this->set_m_ColorMax_4(L_1);
		return;
	}
}
IL2CPP_EXTERN_C  void MinMaxGradient__ctor_m8D0DE766F67FF065E4C2B6DCD841752C447977AA_AdjustorThunk (RuntimeObject * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 * _thisAdjusted = reinterpret_cast<MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 *>(__this + _offset);
	MinMaxGradient__ctor_m8D0DE766F67FF065E4C2B6DCD841752C447977AA(_thisAdjusted, ___color0, method);
}
// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::get_color()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  MinMaxGradient_get_color_m8FA2F2F1E0B9C9E1DFE40CEA325BC5E863C59C90 (MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 * __this, const RuntimeMethod* method)
{
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_ColorMax_4();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  MinMaxGradient_get_color_m8FA2F2F1E0B9C9E1DFE40CEA325BC5E863C59C90_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 * _thisAdjusted = reinterpret_cast<MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 *>(__this + _offset);
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  _returnValue;
	_returnValue = MinMaxGradient_get_color_m8FA2F2F1E0B9C9E1DFE40CEA325BC5E863C59C90(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MinMaxGradient::op_Implicit(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  MinMaxGradient_op_Implicit_m5F938B5644AF6BE5DF525839E922B88456B614FA (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color0, const RuntimeMethod* method)
{
	MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = ___color0;
		MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  L_1;
		memset((&L_1), 0, sizeof(L_1));
		MinMaxGradient__ctor_m8D0DE766F67FF065E4C2B6DCD841752C447977AA((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/NoiseModule
IL2CPP_EXTERN_C void NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591_marshal_pinvoke(const NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591& unmarshaled, NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'NoiseModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591_marshal_pinvoke_back(const NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591_marshaled_pinvoke& marshaled, NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'NoiseModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/NoiseModule
IL2CPP_EXTERN_C void NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591_marshal_pinvoke_cleanup(NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/NoiseModule
IL2CPP_EXTERN_C void NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591_marshal_com(const NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591& unmarshaled, NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'NoiseModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591_marshal_com_back(const NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591_marshaled_com& marshaled, NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'NoiseModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/NoiseModule
IL2CPP_EXTERN_C void NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591_marshal_com_cleanup(NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/NoiseModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NoiseModule__ctor_mE450BC95A9B436FA1627E4D24B7B4E7E723DDB5B (NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void NoiseModule__ctor_mE450BC95A9B436FA1627E4D24B7B4E7E723DDB5B_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591 * _thisAdjusted = reinterpret_cast<NoiseModule_t74C71EB8386C1CF92A748B051690D34F4CB04591 *>(__this + _offset);
	NoiseModule__ctor_mE450BC95A9B436FA1627E4D24B7B4E7E723DDB5B(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.ParticleSystem/Particle::set_lifetime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_lifetime_mCE97F9D17D1E660DB9D9F3244150CB6624A74DC3 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Particle_set_remainingLifetime_mE521DF9387892C00B3F21D4F76F3A55E77AFC6BE((Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *)__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void Particle_set_lifetime_mCE97F9D17D1E660DB9D9F3244150CB6624A74DC3_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * _thisAdjusted = reinterpret_cast<Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *>(__this + _offset);
	Particle_set_lifetime_mCE97F9D17D1E660DB9D9F3244150CB6624A74DC3(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_position_mB749E41CD3C4C6DF501C0D68B6211CF7E2217FC2 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___value0;
		__this->set_m_Position_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void Particle_set_position_mB749E41CD3C4C6DF501C0D68B6211CF7E2217FC2_AdjustorThunk (RuntimeObject * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * _thisAdjusted = reinterpret_cast<Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *>(__this + _offset);
	Particle_set_position_mB749E41CD3C4C6DF501C0D68B6211CF7E2217FC2(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_velocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_velocity_m4894BD6A75E7A1FCD433927F93396AA29A59709B (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___value0;
		__this->set_m_Velocity_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void Particle_set_velocity_m4894BD6A75E7A1FCD433927F93396AA29A59709B_AdjustorThunk (RuntimeObject * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * _thisAdjusted = reinterpret_cast<Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *>(__this + _offset);
	Particle_set_velocity_m4894BD6A75E7A1FCD433927F93396AA29A59709B(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_remainingLifetime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_remainingLifetime_mE521DF9387892C00B3F21D4F76F3A55E77AFC6BE (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Lifetime_11(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void Particle_set_remainingLifetime_mE521DF9387892C00B3F21D4F76F3A55E77AFC6BE_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * _thisAdjusted = reinterpret_cast<Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *>(__this + _offset);
	Particle_set_remainingLifetime_mE521DF9387892C00B3F21D4F76F3A55E77AFC6BE(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_startLifetime(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_startLifetime_m7AC09262BBFE8818EA6B61BDB62264184CD27A82 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_StartLifetime_12(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void Particle_set_startLifetime_m7AC09262BBFE8818EA6B61BDB62264184CD27A82_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * _thisAdjusted = reinterpret_cast<Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *>(__this + _offset);
	Particle_set_startLifetime_m7AC09262BBFE8818EA6B61BDB62264184CD27A82(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_startColor_mA7B0363E82B7A7CBB1C83F8C5D49FB7B7AF75554 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___value0, const RuntimeMethod* method)
{
	{
		Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  L_0 = ___value0;
		__this->set_m_StartColor_8(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void Particle_set_startColor_mA7B0363E82B7A7CBB1C83F8C5D49FB7B7AF75554_AdjustorThunk (RuntimeObject * __this, Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * _thisAdjusted = reinterpret_cast<Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *>(__this + _offset);
	Particle_set_startColor_mA7B0363E82B7A7CBB1C83F8C5D49FB7B7AF75554(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_randomSeed(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_randomSeed_m8FD7A4DB7F8E7EBDEF2C51A28197F8D9D7CB6E04 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, uint32_t ___value0, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_m_RandomSeed_9(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void Particle_set_randomSeed_m8FD7A4DB7F8E7EBDEF2C51A28197F8D9D7CB6E04_AdjustorThunk (RuntimeObject * __this, uint32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * _thisAdjusted = reinterpret_cast<Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *>(__this + _offset);
	Particle_set_randomSeed_m8FD7A4DB7F8E7EBDEF2C51A28197F8D9D7CB6E04(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_startSize_m7CDEE5B620B3D26B4CC5C1DA7C6E24ACCCF64466 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		float L_1 = ___value0;
		float L_2 = ___value0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->set_m_StartSize_7(L_3);
		return;
	}
}
IL2CPP_EXTERN_C  void Particle_set_startSize_m7CDEE5B620B3D26B4CC5C1DA7C6E24ACCCF64466_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * _thisAdjusted = reinterpret_cast<Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *>(__this + _offset);
	Particle_set_startSize_m7CDEE5B620B3D26B4CC5C1DA7C6E24ACCCF64466(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_rotation3D(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_rotation3D_m0F402760524A81307FA4940751751C44DF2F77D0 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___value0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_0, (0.0174532924f), /*hidden argument*/NULL);
		__this->set_m_Rotation_5(L_1);
		uint32_t L_2 = __this->get_m_Flags_16();
		__this->set_m_Flags_16(((int32_t)((int32_t)L_2|(int32_t)2)));
		return;
	}
}
IL2CPP_EXTERN_C  void Particle_set_rotation3D_m0F402760524A81307FA4940751751C44DF2F77D0_AdjustorThunk (RuntimeObject * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * _thisAdjusted = reinterpret_cast<Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *>(__this + _offset);
	Particle_set_rotation3D_m0F402760524A81307FA4940751751C44DF2F77D0(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_angularVelocity3D(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Particle_set_angularVelocity3D_mE9A9544DF33CD0CCF4F1CA14994A2C436E4DF1F8 (Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___value0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_0, (0.0174532924f), /*hidden argument*/NULL);
		__this->set_m_AngularVelocity_6(L_1);
		uint32_t L_2 = __this->get_m_Flags_16();
		__this->set_m_Flags_16(((int32_t)((int32_t)L_2|(int32_t)2)));
		return;
	}
}
IL2CPP_EXTERN_C  void Particle_set_angularVelocity3D_mE9A9544DF33CD0CCF4F1CA14994A2C436E4DF1F8_AdjustorThunk (RuntimeObject * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 * _thisAdjusted = reinterpret_cast<Particle_tDAEF22C4F9FB70E048551ECB203B6A81185832E1 *>(__this + _offset);
	Particle_set_angularVelocity3D_mE9A9544DF33CD0CCF4F1CA14994A2C436E4DF1F8(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/RotationBySpeedModule
IL2CPP_EXTERN_C void RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD_marshal_pinvoke(const RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD& unmarshaled, RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD_marshal_pinvoke_back(const RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD_marshaled_pinvoke& marshaled, RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/RotationBySpeedModule
IL2CPP_EXTERN_C void RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD_marshal_pinvoke_cleanup(RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/RotationBySpeedModule
IL2CPP_EXTERN_C void RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD_marshal_com(const RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD& unmarshaled, RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD_marshal_com_back(const RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD_marshaled_com& marshaled, RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/RotationBySpeedModule
IL2CPP_EXTERN_C void RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD_marshal_com_cleanup(RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/RotationBySpeedModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RotationBySpeedModule__ctor_mCEAFF0BBEC6AB5FB3E59FD1584B22079A2142248 (RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void RotationBySpeedModule__ctor_mCEAFF0BBEC6AB5FB3E59FD1584B22079A2142248_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD * _thisAdjusted = reinterpret_cast<RotationBySpeedModule_t9EFC4CF3826F9F6FF2AFC3F0C25CEBC8E36FF0AD *>(__this + _offset);
	RotationBySpeedModule__ctor_mCEAFF0BBEC6AB5FB3E59FD1584B22079A2142248(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/RotationOverLifetimeModule
IL2CPP_EXTERN_C void RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B_marshal_pinvoke(const RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B& unmarshaled, RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B_marshal_pinvoke_back(const RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B_marshaled_pinvoke& marshaled, RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/RotationOverLifetimeModule
IL2CPP_EXTERN_C void RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B_marshal_pinvoke_cleanup(RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/RotationOverLifetimeModule
IL2CPP_EXTERN_C void RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B_marshal_com(const RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B& unmarshaled, RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B_marshal_com_back(const RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B_marshaled_com& marshaled, RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/RotationOverLifetimeModule
IL2CPP_EXTERN_C void RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B_marshal_com_cleanup(RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/RotationOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RotationOverLifetimeModule__ctor_mA0E0BDF5A88582DFEC3FAB42F8B237611C8E0182 (RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void RotationOverLifetimeModule__ctor_mA0E0BDF5A88582DFEC3FAB42F8B237611C8E0182_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B * _thisAdjusted = reinterpret_cast<RotationOverLifetimeModule_t78F62EF75295ADEF46B90235B7EB163AED016D2B *>(__this + _offset);
	RotationOverLifetimeModule__ctor_mA0E0BDF5A88582DFEC3FAB42F8B237611C8E0182(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ShapeModule
IL2CPP_EXTERN_C void ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA_marshal_pinvoke(const ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA& unmarshaled, ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA_marshal_pinvoke_back(const ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA_marshaled_pinvoke& marshaled, ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ShapeModule
IL2CPP_EXTERN_C void ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA_marshal_pinvoke_cleanup(ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ShapeModule
IL2CPP_EXTERN_C void ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA_marshal_com(const ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA& unmarshaled, ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA_marshal_com_back(const ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA_marshaled_com& marshaled, ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ShapeModule
IL2CPP_EXTERN_C void ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA_marshal_com_cleanup(ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/ShapeModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShapeModule__ctor_mB0CE9D41BBE5FFA5B70F465541BA63D9BEEA7264 (ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void ShapeModule__ctor_mB0CE9D41BBE5FFA5B70F465541BA63D9BEEA7264_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA * _thisAdjusted = reinterpret_cast<ShapeModule_t3A89D143F5CAF357ECCE8B4A4F2BBD57816F98CA *>(__this + _offset);
	ShapeModule__ctor_mB0CE9D41BBE5FFA5B70F465541BA63D9BEEA7264(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/SizeBySpeedModule
IL2CPP_EXTERN_C void SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752_marshal_pinvoke(const SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752& unmarshaled, SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752_marshal_pinvoke_back(const SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752_marshaled_pinvoke& marshaled, SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/SizeBySpeedModule
IL2CPP_EXTERN_C void SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752_marshal_pinvoke_cleanup(SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/SizeBySpeedModule
IL2CPP_EXTERN_C void SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752_marshal_com(const SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752& unmarshaled, SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752_marshal_com_back(const SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752_marshaled_com& marshaled, SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/SizeBySpeedModule
IL2CPP_EXTERN_C void SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752_marshal_com_cleanup(SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/SizeBySpeedModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SizeBySpeedModule__ctor_m479D4802A862D4E07AAF2B47833852A62A17F6DE (SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void SizeBySpeedModule__ctor_m479D4802A862D4E07AAF2B47833852A62A17F6DE_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752 * _thisAdjusted = reinterpret_cast<SizeBySpeedModule_t156070A63E0A483D36BCAE783BA554CDA7CAF752 *>(__this + _offset);
	SizeBySpeedModule__ctor_m479D4802A862D4E07AAF2B47833852A62A17F6DE(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/SizeOverLifetimeModule
IL2CPP_EXTERN_C void SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD_marshal_pinvoke(const SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD& unmarshaled, SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD_marshal_pinvoke_back(const SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD_marshaled_pinvoke& marshaled, SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/SizeOverLifetimeModule
IL2CPP_EXTERN_C void SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD_marshal_pinvoke_cleanup(SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/SizeOverLifetimeModule
IL2CPP_EXTERN_C void SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD_marshal_com(const SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD& unmarshaled, SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD_marshal_com_back(const SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD_marshaled_com& marshaled, SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/SizeOverLifetimeModule
IL2CPP_EXTERN_C void SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD_marshal_com_cleanup(SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/SizeOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SizeOverLifetimeModule__ctor_mCA80885490FC3B1010887B10BDE21A0DACEFB557 (SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void SizeOverLifetimeModule__ctor_mCA80885490FC3B1010887B10BDE21A0DACEFB557_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD * _thisAdjusted = reinterpret_cast<SizeOverLifetimeModule_t0EF80B7F6333637F781C2A0008F6DADBEA0B45BD *>(__this + _offset);
	SizeOverLifetimeModule__ctor_mCA80885490FC3B1010887B10BDE21A0DACEFB557(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/SubEmittersModule
IL2CPP_EXTERN_C void SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C_marshal_pinvoke(const SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C& unmarshaled, SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SubEmittersModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C_marshal_pinvoke_back(const SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C_marshaled_pinvoke& marshaled, SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SubEmittersModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/SubEmittersModule
IL2CPP_EXTERN_C void SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C_marshal_pinvoke_cleanup(SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/SubEmittersModule
IL2CPP_EXTERN_C void SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C_marshal_com(const SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C& unmarshaled, SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SubEmittersModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C_marshal_com_back(const SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C_marshaled_com& marshaled, SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SubEmittersModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/SubEmittersModule
IL2CPP_EXTERN_C void SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C_marshal_com_cleanup(SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/SubEmittersModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SubEmittersModule__ctor_m39DB9FE3DF4B1E15890712EF4E4024818D568A3C (SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void SubEmittersModule__ctor_m39DB9FE3DF4B1E15890712EF4E4024818D568A3C_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C * _thisAdjusted = reinterpret_cast<SubEmittersModule_t0336A6F2B5D3339D93468AED015E2356FBE9E61C *>(__this + _offset);
	SubEmittersModule__ctor_m39DB9FE3DF4B1E15890712EF4E4024818D568A3C(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/TextureSheetAnimationModule
IL2CPP_EXTERN_C void TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2_marshal_pinvoke(const TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2& unmarshaled, TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TextureSheetAnimationModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2_marshal_pinvoke_back(const TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2_marshaled_pinvoke& marshaled, TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TextureSheetAnimationModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/TextureSheetAnimationModule
IL2CPP_EXTERN_C void TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2_marshal_pinvoke_cleanup(TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/TextureSheetAnimationModule
IL2CPP_EXTERN_C void TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2_marshal_com(const TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2& unmarshaled, TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TextureSheetAnimationModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2_marshal_com_back(const TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2_marshaled_com& marshaled, TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TextureSheetAnimationModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/TextureSheetAnimationModule
IL2CPP_EXTERN_C void TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2_marshal_com_cleanup(TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/TextureSheetAnimationModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextureSheetAnimationModule__ctor_mF02B5303373B93D856A5BDF92B4E4AABC7CB2F65 (TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void TextureSheetAnimationModule__ctor_mF02B5303373B93D856A5BDF92B4E4AABC7CB2F65_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2 * _thisAdjusted = reinterpret_cast<TextureSheetAnimationModule_t66D02BA0838858C4921EFC059CC3412141A8FCF2 *>(__this + _offset);
	TextureSheetAnimationModule__ctor_mF02B5303373B93D856A5BDF92B4E4AABC7CB2F65(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/TrailModule
IL2CPP_EXTERN_C void TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8_marshal_pinvoke(const TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8& unmarshaled, TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TrailModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8_marshal_pinvoke_back(const TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8_marshaled_pinvoke& marshaled, TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TrailModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/TrailModule
IL2CPP_EXTERN_C void TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8_marshal_pinvoke_cleanup(TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/TrailModule
IL2CPP_EXTERN_C void TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8_marshal_com(const TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8& unmarshaled, TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TrailModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8_marshal_com_back(const TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8_marshaled_com& marshaled, TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TrailModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/TrailModule
IL2CPP_EXTERN_C void TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8_marshal_com_cleanup(TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/TrailModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrailModule__ctor_m15960ADC2381E58BE70FF2653288599694915412 (TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void TrailModule__ctor_m15960ADC2381E58BE70FF2653288599694915412_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8 * _thisAdjusted = reinterpret_cast<TrailModule_t95D15372971E64A1977B7587ABAA87B1A31EF5C8 *>(__this + _offset);
	TrailModule__ctor_m15960ADC2381E58BE70FF2653288599694915412(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/Trails
IL2CPP_EXTERN_C void Trails_tE9352AAF21D22D5007179B507019C44C823013F2_marshal_pinvoke(const Trails_tE9352AAF21D22D5007179B507019C44C823013F2& unmarshaled, Trails_tE9352AAF21D22D5007179B507019C44C823013F2_marshaled_pinvoke& marshaled)
{
	Exception_t* ___positions_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'positions' of type 'Trails'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___positions_0Exception, NULL);
}
IL2CPP_EXTERN_C void Trails_tE9352AAF21D22D5007179B507019C44C823013F2_marshal_pinvoke_back(const Trails_tE9352AAF21D22D5007179B507019C44C823013F2_marshaled_pinvoke& marshaled, Trails_tE9352AAF21D22D5007179B507019C44C823013F2& unmarshaled)
{
	Exception_t* ___positions_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'positions' of type 'Trails'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___positions_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/Trails
IL2CPP_EXTERN_C void Trails_tE9352AAF21D22D5007179B507019C44C823013F2_marshal_pinvoke_cleanup(Trails_tE9352AAF21D22D5007179B507019C44C823013F2_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/Trails
IL2CPP_EXTERN_C void Trails_tE9352AAF21D22D5007179B507019C44C823013F2_marshal_com(const Trails_tE9352AAF21D22D5007179B507019C44C823013F2& unmarshaled, Trails_tE9352AAF21D22D5007179B507019C44C823013F2_marshaled_com& marshaled)
{
	Exception_t* ___positions_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'positions' of type 'Trails'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___positions_0Exception, NULL);
}
IL2CPP_EXTERN_C void Trails_tE9352AAF21D22D5007179B507019C44C823013F2_marshal_com_back(const Trails_tE9352AAF21D22D5007179B507019C44C823013F2_marshaled_com& marshaled, Trails_tE9352AAF21D22D5007179B507019C44C823013F2& unmarshaled)
{
	Exception_t* ___positions_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'positions' of type 'Trails'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___positions_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/Trails
IL2CPP_EXTERN_C void Trails_tE9352AAF21D22D5007179B507019C44C823013F2_marshal_com_cleanup(Trails_tE9352AAF21D22D5007179B507019C44C823013F2_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/TriggerModule
IL2CPP_EXTERN_C void TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8_marshal_pinvoke(const TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8& unmarshaled, TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TriggerModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8_marshal_pinvoke_back(const TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8_marshaled_pinvoke& marshaled, TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TriggerModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/TriggerModule
IL2CPP_EXTERN_C void TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8_marshal_pinvoke_cleanup(TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/TriggerModule
IL2CPP_EXTERN_C void TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8_marshal_com(const TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8& unmarshaled, TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TriggerModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8_marshal_com_back(const TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8_marshaled_com& marshaled, TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TriggerModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/TriggerModule
IL2CPP_EXTERN_C void TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8_marshal_com_cleanup(TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/TriggerModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TriggerModule__ctor_mEB88CDC59DC3AFBC343D3042488445F6E8A5AE9D (TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void TriggerModule__ctor_mEB88CDC59DC3AFBC343D3042488445F6E8A5AE9D_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8 * _thisAdjusted = reinterpret_cast<TriggerModule_tA3D5C134281500D269541927233AE024546EA3A8 *>(__this + _offset);
	TriggerModule__ctor_mEB88CDC59DC3AFBC343D3042488445F6E8A5AE9D(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/VelocityOverLifetimeModule
IL2CPP_EXTERN_C void VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924_marshal_pinvoke(const VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924& unmarshaled, VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'VelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924_marshal_pinvoke_back(const VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924_marshaled_pinvoke& marshaled, VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'VelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/VelocityOverLifetimeModule
IL2CPP_EXTERN_C void VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924_marshal_pinvoke_cleanup(VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/VelocityOverLifetimeModule
IL2CPP_EXTERN_C void VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924_marshal_com(const VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924& unmarshaled, VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924_marshaled_com& marshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'VelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
IL2CPP_EXTERN_C void VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924_marshal_com_back(const VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924_marshaled_com& marshaled, VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924& unmarshaled)
{
	Exception_t* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'VelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/VelocityOverLifetimeModule
IL2CPP_EXTERN_C void VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924_marshal_com_cleanup(VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/VelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VelocityOverLifetimeModule__ctor_m03EB5FA8BB69309D7DA91CE70EE3A9E65C60AE66 (VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924 * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void VelocityOverLifetimeModule__ctor_m03EB5FA8BB69309D7DA91CE70EE3A9E65C60AE66_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___particleSystem0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924 * _thisAdjusted = reinterpret_cast<VelocityOverLifetimeModule_t582BD438594477516D51E96D99D31C91676A0924 *>(__this + _offset);
	VelocityOverLifetimeModule__ctor_m03EB5FA8BB69309D7DA91CE70EE3A9E65C60AE66(_thisAdjusted, ___particleSystem0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
