﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void Character_tCB5C2B96512E4E769F3BC54DA7AF4653B4CED7A8_CustomAttributesCacheGenerator_jumpSound(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x75\x6E\x64"), NULL);
	}
}
static void CharacterCollider_t86F88462B767E8A2D5C8EA72D1650794C97DA452_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var), NULL);
	}
}
static void CharacterCollider_t86F88462B767E8A2D5C8EA72D1650794C97DA452_CustomAttributesCacheGenerator_coinSound(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x75\x6E\x64"), NULL);
	}
}
static void CharacterCollider_t86F88462B767E8A2D5C8EA72D1650794C97DA452_CustomAttributesCacheGenerator_magnetCoins(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CharacterCollider_t86F88462B767E8A2D5C8EA72D1650794C97DA452_CustomAttributesCacheGenerator_CharacterCollider_InvincibleTimer_m288B818C19163001F8B409969DD901CC26652A6F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_0_0_0_var), NULL);
	}
}
static void U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_CustomAttributesCacheGenerator_U3CInvincibleTimerU3Ed__34__ctor_m8D5D9230E71664C4F1EB7EEE9B2622E51E2544F1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_CustomAttributesCacheGenerator_U3CInvincibleTimerU3Ed__34_System_IDisposable_Dispose_mFADBB6E9EA2EF95698FB131E7484A606EDB1BB86(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_CustomAttributesCacheGenerator_U3CInvincibleTimerU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAAB39BBB0D3248E1E0AD17A9A10388FAB3D2FAD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_CustomAttributesCacheGenerator_U3CInvincibleTimerU3Ed__34_System_Collections_IEnumerator_Reset_m7FBA665B91C3D5A88CC60FAC78EAACBBEE61BD70(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_CustomAttributesCacheGenerator_U3CInvincibleTimerU3Ed__34_System_Collections_IEnumerator_get_Current_mA433FEE9A1864721F75346E1B31F96B527DAAD16(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CharacterDatabase_t7F0A939654780301C6BFFBA430603DE24B5F248F_CustomAttributesCacheGenerator_CharacterDatabase_LoadDatabase_m8E13C9F8709E0C750E508ABC9FBE05C59B2F8F74(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_tB8ABCE697E62B164B16486A6D80D35AB1BFBF30C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7__ctor_mC7CDF58302D7B98E7EB22648FAAC2FC4CDE030AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_IDisposable_Dispose_m524840546B4184DAFB9505BD13E5FD60665F9838(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE917FBCAB2F35B1DC80946FC5FB59DBE5AE0569E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_Reset_m342146DBC2100769356803F0C7ECA06B004D92A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_get_Current_mC1057775743DC5D326EABE7E00BB68E33A430501(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CharacterInputController_tBD51BBD3B7CBE4218E9C1C9CEBC66D990042A13E_CustomAttributesCacheGenerator_jumpLength(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x74\x72\x6F\x6C\x73"), NULL);
	}
}
static void CharacterInputController_tBD51BBD3B7CBE4218E9C1C9CEBC66D990042A13E_CustomAttributesCacheGenerator_slideSound(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x75\x6E\x64\x73"), NULL);
	}
}
static void Consumable_t515F9B4C81CF91F4B0D50759706BA4C1B32832B3_CustomAttributesCacheGenerator_Consumable_Started_mFCCE21EDCF35D6CA314513F2FE36243FB1444049(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_0_0_0_var), NULL);
	}
}
static void Consumable_t515F9B4C81CF91F4B0D50759706BA4C1B32832B3_CustomAttributesCacheGenerator_Consumable_TimedRelease_m4CF53D8423644C0C574BD9846F3E3C9BA221BD89(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_0_0_0_var), NULL);
	}
}
static void U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_CustomAttributesCacheGenerator_U3CStartedU3Ed__19__ctor_mEF42D919739E2A363EF86BC71297EA731B63585B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_CustomAttributesCacheGenerator_U3CStartedU3Ed__19_System_IDisposable_Dispose_mF4BB5903FDC60C398A1FBCF51F917FAEA78FF26C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_CustomAttributesCacheGenerator_U3CStartedU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33766A3905DCDAC9404C0B993C384237C3281A12(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_CustomAttributesCacheGenerator_U3CStartedU3Ed__19_System_Collections_IEnumerator_Reset_m2D42E18F669C84497F3B35E7633558E42D7ACBCB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_CustomAttributesCacheGenerator_U3CStartedU3Ed__19_System_Collections_IEnumerator_get_Current_mCA28224BB04B07A6F7D136CB5986F7534F9F5270(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_CustomAttributesCacheGenerator_U3CTimedReleaseU3Ed__20__ctor_m1F59A14FF7931B4EC7E797731AAFDF24606E8E67(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_CustomAttributesCacheGenerator_U3CTimedReleaseU3Ed__20_System_IDisposable_Dispose_m75E07D756D7211E8CDD6C1F10E9C823DBF382C20(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_CustomAttributesCacheGenerator_U3CTimedReleaseU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99CAAC6AC60A1518342D89A757586121A19C3830(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_CustomAttributesCacheGenerator_U3CTimedReleaseU3Ed__20_System_Collections_IEnumerator_Reset_m5B0ECA825566D38638A998C40993C898EFAE37C5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_CustomAttributesCacheGenerator_U3CTimedReleaseU3Ed__20_System_Collections_IEnumerator_get_Current_m3D3236E4C725D7CA6CDD038A022E03D87FFCA625(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ConsumableDatabase_t85AE4594D2DF58D42C7C83B5D3E1382C5A9328D6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x73\x75\x6D\x61\x62\x6C\x65\x73"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x73\x68\x20\x44\x61\x73\x68\x2F\x43\x6F\x6E\x73\x75\x6D\x61\x62\x6C\x65\x73\x20\x44\x61\x74\x61\x62\x61\x73\x65"), NULL);
	}
}
static void ExtraLife_tD4FEB29BB52996F1FDA7B0F0958A6F2607B0DF5B_CustomAttributesCacheGenerator_ExtraLife_Started_m7EF4AD4261EDC04DAE7F8EB7986821CA5A72DA50(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_0_0_0_var), NULL);
	}
}
static void ExtraLife_tD4FEB29BB52996F1FDA7B0F0958A6F2607B0DF5B_CustomAttributesCacheGenerator_ExtraLife_U3CU3En__0_mD7F4F46A4A1EA7E8E75497F878041BBF3E5B3234(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[1];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_CustomAttributesCacheGenerator_U3CStartedU3Ed__7__ctor_mFCE003E9A8AF54E840895F8FCAFAC3A0569FE580(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_CustomAttributesCacheGenerator_U3CStartedU3Ed__7_System_IDisposable_Dispose_mA224EA171282339A50662E2C06D20A13FCB8EEB8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_CustomAttributesCacheGenerator_U3CStartedU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18DA349166726DEDC3B52E76B536293ED38E3212(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_CustomAttributesCacheGenerator_U3CStartedU3Ed__7_System_Collections_IEnumerator_Reset_mE90475D8C756EFFBFB2E739183FBEE5AC4808111(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_CustomAttributesCacheGenerator_U3CStartedU3Ed__7_System_Collections_IEnumerator_get_Current_m8E89D7830FEAE1FE57FAEFF1258ADB4C136A4764(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Invincibility_tF6E738A3FEEB9C53A30E6E290AA5DF31D8ECD95F_CustomAttributesCacheGenerator_Invincibility_Started_mB345077AEBAE4D21E7FA503F804654AFCDEA0F81(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_0_0_0_var), NULL);
	}
}
static void Invincibility_tF6E738A3FEEB9C53A30E6E290AA5DF31D8ECD95F_CustomAttributesCacheGenerator_Invincibility_U3CU3En__0_mE46F1B9B439B5512A3CAA99F78C22D5ADE77DF01(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[1];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_CustomAttributesCacheGenerator_U3CStartedU3Ed__5__ctor_mFC33F89F8403094329D9B7C5C6DAED5681940F85(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_CustomAttributesCacheGenerator_U3CStartedU3Ed__5_System_IDisposable_Dispose_mA9C7C9600CD551FCC31F1521AE88E77526F2BC72(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_CustomAttributesCacheGenerator_U3CStartedU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86FEB0F1746E16D84EC94159AA702C98ACF8F616(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_CustomAttributesCacheGenerator_U3CStartedU3Ed__5_System_Collections_IEnumerator_Reset_mC5A0AA68ACC13F2582248F429790F7C0E41F3368(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_CustomAttributesCacheGenerator_U3CStartedU3Ed__5_System_Collections_IEnumerator_get_Current_m277672D5AC175E17D0776DAB55F13F08455AE19E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Score2Multiplier_t6445C7B493E4CFF579CC034AEF0B9ECFB1477E36_CustomAttributesCacheGenerator_Score2Multiplier_Started_mA87019FA02A9CDB61CB55F4E08471BDB3663E6B5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_0_0_0_var), NULL);
	}
}
static void Score2Multiplier_t6445C7B493E4CFF579CC034AEF0B9ECFB1477E36_CustomAttributesCacheGenerator_Score2Multiplier_U3CU3En__0_mD98F91578AC7A43A24EA7E614E30A51F2FF5E861(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[1];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_CustomAttributesCacheGenerator_U3CStartedU3Ed__4__ctor_mA80CFD61C96A66A860A2B9D78DEC1BBF6FDBF35E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_CustomAttributesCacheGenerator_U3CStartedU3Ed__4_System_IDisposable_Dispose_m6B1A0F182B3D09478075E36E4A3E202456072418(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_CustomAttributesCacheGenerator_U3CStartedU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6290AA1767879126BFB0451959FC7BE49CC0BCD5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_CustomAttributesCacheGenerator_U3CStartedU3Ed__4_System_Collections_IEnumerator_Reset_mE096394F9BA4B1CEA2ACBF2D68F6C9C97FC27BE5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_CustomAttributesCacheGenerator_U3CStartedU3Ed__4_System_Collections_IEnumerator_get_Current_mF1A628526F6459B7999E32BA331364F8FB1600FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AState_t720F28BD98CE2B2D7E6AD3172B26CDD06D9DB34E_CustomAttributesCacheGenerator_manager(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GameState_t220CA73AF42CC54408408696DBFFAB424F309FAE_CustomAttributesCacheGenerator_coinText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49"), NULL);
	}
}
static void GameState_t220CA73AF42CC54408408696DBFFAB424F309FAE_CustomAttributesCacheGenerator_PowerupIconPrefab(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x61\x62\x73"), NULL);
	}
}
static void GameState_t220CA73AF42CC54408408696DBFFAB424F309FAE_CustomAttributesCacheGenerator_GameState_WaitForGameOver_mAC7CAC58332275915F6E5F2368EB63CDEA165465(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_0_0_0_var), NULL);
	}
}
static void U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_CustomAttributesCacheGenerator_U3CWaitForGameOverU3Ed__47__ctor_m7E4A5B3AF180B587FB987475A2748DCB7C1D2221(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_CustomAttributesCacheGenerator_U3CWaitForGameOverU3Ed__47_System_IDisposable_Dispose_m20E1D453FE2C885C6F7E3AAD6FCC88CE46FAE8E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_CustomAttributesCacheGenerator_U3CWaitForGameOverU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD83D4561CDD0BDE00A1B289E7028A850BA4B319E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_CustomAttributesCacheGenerator_U3CWaitForGameOverU3Ed__47_System_Collections_IEnumerator_Reset_m92879365CFED874A18B5C23FF6FC651280ECAA03(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_CustomAttributesCacheGenerator_U3CWaitForGameOverU3Ed__47_System_Collections_IEnumerator_get_Current_mD3F369BFA826A6D7DC4DCAD5F60CD6BA4B8B9B20(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_charNameDisplay(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x72\x20\x55\x49"), NULL);
	}
}
static void LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_themeNameDisplay(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x6D\x65\x20\x55\x49"), NULL);
	}
}
static void LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_powerupSelect(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x77\x65\x72\x55\x70\x20\x55\x49"), NULL);
	}
}
static void LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_accessoriesSelector(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x63\x65\x73\x73\x6F\x72\x79\x20\x55\x49"), NULL);
	}
}
static void LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_leaderboard(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x74\x68\x65\x72\x20\x44\x61\x74\x61"), NULL);
	}
}
static void LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_consumableIcon(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x61\x62\x73"), NULL);
	}
}
static void LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_LoadoutState_PopulateTheme_m22B65F81A35B1F3F5091DC053BE48EC9129A88B6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_0_0_0_var), NULL);
	}
}
static void LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_LoadoutState_PopulateCharacters_m8397B1FD2F99F48E00EBEBA08CA64064BEAD07F3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_0_0_0_var), NULL);
	}
}
static void U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_CustomAttributesCacheGenerator_U3CPopulateThemeU3Ed__41__ctor_m3A2B0FD2BD3D5B9799640E208D97DCB2BCA8B9D7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_CustomAttributesCacheGenerator_U3CPopulateThemeU3Ed__41_System_IDisposable_Dispose_m9A46096A483CD89012F436A25C5CAC19694F49B9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_CustomAttributesCacheGenerator_U3CPopulateThemeU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB13834AC2759BF9DA4A1BF432A3BEB6C9B4E82F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_CustomAttributesCacheGenerator_U3CPopulateThemeU3Ed__41_System_Collections_IEnumerator_Reset_m13C7C872722C3D5F0E8E69CBD6A7E7BB0C25AF5A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_CustomAttributesCacheGenerator_U3CPopulateThemeU3Ed__41_System_Collections_IEnumerator_get_Current_mFE6C00DBD349F62EA1C27BC3E2AD348824A469A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_CustomAttributesCacheGenerator_U3CPopulateCharactersU3Ed__42__ctor_m1B9F99AA08D99489D2AED553F7213B94D3201241(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_CustomAttributesCacheGenerator_U3CPopulateCharactersU3Ed__42_System_IDisposable_Dispose_m640DE281406BD6BE814BEBE00DF21CA316B4DB3E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_CustomAttributesCacheGenerator_U3CPopulateCharactersU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A6117B73BA8DA99A0AC10CFAD8498A1D9057462(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_CustomAttributesCacheGenerator_U3CPopulateCharactersU3Ed__42_System_Collections_IEnumerator_Reset_mA612E390F3E7900D2046E6208FCA39AC74BB259E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_CustomAttributesCacheGenerator_U3CPopulateCharactersU3Ed__42_System_Collections_IEnumerator_get_Current_m67D7CE7A285EA9E670FC0AC6D6C8DA1E0A73E605(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_OnDisplayAuthentication(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_OnLoginSuccess(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_OnPlayFabError(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_add_OnDisplayAuthentication_m09307D1077EFDBA69B680DBC3E4565061E3A3C47(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_remove_OnDisplayAuthentication_mF91D71B93627024F5084926D6C3C842A8F4AC2B1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_add_OnLoginSuccess_m1B44BC59674DA4AEB0F57AD3CA283179AF5EB607(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_remove_OnLoginSuccess_m6A6AB26D85E1CFEF14668DF1E7338AEF91C54DA0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_add_OnPlayFabError_m06D8796B2EC01FCEBFCAAEAECC1FAC972EF83DF4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_remove_OnPlayFabError_mF5A068C29D48826E5FB846DC0D38ED5A19DE9397(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_U3CAuthenticateEmailPasswordU3Eb__43_2_m73A9B7F2804D6DAF65391CC91DE8227C2511B59F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_U3CAddAccountAndPasswordU3Eb__44_0_mEBB431D2D3658DEA91D3EAFA612584CE7C5A5F98(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tCEF0EAFF66484E6065F07B8F3B2060C629714BEB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass44_0_tF919794273E1D4A3F099C3EC50E62BED7D48AD3D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass48_0_t6286B5EF19A2282BC46D6CD5712C476ED3B1CADC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AllLaneObstacle_tF223D8F6F0DEEF7B4180B8CA8B725DB627F14177_CustomAttributesCacheGenerator_AllLaneObstacle_Spawn_mF8A58ED12FC222A217D34C18056902F48CB12682(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_0_0_0_var), NULL);
	}
}
static void U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_CustomAttributesCacheGenerator_U3CSpawnU3Ed__0__ctor_m3C12D98CEE9084D1104D3AC19E4DB24F14049F5A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_CustomAttributesCacheGenerator_U3CSpawnU3Ed__0_System_IDisposable_Dispose_m7D173ED73B59596164E3F93AE0B2E81854F5FBD7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_CustomAttributesCacheGenerator_U3CSpawnU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACD1991881DDF201112A85D5567ED427D37AFBF2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_CustomAttributesCacheGenerator_U3CSpawnU3Ed__0_System_Collections_IEnumerator_Reset_m34B9730116CAEC77500E14E35394DEF368BB6B1A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_CustomAttributesCacheGenerator_U3CSpawnU3Ed__0_System_Collections_IEnumerator_get_Current_m036F1B038E494E9989082CB4147D70A2657D4FCC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Missile_t6ABA353FEF83A9A14E414B5C72E274249E8B39A1_CustomAttributesCacheGenerator_U3Cm_ReadyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Missile_t6ABA353FEF83A9A14E414B5C72E274249E8B39A1_CustomAttributesCacheGenerator_Missile_get_m_Ready_m69813E8B42B2AE6A82E2930164888C66A28319BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Missile_t6ABA353FEF83A9A14E414B5C72E274249E8B39A1_CustomAttributesCacheGenerator_Missile_set_m_Ready_m6E5187A046605EFBB373B5B562ABBB66F08F56A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Missile_t6ABA353FEF83A9A14E414B5C72E274249E8B39A1_CustomAttributesCacheGenerator_Missile_Spawn_m70568F98979CB913200F45539DA71B8E644178BC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_0_0_0_var), NULL);
	}
}
static void U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_CustomAttributesCacheGenerator_U3CSpawnU3Ed__15__ctor_m8B6555F0727267379D91497503A517A14A935396(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_CustomAttributesCacheGenerator_U3CSpawnU3Ed__15_System_IDisposable_Dispose_m90909754072CA037BC9341EBD318B487BDD7F075(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_CustomAttributesCacheGenerator_U3CSpawnU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEB6FB3ACA001BB1AEE3F356E8AD6242451DFD8F9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_CustomAttributesCacheGenerator_U3CSpawnU3Ed__15_System_Collections_IEnumerator_Reset_m199A0A54883A81C9D0395A881146EEE0A7200F2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_CustomAttributesCacheGenerator_U3CSpawnU3Ed__15_System_Collections_IEnumerator_get_Current_m26E129A15722B49953843E3835FEFAAB4D29DEF3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Obstacle_t8EF4AB1C395FB968F62F04C8506BA31ED876DBB8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var), NULL);
	}
}
static void PatrollingObstacle_tC7D71D93154FA49BED446C89DEAB59973896BFE4_CustomAttributesCacheGenerator_minTime(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x74\x69\x6D\x65\x20\x74\x6F\x20\x63\x72\x6F\x73\x73\x20\x61\x6C\x6C\x20\x6C\x61\x6E\x65\x73\x2E"), NULL);
	}
}
static void PatrollingObstacle_tC7D71D93154FA49BED446C89DEAB59973896BFE4_CustomAttributesCacheGenerator_maxTime(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x74\x69\x6D\x65\x20\x74\x6F\x20\x63\x72\x6F\x73\x73\x20\x61\x6C\x6C\x20\x6C\x61\x6E\x65\x73\x2E"), NULL);
	}
}
static void PatrollingObstacle_tC7D71D93154FA49BED446C89DEAB59973896BFE4_CustomAttributesCacheGenerator_animator(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x76\x65\x20\x65\x6D\x70\x74\x79\x20\x69\x66\x20\x6E\x6F\x20\x61\x6E\x69\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void PatrollingObstacle_tC7D71D93154FA49BED446C89DEAB59973896BFE4_CustomAttributesCacheGenerator_PatrollingObstacle_Spawn_mEE60D0668D6019FEA3CCEE9C8A8EBD7F333D0DF1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_0_0_0_var), NULL);
	}
}
static void U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_CustomAttributesCacheGenerator_U3CSpawnU3Ed__13__ctor_mC6D26905014FBE3C109464828A997016EA7CA70D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_CustomAttributesCacheGenerator_U3CSpawnU3Ed__13_System_IDisposable_Dispose_m1DE303152F8C88637C1358770031EF4826E9D74F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_CustomAttributesCacheGenerator_U3CSpawnU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mECAEF1106886D61093B7A6AD5CCA830D8B8655EA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_CustomAttributesCacheGenerator_U3CSpawnU3Ed__13_System_Collections_IEnumerator_Reset_mF723AA7FEE5B81A88F02DD1007343BF6D75AC0CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_CustomAttributesCacheGenerator_U3CSpawnU3Ed__13_System_Collections_IEnumerator_get_Current_m447841FDAE3F33326E46F2162D478C731F852B1E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SimpleBarricade_tF4AF9ADD30D2E2E68E8F2F005360CC366699D786_CustomAttributesCacheGenerator_SimpleBarricade_Spawn_m6BE7D289AB407ADB5BD4B5FEFD3BA5B6B969FED3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_0_0_0_var), NULL);
	}
}
static void U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_CustomAttributesCacheGenerator_U3CSpawnU3Ed__4__ctor_m9C4DBA85AE3E02D720C6EAECA102687880E5AA9B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_CustomAttributesCacheGenerator_U3CSpawnU3Ed__4_System_IDisposable_Dispose_mF994CA2235DD73A8DC295DDEF1A35EC4710CCA91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_CustomAttributesCacheGenerator_U3CSpawnU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5EFD4F9B9380DC9E690993D411F37E0164D5B31F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_CustomAttributesCacheGenerator_U3CSpawnU3Ed__4_System_Collections_IEnumerator_Reset_m6C86770A44474E359195E84BD69DC698AC95B7D9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_CustomAttributesCacheGenerator_U3CSpawnU3Ed__4_System_Collections_IEnumerator_get_Current_m760CF586EB4B879F894A560953BE3F08CE62A641(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass40_0_t9DE1707026879C20EB19338B368E29EC46F31FEA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass41_0_t5DDE2272796C841D27664890A28F96BBE67B18AB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t81FA968C80A876E68FAB8BD07B9EAF15DD7D0E7D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MusicPlayer_tED49C79A2679189A904E6BC22CD55275F67D4D7D_CustomAttributesCacheGenerator_MusicPlayer_RestartAllStems_m1A1022BBB009E13E5D9C09DF69C5041BEFEA5BB0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_0_0_0_var), NULL);
	}
}
static void U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_CustomAttributesCacheGenerator_U3CRestartAllStemsU3Ed__11__ctor_m3F0E2479A180FB3E399F7C5CF8B71BB15DB66295(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_CustomAttributesCacheGenerator_U3CRestartAllStemsU3Ed__11_System_IDisposable_Dispose_m6FEDE0E02A173C3646F429439283D391270E4B65(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_CustomAttributesCacheGenerator_U3CRestartAllStemsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50388D880EE7347B31D3B2E552933F6F11D19777(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_CustomAttributesCacheGenerator_U3CRestartAllStemsU3Ed__11_System_Collections_IEnumerator_Reset_m79446E16DFB91914B0E767F34FA23F8C74CDC0DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_CustomAttributesCacheGenerator_U3CRestartAllStemsU3Ed__11_System_Collections_IEnumerator_get_Current_mA2D3590BA4EA306951314F5F05C52A31C3A8EA1F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ThemeData_t72ED6CABD4A7B06CA8FB8CDEA2E6F939908C3640_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x6D\x65\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x43\x69\x74\x79\x20\x44\x61\x73\x68\x2F\x54\x68\x65\x6D\x65\x20\x44\x61\x74\x61"), NULL);
	}
}
static void ThemeData_t72ED6CABD4A7B06CA8FB8CDEA2E6F939908C3640_CustomAttributesCacheGenerator_themeName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x6D\x65\x20\x44\x61\x74\x61"), NULL);
	}
}
static void ThemeData_t72ED6CABD4A7B06CA8FB8CDEA2E6F939908C3640_CustomAttributesCacheGenerator_zones(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void ThemeData_t72ED6CABD4A7B06CA8FB8CDEA2E6F939908C3640_CustomAttributesCacheGenerator_cloudPrefabs(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x63\x6F\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void ThemeDatabase_tF7F95EE0A7F2EBE682922A887AB58A5803E1C1E3_CustomAttributesCacheGenerator_ThemeDatabase_LoadDatabase_m4F775ED28F2F50411A0E7DFF276F651D46836758(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_tC783B8D0E958957C54A8CA642EC5C3F87162C53E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7__ctor_mD72AAC64AA5475D057F79B59997BF597BA5DF227(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_IDisposable_Dispose_mE6CA3308E82B8A002709AC9486224960458478FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48B117B0CD0DDB72351D96529C5A0B78E7866475(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_Reset_mC6A56CA03B68D5D83BDF69977C914EC5F710C119(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_get_Current_mDA20F872592A1AFEC3753B785647F92C12CDE482(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_characterController(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x72\x61\x63\x74\x65\x72\x20\x26\x20\x4D\x6F\x76\x65\x6D\x65\x6E\x74\x73"), NULL);
	}
}
static void TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_consumableDatabase(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x6A\x65\x63\x74\x73"), NULL);
	}
}
static void TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_parallaxRoot(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x72\x61\x6C\x6C\x61\x78"), NULL);
	}
}
static void TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_U3CisLoadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_TrackManager_get_isLoaded_mDAAB60A07D0AF02B2D53ED140AD71F69EF07A144(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_TrackManager_set_isLoaded_m716AC7275D329E58751D1245126F5F718A1ECD19(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_TrackManager_WaitToStart_m1060A329DAF9089A4F26B2D71621128458A42BE2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_0_0_0_var), NULL);
	}
}
static void TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_TrackManager_Begin_m65D7D70E8B8B2AF72ACB9A02DFB5412654A397DF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_0_0_0_var), NULL);
	}
}
static void TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_TrackManager_SpawnNewSegment_m1135BFF3841F4A1A83B479EF8775E3D358EBAD71(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_0_0_0_var), NULL);
	}
}
static void TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_TrackManager_SpawnFromAssetReference_mD7A7661417A273DEF823C2B338BAB8EACF409C6D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_0_0_0_var), NULL);
	}
}
static void TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_TrackManager_SpawnCoinAndPowerup_m5081C041651AE4A620B03B956796D8685AD4687C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_0_0_0_var), NULL);
	}
}
static void U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__84__ctor_m3D4B56311A798974B7C2A8E3D20E1A8306DAAF2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__84_System_IDisposable_Dispose_mA4BA8648817A41A3ED3B3CD9F1E3CEEAAD368241(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__84_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85238BFAE3597C9261B286DEAB50F3C929290B3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__84_System_Collections_IEnumerator_Reset_m9384C8A83680182D7B3994AF38C77A948111E2B8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__84_System_Collections_IEnumerator_get_Current_mE5C8B43BE08F6CFEE838E38084B2ED4832206958(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_CustomAttributesCacheGenerator_U3CBeginU3Ed__85__ctor_mAB58B7584FAD5B4633771C03EA4FFB71685A423E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_CustomAttributesCacheGenerator_U3CBeginU3Ed__85_System_IDisposable_Dispose_mD03DCDA26327337D788708EEBD8EBD64A7B241A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_CustomAttributesCacheGenerator_U3CBeginU3Ed__85_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B38DEBEF616B175340DB85F7FA19B5A9E091E8F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_CustomAttributesCacheGenerator_U3CBeginU3Ed__85_System_Collections_IEnumerator_Reset_m4795B9DAC83F02487C6DDCAD0AB9251E36630D12(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_CustomAttributesCacheGenerator_U3CBeginU3Ed__85_System_Collections_IEnumerator_get_Current_m14BA90226D7380E67EB837C5A862853928767C31(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_CustomAttributesCacheGenerator_U3CSpawnNewSegmentU3Ed__93__ctor_mC3946A80D2D4BB2E06F0A60CE828B9473567175F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_CustomAttributesCacheGenerator_U3CSpawnNewSegmentU3Ed__93_System_IDisposable_Dispose_mAE57C25E300472D1292F1AF22404843AF455CDC0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_CustomAttributesCacheGenerator_U3CSpawnNewSegmentU3Ed__93_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3F18702C42FFE48A79ABD648500AD6DB065AD63B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_CustomAttributesCacheGenerator_U3CSpawnNewSegmentU3Ed__93_System_Collections_IEnumerator_Reset_mD50E95DA7F2E82EDD8149A3E491986A675D1802A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_CustomAttributesCacheGenerator_U3CSpawnNewSegmentU3Ed__93_System_Collections_IEnumerator_get_Current_m188CB9FEF972ECC0872CFF268175B3DFAB15C427(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_CustomAttributesCacheGenerator_U3CSpawnFromAssetReferenceU3Ed__95__ctor_m1C51A408EB6060A7133D3180C664171034AD8EC0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_CustomAttributesCacheGenerator_U3CSpawnFromAssetReferenceU3Ed__95_System_IDisposable_Dispose_m27A19B08B4619C2D7B4DFAA3FD804C5622C1A55A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_CustomAttributesCacheGenerator_U3CSpawnFromAssetReferenceU3Ed__95_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A1C8209672A1660F8736CB5C54B72A727BF2217(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_CustomAttributesCacheGenerator_U3CSpawnFromAssetReferenceU3Ed__95_System_Collections_IEnumerator_Reset_mF48630E55DA0DB51C12C225529808B68EFA8EB4F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_CustomAttributesCacheGenerator_U3CSpawnFromAssetReferenceU3Ed__95_System_Collections_IEnumerator_get_Current_m8F62B25EAA98A90D4BF4C29D773B99F262E649A2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_CustomAttributesCacheGenerator_U3CSpawnCoinAndPowerupU3Ed__96__ctor_m6919C148252339ED3E1494B186A1BED7A984C7FE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_CustomAttributesCacheGenerator_U3CSpawnCoinAndPowerupU3Ed__96_System_IDisposable_Dispose_m87B6CB6FF3D58454AC6347C9A812361448BCBC27(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_CustomAttributesCacheGenerator_U3CSpawnCoinAndPowerupU3Ed__96_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5A5E99D6802DA7A1700EEC345C4AB9394D9B6A76(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_CustomAttributesCacheGenerator_U3CSpawnCoinAndPowerupU3Ed__96_System_Collections_IEnumerator_Reset_mCD39A066C923D8FE2C3DF6DA90383819ACD15967(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_CustomAttributesCacheGenerator_U3CSpawnCoinAndPowerupU3Ed__96_System_Collections_IEnumerator_get_Current_m31B94A4C2C7A97578617509C9D5EDB57B9F69C84(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TrackSegment_t4A105930E9F547CAEA090585BD9AAFFBA31F7F46_CustomAttributesCacheGenerator_obstaclePositions(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Leaderboard_tAB679EAA840F4FCFD21C27253A52F5C8298FC0F4_CustomAttributesCacheGenerator_Leaderboard_U3CToggleOpenGlobalU3Eb__10_0_m58D1860E8B526AE4B49F21139C57BB0A5FE6A61D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tA3560C4A8F19EF1CD7F9345E02F9633DE06EC868_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t7B0A6C75202ADBB22508DED0B52377DD379FC949_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MissionUI_t0BBE1F9E47FD940876354E1DFF9553B63A8CCE16_CustomAttributesCacheGenerator_MissionUI_Open_mC4CDD6194BB5A512C4494663E89FE6F27DC27B2E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_0_0_0_var), NULL);
	}
}
static void U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_CustomAttributesCacheGenerator_U3COpenU3Ed__3__ctor_m8AC977975366A5A4ED5DCAB21A1D37D1B95EB264(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_CustomAttributesCacheGenerator_U3COpenU3Ed__3_System_IDisposable_Dispose_m5F0713DDBBA80E241AE5A97F9DEAC6AE128AF282(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_CustomAttributesCacheGenerator_U3COpenU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F4DB819EEEDD4C7D847BE29D85CA72CFB80E644(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_CustomAttributesCacheGenerator_U3COpenU3Ed__3_System_Collections_IEnumerator_Reset_m6D1892BA8644ABC732A24538492BCDB875868DB7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_CustomAttributesCacheGenerator_U3COpenU3Ed__3_System_Collections_IEnumerator_get_Current_m365CCE1036E1123F495161146CC9A9C1811E761E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PowerupIcon_t0BA95E23D3363A65D5EF65DA74C1C334B1D86F94_CustomAttributesCacheGenerator_linkedConsumable(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SetTransperancy_tB5A4C44181F425BA0D99A1C1FF636566D8AF527B_CustomAttributesCacheGenerator_alphaLevel(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ShopAccessoriesList_t2EC5A09FC4B0B5512B32B0DEE580383CD41E2AB3_CustomAttributesCacheGenerator_ShopAccessoriesList_U3CPopulateU3Eb__2_0_mC229749232D40E11CC2DB0E032E9FB417AE22513(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tCF9624AF3CF0A3A8DAE5A05C1B54C2D99D5CF24E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_tA769E94E8D5F659B1B1D2CDDBB526B4BF58E6405_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_1_t61AB3A13A4E121CD68676598DF4F632E9532A197_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t9C24132F0EEBE838D1A5C7100E281E7543E98F69_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_1_t29370C70BD87A6394B635428A0A52300F883C0B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t8B0FB9C8B9E352363C202C5FBBC436A235AB134C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_1_tF2AD3996DDDBD9A76D31EFAB7424E124FD22A960_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t72E89848AC081FC6069DB0BE88F800B769C799FD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_1_t15E4886E0950F5EF8854D6F1AA1DAB503737AEE3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShopUI_t5A7E39FEBB271934066F2EB2B09E9F16DDF06D7E_CustomAttributesCacheGenerator_coinCounter(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49"), NULL);
	}
}
static void StartButton_tF654A80376CB34ECDE2DD58C37A597E464133130_CustomAttributesCacheGenerator_StartButton_LoadAynchronously_m11F5469D84EDFD0C642BCEBE2FE2F5B5E8C856AF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_0_0_0_var), NULL);
	}
}
static void U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_CustomAttributesCacheGenerator_U3CLoadAynchronouslyU3Ed__4__ctor_m2C80714EAC810CC120B102341525405571574FFF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_CustomAttributesCacheGenerator_U3CLoadAynchronouslyU3Ed__4_System_IDisposable_Dispose_m42FC8E64428FC380A50BBA8DCE4D27BA73AB23D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_CustomAttributesCacheGenerator_U3CLoadAynchronouslyU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5557547C31F7B97F06DB286DA468D7915B50A649(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_CustomAttributesCacheGenerator_U3CLoadAynchronouslyU3Ed__4_System_Collections_IEnumerator_Reset_m57603CABA0EE1D1465CF65D883D6F5CB7A50BC17(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_CustomAttributesCacheGenerator_U3CLoadAynchronouslyU3Ed__4_System_Collections_IEnumerator_get_Current_m6481BEC178F1D855C17F9B03FB18A3D02F57DFFB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WorldCurver_tA8590085FD58F22614247EB3DD70DA3084EED595_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void WorldCurver_tA8590085FD58F22614247EB3DD70DA3084EED595_CustomAttributesCacheGenerator_curveStrength(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -0.100000001f, 0.100000001f, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[237] = 
{
	CharacterCollider_t86F88462B767E8A2D5C8EA72D1650794C97DA452_CustomAttributesCacheGenerator,
	ConsumableDatabase_t85AE4594D2DF58D42C7C83B5D3E1382C5A9328D6_CustomAttributesCacheGenerator,
	Obstacle_t8EF4AB1C395FB968F62F04C8506BA31ED876DBB8_CustomAttributesCacheGenerator,
	ThemeData_t72ED6CABD4A7B06CA8FB8CDEA2E6F939908C3640_CustomAttributesCacheGenerator,
	WorldCurver_tA8590085FD58F22614247EB3DD70DA3084EED595_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator,
	U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_CustomAttributesCacheGenerator,
	U3CU3Ec_tB8ABCE697E62B164B16486A6D80D35AB1BFBF30C_CustomAttributesCacheGenerator,
	U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_CustomAttributesCacheGenerator,
	U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_CustomAttributesCacheGenerator,
	U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_CustomAttributesCacheGenerator,
	U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_CustomAttributesCacheGenerator,
	U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_CustomAttributesCacheGenerator,
	U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_CustomAttributesCacheGenerator,
	U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_CustomAttributesCacheGenerator,
	U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_CustomAttributesCacheGenerator,
	U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_CustomAttributesCacheGenerator,
	U3CU3Ec_tCEF0EAFF66484E6065F07B8F3B2060C629714BEB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass44_0_tF919794273E1D4A3F099C3EC50E62BED7D48AD3D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass48_0_t6286B5EF19A2282BC46D6CD5712C476ED3B1CADC_CustomAttributesCacheGenerator,
	U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_CustomAttributesCacheGenerator,
	U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_CustomAttributesCacheGenerator,
	U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_CustomAttributesCacheGenerator,
	U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass40_0_t9DE1707026879C20EB19338B368E29EC46F31FEA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass41_0_t5DDE2272796C841D27664890A28F96BBE67B18AB_CustomAttributesCacheGenerator,
	U3CU3Ec_t81FA968C80A876E68FAB8BD07B9EAF15DD7D0E7D_CustomAttributesCacheGenerator,
	U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_CustomAttributesCacheGenerator,
	U3CU3Ec_tC783B8D0E958957C54A8CA642EC5C3F87162C53E_CustomAttributesCacheGenerator,
	U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_CustomAttributesCacheGenerator,
	U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_CustomAttributesCacheGenerator,
	U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_CustomAttributesCacheGenerator,
	U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_CustomAttributesCacheGenerator,
	U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_CustomAttributesCacheGenerator,
	U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_CustomAttributesCacheGenerator,
	U3CU3Ec_tA3560C4A8F19EF1CD7F9345E02F9633DE06EC868_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t7B0A6C75202ADBB22508DED0B52377DD379FC949_CustomAttributesCacheGenerator,
	U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_tCF9624AF3CF0A3A8DAE5A05C1B54C2D99D5CF24E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_tA769E94E8D5F659B1B1D2CDDBB526B4BF58E6405_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_1_t61AB3A13A4E121CD68676598DF4F632E9532A197_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t9C24132F0EEBE838D1A5C7100E281E7543E98F69_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_1_t29370C70BD87A6394B635428A0A52300F883C0B0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t8B0FB9C8B9E352363C202C5FBBC436A235AB134C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_1_tF2AD3996DDDBD9A76D31EFAB7424E124FD22A960_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t72E89848AC081FC6069DB0BE88F800B769C799FD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_1_t15E4886E0950F5EF8854D6F1AA1DAB503737AEE3_CustomAttributesCacheGenerator,
	U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_CustomAttributesCacheGenerator,
	Character_tCB5C2B96512E4E769F3BC54DA7AF4653B4CED7A8_CustomAttributesCacheGenerator_jumpSound,
	CharacterCollider_t86F88462B767E8A2D5C8EA72D1650794C97DA452_CustomAttributesCacheGenerator_coinSound,
	CharacterCollider_t86F88462B767E8A2D5C8EA72D1650794C97DA452_CustomAttributesCacheGenerator_magnetCoins,
	CharacterInputController_tBD51BBD3B7CBE4218E9C1C9CEBC66D990042A13E_CustomAttributesCacheGenerator_jumpLength,
	CharacterInputController_tBD51BBD3B7CBE4218E9C1C9CEBC66D990042A13E_CustomAttributesCacheGenerator_slideSound,
	AState_t720F28BD98CE2B2D7E6AD3172B26CDD06D9DB34E_CustomAttributesCacheGenerator_manager,
	GameState_t220CA73AF42CC54408408696DBFFAB424F309FAE_CustomAttributesCacheGenerator_coinText,
	GameState_t220CA73AF42CC54408408696DBFFAB424F309FAE_CustomAttributesCacheGenerator_PowerupIconPrefab,
	LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_charNameDisplay,
	LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_themeNameDisplay,
	LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_powerupSelect,
	LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_accessoriesSelector,
	LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_leaderboard,
	LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_consumableIcon,
	PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_OnDisplayAuthentication,
	PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_OnLoginSuccess,
	PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_OnPlayFabError,
	Missile_t6ABA353FEF83A9A14E414B5C72E274249E8B39A1_CustomAttributesCacheGenerator_U3Cm_ReadyU3Ek__BackingField,
	PatrollingObstacle_tC7D71D93154FA49BED446C89DEAB59973896BFE4_CustomAttributesCacheGenerator_minTime,
	PatrollingObstacle_tC7D71D93154FA49BED446C89DEAB59973896BFE4_CustomAttributesCacheGenerator_maxTime,
	PatrollingObstacle_tC7D71D93154FA49BED446C89DEAB59973896BFE4_CustomAttributesCacheGenerator_animator,
	ThemeData_t72ED6CABD4A7B06CA8FB8CDEA2E6F939908C3640_CustomAttributesCacheGenerator_themeName,
	ThemeData_t72ED6CABD4A7B06CA8FB8CDEA2E6F939908C3640_CustomAttributesCacheGenerator_zones,
	ThemeData_t72ED6CABD4A7B06CA8FB8CDEA2E6F939908C3640_CustomAttributesCacheGenerator_cloudPrefabs,
	TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_characterController,
	TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_consumableDatabase,
	TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_parallaxRoot,
	TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_U3CisLoadedU3Ek__BackingField,
	TrackSegment_t4A105930E9F547CAEA090585BD9AAFFBA31F7F46_CustomAttributesCacheGenerator_obstaclePositions,
	PowerupIcon_t0BA95E23D3363A65D5EF65DA74C1C334B1D86F94_CustomAttributesCacheGenerator_linkedConsumable,
	SetTransperancy_tB5A4C44181F425BA0D99A1C1FF636566D8AF527B_CustomAttributesCacheGenerator_alphaLevel,
	ShopUI_t5A7E39FEBB271934066F2EB2B09E9F16DDF06D7E_CustomAttributesCacheGenerator_coinCounter,
	WorldCurver_tA8590085FD58F22614247EB3DD70DA3084EED595_CustomAttributesCacheGenerator_curveStrength,
	CharacterCollider_t86F88462B767E8A2D5C8EA72D1650794C97DA452_CustomAttributesCacheGenerator_CharacterCollider_InvincibleTimer_m288B818C19163001F8B409969DD901CC26652A6F,
	CharacterDatabase_t7F0A939654780301C6BFFBA430603DE24B5F248F_CustomAttributesCacheGenerator_CharacterDatabase_LoadDatabase_m8E13C9F8709E0C750E508ABC9FBE05C59B2F8F74,
	Consumable_t515F9B4C81CF91F4B0D50759706BA4C1B32832B3_CustomAttributesCacheGenerator_Consumable_Started_mFCCE21EDCF35D6CA314513F2FE36243FB1444049,
	Consumable_t515F9B4C81CF91F4B0D50759706BA4C1B32832B3_CustomAttributesCacheGenerator_Consumable_TimedRelease_m4CF53D8423644C0C574BD9846F3E3C9BA221BD89,
	ExtraLife_tD4FEB29BB52996F1FDA7B0F0958A6F2607B0DF5B_CustomAttributesCacheGenerator_ExtraLife_Started_m7EF4AD4261EDC04DAE7F8EB7986821CA5A72DA50,
	ExtraLife_tD4FEB29BB52996F1FDA7B0F0958A6F2607B0DF5B_CustomAttributesCacheGenerator_ExtraLife_U3CU3En__0_mD7F4F46A4A1EA7E8E75497F878041BBF3E5B3234,
	Invincibility_tF6E738A3FEEB9C53A30E6E290AA5DF31D8ECD95F_CustomAttributesCacheGenerator_Invincibility_Started_mB345077AEBAE4D21E7FA503F804654AFCDEA0F81,
	Invincibility_tF6E738A3FEEB9C53A30E6E290AA5DF31D8ECD95F_CustomAttributesCacheGenerator_Invincibility_U3CU3En__0_mE46F1B9B439B5512A3CAA99F78C22D5ADE77DF01,
	Score2Multiplier_t6445C7B493E4CFF579CC034AEF0B9ECFB1477E36_CustomAttributesCacheGenerator_Score2Multiplier_Started_mA87019FA02A9CDB61CB55F4E08471BDB3663E6B5,
	Score2Multiplier_t6445C7B493E4CFF579CC034AEF0B9ECFB1477E36_CustomAttributesCacheGenerator_Score2Multiplier_U3CU3En__0_mD98F91578AC7A43A24EA7E614E30A51F2FF5E861,
	GameState_t220CA73AF42CC54408408696DBFFAB424F309FAE_CustomAttributesCacheGenerator_GameState_WaitForGameOver_mAC7CAC58332275915F6E5F2368EB63CDEA165465,
	LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_LoadoutState_PopulateTheme_m22B65F81A35B1F3F5091DC053BE48EC9129A88B6,
	LoadoutState_t75D5211A7D64A9D7EDC6F73DD3CA4E837DE1146F_CustomAttributesCacheGenerator_LoadoutState_PopulateCharacters_m8397B1FD2F99F48E00EBEBA08CA64064BEAD07F3,
	PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_add_OnDisplayAuthentication_m09307D1077EFDBA69B680DBC3E4565061E3A3C47,
	PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_remove_OnDisplayAuthentication_mF91D71B93627024F5084926D6C3C842A8F4AC2B1,
	PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_add_OnLoginSuccess_m1B44BC59674DA4AEB0F57AD3CA283179AF5EB607,
	PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_remove_OnLoginSuccess_m6A6AB26D85E1CFEF14668DF1E7338AEF91C54DA0,
	PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_add_OnPlayFabError_m06D8796B2EC01FCEBFCAAEAECC1FAC972EF83DF4,
	PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_remove_OnPlayFabError_mF5A068C29D48826E5FB846DC0D38ED5A19DE9397,
	PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_U3CAuthenticateEmailPasswordU3Eb__43_2_m73A9B7F2804D6DAF65391CC91DE8227C2511B59F,
	PlayFabAuthService_t8DC34A9F574556EAB0ED55A9EE17809427480891_CustomAttributesCacheGenerator_PlayFabAuthService_U3CAddAccountAndPasswordU3Eb__44_0_mEBB431D2D3658DEA91D3EAFA612584CE7C5A5F98,
	AllLaneObstacle_tF223D8F6F0DEEF7B4180B8CA8B725DB627F14177_CustomAttributesCacheGenerator_AllLaneObstacle_Spawn_mF8A58ED12FC222A217D34C18056902F48CB12682,
	Missile_t6ABA353FEF83A9A14E414B5C72E274249E8B39A1_CustomAttributesCacheGenerator_Missile_get_m_Ready_m69813E8B42B2AE6A82E2930164888C66A28319BB,
	Missile_t6ABA353FEF83A9A14E414B5C72E274249E8B39A1_CustomAttributesCacheGenerator_Missile_set_m_Ready_m6E5187A046605EFBB373B5B562ABBB66F08F56A4,
	Missile_t6ABA353FEF83A9A14E414B5C72E274249E8B39A1_CustomAttributesCacheGenerator_Missile_Spawn_m70568F98979CB913200F45539DA71B8E644178BC,
	PatrollingObstacle_tC7D71D93154FA49BED446C89DEAB59973896BFE4_CustomAttributesCacheGenerator_PatrollingObstacle_Spawn_mEE60D0668D6019FEA3CCEE9C8A8EBD7F333D0DF1,
	SimpleBarricade_tF4AF9ADD30D2E2E68E8F2F005360CC366699D786_CustomAttributesCacheGenerator_SimpleBarricade_Spawn_m6BE7D289AB407ADB5BD4B5FEFD3BA5B6B969FED3,
	MusicPlayer_tED49C79A2679189A904E6BC22CD55275F67D4D7D_CustomAttributesCacheGenerator_MusicPlayer_RestartAllStems_m1A1022BBB009E13E5D9C09DF69C5041BEFEA5BB0,
	ThemeDatabase_tF7F95EE0A7F2EBE682922A887AB58A5803E1C1E3_CustomAttributesCacheGenerator_ThemeDatabase_LoadDatabase_m4F775ED28F2F50411A0E7DFF276F651D46836758,
	TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_TrackManager_get_isLoaded_mDAAB60A07D0AF02B2D53ED140AD71F69EF07A144,
	TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_TrackManager_set_isLoaded_m716AC7275D329E58751D1245126F5F718A1ECD19,
	TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_TrackManager_WaitToStart_m1060A329DAF9089A4F26B2D71621128458A42BE2,
	TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_TrackManager_Begin_m65D7D70E8B8B2AF72ACB9A02DFB5412654A397DF,
	TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_TrackManager_SpawnNewSegment_m1135BFF3841F4A1A83B479EF8775E3D358EBAD71,
	TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_TrackManager_SpawnFromAssetReference_mD7A7661417A273DEF823C2B338BAB8EACF409C6D,
	TrackManager_t47C94A1C95A9AEB62390737AE39A6FC903B2139D_CustomAttributesCacheGenerator_TrackManager_SpawnCoinAndPowerup_m5081C041651AE4A620B03B956796D8685AD4687C,
	Leaderboard_tAB679EAA840F4FCFD21C27253A52F5C8298FC0F4_CustomAttributesCacheGenerator_Leaderboard_U3CToggleOpenGlobalU3Eb__10_0_m58D1860E8B526AE4B49F21139C57BB0A5FE6A61D,
	MissionUI_t0BBE1F9E47FD940876354E1DFF9553B63A8CCE16_CustomAttributesCacheGenerator_MissionUI_Open_mC4CDD6194BB5A512C4494663E89FE6F27DC27B2E,
	ShopAccessoriesList_t2EC5A09FC4B0B5512B32B0DEE580383CD41E2AB3_CustomAttributesCacheGenerator_ShopAccessoriesList_U3CPopulateU3Eb__2_0_mC229749232D40E11CC2DB0E032E9FB417AE22513,
	StartButton_tF654A80376CB34ECDE2DD58C37A597E464133130_CustomAttributesCacheGenerator_StartButton_LoadAynchronously_m11F5469D84EDFD0C642BCEBE2FE2F5B5E8C856AF,
	U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_CustomAttributesCacheGenerator_U3CInvincibleTimerU3Ed__34__ctor_m8D5D9230E71664C4F1EB7EEE9B2622E51E2544F1,
	U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_CustomAttributesCacheGenerator_U3CInvincibleTimerU3Ed__34_System_IDisposable_Dispose_mFADBB6E9EA2EF95698FB131E7484A606EDB1BB86,
	U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_CustomAttributesCacheGenerator_U3CInvincibleTimerU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAAB39BBB0D3248E1E0AD17A9A10388FAB3D2FAD9,
	U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_CustomAttributesCacheGenerator_U3CInvincibleTimerU3Ed__34_System_Collections_IEnumerator_Reset_m7FBA665B91C3D5A88CC60FAC78EAACBBEE61BD70,
	U3CInvincibleTimerU3Ed__34_t2F04E8331BC67E5301B3F5C6F2C3D54A1DC7A67A_CustomAttributesCacheGenerator_U3CInvincibleTimerU3Ed__34_System_Collections_IEnumerator_get_Current_mA433FEE9A1864721F75346E1B31F96B527DAAD16,
	U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7__ctor_mC7CDF58302D7B98E7EB22648FAAC2FC4CDE030AD,
	U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_IDisposable_Dispose_m524840546B4184DAFB9505BD13E5FD60665F9838,
	U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE917FBCAB2F35B1DC80946FC5FB59DBE5AE0569E,
	U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_Reset_m342146DBC2100769356803F0C7ECA06B004D92A0,
	U3CLoadDatabaseU3Ed__7_t0D5D09C9E439AA31F5C8274C049A211594E134A0_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_get_Current_mC1057775743DC5D326EABE7E00BB68E33A430501,
	U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_CustomAttributesCacheGenerator_U3CStartedU3Ed__19__ctor_mEF42D919739E2A363EF86BC71297EA731B63585B,
	U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_CustomAttributesCacheGenerator_U3CStartedU3Ed__19_System_IDisposable_Dispose_mF4BB5903FDC60C398A1FBCF51F917FAEA78FF26C,
	U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_CustomAttributesCacheGenerator_U3CStartedU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33766A3905DCDAC9404C0B993C384237C3281A12,
	U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_CustomAttributesCacheGenerator_U3CStartedU3Ed__19_System_Collections_IEnumerator_Reset_m2D42E18F669C84497F3B35E7633558E42D7ACBCB,
	U3CStartedU3Ed__19_tD5283EA0A35260FA47AE94B160E6A622F812F015_CustomAttributesCacheGenerator_U3CStartedU3Ed__19_System_Collections_IEnumerator_get_Current_mCA28224BB04B07A6F7D136CB5986F7534F9F5270,
	U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_CustomAttributesCacheGenerator_U3CTimedReleaseU3Ed__20__ctor_m1F59A14FF7931B4EC7E797731AAFDF24606E8E67,
	U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_CustomAttributesCacheGenerator_U3CTimedReleaseU3Ed__20_System_IDisposable_Dispose_m75E07D756D7211E8CDD6C1F10E9C823DBF382C20,
	U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_CustomAttributesCacheGenerator_U3CTimedReleaseU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99CAAC6AC60A1518342D89A757586121A19C3830,
	U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_CustomAttributesCacheGenerator_U3CTimedReleaseU3Ed__20_System_Collections_IEnumerator_Reset_m5B0ECA825566D38638A998C40993C898EFAE37C5,
	U3CTimedReleaseU3Ed__20_t3327B6228CEFD2C81B331FE91F45B218CC71F155_CustomAttributesCacheGenerator_U3CTimedReleaseU3Ed__20_System_Collections_IEnumerator_get_Current_m3D3236E4C725D7CA6CDD038A022E03D87FFCA625,
	U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_CustomAttributesCacheGenerator_U3CStartedU3Ed__7__ctor_mFCE003E9A8AF54E840895F8FCAFAC3A0569FE580,
	U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_CustomAttributesCacheGenerator_U3CStartedU3Ed__7_System_IDisposable_Dispose_mA224EA171282339A50662E2C06D20A13FCB8EEB8,
	U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_CustomAttributesCacheGenerator_U3CStartedU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18DA349166726DEDC3B52E76B536293ED38E3212,
	U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_CustomAttributesCacheGenerator_U3CStartedU3Ed__7_System_Collections_IEnumerator_Reset_mE90475D8C756EFFBFB2E739183FBEE5AC4808111,
	U3CStartedU3Ed__7_tBB52CCC3E031D3DAFDD301BAEA3BCCF1CA336A84_CustomAttributesCacheGenerator_U3CStartedU3Ed__7_System_Collections_IEnumerator_get_Current_m8E89D7830FEAE1FE57FAEFF1258ADB4C136A4764,
	U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_CustomAttributesCacheGenerator_U3CStartedU3Ed__5__ctor_mFC33F89F8403094329D9B7C5C6DAED5681940F85,
	U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_CustomAttributesCacheGenerator_U3CStartedU3Ed__5_System_IDisposable_Dispose_mA9C7C9600CD551FCC31F1521AE88E77526F2BC72,
	U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_CustomAttributesCacheGenerator_U3CStartedU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86FEB0F1746E16D84EC94159AA702C98ACF8F616,
	U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_CustomAttributesCacheGenerator_U3CStartedU3Ed__5_System_Collections_IEnumerator_Reset_mC5A0AA68ACC13F2582248F429790F7C0E41F3368,
	U3CStartedU3Ed__5_t13B8986223A887A4D81124E4C785C80536FAEA7D_CustomAttributesCacheGenerator_U3CStartedU3Ed__5_System_Collections_IEnumerator_get_Current_m277672D5AC175E17D0776DAB55F13F08455AE19E,
	U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_CustomAttributesCacheGenerator_U3CStartedU3Ed__4__ctor_mA80CFD61C96A66A860A2B9D78DEC1BBF6FDBF35E,
	U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_CustomAttributesCacheGenerator_U3CStartedU3Ed__4_System_IDisposable_Dispose_m6B1A0F182B3D09478075E36E4A3E202456072418,
	U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_CustomAttributesCacheGenerator_U3CStartedU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6290AA1767879126BFB0451959FC7BE49CC0BCD5,
	U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_CustomAttributesCacheGenerator_U3CStartedU3Ed__4_System_Collections_IEnumerator_Reset_mE096394F9BA4B1CEA2ACBF2D68F6C9C97FC27BE5,
	U3CStartedU3Ed__4_t2417F6577DFA2A1E9A29170B64DBF28399D38518_CustomAttributesCacheGenerator_U3CStartedU3Ed__4_System_Collections_IEnumerator_get_Current_mF1A628526F6459B7999E32BA331364F8FB1600FB,
	U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_CustomAttributesCacheGenerator_U3CWaitForGameOverU3Ed__47__ctor_m7E4A5B3AF180B587FB987475A2748DCB7C1D2221,
	U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_CustomAttributesCacheGenerator_U3CWaitForGameOverU3Ed__47_System_IDisposable_Dispose_m20E1D453FE2C885C6F7E3AAD6FCC88CE46FAE8E0,
	U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_CustomAttributesCacheGenerator_U3CWaitForGameOverU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD83D4561CDD0BDE00A1B289E7028A850BA4B319E,
	U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_CustomAttributesCacheGenerator_U3CWaitForGameOverU3Ed__47_System_Collections_IEnumerator_Reset_m92879365CFED874A18B5C23FF6FC651280ECAA03,
	U3CWaitForGameOverU3Ed__47_t07348CB4B525AFB0236CD18E5F5F332342CFC56A_CustomAttributesCacheGenerator_U3CWaitForGameOverU3Ed__47_System_Collections_IEnumerator_get_Current_mD3F369BFA826A6D7DC4DCAD5F60CD6BA4B8B9B20,
	U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_CustomAttributesCacheGenerator_U3CPopulateThemeU3Ed__41__ctor_m3A2B0FD2BD3D5B9799640E208D97DCB2BCA8B9D7,
	U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_CustomAttributesCacheGenerator_U3CPopulateThemeU3Ed__41_System_IDisposable_Dispose_m9A46096A483CD89012F436A25C5CAC19694F49B9,
	U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_CustomAttributesCacheGenerator_U3CPopulateThemeU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB13834AC2759BF9DA4A1BF432A3BEB6C9B4E82F,
	U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_CustomAttributesCacheGenerator_U3CPopulateThemeU3Ed__41_System_Collections_IEnumerator_Reset_m13C7C872722C3D5F0E8E69CBD6A7E7BB0C25AF5A,
	U3CPopulateThemeU3Ed__41_t62EC46C492BE3DEC0EC4131C1E87FF5D100C9C66_CustomAttributesCacheGenerator_U3CPopulateThemeU3Ed__41_System_Collections_IEnumerator_get_Current_mFE6C00DBD349F62EA1C27BC3E2AD348824A469A0,
	U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_CustomAttributesCacheGenerator_U3CPopulateCharactersU3Ed__42__ctor_m1B9F99AA08D99489D2AED553F7213B94D3201241,
	U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_CustomAttributesCacheGenerator_U3CPopulateCharactersU3Ed__42_System_IDisposable_Dispose_m640DE281406BD6BE814BEBE00DF21CA316B4DB3E,
	U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_CustomAttributesCacheGenerator_U3CPopulateCharactersU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A6117B73BA8DA99A0AC10CFAD8498A1D9057462,
	U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_CustomAttributesCacheGenerator_U3CPopulateCharactersU3Ed__42_System_Collections_IEnumerator_Reset_mA612E390F3E7900D2046E6208FCA39AC74BB259E,
	U3CPopulateCharactersU3Ed__42_tAED3C1E145581DDDBEDACB3AA08FC6A8FC40CFBE_CustomAttributesCacheGenerator_U3CPopulateCharactersU3Ed__42_System_Collections_IEnumerator_get_Current_m67D7CE7A285EA9E670FC0AC6D6C8DA1E0A73E605,
	U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_CustomAttributesCacheGenerator_U3CSpawnU3Ed__0__ctor_m3C12D98CEE9084D1104D3AC19E4DB24F14049F5A,
	U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_CustomAttributesCacheGenerator_U3CSpawnU3Ed__0_System_IDisposable_Dispose_m7D173ED73B59596164E3F93AE0B2E81854F5FBD7,
	U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_CustomAttributesCacheGenerator_U3CSpawnU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACD1991881DDF201112A85D5567ED427D37AFBF2,
	U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_CustomAttributesCacheGenerator_U3CSpawnU3Ed__0_System_Collections_IEnumerator_Reset_m34B9730116CAEC77500E14E35394DEF368BB6B1A,
	U3CSpawnU3Ed__0_tA5B3BAA350A77626DDE7897A9B9801DA7EC63C58_CustomAttributesCacheGenerator_U3CSpawnU3Ed__0_System_Collections_IEnumerator_get_Current_m036F1B038E494E9989082CB4147D70A2657D4FCC,
	U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_CustomAttributesCacheGenerator_U3CSpawnU3Ed__15__ctor_m8B6555F0727267379D91497503A517A14A935396,
	U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_CustomAttributesCacheGenerator_U3CSpawnU3Ed__15_System_IDisposable_Dispose_m90909754072CA037BC9341EBD318B487BDD7F075,
	U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_CustomAttributesCacheGenerator_U3CSpawnU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEB6FB3ACA001BB1AEE3F356E8AD6242451DFD8F9,
	U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_CustomAttributesCacheGenerator_U3CSpawnU3Ed__15_System_Collections_IEnumerator_Reset_m199A0A54883A81C9D0395A881146EEE0A7200F2F,
	U3CSpawnU3Ed__15_t568EB4A30327621809F743DD28056A5C5A654E1F_CustomAttributesCacheGenerator_U3CSpawnU3Ed__15_System_Collections_IEnumerator_get_Current_m26E129A15722B49953843E3835FEFAAB4D29DEF3,
	U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_CustomAttributesCacheGenerator_U3CSpawnU3Ed__13__ctor_mC6D26905014FBE3C109464828A997016EA7CA70D,
	U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_CustomAttributesCacheGenerator_U3CSpawnU3Ed__13_System_IDisposable_Dispose_m1DE303152F8C88637C1358770031EF4826E9D74F,
	U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_CustomAttributesCacheGenerator_U3CSpawnU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mECAEF1106886D61093B7A6AD5CCA830D8B8655EA,
	U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_CustomAttributesCacheGenerator_U3CSpawnU3Ed__13_System_Collections_IEnumerator_Reset_mF723AA7FEE5B81A88F02DD1007343BF6D75AC0CB,
	U3CSpawnU3Ed__13_tBEDCBB0246BC0AFDFF52514B15245E2626BBE3DD_CustomAttributesCacheGenerator_U3CSpawnU3Ed__13_System_Collections_IEnumerator_get_Current_m447841FDAE3F33326E46F2162D478C731F852B1E,
	U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_CustomAttributesCacheGenerator_U3CSpawnU3Ed__4__ctor_m9C4DBA85AE3E02D720C6EAECA102687880E5AA9B,
	U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_CustomAttributesCacheGenerator_U3CSpawnU3Ed__4_System_IDisposable_Dispose_mF994CA2235DD73A8DC295DDEF1A35EC4710CCA91,
	U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_CustomAttributesCacheGenerator_U3CSpawnU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5EFD4F9B9380DC9E690993D411F37E0164D5B31F,
	U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_CustomAttributesCacheGenerator_U3CSpawnU3Ed__4_System_Collections_IEnumerator_Reset_m6C86770A44474E359195E84BD69DC698AC95B7D9,
	U3CSpawnU3Ed__4_tA15B858B718794C037F1A94DF7E716DDCDEF0599_CustomAttributesCacheGenerator_U3CSpawnU3Ed__4_System_Collections_IEnumerator_get_Current_m760CF586EB4B879F894A560953BE3F08CE62A641,
	U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_CustomAttributesCacheGenerator_U3CRestartAllStemsU3Ed__11__ctor_m3F0E2479A180FB3E399F7C5CF8B71BB15DB66295,
	U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_CustomAttributesCacheGenerator_U3CRestartAllStemsU3Ed__11_System_IDisposable_Dispose_m6FEDE0E02A173C3646F429439283D391270E4B65,
	U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_CustomAttributesCacheGenerator_U3CRestartAllStemsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50388D880EE7347B31D3B2E552933F6F11D19777,
	U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_CustomAttributesCacheGenerator_U3CRestartAllStemsU3Ed__11_System_Collections_IEnumerator_Reset_m79446E16DFB91914B0E767F34FA23F8C74CDC0DD,
	U3CRestartAllStemsU3Ed__11_tCE78F1B04FEF609B4F67EC6C134DD67B07B28BE6_CustomAttributesCacheGenerator_U3CRestartAllStemsU3Ed__11_System_Collections_IEnumerator_get_Current_mA2D3590BA4EA306951314F5F05C52A31C3A8EA1F,
	U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7__ctor_mD72AAC64AA5475D057F79B59997BF597BA5DF227,
	U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_IDisposable_Dispose_mE6CA3308E82B8A002709AC9486224960458478FB,
	U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48B117B0CD0DDB72351D96529C5A0B78E7866475,
	U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_Reset_mC6A56CA03B68D5D83BDF69977C914EC5F710C119,
	U3CLoadDatabaseU3Ed__7_t780622F3F919B05D6A50ECB9DC2FBAF23D1DB48F_CustomAttributesCacheGenerator_U3CLoadDatabaseU3Ed__7_System_Collections_IEnumerator_get_Current_mDA20F872592A1AFEC3753B785647F92C12CDE482,
	U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__84__ctor_m3D4B56311A798974B7C2A8E3D20E1A8306DAAF2F,
	U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__84_System_IDisposable_Dispose_mA4BA8648817A41A3ED3B3CD9F1E3CEEAAD368241,
	U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__84_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85238BFAE3597C9261B286DEAB50F3C929290B3B,
	U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__84_System_Collections_IEnumerator_Reset_m9384C8A83680182D7B3994AF38C77A948111E2B8,
	U3CWaitToStartU3Ed__84_t634496140777A82B1A1ACB0B50CBEE87F5D5FE7E_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__84_System_Collections_IEnumerator_get_Current_mE5C8B43BE08F6CFEE838E38084B2ED4832206958,
	U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_CustomAttributesCacheGenerator_U3CBeginU3Ed__85__ctor_mAB58B7584FAD5B4633771C03EA4FFB71685A423E,
	U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_CustomAttributesCacheGenerator_U3CBeginU3Ed__85_System_IDisposable_Dispose_mD03DCDA26327337D788708EEBD8EBD64A7B241A0,
	U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_CustomAttributesCacheGenerator_U3CBeginU3Ed__85_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B38DEBEF616B175340DB85F7FA19B5A9E091E8F,
	U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_CustomAttributesCacheGenerator_U3CBeginU3Ed__85_System_Collections_IEnumerator_Reset_m4795B9DAC83F02487C6DDCAD0AB9251E36630D12,
	U3CBeginU3Ed__85_tE03C87FFB525A803105C299DC98FDBA5381E87DA_CustomAttributesCacheGenerator_U3CBeginU3Ed__85_System_Collections_IEnumerator_get_Current_m14BA90226D7380E67EB837C5A862853928767C31,
	U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_CustomAttributesCacheGenerator_U3CSpawnNewSegmentU3Ed__93__ctor_mC3946A80D2D4BB2E06F0A60CE828B9473567175F,
	U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_CustomAttributesCacheGenerator_U3CSpawnNewSegmentU3Ed__93_System_IDisposable_Dispose_mAE57C25E300472D1292F1AF22404843AF455CDC0,
	U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_CustomAttributesCacheGenerator_U3CSpawnNewSegmentU3Ed__93_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3F18702C42FFE48A79ABD648500AD6DB065AD63B,
	U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_CustomAttributesCacheGenerator_U3CSpawnNewSegmentU3Ed__93_System_Collections_IEnumerator_Reset_mD50E95DA7F2E82EDD8149A3E491986A675D1802A,
	U3CSpawnNewSegmentU3Ed__93_t05F5330EC6009DA86C995660483A83E2D8AB0EA5_CustomAttributesCacheGenerator_U3CSpawnNewSegmentU3Ed__93_System_Collections_IEnumerator_get_Current_m188CB9FEF972ECC0872CFF268175B3DFAB15C427,
	U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_CustomAttributesCacheGenerator_U3CSpawnFromAssetReferenceU3Ed__95__ctor_m1C51A408EB6060A7133D3180C664171034AD8EC0,
	U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_CustomAttributesCacheGenerator_U3CSpawnFromAssetReferenceU3Ed__95_System_IDisposable_Dispose_m27A19B08B4619C2D7B4DFAA3FD804C5622C1A55A,
	U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_CustomAttributesCacheGenerator_U3CSpawnFromAssetReferenceU3Ed__95_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A1C8209672A1660F8736CB5C54B72A727BF2217,
	U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_CustomAttributesCacheGenerator_U3CSpawnFromAssetReferenceU3Ed__95_System_Collections_IEnumerator_Reset_mF48630E55DA0DB51C12C225529808B68EFA8EB4F,
	U3CSpawnFromAssetReferenceU3Ed__95_tA81270B401F2433F58FCAAC21CD71FE94FBEFCB2_CustomAttributesCacheGenerator_U3CSpawnFromAssetReferenceU3Ed__95_System_Collections_IEnumerator_get_Current_m8F62B25EAA98A90D4BF4C29D773B99F262E649A2,
	U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_CustomAttributesCacheGenerator_U3CSpawnCoinAndPowerupU3Ed__96__ctor_m6919C148252339ED3E1494B186A1BED7A984C7FE,
	U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_CustomAttributesCacheGenerator_U3CSpawnCoinAndPowerupU3Ed__96_System_IDisposable_Dispose_m87B6CB6FF3D58454AC6347C9A812361448BCBC27,
	U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_CustomAttributesCacheGenerator_U3CSpawnCoinAndPowerupU3Ed__96_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5A5E99D6802DA7A1700EEC345C4AB9394D9B6A76,
	U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_CustomAttributesCacheGenerator_U3CSpawnCoinAndPowerupU3Ed__96_System_Collections_IEnumerator_Reset_mCD39A066C923D8FE2C3DF6DA90383819ACD15967,
	U3CSpawnCoinAndPowerupU3Ed__96_t96A2395431AE0BB77B7EC129DC5974E0237DB9C0_CustomAttributesCacheGenerator_U3CSpawnCoinAndPowerupU3Ed__96_System_Collections_IEnumerator_get_Current_m31B94A4C2C7A97578617509C9D5EDB57B9F69C84,
	U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_CustomAttributesCacheGenerator_U3COpenU3Ed__3__ctor_m8AC977975366A5A4ED5DCAB21A1D37D1B95EB264,
	U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_CustomAttributesCacheGenerator_U3COpenU3Ed__3_System_IDisposable_Dispose_m5F0713DDBBA80E241AE5A97F9DEAC6AE128AF282,
	U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_CustomAttributesCacheGenerator_U3COpenU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F4DB819EEEDD4C7D847BE29D85CA72CFB80E644,
	U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_CustomAttributesCacheGenerator_U3COpenU3Ed__3_System_Collections_IEnumerator_Reset_m6D1892BA8644ABC732A24538492BCDB875868DB7,
	U3COpenU3Ed__3_t8F0C53B649A8CD4A765C36DB41F233E0DD356622_CustomAttributesCacheGenerator_U3COpenU3Ed__3_System_Collections_IEnumerator_get_Current_m365CCE1036E1123F495161146CC9A9C1811E761E,
	U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_CustomAttributesCacheGenerator_U3CLoadAynchronouslyU3Ed__4__ctor_m2C80714EAC810CC120B102341525405571574FFF,
	U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_CustomAttributesCacheGenerator_U3CLoadAynchronouslyU3Ed__4_System_IDisposable_Dispose_m42FC8E64428FC380A50BBA8DCE4D27BA73AB23D6,
	U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_CustomAttributesCacheGenerator_U3CLoadAynchronouslyU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5557547C31F7B97F06DB286DA468D7915B50A649,
	U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_CustomAttributesCacheGenerator_U3CLoadAynchronouslyU3Ed__4_System_Collections_IEnumerator_Reset_m57603CABA0EE1D1465CF65D883D6F5CB7A50BC17,
	U3CLoadAynchronouslyU3Ed__4_tC74F61D3DE10C8AB50BD4C77DA42E20B44C3909A_CustomAttributesCacheGenerator_U3CLoadAynchronouslyU3Ed__4_System_Collections_IEnumerator_get_Current_m6481BEC178F1D855C17F9B03FB18A3D02F57DFFB,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
